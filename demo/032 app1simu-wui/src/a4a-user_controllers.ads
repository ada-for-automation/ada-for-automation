
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with A4A.Web.Controllers.IPB_Controllers;
with A4A.Web.Controllers.LED_Controllers;
with A4A.Web.Controllers.Switch3_Controllers;
use A4A.Web.Controllers;

--  tag::controllers[]
package A4A.User_Controllers is

   --------------------------------------------------------------------
   --  User Controllers
   --------------------------------------------------------------------

   --
   --  General Panel controllers
   --

   Auto_Manu_Simu_Switch3 : aliased Switch3_Controllers.Instance;
   Ack_Faults_Simu_IPB    : aliased IPB_Controllers.Instance;

   Auto_LED       : aliased LED_Controllers.Instance;
   Manu_LED       : aliased LED_Controllers.Instance;
   Ack_Faults_LED : aliased LED_Controllers.Instance;

   --
   --  Tank and Level Switches controllers
   --

   Tank_Simu_Auto_IPB  : aliased IPB_Controllers.Instance;
   LS_11_Simu_On_IPB   : aliased IPB_Controllers.Instance;
   LS_12_Simu_On_IPB   : aliased IPB_Controllers.Instance;

   LS_11_On_LED   : aliased LED_Controllers.Instance;
   LS_12_On_LED   : aliased LED_Controllers.Instance;

   --
   --  Valve 14 controllers
   --

   V14_Simu_Auto_IPB            : aliased IPB_Controllers.Instance;
   V14_Simu_Pos_Open_Manu_IPB   : aliased IPB_Controllers.Instance;
   V14_Simu_Pos_Closed_Manu_IPB : aliased IPB_Controllers.Instance;

   V14_Pos_Open_LED   : aliased LED_Controllers.Instance;
   V14_Pos_Closed_LED : aliased LED_Controllers.Instance;
   V14_Coil_LED       : aliased LED_Controllers.Instance;

   --
   --  Pump 13 controllers
   --

   P13_Simu_Auto_IPB    : aliased IPB_Controllers.Instance;
   P13_Simu_FB_Manu_IPB : aliased IPB_Controllers.Instance;

   P13_FB_LED     : aliased LED_Controllers.Instance;
   P13_Coil_LED   : aliased LED_Controllers.Instance;

end A4A.User_Controllers;
--  end::controllers[]
