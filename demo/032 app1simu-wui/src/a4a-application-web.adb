
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Gnoga.Application.Multi_Connect; use Gnoga;

with A4A.User_Objects; use A4A.User_Objects;
with A4A.User_Simul_Objects; use A4A.User_Simul_Objects;

with A4A.User_Controllers; use A4A.User_Controllers;

with A4A.Web.Pages.General_Status;
with A4A.Web.Pages.MBTCP_Server_Status;
with A4A.Web.Pages.Home;
use A4A.Web.Pages;

with A4A.Web.Web_Server_Config;

package body A4A.Application.Web is

   procedure Connect_Data is
   begin

      --
      --  General Panel controllers
      --

      Auto_Manu_Simu_Switch3.Set_Model
        (Command_Bit1  => Auto_Simu'Access,
         Command_Bit2  => Manu_Simu'Access);

      Ack_Faults_Simu_IPB.Set_Model
        (Status_Bit  => Ack_Faults_Simu'Access);

      Auto_LED.Set_Model
        (Status_Bit  => A4A.User_Objects.Auto'Access);

      Manu_LED.Set_Model
        (Status_Bit  => Manu'Access);

      Ack_Faults_LED.Set_Model
        (Status_Bit  => Ack_Faults'Access);

      --
      --  Tank and Level Switches controllers
      --

      Tank_Simu_Auto_IPB.Set_Model
        (Status_Bit  => Tank_Simu_Auto'Access);

      LS_11_Simu_On_IPB.Set_Model
        (Status_Bit  => Level_Switch_11_Simu_On'Access);

      LS_12_Simu_On_IPB.Set_Model
        (Status_Bit  => Level_Switch_12_Simu_On'Access);

      LS_11_On_LED.Set_Model
        (Status_Bit  => Level_Switch_11'Access);

      LS_12_On_LED.Set_Model
        (Status_Bit  => Level_Switch_12'Access);

      --
      --  Valve 14 controllers
      --

      V14_Simu_Auto_IPB.Set_Model
        (Status_Bit  => Valve_14_Simu_Auto'Access);

      V14_Simu_Pos_Open_Manu_IPB.Set_Model
        (Status_Bit  => Valve_14_Simu_Pos_Open_Manu'Access);

      V14_Simu_Pos_Closed_Manu_IPB.Set_Model
        (Status_Bit  => Valve_14_Simu_Pos_Closed_Manu'Access);

      V14_Pos_Open_LED.Set_Model
        (Status_Bit  => Valve_14_Pos_Open'Access);

      V14_Pos_Closed_LED.Set_Model
        (Status_Bit  => Valve_14_Pos_Closed'Access);

      V14_Coil_LED.Set_Model
        (Status_Bit  => Valve_14_Coil'Access);

      --
      --  Pump 13 controllers
      --

      P13_Simu_Auto_IPB.Set_Model
        (Status_Bit  => MyPump13_Simu_Auto'Access);

      P13_Simu_FB_Manu_IPB.Set_Model
        (Status_Bit  => MyPump13_Simu_FB_Manu'Access);

      P13_FB_LED.Set_Model
        (Status_Bit  => MyPump13_FeedBack'Access);

      P13_Coil_LED.Set_Model
        (Status_Bit  => MyPump13_Coil'Access);

   end Connect_Data;

   procedure Update_Data is
   begin

         --
         --  General Panel controllers
         --

         Auto_Manu_Simu_Switch3.Update;
         Ack_Faults_Simu_IPB.Update;

         Auto_LED.Update;
         Manu_LED.Update;
         Ack_Faults_LED.Update;

         --
         --  Tank and Level Switches controllers
         --

         Tank_Simu_Auto_IPB.Update;
         LS_11_Simu_On_IPB.Update;
         LS_12_Simu_On_IPB.Update;

         LS_11_On_LED.Update;
         LS_12_On_LED.Update;

         --
         --  Valve 14 controllers
         --

         V14_Simu_Auto_IPB.Update;
         V14_Simu_Pos_Open_Manu_IPB.Update;
         V14_Simu_Pos_Closed_Manu_IPB.Update;

         V14_Pos_Open_LED.Update;
         V14_Pos_Closed_LED.Update;
         V14_Coil_LED.Update;

         --
         --  Pump 13 controllers
         --

         P13_Simu_Auto_IPB.Update;
         P13_Simu_FB_Manu_IPB.Update;

         P13_FB_LED.Update;
         P13_Coil_LED.Update;

   end Update_Data;

   procedure On_Server_Start is
   begin

      Gnoga.Application.Multi_Connect.Initialize
        (Event => Home.On_Connect'Access,
         Port  => A4A.Web.Web_Server_Config.Port,
         Boot  => "000-boot.html");

      Gnoga.Application.Multi_Connect.On_Connect_Handler
        (Event => General_Status.On_Connect'Access,
         Path => "general_status");

      Gnoga.Application.Multi_Connect.On_Connect_Handler
        (Event => MBTCP_Server_Status.On_Connect'Access,
         Path => "mbtcp_server_status");

      Gnoga.Application.Title ("Ada for Automation App1Simu with Gnoga");

   end On_Server_Start;

end A4A.Application.Web;
