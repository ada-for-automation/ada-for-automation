
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

package A4A.Application.Identification is

   function Get_Application_Name return String;

   function Get_Application_Version return String;

   function Get_Application_Description return String;

private

   Application_Name : String := "Hilscher Kernel 7 Test Application";

   Application_Version : String := "2022/05/31";

   Application_Description : String :=
     "<p>This is the Hilscher Kernel 7 Test Application.</p>" & CRLF
     & CRLF
     & "<p>The purpose of this application is to demonstrate "
     & "the following capabilities :</p>" & CRLF
     & "<ul>" & CRLF
     & "<li>Command Line Interface,</li>" & CRLF
     & "<li>Modbus TCP IO Scanning to connect Server devices,</li>" & CRLF
     & "<li>Hilscher cifX binding,</li>" & CRLF
     & "<li>some of the functions and objects of the library.</li>" & CRLF
     & "</ul>" & CRLF
     & CRLF
     & "<p>There is a Web UI provided by Gnoga.</p>"
     & "<p>It is supposed to be run in conjunction with Modbus TCP Server "
     & "Devices and a Master equipment on a supported field bus system.</p>";

end A4A.Application.Identification;
