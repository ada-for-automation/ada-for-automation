
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Float_Text_IO;

with Gnoga.Gui.Base;

with A4A.User_Objects; use A4A.User_Objects;
with A4A.User_Controllers; use A4A.User_Controllers;

with A4A.Web.Controllers.Connection_Stats_Controllers;
use A4A.Web.Controllers;

with UXStrings; use UXStrings;

package body A4A.Web.Pages.Home is

   overriding
   procedure Update_Page (Web_Page : access Instance) is
      LT_10_Value_String     : String (1 .. 20);
      LT_10_Bar_String       : String (1 .. 20);
      First_Digit            : Integer;
   begin

      Web_Page.Common_View.Connection_Stats_View.Update_View;

      --
      --  Devices views
      --

      Web_Page.Home_View.MyPump13_View.Update_View;
      Web_Page.Home_View.Valve_14_View.Update_View;

      --
      --  Devices command views
      --

      Web_Page.Home_View.Contactor_Cmd_View.Update_View;
      Web_Page.Home_View.Valve_Cmd_View.Update_View;

      --
      --  Mode of operation views
      --

      Web_Page.Home_View.Mode_Auto_LED_View.Update_View;
      Web_Page.Home_View.Mode_Manu_LED_View.Update_View;

      Web_Page.Home_View.LS_11_Faulty_LED_View.Update_View;
      Web_Page.Home_View.LS_12_Faulty_LED_View.Update_View;

      if Level_Transmitter_10_Value <
        Web_Page.Home_View.LT_10_Value - Dead_Band
        or Level_Transmitter_10_Value >
          Web_Page.Home_View.LT_10_Value + Dead_Band
      then
         Ada.Float_Text_IO.Put (To   => LT_10_Value_String,
                                Item => Level_Transmitter_10_Value,
                                Aft  => 2,
                                Exp  => 0);
         Web_Page.Home_View.LT_10_Value_View.Inner_HTML
           (From_UTF_8 (LT_10_Value_String & " %"));
         Web_Page.Home_View.LT_10_Value := Level_Transmitter_10_Value;

         Ada.Float_Text_IO.Put (To   => LT_10_Bar_String,
                                Item => Level_Transmitter_10_Value / 2.0,
                                Aft  => 0,
                                Exp  => 0);

         --  The attribute "height" does not like blank spaces
         for Index in LT_10_Bar_String'Range loop
            First_Digit := Index;
            exit when LT_10_Bar_String (Index) /= ' ';
         end loop;

         Web_Page.Home_View.LT_10_Bar_View.Attribute
           ("height",
            From_UTF_8
              (LT_10_Bar_String (First_Digit .. LT_10_Bar_String'Last)));
      end if;

      --
      --  General Panel views
      --

      Web_Page.Home_View.Auto_LED_View.Update_View;
      Web_Page.Home_View.Manu_LED_View.Update_View;
      Web_Page.Home_View.Ack_Faults_LED_View.Update_View;

      --
      --  Tank and Level Switches views
      --

      Web_Page.Home_View.LS_11_On_LED_View.Update_View;
      Web_Page.Home_View.LS_12_On_LED_View.Update_View;

      if Level_Transmitter_10_Measure /= Web_Page.Home_View.LT_10_Measure then
         Web_Page.Home_View.LT_10_Measure_View.Inner_HTML
           (From_UTF_8 (Level_Transmitter_10_Measure'Img));
         Web_Page.Home_View.LT_10_Measure := Level_Transmitter_10_Measure;
      end if;

      Web_Page.Home_View.LT_10_XHHH_LED_View.Update_View;
      Web_Page.Home_View.LT_10_XHH_LED_View.Update_View;
      Web_Page.Home_View.LT_10_XH_LED_View.Update_View;
      Web_Page.Home_View.LT_10_XL_LED_View.Update_View;
      Web_Page.Home_View.LT_10_XLL_LED_View.Update_View;
      Web_Page.Home_View.LT_10_XLLL_LED_View.Update_View;

      --
      --  Valve 14 views
      --

      Web_Page.Home_View.V14_Pos_Open_LED_View.Update_View;
      Web_Page.Home_View.V14_Pos_Closed_LED_View.Update_View;
      Web_Page.Home_View.V14_Coil_LED_View.Update_View;

      --
      --  Pump 13 views
      --

      Web_Page.Home_View.P13_FB_LED_View.Update_View;
      Web_Page.Home_View.P13_Coil_LED_View.Update_View;

   end Update_Page;

   procedure On_Connect
     (Main_Window : in out Gnoga.Gui.Window.Window_Type'Class;
      Connection  : access
        Gnoga.Application.Multi_Connect.Connection_Holder_Type)
   is
      Web_Page : constant Instance_Access := new Instance;

      My_Ident : aliased String := "A4A.Web.Pages.Home_View_Updater";
      View_Updater_Task : A4A.Web.Pages.Updater_Task_Type
        (My_Ident                    => My_Ident'Access,
         Web_Page                    => Web_Page);

      procedure Side_Nav_Synos_Open_On_Click
        (Object : in out Gnoga.Gui.Base.Base_Type'Class);
      procedure Side_Nav_Synos_Open_On_Click
        (Object : in out Gnoga.Gui.Base.Base_Type'Class) is
         pragma Unreferenced (Object);
      begin
         Web_Page.Home_View.Side_Nav_Synos_View.Display ("block");
         Web_Page.Home_View.Side_Nav_Synos_View.Top ("0px");
      end Side_Nav_Synos_Open_On_Click;

      procedure Side_Nav_Synos_Close_On_Click
        (Object : in out Gnoga.Gui.Base.Base_Type'Class);
      procedure Side_Nav_Synos_Close_On_Click
        (Object : in out Gnoga.Gui.Base.Base_Type'Class) is
         pragma Unreferenced (Object);
      begin
         Web_Page.Home_View.Side_Nav_Synos_View.Display ("none");
      end Side_Nav_Synos_Close_On_Click;

      procedure Synos_Close;
      procedure Synos_Close is
      begin
         Web_Page.Home_View.Syno1_View.Display ("none");
         Web_Page.Home_View.Syno2_View.Display ("none");
      end Synos_Close;

      procedure Side_Nav_Synos_Syno1_On_Click
        (Object : in out Gnoga.Gui.Base.Base_Type'Class);
      procedure Side_Nav_Synos_Syno1_On_Click
        (Object : in out Gnoga.Gui.Base.Base_Type'Class) is
         pragma Unreferenced (Object);
      begin
         Web_Page.Home_View.Current_Syno_View.Display ("none");
         Web_Page.Home_View.Current_Syno_View :=
           Web_Page.Home_View.Syno1_View'Access;
         Web_Page.Home_View.Current_Syno_View.Display ("block");
         Web_Page.Home_View.Main_Header_Title_View.Inner_HTML ("Process View");
         Web_Page.Home_View.Side_Nav_Synos_View.Display ("none");
      end Side_Nav_Synos_Syno1_On_Click;

      procedure Side_Nav_Synos_Syno2_On_Click
        (Object : in out Gnoga.Gui.Base.Base_Type'Class);
      procedure Side_Nav_Synos_Syno2_On_Click
        (Object : in out Gnoga.Gui.Base.Base_Type'Class) is
         pragma Unreferenced (Object);
      begin
         Web_Page.Home_View.Current_Syno_View.Display ("none");
         Web_Page.Home_View.Current_Syno_View :=
           Web_Page.Home_View.Syno2_View'Access;
         Web_Page.Home_View.Current_Syno_View.Display ("block");
         Web_Page.Home_View.Main_Header_Title_View
           .Inner_HTML ("Inputs / Outputs View");
         Web_Page.Home_View.Side_Nav_Synos_View.Display ("none");
      end Side_Nav_Synos_Syno2_On_Click;

      procedure Set_Handlers;
      procedure Set_Handlers is
      begin
         Web_Page.Home_View.Side_Nav_Synos_Open_View.On_Click_Handler
           (Side_Nav_Synos_Open_On_Click'Unrestricted_Access);

         Web_Page.Home_View.Side_Nav_Synos_Close_View.On_Click_Handler
           (Side_Nav_Synos_Close_On_Click'Unrestricted_Access);

         Web_Page.Home_View.Side_Nav_Synos_Syno1_View.On_Click_Handler
           (Side_Nav_Synos_Syno1_On_Click'Unrestricted_Access);

         Web_Page.Home_View.Side_Nav_Synos_Syno2_View.On_Click_Handler
           (Side_Nav_Synos_Syno2_On_Click'Unrestricted_Access);

      end Set_Handlers;

   begin
      Main_Window.Connection_Data (Web_Page);

      Main_Window.Document.Load_CSS (From_UTF_8 (W3_CSS_URL));
      Main_Window.Document.Load_CSS (From_UTF_8 (Font_Awesome_CSS_URL));
      Main_Window.Document.Load_CSS (From_UTF_8 (APP_CSS_URL));

      Connection_Stats_Controllers.Connection;

      Common_View_Setup (Web_Page.Common_View,
                         Main_Window'Unchecked_Access,
                         "050-main.html");

      Web_Page.Home_View.Side_Nav_Synos_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "side-nav-synos");

      Web_Page.Home_View.Side_Nav_Synos_Open_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "side-nav-synos-open");

      Web_Page.Home_View.Side_Nav_Synos_Close_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "side-nav-synos-close");

      Web_Page.Home_View.Side_Nav_Synos_Syno1_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "side-nav-synos-syno1");

      Web_Page.Home_View.Side_Nav_Synos_Syno2_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "side-nav-synos-syno2");

      Web_Page.Home_View.Main_Header_Title_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "main-header-title");

      Web_Page.Home_View.Syno1_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "syno1");

      Web_Page.Home_View.Syno2_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "syno2");

      Synos_Close;
      Web_Page.Home_View.Current_Syno_View :=
        Web_Page.Home_View.Syno1_View'Access;
      Web_Page.Home_View.Current_Syno_View.Display ("block");
      Web_Page.Home_View.Main_Header_Title_View.Inner_HTML ("Process View");

      --
      --  Devices command views
      --

      Web_Page.Home_View.Contactor_Cmd_View.Setup
        (Parent     => Web_Page.Common_View.View,
         View_Id    => "contactor-cmd-view",
         Command_Id => "contactor-cmd-button",
         Title_Id   => "contactor-cmd-title",
         Contactor_View_Id => "contactor-cmd-pump",
         Contactor_Feed_Back_Id => "contactor-cmd-fb-led",
         Contactor_Coil_Id      => "contactor-cmd-coil-led");

      Web_Page.Home_View.Valve_Cmd_View.Setup
        (Parent        => Web_Page.Common_View.View,
         View_Id       => "valve-cmd-view",
         Command_Id    => "valve-cmd-button",
         Title_Id      => "valve-cmd-title",
         Valve_View_Id => "valve-cmd-valve",
         Valve_Pos_Open_Id   => "valve-cmd-open-led",
         Valve_Pos_Closed_Id => "valve-cmd-closed-led",
         Valve_Coil_Id       => "valve-cmd-coil-led");

      --
      --  Devices views
      --

      Web_Page.Home_View.MyPump13_View.Setup
        (Controller => MyPump13_Symbol'Access,
         Cmd_View => Web_Page.Home_View.Contactor_Cmd_View'Access);
      Web_Page.Home_View.MyPump13_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "pump13");
      Web_Page.Home_View.MyPump13_View.On_Click_Handler
        (Contactor_Views.On_Click'Access);

      Web_Page.Home_View.Valve_14_View.Setup
        (Controller => Valve_14_Symbol'Access,
         Cmd_View => Web_Page.Home_View.Valve_Cmd_View'Access);
      Web_Page.Home_View.Valve_14_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "valve14");
      Web_Page.Home_View.Valve_14_View.On_Click_Handler
        (Valve_Views.On_Click'Access);

      --
      --  Mode of operation views
      --

      Web_Page.Home_View.Mode_Auto_LED_View.Setup
        (Parent     => Web_Page.Common_View.View,
         Controller => Mode_Auto_LED'Access,
         Id         => "mode-auto-led");

      Web_Page.Home_View.Mode_Manu_LED_View.Setup
        (Web_Page.Common_View.View, Mode_Manu_LED'Access, "mode-manu-led");

      --
      --  Level Transmitter 10 views
      --

      Web_Page.Home_View.LT_10_Value_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "lt10-value");

      Web_Page.Home_View.LT_10_XHHH_LED_View.Setup
        (Web_Page.Common_View.View, LT_10_XHHH_LED'Access, "lt10-xhhh-led");

      Web_Page.Home_View.LT_10_XHH_LED_View.Setup
        (Web_Page.Common_View.View, LT_10_XHH_LED'Access, "lt10-xhh-led");

      Web_Page.Home_View.LT_10_XH_LED_View.Setup
        (Web_Page.Common_View.View, LT_10_XH_LED'Access, "lt10-xh-led");

      Web_Page.Home_View.LT_10_XL_LED_View.Setup
        (Web_Page.Common_View.View, LT_10_XL_LED'Access, "lt10-xl-led");

      Web_Page.Home_View.LT_10_XLL_LED_View.Setup
        (Web_Page.Common_View.View, LT_10_XLL_LED'Access, "lt10-xll-led");

      Web_Page.Home_View.LT_10_XLLL_LED_View.Setup
        (Web_Page.Common_View.View, LT_10_XLLL_LED'Access, "lt10-xlll-led");

      Web_Page.Home_View.LT_10_Bar_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "lt10-bar");

      --
      --  General Panel views
      --

      Web_Page.Home_View.Auto_LED_View.Setup
        (Web_Page.Common_View.View, Auto_LED'Access, "auto-led");

      Web_Page.Home_View.Manu_LED_View.Setup
        (Web_Page.Common_View.View, Manu_LED'Access, "manu-led");

      Web_Page.Home_View.Ack_Faults_LED_View.Setup
        (Web_Page.Common_View.View, Ack_Faults_LED'Access, "ack-led");

      --
      --  Tank and Level Switches views
      --

      Web_Page.Home_View.LS_11_On_LED_View.Setup
        (Web_Page.Common_View.View, LS_11_On_LED'Access, "ls11-led");

      Web_Page.Home_View.LS_12_On_LED_View.Setup
        (Web_Page.Common_View.View, LS_12_On_LED'Access, "ls12-led");

      Web_Page.Home_View.LS_11_Faulty_LED_View.Setup
        (Web_Page.Common_View.View, LS_11_Faulty_LED'Access, "ls11-sh-led");

      Web_Page.Home_View.LS_12_Faulty_LED_View.Setup
        (Web_Page.Common_View.View, LS_12_Faulty_LED'Access, "ls12-sl-led");

      Web_Page.Home_View.LT_10_Measure_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "lt10-measure");

      --
      --  Valve 14 views
      --

      Web_Page.Home_View.V14_Pos_Open_LED_View.Setup
        (Web_Page.Common_View.View, V14_Pos_Open_LED'Access,
         "valve14-open-led");

      Web_Page.Home_View.V14_Pos_Closed_LED_View.Setup
        (Web_Page.Common_View.View, V14_Pos_Closed_LED'Access,
         "valve14-closed-led");

      Web_Page.Home_View.V14_Coil_LED_View.Setup
        (Web_Page.Common_View.View, V14_Coil_LED'Access, "valve14-coil-led");

      --
      --  Pump 13 views
      --

      Web_Page.Home_View.P13_FB_LED_View.Setup
        (Web_Page.Common_View.View, P13_FB_LED'Access, "pump13-fb-led");

      Web_Page.Home_View.P13_Coil_LED_View.Setup
        (Web_Page.Common_View.View, P13_Coil_LED'Access, "pump13-coil-led");

      Set_Handlers;

      View_Updater_Task.Start;

      Connection.Hold;

      View_Updater_Task.Stop;

   end On_Connect;

end A4A.Web.Pages.Home;
