= Ada for Automation Demo Application: 010 a4a_piano
Stéphane LOS
v2022.05, 2022-05-31
:doctype: book
:lang: en
:toc: left
:toclevels: 4
:icons: font
:numbered:
:Author Initials: SLO
:source-highlighter: rouge
:imagesdir: images
:title-logo-image: image:../../../book/images/A4A_logo1-4 TakeOff.png[Logo, pdfwidth=80%]

include::../../book/A4A_Links.asciidoc[]

== Description

=== Ada for Automation

include::../../book/A4A_Description-en.asciidoc[]

=== This demo application

This is a demo application featuring:

* a basic command line interface,

* a basic web user interface making use of Gnoga,

* a kernel with a Modbus TCP Server (K0),

* a trivial application that mimics 16 push buttons and 16 LEDs with a web interface.

== Projects diagram

The following picture shows the diagram of projects :

[plantuml, diagram-classes, png]     
----
@startuml

class Shared 		<< Project >>
note left: Shared\nsettings

class Libmodbus 	<< Project >>
note left: Modbus\nlibrary

class Gnoga 		<< Project >>
note left: Ada Web Framework\nDOM Manipulation library

class A4A 		<< Project >>

class A4A_K0 		<< Project >>
note right: Kernel 0\nModbus TCP Server

class A4A_Piano 	<< Project >>
note right: Application with\nCommand Line and WUI Interfaces

Shared - A4A : uses <
A4A <|-- A4A_K0
Shared - A4A_K0 : uses <
Libmodbus - A4A_K0 : uses <
A4A_K0 <|-- A4A_Piano
Gnoga - A4A_Piano : uses <

@enduml
----

include::../A4A_DemoAppsLicence-en.asciidoc[]

== Building

The provided makefile uses {gprbuild} and provides six targets:

* all : builds the executable,

* app_doc : creates the documentation of the source code,

* clean : cleans the space.

Additionally one can generate some documentation using {adoc} with :

* read_me_html : generates the README in HTML format,

* read_me_pdf : generates the README in PDF format,

* read_me : generates the README in both formats.

== Running

Of course, this application is of interest only if a Modbus TCP Client application is talking to it.

Good candidates are a SCADA or a PLC but, if none is at your disposal, you could use one of :

* 020 a4a-k1-cli,

* 021 a4a-k1-gui,

* 022 a4a-k1-wui,

* your own.

In a console:

Build the application:

`make`

Optionally create the documentation:

`make app_doc`

Run the application:

`make run`

Use Ctrl+C to exit.

Optionally clean all:

`make clean`

== Directories

*bin* +
Where you will find the executable.

*doc* +
The place where {gnatdoc} would create the documentation.

*obj* +
Build artifacts go here.

*src* +
Application source files.

== Application

This is a basic *Ada for Automation* application which implements a Modbus TCP
Server that mimics 16 push buttons and 16 LEDs with a web interface.

It has a Command Line and Web User Interfaces and listen to Modbus TCP requests.

=== Deployment diagram

[plantuml, diagram-deployment, png]     
----
@startuml

node "HMI Station" as Node0 {
  component "Browser" as N0_Browser <<executable>>
}

node "Client" as Node1 {
  component "022 a4a-k1-wui" as N1_App <<executable>>
  component "libmodbus" as N1_LibModbus <<library>>
  component "Gnoga" as N1_Gnoga <<library>>
  component "SimpleComponents" as N1_Simple <<library>>
}
note top of Node1: Could also be :\n- 020 a4a-k1-cli\n- 021 a4a-k1-gui\n- your own\n- a SCADA\n- or a PLC

node "Server" as Node2 {
  component "010 a4a_piano" as N2_App <<executable>>
  component "libmodbus" as N2_LibModbus <<library>>
  component "Gnoga" as N2_Gnoga <<library>>
  component "SimpleComponents" as N2_Simple <<library>>
}

N0_Browser -(0- N1_Simple : HTTP/HTTPS
N0_Browser -(0- N2_Simple : HTTP/HTTPS

N1_App -(0- N1_LibModbus
N1_Gnoga -0)- N1_App
N1_Simple -0)- N1_Gnoga

N2_App -(0- N2_LibModbus
N2_Gnoga -0)- N2_App
N2_Simple -0)- N2_Gnoga

N1_LibModbus -ri(0- N2_LibModbus : Modbus TCP

@enduml
----

<<<

=== Activity diagram

The Kernel manages the communication channel and provides an interface to it, namely the package "A4A.Memory.MBTCP_IOSlave".

[plantuml, diagram-activity-initialization, png]     
----
@startuml

|kernel|
start
partition Initialization {
  :setup;
  :init internal variables;
  :start communication;

  |#AntiqueWhite|application|
  :Cold_Start();

  |#Bisque|user|
  :nothing;
}

|kernel|
(A)

@enduml
----

[plantuml, diagram-activity-running, png]     
----
@startuml

|kernel|
(A)
partition Running {
repeat : Main_Loop

  :check application watchdog;
  :check communication watchdog;
  :get inputs;

  |#AntiqueWhite|application|
  :Main_Cyclic();

  |#Bisque|user|
  :Map_Inputs();
  :Map_Outputs();

  |application|
  :return;

  |kernel|
  :set outputs;
  :housekeeping;

repeat while (Quit ?)
}
(B)

@enduml
----

[plantuml, diagram-activity-finalization, png]     
----
@startuml

|kernel|
(B)
partition Finalization {
  |#AntiqueWhite|application|
  :Closing();

  |#Bisque|user|
  :nothing;

  |kernel|
  :stop communication;
  :De-initialization;
}
stop

@enduml
----

<<<

=== Modbus TCP Server Configuration

."./src/a4a-application-mbtcp_server_config.ads"
[source,ada]
----
include::./src/a4a-application-mbtcp_server_config.ads[tag=config]
----

<1> Modbus TCP Server port : 1504

<<<

=== User objects Definition

."./src/a4a-user_objects.ads"
[source,ada]
----
include::./src/a4a-user_objects.ads[tag=objects]
----

<1> An array of 16 Input bits that a Modbus TCP client can read is defined.

<2> As well, an array of 16 Coils can be written by the client.

<<<

=== User Functions

."./src/a4a-user_functions.adb"
[source,ada]
----
include::./src/a4a-user_functions.adb[tag=functions]
----

User functions are defined to :

<1> get the inputs from the server,

<2> set server ouputs.

<<<

=== User Application

."./src/a4a-application.adb"
[source,ada]
----
include::./src/a4a-application.adb[tag=application]
----

The application cyclically :

<1> gets the inputs from the server,

<2> sets server ouputs.

=== Web server and User Interface

Hereafter is a diagram showing architecture and information flow for the Web UI.

An article is available, in French though : +
https://slo-ist.fr/ada4automation/a4a-modbus-tcp-server-web-hmi-a4a_piano

image::../../../book/images/A4A-Piano.png[]


