
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

package A4A.Application.Identification is

   function Get_Application_Name return String;

   function Get_Application_Version return String;

   function Get_Application_Description return String;

private

   Application_Name : String :=
     "Ada for Automation Modbus TCP Server Piano";

   Application_Version : String := "2023/08/29";

   Application_Description : String :=
     "<p>This is the Modbus TCP Server Piano application.</p>" & CRLF
     & CRLF
     & "<p>The purpose of this application is to demonstrate "
     & "the following capabilities :</p>" & CRLF
     & "<ul>" & CRLF
     & "<li>Command Line Interface,</li>" & CRLF
     & "<li>Web User Interface making use of Gnoga,</li>" & CRLF
     & "<li>Modbus TCP Server to connect a SCADA system or PLC,</li>" & CRLF
     & "<li>some of the functions and objects of the library.</li>" & CRLF
     & "</ul>" & CRLF
     & CRLF
     & "<p>It is supposed to be run in conjunction with a SCADA system or "
     & "some Modbus TCP Client PLC.</p>"
     & CRLF
     & "<p>It is a trivial application that mimics 16 push buttons and 16 LEDs"
     & " with a web interface.</p>"
     & CRLF
     & "<p>There is a Web UI provided by Gnoga.</p>";

end A4A.Application.Identification;
