
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with A4A.Web.Controllers.IPB_Controllers;
with A4A.Web.Controllers.LED_Controllers;
use A4A.Web.Controllers;

--  tag::controllers[]
package A4A.User_Controllers is

   --------------------------------------------------------------------
   --  User Controllers
   --------------------------------------------------------------------

   IPB1 : aliased IPB_Controllers.Instance;
   IPB2 : aliased IPB_Controllers.Instance;
   IPB3 : aliased IPB_Controllers.Instance;
   IPB4 : aliased IPB_Controllers.Instance;

   type IPBs_Byte is array (0 .. 7) of aliased IPB_Controllers.Instance;
   type LEDs_Byte is array (0 .. 7) of aliased LED_Controllers.Instance;

   Input_IPBs_Byte_0  : IPBs_Byte;
   Input_IPBs_Byte_1  : IPBs_Byte;

   Output_LEDs_Byte_0 : LEDs_Byte;
   Output_LEDs_Byte_1 : LEDs_Byte;

end A4A.User_Controllers;
--  end::controllers[]
