
//*****************************************************************************
// MW0_Gauge
//*****************************************************************************

var MW0_Gauge_Config = liquidFillGaugeDefaultSettings();
MW0_Gauge_Config.circleThickness = 0.15;
MW0_Gauge_Config.circleColor = "#178BCA";
MW0_Gauge_Config.textColor = "#045681";
MW0_Gauge_Config.waveTextColor = "#A4DBf8";
MW0_Gauge_Config.waveColor = "#178BCA";
MW0_Gauge_Config.textVertPosition = 0.8;
MW0_Gauge_Config.waveAnimateTime = 1000;
MW0_Gauge_Config.waveHeight = 0.05;
MW0_Gauge_Config.waveAnimate = true;
MW0_Gauge_Config.waveRise = false;
MW0_Gauge_Config.waveHeightScaling = false;
MW0_Gauge_Config.waveOffset = 0.25;
MW0_Gauge_Config.textSize = 0.75;
MW0_Gauge_Config.waveCount = 3;

var MW0_Gauge = loadLiquidFillGauge("MW0-gauge", 0.0, MW0_Gauge_Config);

//*****************************************************************************
// DB1_DBW0_Gauge
//*****************************************************************************

var DB1_DBW0_Gauge_Config = liquidFillGaugeDefaultSettings();
DB1_DBW0_Gauge_Config.circleThickness = 0.15;
DB1_DBW0_Gauge_Config.circleColor = "#00cc00";
DB1_DBW0_Gauge_Config.textColor = "#009900";
DB1_DBW0_Gauge_Config.waveTextColor = "#66ff66";
DB1_DBW0_Gauge_Config.waveColor = "#00cc00";
DB1_DBW0_Gauge_Config.textVertPosition = 0.8;
DB1_DBW0_Gauge_Config.waveAnimateTime = 1000;
DB1_DBW0_Gauge_Config.waveHeight = 0.05;
DB1_DBW0_Gauge_Config.waveAnimate = true;
DB1_DBW0_Gauge_Config.waveRise = false;
DB1_DBW0_Gauge_Config.waveHeightScaling = false;
DB1_DBW0_Gauge_Config.waveOffset = 0.25;
DB1_DBW0_Gauge_Config.textSize = 0.75;
DB1_DBW0_Gauge_Config.waveCount = 3;

var DB1_DBW0_Gauge = loadLiquidFillGauge("DB1-DBW0-gauge", 0.0, DB1_DBW0_Gauge_Config);

//*****************************************************************************
// IW0_Gauge
//*****************************************************************************

var IW0_Gauge_Config = liquidFillGaugeDefaultSettings();
IW0_Gauge_Config.circleThickness = 0.15;
IW0_Gauge_Config.circleColor = "#ffcc00";
IW0_Gauge_Config.textColor = "#e6b800";
IW0_Gauge_Config.waveTextColor = "#ffe066";
IW0_Gauge_Config.waveColor = "#ffcc00";
IW0_Gauge_Config.textVertPosition = 0.8;
IW0_Gauge_Config.waveAnimateTime = 1000;
IW0_Gauge_Config.waveHeight = 0.05;
IW0_Gauge_Config.waveAnimate = true;
IW0_Gauge_Config.waveRise = false;
IW0_Gauge_Config.waveHeightScaling = false;
IW0_Gauge_Config.waveOffset = 0.25;
IW0_Gauge_Config.textSize = 0.75;
IW0_Gauge_Config.waveCount = 3;

var IW0_Gauge = loadLiquidFillGauge("IW0-gauge", 0.0, IW0_Gauge_Config);

//*****************************************************************************
// QW0_Gauge
//*****************************************************************************

var QW0_Gauge_Config = liquidFillGaugeDefaultSettings();
QW0_Gauge_Config.circleThickness = 0.15;
QW0_Gauge_Config.circleColor = "#ff3300";
QW0_Gauge_Config.textColor = "#cc2900";
QW0_Gauge_Config.waveTextColor = "#ff8566";
QW0_Gauge_Config.waveColor = "#ff3300";
QW0_Gauge_Config.textVertPosition = 0.8;
QW0_Gauge_Config.waveAnimateTime = 1000;
QW0_Gauge_Config.waveHeight = 0.05;
QW0_Gauge_Config.waveAnimate = true;
QW0_Gauge_Config.waveRise = false;
QW0_Gauge_Config.waveHeightScaling = false;
QW0_Gauge_Config.waveOffset = 0.25;
QW0_Gauge_Config.textSize = 0.75;
QW0_Gauge_Config.waveCount = 3;

var QW0_Gauge = loadLiquidFillGauge("QW0-gauge", 0.0, QW0_Gauge_Config);
