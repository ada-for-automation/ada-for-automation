
import * as Utils from "/js/utils.js";

Chart.register(ChartStreaming);

const data = {
  datasets: [
    {
      label: 'MW0 (Linear Interpolation)',
      backgroundColor: Utils.transparentize(Utils.CHART_COLORS.red, 0.5),
      borderColor: Utils.CHART_COLORS.red,
      borderDash: [8, 4],
      data: []
    },
    {
      label: 'DB1.DBW0 (Cubic Interpolation)',
      backgroundColor: Utils.transparentize(Utils.CHART_COLORS.blue, 0.5),
      borderColor: Utils.CHART_COLORS.blue,
      cubicInterpolationMode: 'monotone',
      data: []
    }
  ]
};

const onReceive = function (index, event) {
  this.data.datasets[index].data.push({
    x: event.timestamp,
    y: event.value
  });
  this.update('quiet');
};

const traces = [];

const startFeed = (chart, index) => {
  traces[index] = (event) => {
    onReceive.call(chart, index, event);
  };
};

const stopFeed = () => {
};

const start = chart => {
  startFeed(chart, 0);
  startFeed(chart, 1);
};

const stop = () => {
  stopFeed();
};

const onRefresh = (index, value) => {
  traces[index]({
    timestamp: Date.now(),
    value: value
  });
};

const config = {
  type: 'line',
  data: data,
  plugins: [
    {
      start: start,
      stop: stop
    }
  ],
  options: {
    frameRate: 1, // chart is drawn 1 times every second
    scales: {
      x: {
        type: 'realtime',
        realtime: {
          duration: 600000,  // Duration of the chart in milliseconds
          delay: 2000
        }
      },
      y: {
        title: {
          display: true,
          text: 'Value'
        }
      }
    },
    interaction: {
      intersect: false
    }
  }
};

const myChart = new Chart(
  document.getElementById('myChart'),
  config
);

export { onRefresh };
