
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Streams;        use Ada.Streams;
with GNAT.Sockets.MQTT;  use GNAT.Sockets.MQTT;

with Synchronization.Events;

package A4A.MQTT_Client is

   type Instance is new MQTT_Peer with private;

   overriding
   procedure Finalize (Client : in out Instance);

   function Get_Name (Client : Instance) return String;

   function Got_Ping_Response (Client : Instance) return Boolean;
   procedure Reset_Ping_Response (Client : in out Instance);

   overriding
   procedure On_Connect_Accepted
     (Peer            : in out Instance;
      Session_Present : Boolean);

   overriding
   procedure On_Connect_Rejected
     (Peer     : in out Instance;
      Response : Connect_Response);

   overriding
   procedure On_Ping_Response (Peer : in out Instance);

   overriding
   procedure On_Publish
     (Peer      : in out Instance;
      Topic     : String;
      Message   : Stream_Element_Array;
      Packet    : Packet_Identification;
      Duplicate : Boolean;
      Retain    : Boolean);

   overriding
   procedure On_Subscribe_Acknowledgement
     (Peer   : in out Instance;
      Packet : Packet_Identifier;
      Codes  : Return_Code_List);

   procedure Set_Name (Peer : in out Instance; Name : String);

   procedure Reset_Event (Peer : in out Instance);
   procedure Wait_For_Event (Peer : in out Instance);

private
   use Synchronization.Events;

   type String_Ptr is access String;

   type Instance is new MQTT_Peer with
      record
         Name          : String_Ptr;
         Action        : Event; -- Connected/subscribed
         Ping_Response : Boolean;
      end record;

end A4A.MQTT_Client;
