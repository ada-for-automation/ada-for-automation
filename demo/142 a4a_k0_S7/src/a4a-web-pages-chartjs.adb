
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Gnoga.Gui.Plugin.ChartJS;

with A4A.User_Objects; use A4A.User_Objects;

with A4A.Web.Controllers.Connection_Stats_Controllers;
use A4A.Web.Controllers;

with A4A.Web.Script;
use A4A.Web.Script;

with UXStrings; use UXStrings;

package body A4A.Web.Pages.Chartjs is

   overriding
   procedure Update_Page (Web_Page : access Instance) is
   begin

      Web_Page.Common_View.Connection_Stats_View.Update_View;

      if MW0 /= Web_Page.Chart_View.MW0 then
         Gnoga.Gui.Plugin.ChartJS.Chart_Refresh
           (ID     => Web_Page.My_Connection_ID,
            Index  => "0",
            Value  => From_UTF_8
              (Integer'Image (Integer (Word_To_Int (MW0)))));

         Web_Page.Chart_View.MW0 := MW0;
      end if;

      if DB1_W0 /= Web_Page.Chart_View.DB1_W0 then
         Gnoga.Gui.Plugin.ChartJS.Chart_Refresh
           (ID     => Web_Page.My_Connection_ID,
            Index  => "1",
            Value  => From_UTF_8
              (Integer'Image (Integer (Word_To_Int (DB1_W0)))));

         Web_Page.Chart_View.DB1_W0 := DB1_W0;
      end if;

   end Update_Page;

   procedure On_Connect
     (Main_Window : in out Gnoga.Gui.Window.Window_Type'Class;
      Connection  : access
        Gnoga.Application.Multi_Connect.Connection_Holder_Type)
   is
      Web_Page : constant Instance_Access := new Instance;

      My_Ident : aliased String := "A4A.Web.Pages.Chartjs_View_Updater";
      View_Updater_Task : A4A.Web.Pages.Updater_Task_Type
        (My_Ident                    => My_Ident'Access,
         Web_Page                    => Web_Page);

   begin
      Main_Window.Connection_Data (Web_Page);

      Main_Window.Document.Load_CSS (From_UTF_8 (W3_CSS_URL));
      Main_Window.Document.Load_CSS (From_UTF_8 (Font_Awesome_CSS_URL));
      Main_Window.Document.Load_CSS (From_UTF_8 (APP_CSS_URL));

      Connection_Stats_Controllers.Connection;

      Common_View_Setup (Web_Page.Common_View,
                         Main_Window'Unchecked_Access,
                         "051-chart.html");

      Web_Page.My_Connection_ID :=
        Web_Page.Common_View.Main_Window.Connection_ID;

      Gnoga.Gui.Plugin.ChartJS.Load_ChartJS (Main_Window);

      delay 0.1;

      Load_My_Script (Main_Window, "/js/mychart.js");

      delay 0.1;

      View_Updater_Task.Start;

      Connection.Hold;

      View_Updater_Task.Stop;

   end On_Connect;

end A4A.Web.Pages.Chartjs;
