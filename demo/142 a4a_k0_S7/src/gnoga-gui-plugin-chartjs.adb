------------------------------------------------------------------------------
--                                                                          --
--                   GNOGA - The GNU Omnificent GUI for Ada                 --
--                                                                          --
--              G N O G A . G U I . P L U G I N . C H A R T J S             --
--                                                                          --
--                                 B o d y                                  --
--                                                                          --
--                                                                          --
--                     Copyright (C) 2021 Stephane LOS                      --
--                                                                          --
--  This library is free software;  you can redistribute it and/or modify   --
--  it under terms of the  GNU General Public License  as published by the  --
--  Free Software  Foundation;  either version 3,  or (at your  option) any --
--  later version. This library is distributed in the hope that it will be  --
--  useful, but WITHOUT ANY WARRANTY;  without even the implied warranty of --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                    --
--                                                                          --
--  As a special exception under Section 7 of GPL version 3, you are        --
--  granted additional permissions described in the GCC Runtime Library     --
--  Exception, version 3.1, as published by the Free Software Foundation.   --
--                                                                          --
--  You should have received a copy of the GNU General Public License and   --
--  a copy of the GCC Runtime Library Exception along with this program;    --
--  see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see   --
--  <http://www.gnu.org/licenses/>.                                         --
--                                                                          --
--  As a special exception, if other files instantiate generics from this   --
--  unit, or you link this unit with other files to produce an executable,  --
--  this  unit  does not  by itself cause  the resulting executable to be   --
--  covered by the GNU General Public License. This exception does not      --
--  however invalidate any other reasons why the executable file might be   --
--  covered by the  GNU Public License.                                     --
--                                                                          --
--  For more information please go to http://www.gnoga.com                  --
------------------------------------------------------------------------------

with Gnoga.Server.Connection;

package body Gnoga.Gui.Plugin.ChartJS is

   -------------------
   -- Load_ChartJS  --
   -------------------

   procedure Load_ChartJS
     (Window : in out Gnoga.Gui.Window.Window_Type'Class) is

      script_Luxon : constant String :=
        "<script "
        & "src='https://cdn.jsdelivr.net/npm/luxon@1.28.0'>"
        & "</script>";

      script_ChartJS : constant String :=
        "<script "
        & "src='https://cdn.jsdelivr.net/npm/chart.js@3.6.0'>"
        & "</script>";

      script_Adapter_Luxon : constant String :=
        "<script "
        & "src='https://cdn.jsdelivr.net/npm/chartjs-adapter-luxon@1.1.0'>"
        & "</script>";

      script_Chart_Streaming : constant String :=
        "<script "
        & "src='https://cdn.jsdelivr.net/npm/chartjs-plugin-streaming@2.0.0'>"
        & "</script>";

      --  scripts_to_load : constant String :=
      --    script_Luxon
      --    & script_ChartJS
      --    & script_Adapter_Luxon
      --    & script_Chart_Streaming;
   begin
      Window.Document.Head_Element.jQuery_Execute
      ("append('" & Escape_Quotes (script_Luxon) & "')");

      Window.Document.Head_Element.jQuery_Execute
      ("append('" & Escape_Quotes (script_ChartJS) & "')");

      delay 0.1;

      Window.Document.Head_Element.jQuery_Execute
      ("append('" & Escape_Quotes (script_Adapter_Luxon) & "')");

      Window.Document.Head_Element.jQuery_Execute
      ("append('" & Escape_Quotes (script_Chart_Streaming) & "')");

      delay 0.1;

      Window.Document.Head_Element.jQuery_Execute
      ("append('" &
       Escape_Quotes
         ("<script src='/js/mychartmod.js' type='module'></script>")
       & "')");

   end Load_ChartJS;

   -------------------
   -- Chart_Refresh --
   -------------------

   procedure Chart_Refresh
     (ID     : in Gnoga.Types.Connection_ID;
      Index  : in String;
      Value  : String) is
      Result : constant String := Gnoga.Server.Connection.Execute_Script
        (ID     => ID,
         Script => Escape_Quotes
           ("Refresh('" & Index & "', '" & Value & "');"));
   begin
      null;
   end Chart_Refresh;

end Gnoga.Gui.Plugin.ChartJS;
