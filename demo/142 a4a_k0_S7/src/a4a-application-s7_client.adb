
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Exceptions; use Ada.Exceptions;

with A4A.Log;
with A4A.Logger; use A4A.Logger;

--  tag::s7_client[]
with A4A.User_Functions; use A4A.User_Functions;

package body A4A.Application.S7_Client is

   task body Scanner is
      My_Ident : constant String := "A4A.Application.S7_Client.Scanner";
   begin

      accept Start;

      loop

         S7Client_Test;    --  <1>

         select
            accept Stop;
            A4A.Log.Logger.Put (Who       => My_Ident,
                                What      => "I'm dead !",
                                Log_Level => Level_Info);
            exit;
         or
            delay 0.5;     --  <2>
         end select;

      end loop;

   exception

      when Error : others =>
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => Exception_Information (Error));

         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Aborted !");
   end Scanner;

end A4A.Application.S7_Client;
--  end::s7_client[]
