
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with A4A.Protocols; use A4A.Protocols.IP_Address_Strings;

--  tag::config[]
package A4A.Application.S7_Client_Config is

   --------------------------------------------------------------------
   --  S7 Client configuration
   --------------------------------------------------------------------
   Remote_IP_Address : String := "195.13.34.87";
   Local_IP_Address  : String := "192.168.253.240";

   Server_IP_Address : Bounded_String :=
    To_Bounded_String (Remote_IP_Address);    --  <1>
   Server_TCP_Port   : Positive := 102;      --  <2>

   CPU_Rack       : Natural  := 0;           --  <3>
   CPU_Slot       : Natural  := 3;           --  <4>
end A4A.Application.S7_Client_Config;
--  end::config[]
