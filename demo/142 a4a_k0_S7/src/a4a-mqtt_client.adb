
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Text_IO;

with GNAT.Sockets.Server;    use GNAT.Sockets.Server;
with Strings_Edit.Integers;  use Strings_Edit.Integers;

with Ada.Unchecked_Deallocation;

with A4A.Log;
with A4A.Logger; use A4A.Logger;

package body A4A.MQTT_Client is

   procedure Free is new Ada.Unchecked_Deallocation (String, String_Ptr);

   My_Ident : constant String := "A4A.MQTT_Client";

   overriding
   procedure Finalize (Client : in out Instance) is
   begin
      Finalize (MQTT_Peer (Client));
      Free (Client.Name);

      A4A.Log.Logger.Put (Who       => My_Ident & ".Finalize",
                          What      => "Done !",
                          Log_Level => Level_Info);
   end Finalize;

   function Get_Name (Client : Instance) return String is
   begin
      if Client.Name = null then
         return GNAT.Sockets.Image (Get_Client_Address (Client));
      else
         return Client.Name.all;
      end if;
   end Get_Name;

   function Got_Ping_Response (Client : Instance) return Boolean is
   begin
      return Client.Ping_Response;
   end Got_Ping_Response;

   procedure Reset_Ping_Response (Client : in out Instance) is
   begin
      Client.Ping_Response := False;
   end Reset_Ping_Response;

   overriding
   procedure On_Connect_Accepted
     (Peer            : in out Instance;
      Session_Present : Boolean)  is
   begin
      A4A.Log.Logger.Put (Who       => My_Ident & ".On_Connect_Accepted",
                          What      => "Connect accepted",
                          Log_Level => Level_Info);

      Peer.Action.Signal;
   end On_Connect_Accepted;

   overriding
   procedure On_Connect_Rejected
     (Peer     : in out Instance;
      Response : Connect_Response)  is
   begin
      A4A.Log.Logger.Put (Who       => My_Ident & ".On_Connect_Rejected",
                          What      => "Connect rejected " & Image (Response),
                          Log_Level => Level_Info);

      Peer.Action.Signal;
   end On_Connect_Rejected;

   overriding
   procedure On_Ping_Response (Peer : in out Instance) is
      --  pragma Unreferenced (Peer);
   begin
      Peer.Ping_Response := True;

      A4A.Log.Logger.Put (Who       => My_Ident & ".On_Ping_Response",
                          What      => "Ping response",
                          Log_Level => Level_Info);
   end On_Ping_Response;

   overriding
   procedure On_Publish
     (Peer      : in out Instance;
      Topic     : String;
      Message   : Stream_Element_Array;
      Packet    : Packet_Identification;
      Duplicate : Boolean;
      Retain    : Boolean)  is
   begin
      A4A.Log.Logger.Put
        (Who       => My_Ident & ".On_Publish",
         What      => "Message " & Topic & "=" & Image (Message),
         Log_Level => Level_Info);

      On_Publish
        (MQTT_Peer (Peer),
         Topic,
         Message,
         Packet,
         Duplicate,
         Retain);

      Peer.Action.Signal;
   end On_Publish;

   overriding
   procedure On_Subscribe_Acknowledgement
     (Peer   : in out Instance;
      Packet : Packet_Identifier;
      Codes  : Return_Code_List)  is
   begin
      --  Put (">Subscribed " & Image (Integer (Packet)) & ":");
      --  for Index in Codes'Range loop
      --     if Index /= Codes'First then
      --        Put (", ");
      --     end if;
      --     if Codes (Index).Success then
      --        Put (QoS_Level'Image (Codes (Index).QoS));
      --     else
      --        Put ("Failed");
      --     end if;
      --  end loop;
      --  New_Line;
      A4A.Log.Logger.Put
        (Who       => My_Ident & ".On_Subscribe_Acknowledgement",
         What      => "Subscribed " & Image (Integer (Packet)),
         Log_Level => Level_Info);

      Peer.Action.Signal;
   end On_Subscribe_Acknowledgement;

   procedure Reset_Event (Peer : in out Instance) is
   begin
      Peer.Action.Reset;
   end Reset_Event;

   procedure Wait_For_Event (Peer : in out Instance) is
   begin
      select
         Peer.Action.Wait;
      or delay 5.0;
         raise Ada.Text_IO.Data_Error;
      end select;
   end Wait_For_Event;

   procedure Set_Name (Peer : in out Instance; Name : String) is
   begin
      Free (Peer.Name);
      Peer.Name := new String'(Name);
   end Set_Name;

end A4A.MQTT_Client;
