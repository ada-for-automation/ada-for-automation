
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Exceptions;               use Ada.Exceptions;

with A4A.Log;
with A4A.Logger; use A4A.Logger;

with A4A.MQTT_Client;

with A4A.User_Objects;

with A4A.User_Objects_MQTT;

with GNAT.Sockets.MQTT;
with GNAT.Sockets.Server;
with GNAT.Sockets.Server.Handles;

--  tag::functions[]
package body A4A.User_Functions_MQTT is
   My_Ident : constant String := "A4A.User_Functions_MQTT";

   --------------------------------------------------------------------
   --  User functions
   --------------------------------------------------------------------

   --------------------------------------------------------------------
   --  MQTT tests
   --------------------------------------------------------------------

   --  Server_Address : constant String := "test.mosquitto.org";
   Server_Address : constant String := "127.0.0.1";

   Is_Connected : Boolean := False;
   Ping_Sent    : Boolean := False;

   procedure MQTTClient_On_Start is
   begin

      --  GNAT.Sockets.Server.Trace_On
      --    (Factory  => A4A.User_Objects_MQTT.Factory,
      --     Received => GNAT.Sockets.Server.Trace_Decoded,
      --     Sent     => GNAT.Sockets.Server.Trace_Decoded);

      GNAT.Sockets.Server.Handles.Set
        (A4A.User_Objects_MQTT.Reference,
         new A4A.MQTT_Client.Instance
           (Listener             => A4A.User_Objects_MQTT.Server'Access,
            Input_Size           => 80,
            Output_Size          => 10, -- Deliberately small
            Max_Subscribe_Topics => 20));

      declare

         Client : A4A.MQTT_Client.Instance renames
           A4A.MQTT_Client.Instance
             (GNAT.Sockets.Server.Handles.Ptr
                (A4A.User_Objects_MQTT.Reference).all);

      begin

         Client.Set_Overlapped_Size (4);
         --  One response packet

         GNAT.Sockets.Server.Connect
           (A4A.User_Objects_MQTT.Server,
            Client'Unchecked_Access,
            Server_Address,
            GNAT.Sockets.MQTT.MQTT_Port);

         delay 2.0;

         if Client.Is_Connected then

            A4A.Log.Logger.Put
              (Who       => My_Ident & ".MQTTClient_On_Start",
               What      => "MQTT client connected to " & Server_Address,
               Log_Level => Level_Info);

            A4A.Log.Logger.Put
              (Who       => My_Ident & ".MQTTClient_On_Start",
               What      => "Send_Connect",
               Log_Level => Level_Info);

            Client.Send_Connect ("A4AMQTTclient", Keep_Alive => 60.0);
            delay 1.0;

            Is_Connected := True;

         else

            A4A.Log.Logger.Put
              (Who       => My_Ident & ".MQTTClient_On_Start",
               What      =>
                 "MQTT client could not connect to " & Server_Address,
               Log_Level => Level_Info);

         end if;
      end;

   exception
      when Error : others =>
         A4A.Log.Logger.Put (Who  => My_Ident & ".MQTTClient_On_Start",
                             What => "Unexpected exception: " & CRLF
                             & Exception_Information (Error));
   end MQTTClient_On_Start;

   procedure MQTTClient_On_Stop is

      Client : A4A.MQTT_Client.Instance renames
        A4A.MQTT_Client.Instance
          (GNAT.Sockets.Server.Handles.Ptr
             (A4A.User_Objects_MQTT.Reference).all);

   begin

      if Is_Connected then

         A4A.Log.Logger.Put
           (Who       => My_Ident & ".MQTTClient_On_Stop",
            What      => "Send_Disconnect",
            Log_Level => Level_Info);

         Client.Send_Disconnect;
         delay 1.0;

      end if;

      GNAT.Sockets.Server.Handles.Set
        (A4A.User_Objects_MQTT.Reference, null);

      A4A.User_Objects_MQTT.Server.Finalize;

   exception
      when Error : others =>
         A4A.Log.Logger.Put (Who  => My_Ident & ".MQTTClient_On_Stop",
                             What => "Unexpected exception: " & CRLF
                             & Exception_Information (Error));
   end MQTTClient_On_Stop;

   procedure MQTTClient_On_Error is
   begin
      null;
   end MQTTClient_On_Error;

   procedure MQTTClient_Test is

      Client : A4A.MQTT_Client.Instance renames
        A4A.MQTT_Client.Instance
          (GNAT.Sockets.Server.Handles.Ptr
             (A4A.User_Objects_MQTT.Reference).all);

   begin

      if Ping_Sent and not Client.Got_Ping_Response then
         Is_Connected := False;

         A4A.Log.Logger.Put
           (Who       => My_Ident & ".MQTTClient_Test",
            What      => "Got no Ping Response",
            Log_Level => Level_Error);

      end if;

      Ping_Sent := False;

      if Is_Connected then

         Client.Reset_Ping_Response;
         Client.Send_Ping;
         Ping_Sent := True;

         if A4A.User_Objects_MQTT.MW0 /= A4A.User_Objects.MW0 then

            A4A.User_Objects_MQTT.MW0 := A4A.User_Objects.MW0;

            A4A.Log.Logger.Put
              (Who       => My_Ident & ".MQTTClient_Test",
               What      => "Send_Publish",
               Log_Level => Level_Verbose);

            --  Suscribe using :
            --  mosquitto_sub -h test.mosquitto.org -t a4a/s7-315-2dp/mw0 -v
            Client.Send_Publish
              (Topic     => "a4a/s7-315-2dp/mw0",
               Message   => Integer'Image
                 (Integer (Word_To_Int (A4A.User_Objects_MQTT.MW0))),
               Packet    => (QoS => GNAT.Sockets.MQTT.At_Most_Once),
               Duplicate => False,
               Retain    => False);

         end if;

      end if;

   exception
      when Error : others =>
         A4A.Log.Logger.Put (Who  => My_Ident & ".MQTTClient_Test",
                             What => "Unexpected exception: " & CRLF
                             & Exception_Information (Error));
   end MQTTClient_Test;

end A4A.User_Functions_MQTT;
--  end::functions[]
