------------------------------------------------------------------------------
--                                                                          --
--                   GNOGA - The GNU Omnificent GUI for Ada                 --
--                                                                          --
--              G N O G A . G U I . P L U G I N . J U S T G A G E           --
--                                                                          --
--                                 S p e c                                  --
--                                                                          --
--                                                                          --
--                     Copyright (C) 2021 Stephane LOS                      --
--                                                                          --
--  This library is free software;  you can redistribute it and/or modify   --
--  it under terms of the  GNU General Public License  as published by the  --
--  Free Software  Foundation;  either version 3,  or (at your  option) any --
--  later version. This library is distributed in the hope that it will be  --
--  useful, but WITHOUT ANY WARRANTY;  without even the implied warranty of --
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.                    --
--                                                                          --
--  As a special exception under Section 7 of GPL version 3, you are        --
--  granted additional permissions described in the GCC Runtime Library     --
--  Exception, version 3.1, as published by the Free Software Foundation.   --
--                                                                          --
--  You should have received a copy of the GNU General Public License and   --
--  a copy of the GCC Runtime Library Exception along with this program;    --
--  see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see   --
--  <http://www.gnu.org/licenses/>.                                         --
--                                                                          --
--  As a special exception, if other files instantiate generics from this   --
--  unit, or you link this unit with other files to produce an executable,  --
--  this  unit  does not  by itself cause  the resulting executable to be   --
--  covered by the GNU General Public License. This exception does not      --
--  however invalidate any other reasons why the executable file might be   --
--  covered by the  GNU Public License.                                     --
--                                                                          --
--  For more information please go to http://www.gnoga.com                  --
------------------------------------------------------------------------------

with Gnoga.Types;
with Gnoga.Gui.Window;

package Gnoga.Gui.Plugin.JustGage is

   --  JustGage Ada API is inspired by https://github.com/toorshia/justgage.
   --  JustGage.js is released under the (http://opensource.org/licenses/MIT)
   --  MIT License.

   --  Some comments come from JustGage.js documentation:
   --  JustGage.js is a handy JavaScript plugin for generating and animating
   --  nice & clean gauges. It is based on Rapha�l library for vector drawing,
   --  so it's completely resolution independent and self-adjusting.
   --  Oh yes, since it's pure SVG, it works in almost any browser -
   --  IE6+, Chrome, Firefox, Safari, Opera, Android, etc.

   procedure Load_JustGage
     (Window : in out Gnoga.Gui.Window.Window_Type'Class);
   --  Load JustGage code into Window

   procedure Gauge_Refresh
     (ID     : in Gnoga.Types.Connection_ID;
      Gauge  : in String;
      Value  : String);
   --  Refresh the gauge

end Gnoga.Gui.Plugin.JustGage;
