
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Gnoga.Application.Multi_Connect; use Gnoga;

with A4A.Web.Pages.General_Status;
with A4A.Web.Pages.MBTCP_Server_Status;
with A4A.Web.Pages.Home;
with A4A.Web.Pages.Chartjs;
with A4A.Web.Pages.Liquid_Gauges;
with A4A.Web.Pages.ECharts;
with A4A.Web.Pages.S7_Com;
with A4A.Web.Pages.CPU_Infos;
use A4A.Web.Pages;

with A4A.Web.Web_Server_Config;

package body A4A.Application.Web is

   procedure Connect_Data is
   begin

      null;

   end Connect_Data;

   procedure Update_Data is
   begin

      null;

   end Update_Data;

   procedure On_Server_Start is
   begin

      Gnoga.Application.Multi_Connect.Initialize
        (Event => Home.On_Connect'Access,
         Port  => A4A.Web.Web_Server_Config.Port,
         Boot  => "000-boot.html");

      Gnoga.Application.Multi_Connect.On_Connect_Handler
        (Event => Home.On_Connect'Access,
         Path => "dashboard");

      Gnoga.Application.Multi_Connect.On_Connect_Handler
        (Event => Chartjs.On_Connect'Access,
         Path => "chartjs");

      Gnoga.Application.Multi_Connect.On_Connect_Handler
        (Event => S7_Com.On_Connect'Access,
         Path => "s7_com");

      Gnoga.Application.Multi_Connect.On_Connect_Handler
        (Event => CPU_Infos.On_Connect'Access,
         Path => "cpu_info");

      Gnoga.Application.Multi_Connect.On_Connect_Handler
        (Event => Liquid_Gauges.On_Connect'Access,
         Path => "liquid_gauges");

      Gnoga.Application.Multi_Connect.On_Connect_Handler
        (Event => ECharts.On_Connect'Access,
         Path => "echarts");

      Gnoga.Application.Multi_Connect.On_Connect_Handler
        (Event => General_Status.On_Connect'Access,
         Path => "general_status");

      Gnoga.Application.Multi_Connect.On_Connect_Handler
        (Event => MBTCP_Server_Status.On_Connect'Access,
         Path => "mbtcp_server_status");

      Gnoga.Application.Title
        ("Ada for Automation Modbus TCP Server / S7 Gateway");

   end On_Server_Start;

end A4A.Application.Web;
