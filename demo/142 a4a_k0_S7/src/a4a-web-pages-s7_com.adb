
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Text_IO; use Ada.Text_IO;
with Ada.Float_Text_IO;

with A4A.Log;
with A4A.Logger; use A4A.Logger;

with A4A.User_Objects; use A4A.User_Objects;

with A4A.Web.Controllers.Connection_Stats_Controllers;
use A4A.Web.Controllers;

with UXStrings; use UXStrings;

package body A4A.Web.Pages.S7_Com is

   overriding
   procedure Update_Page (Web_Page : access Instance) is
      My_Ident : constant String := "A4A.Web.Pages.S7_Com.Update_Page";
      C1_Dec : Word;

      function Update_Float
        (Value  : in Float;
         Aft    : in Field := Ada.Float_Text_IO.Default_Aft;
         Exp    : in Field := Ada.Float_Text_IO.Default_Exp) return String;
      function Update_Float
        (Value  : in Float;
         Aft    : in Field := Ada.Float_Text_IO.Default_Aft;
         Exp    : in Field := Ada.Float_Text_IO.Default_Exp) return String is
         Value_Str : String (1 .. 30);
      begin
         Ada.Float_Text_IO.Put (To  => Value_Str, Item => Value,
                                Aft => Aft, Exp => Exp);
         return To_UTF_8 (Gnoga.Left_Trim (From_UTF_8 (Value_Str)));

      end Update_Float;

   begin

      Web_Page.Common_View.Connection_Stats_View.Update_View;

      Web_Page.S7_Com_View.Connection_View.Update_Element
        (Status => Connected);

      Web_Page.S7_Com_View.Running_View.Update_Element (Status => Running);

      if MW0 /= Web_Page.S7_Com_View.MW0 then
         Web_Page.S7_Com_View.MW0_View.Inner_HTML
           (From_UTF_8 (Integer'Image (Integer (Word_To_Int (MW0)))));
         Web_Page.S7_Com_View.MW0 := MW0;
      end if;

      Web_Page.S7_Com_View.MW2_View.Update_Element (Value => MW2, Base => 16);

      Web_Page.S7_Com_View.MW4_View.Update_Element (Value => MW4);
      if MW4 /= Web_Page.S7_Com_View.MW4 then
         A4A.Log.Logger.Put
           (Who       => My_Ident,
            What      => "MW4 updated ! "
            & MW4'Img & " : " & Web_Page.S7_Com_View.MW4'Img,
            Log_Level => Level_Info);
         Web_Page.S7_Com_View.MW4_Slider.Value
           (Gnoga.Left_Trim (From_UTF_8 (MW4'Img)));
         Web_Page.S7_Com_View.MW4_Number.Value
           (Gnoga.Left_Trim (From_UTF_8 (MW4'Img)));
         Web_Page.S7_Com_View.MW4 := MW4;
      end if;

      Web_Page.S7_Com_View.MD8_View.Update_Element (Value => MD8, Base => 16);

      Web_Page.S7_Com_View.MD8F_View.Update_Element
        (Value => MD8F, Aft => 2, Exp => 0);

      if MD8F /= Web_Page.S7_Com_View.MD8F then
         declare
            MD8F_Value_Str : constant String :=
              Update_Float (Value => MD8F, Aft => 2, Exp => 0);
         begin
            A4A.Log.Logger.Put
              (Who       => My_Ident,
               What      => "MD8F updated ! " & MD8F'Img
               & " : " & Web_Page.S7_Com_View.MD8F'Img
               & " : " & MD8F_Value_Str,
               Log_Level => Level_Info);

            Web_Page.S7_Com_View.MD8F_Slider.Value
              (From_UTF_8 (MD8F_Value_Str));
            Web_Page.S7_Com_View.MD8F_Number.Value
              (From_UTF_8 (MD8F_Value_Str));
            Web_Page.S7_Com_View.MD8F_Meter.Value
              (From_UTF_8 (MD8F_Value_Str));
            Web_Page.S7_Com_View.MD8F_Number2.Value
              (From_UTF_8 (MD8F_Value_Str));
            Web_Page.S7_Com_View.MD8F := MD8F;
         end;
      end if;

      Web_Page.S7_Com_View.IW0_View.Update_Element (Value => IW0, Base => 16);

      Web_Page.S7_Com_View.QW0_View.Update_Element (Value => QW0, Base => 2);

      Web_Page.S7_Com_View.DB1_W0_B10_View.Update_Element
        (Value => DB1_W0, Base => 10);

      Web_Page.S7_Com_View.DB1_W0_B16_View.Update_Element
        (Value => DB1_W0, Base => 16);

      Web_Page.S7_Com_View.DB1_W0_B02_View.Update_Element
        (Value => DB1_W0, Base => 2);

      if T1 /= Web_Page.S7_Com_View.T1 then
         Web_Page.S7_Com_View.T1_View.Inner_HTML
           (From_UTF_8 (S5Time_Image (s5time => T1)));
         Web_Page.S7_Com_View.T1 := T1;
      end if;

      if C1 /= Web_Page.S7_Com_View.C1 then
         BCD_To_DEC (bcd_val => C1 and 16#0FFF#,
                     dec_val => C1_Dec);
         Web_Page.S7_Com_View.C1_View.Inner_HTML (From_UTF_8 (C1_Dec'Img));
         Web_Page.S7_Com_View.C1 := C1;
      end if;

   end Update_Page;

   procedure On_Connect
     (Main_Window : in out Gnoga.Gui.Window.Window_Type'Class;
      Connection  : access
        Gnoga.Application.Multi_Connect.Connection_Holder_Type)
   is
      Web_Page : constant Instance_Access := new Instance;

      My_Ident : aliased String := "A4A.Web.Pages.S7_Com_View_Updater";
      View_Updater_Task : A4A.Web.Pages.Updater_Task_Type
        (My_Ident                    => My_Ident'Access,
         Web_Page                    => Web_Page);

   begin
      Main_Window.Connection_Data (Web_Page);

      Main_Window.Document.Load_CSS (From_UTF_8 (W3_CSS_URL));
      Main_Window.Document.Load_CSS (From_UTF_8 (Font_Awesome_CSS_URL));
      Main_Window.Document.Load_CSS (From_UTF_8 (APP_CSS_URL));
      Main_Window.Document.Load_CSS ("/css/form.css");

      Connection_Stats_Controllers.Connection;

      Common_View_Setup (Web_Page.Common_View,
                         Main_Window'Unchecked_Access,
                         "052-s7-com.html");

      Web_Page.My_Connection_ID :=
        Web_Page.Common_View.Main_Window.Connection_ID;

      Web_Page.S7_Com_View.Connection_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "s7-connection");

      Web_Page.S7_Com_View.Running_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "s7-running");

      Web_Page.S7_Com_View.MW0_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "mw0");
      Web_Page.S7_Com_View.MW0_View.Inner_HTML (From_UTF_8 (MW0'Img));
      Web_Page.S7_Com_View.MW0 := MW0;

      Web_Page.S7_Com_View.MW2_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "mw2");

      Web_Page.S7_Com_View.MW4_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "mw4");

      Web_Page.S7_Com_View.MD8_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "md8");

      Web_Page.S7_Com_View.MD8F_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "md8f");

      Web_Page.S7_Com_View.IW0_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "iw0");

      Web_Page.S7_Com_View.QW0_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "qw0");

      Web_Page.S7_Com_View.DB1_W0_B10_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "db1_w0_b10");

      Web_Page.S7_Com_View.DB1_W0_B16_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "db1_w0_b16");

      Web_Page.S7_Com_View.DB1_W0_B02_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "db1_w0_b02");

      Web_Page.S7_Com_View.T1_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "t1");

      Web_Page.S7_Com_View.C1_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "c1");

      Web_Page.S7_Com_View.MW4_Slider.Attach_Using_Parent
        (Web_Page.Common_View.View, "sp_range");

      Web_Page.S7_Com_View.MW4_Slider.On_Change_Handler
        (On_Change_Range'Access);

      Web_Page.S7_Com_View.MW4_Number.Attach_Using_Parent
        (Web_Page.Common_View.View, "sp_number");

      Web_Page.S7_Com_View.MW4_Number.On_Change_Handler
        (On_Change_Number'Access);

      Web_Page.S7_Com_View.MD8F_Slider.Attach_Using_Parent
        (Web_Page.Common_View.View, "mv_range");

      Web_Page.S7_Com_View.MD8F_Slider.On_Change_Handler
        (On_Change_Range_MD8F'Access);

      Web_Page.S7_Com_View.MD8F_Number.Attach_Using_Parent
        (Web_Page.Common_View.View, "mv_number");

      Web_Page.S7_Com_View.MD8F_Number.On_Change_Handler
        (On_Change_Number_MD8F'Access);

      Web_Page.S7_Com_View.MD8F_Meter.Attach_Using_Parent
        (Web_Page.Common_View.View, "mv_meter");

      Web_Page.S7_Com_View.MD8F_Number2.Attach_Using_Parent
        (Web_Page.Common_View.View, "mv_number2");

      delay 0.1;

      View_Updater_Task.Start;

      Connection.Hold;

      View_Updater_Task.Stop;

   end On_Connect;

   procedure BCD_To_DEC (bcd_val : in Word;
                         dec_val : out Word)
   is
   begin
      dec_val :=
        (bcd_val and 16#000F#)
        + (Shift_Right (Value => (bcd_val and 16#00F0#), Amount =>  4) *   10)
        + (Shift_Right (Value => (bcd_val and 16#0F00#), Amount =>  8) *  100)
        + (Shift_Right (Value => (bcd_val and 16#F000#), Amount => 12) * 1000);
   end BCD_To_DEC;

   function S5Time_Image (s5time : Word) return String is
      base : constant Word :=
        Shift_Right (Value => (s5time and 16#3000#), Amount => 12);
      count : Word;
      time_ms : DWord;
   begin
      BCD_To_DEC (bcd_val => (s5time and 16#0FFF#), dec_val => count);
      time_ms := DWord (count);
      case base is
         when 16#00# => -- 10 ms
            time_ms := time_ms * 10;
         when 16#01# => -- 100 ms
            time_ms := time_ms * 100;
         when 16#02# => -- 1000 ms
            time_ms := time_ms * 1000;
         when 16#03# => -- 10000 ms
            time_ms := time_ms * 10_000;
         when others => -- unknown
            time_ms := 0;
      end case;
      return time_ms'Img & " ms";
   end S5Time_Image;

   procedure On_Change_Range (Object : in out Gnoga.Gui.Base.Base_Type'Class)
   is
      My_Ident : constant String := "A4A.Web.Pages.S7_Com.On_Change_Range";
      Web_Page : constant Instance_Access :=
        Instance_Access (Object.Connection_Data);
   begin
      MW4_New := Word'Value (To_UTF_8 (Web_Page.S7_Com_View.MW4_Slider.Value));
      MW4_Write := True;

      A4A.Log.Logger.Put
        (Who       => My_Ident,
         What      => "MW4 slided ! " & MW4_New'Img,
         Log_Level => Level_Info);
   end On_Change_Range;

   procedure On_Change_Number (Object : in out Gnoga.Gui.Base.Base_Type'Class)
   is
      My_Ident : constant String := "A4A.Web.Pages.S7_Com.On_Change_Number";
      Web_Page : constant Instance_Access :=
        Instance_Access (Object.Connection_Data);
   begin
      MW4_New := Word'Value (To_UTF_8 (Web_Page.S7_Com_View.MW4_Number.Value));
      MW4_Write := True;

      A4A.Log.Logger.Put
        (Who       => My_Ident,
         What      => "MW4 Number changed ! " & MW4_New'Img,
         Log_Level => Level_Info);
   end On_Change_Number;

   procedure On_Change_Range_MD8F
     (Object : in out Gnoga.Gui.Base.Base_Type'Class)
   is
      My_Ident : constant String :=
        "A4A.Web.Pages.S7_Com.On_Change_Range_MD8F";
      Web_Page : constant Instance_Access :=
        Instance_Access (Object.Connection_Data);
   begin
      MD8F_New := Float'Value
        (To_UTF_8 (Web_Page.S7_Com_View.MD8F_Slider.Value));
      MD8F_Write := True;

      A4A.Log.Logger.Put
        (Who       => My_Ident,
         What      => "MD8F slided ! " & MD8F_New'Img,
         Log_Level => Level_Info);
   end On_Change_Range_MD8F;

   procedure On_Change_Number_MD8F
     (Object : in out Gnoga.Gui.Base.Base_Type'Class)
   is
      My_Ident : constant String :=
        "A4A.Web.Pages.S7_Com.On_Change_Number_MD8F";
      Web_Page : constant Instance_Access :=
        Instance_Access (Object.Connection_Data);
   begin
      MD8F_New := Float'Value
        (To_UTF_8 (Web_Page.S7_Com_View.MD8F_Number.Value));
      MD8F_Write := True;

      A4A.Log.Logger.Put
        (Who       => My_Ident,
         What      => "MD8F Number changed ! " & MD8F_New'Img,
         Log_Level => Level_Info);
   end On_Change_Number_MD8F;

end A4A.Web.Pages.S7_Com;
