
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Gnoga.Gui.Plugin.Liquid_Fill_Gauges;

with A4A.User_Objects; use A4A.User_Objects;

with A4A.Web.Controllers.Connection_Stats_Controllers;
use A4A.Web.Controllers;

with A4A.Web.Script;
use A4A.Web.Script;

with UXStrings; use UXStrings;

package body A4A.Web.Pages.Liquid_Gauges is

   overriding
   procedure Update_Page (Web_Page : access Instance) is
   begin

      Web_Page.Common_View.Connection_Stats_View.Update_View;

      if MW0 /= Web_Page.Gauges_View.MW0 then
         Gnoga.Gui.Plugin.Liquid_Fill_Gauges.Gauge_Refresh
           (ID     => Web_Page.My_Connection_ID,
            Gauge  => "MW0_Gauge",
            Value  => From_UTF_8 (Liquid_Gauge_Value (MW0)));

         Web_Page.Gauges_View.MW0 := MW0;
      end if;

      if IW0 /= Web_Page.Gauges_View.IW0 then
         Gnoga.Gui.Plugin.Liquid_Fill_Gauges.Gauge_Refresh
           (ID     => Web_Page.My_Connection_ID,
            Gauge  => "IW0_Gauge",
            Value  => From_UTF_8 (Liquid_Gauge_Value (IW0)));

         Web_Page.Gauges_View.IW0 := IW0;
      end if;

      if QW0 /= Web_Page.Gauges_View.QW0 then
         Gnoga.Gui.Plugin.Liquid_Fill_Gauges.Gauge_Refresh
           (ID     => Web_Page.My_Connection_ID,
            Gauge  => "QW0_Gauge",
            Value  => From_UTF_8 (Liquid_Gauge_Value (QW0)));

         Web_Page.Gauges_View.QW0 := QW0;
      end if;

      if DB1_W0 /= Web_Page.Gauges_View.DB1_W0 then
         Gnoga.Gui.Plugin.Liquid_Fill_Gauges.Gauge_Refresh
           (ID     => Web_Page.My_Connection_ID,
            Gauge  => "DB1_DBW0_Gauge",
            Value  => From_UTF_8 (Liquid_Gauge_Value (DB1_W0)));

         Web_Page.Gauges_View.DB1_W0 := DB1_W0;
      end if;

   end Update_Page;

   procedure On_Connect
     (Main_Window : in out Gnoga.Gui.Window.Window_Type'Class;
      Connection  : access
        Gnoga.Application.Multi_Connect.Connection_Holder_Type)
   is
      Web_Page : constant Instance_Access := new Instance;

      My_Ident : aliased String := "A4A.Web.Pages.Liquid_Gauges_View_Updater";
      View_Updater_Task : A4A.Web.Pages.Updater_Task_Type
        (My_Ident                    => My_Ident'Access,
         Web_Page                    => Web_Page);

   begin
      Main_Window.Connection_Data (Web_Page);

      Main_Window.Document.Load_CSS (From_UTF_8 (W3_CSS_URL));
      Main_Window.Document.Load_CSS (From_UTF_8 (Font_Awesome_CSS_URL));
      Main_Window.Document.Load_CSS (From_UTF_8 (APP_CSS_URL));
      Main_Window.Document.Load_CSS ("/css/liquidfillgauges.css");

      Connection_Stats_Controllers.Connection;

      Common_View_Setup (Web_Page.Common_View,
                         Main_Window'Unchecked_Access,
                         "054-liquid-gauges.html");

      Web_Page.My_Connection_ID :=
        Web_Page.Common_View.Main_Window.Connection_ID;

      Gnoga.Gui.Plugin.Liquid_Fill_Gauges.Load_Liquid_Gauges (Main_Window);

      delay 0.1;

      Load_My_Script (Main_Window, "/js/MyLiquidGauges.js");

      delay 0.1;

      View_Updater_Task.Start;

      Connection.Hold;

      View_Updater_Task.Stop;

   end On_Connect;

   function Liquid_Gauge_Value (Value : Word) return String is
      Value_Percent : constant Float := (Float (Value) * 100.0) / 65535.0;
   begin
      return Value_Percent'Image;
   end Liquid_Gauge_Value;

end A4A.Web.Pages.Liquid_Gauges;
