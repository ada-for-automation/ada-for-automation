
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

package A4A.Application.Identification is

   function Get_Application_Name return String;

   function Get_Application_Version return String;

   function Get_Application_Description return String;

private

   Application_Name : String :=
     "Hilscher cifX Test Application (app6) with GUI";

   Application_Version : String := "2015/12/24";

   Application_Description : String :=
     "This is the test application 6." & CRLF
     & CRLF
     & "The purpose of this application is to demonstrate "
     & "the following capabilities :" & CRLF
     & " * Command Line or Graphical User Interface," & CRLF
     & " * Modbus TCP Server to connect a SCADA system," & CRLF
     & " * Modbus TCP Clients to connect to IO servers," & CRLF
     & " * Hilscher cifX binding with two channels," & CRLF
     & " * some of the functions and objects of the library." & CRLF
     & CRLF
     & "There is a GUI using GtkAda." & CRLF
     & CRLF
     & "It is supposed to be run in conjunction with a SCADA system and "
     & "slave devices on a supported field bus system.";

end A4A.Application.Identification;
