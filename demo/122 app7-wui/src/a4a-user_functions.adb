
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with A4A.Memory.Hilscher_Fieldbus1;
use A4A.Memory;

with A4A.Library.Conversion; use A4A.Library.Conversion;
with A4A.User_Objects; use A4A.User_Objects;

package body A4A.User_Functions is

   --------------------------------------------------------------------
   --  User functions
   --------------------------------------------------------------------

   procedure Map_Inputs is
   begin

      Status_Word_Byte0 := Hilscher_Fieldbus1.Inputs (0);
      Byte_To_Booleans (Byte_in       => Status_Word_Byte0,
                        Boolean_out00 => Coils (0),
                        Boolean_out01 => Coils (1),
                        Boolean_out02 => Coils (2),
                        Boolean_out03 => Coils (3),
                        Boolean_out04 => Coils (4),
                        Boolean_out05 => Coils (5),
                        Boolean_out06 => Coils (6),
                        Boolean_out07 => Coils (7)
                       );

      Status_Word_Byte1 := Hilscher_Fieldbus1.Inputs (1);
      Byte_To_Booleans (Byte_in       => Status_Word_Byte1,
                        Boolean_out00 => Coils (8),
                        Boolean_out01 => Coils (9),
                        Boolean_out02 => Coils (10),
                        Boolean_out03 => Coils (11),
                        Boolean_out04 => Coils (12),
                        Boolean_out05 => Coils (13),
                        Boolean_out06 => Coils (14),
                        Boolean_out07 => Coils (15)
                       );

      Bytes_To_Word (LSB_Byte => Status_Word_Byte0,
                     MSB_Byte => Status_Word_Byte1,
                     Word_out => Status_Word);

      Bytes_To_DWord (Byte0 => Hilscher_Fieldbus1.Inputs (2),
                      Byte1 => Hilscher_Fieldbus1.Inputs (3),
                      Byte2 => Hilscher_Fieldbus1.Inputs (4),
                      Byte3 => Hilscher_Fieldbus1.Inputs (5),
                      DWord_out => Actual_Velocity_INC_DWord
                     );

      Actual_Velocity_INC := DWord_To_DInt (Actual_Velocity_INC_DWord);

      Operation_Mode_Display := Byte_To_SInt (Hilscher_Fieldbus1.Inputs (6));

   end Map_Inputs;

   procedure Map_Outputs is
   begin

      Booleans_To_Byte (Boolean_in00 => Input_Bits (0),
                        Boolean_in01 => Input_Bits (1),
                        Boolean_in02 => Input_Bits (2),
                        Boolean_in03 => Input_Bits (3),
                        Boolean_in04 => Input_Bits (4),
                        Boolean_in05 => Input_Bits (5),
                        Boolean_in06 => Input_Bits (6),
                        Boolean_in07 => Input_Bits (7),
                        Byte_out     => Control_Word_Byte0
                        );

      Booleans_To_Byte (Boolean_in00 => Input_Bits (8),
                        Boolean_in01 => Input_Bits (9),
                        Boolean_in02 => Input_Bits (10),
                        Boolean_in03 => Input_Bits (11),
                        Boolean_in04 => Input_Bits (12),
                        Boolean_in05 => Input_Bits (13),
                        Boolean_in06 => Input_Bits (14),
                        Boolean_in07 => Input_Bits (15),
                        Byte_out     => Control_Word_Byte1
                        );

      Bytes_To_Word (LSB_Byte => Control_Word_Byte0,
                     MSB_Byte => Control_Word_Byte1,
                     Word_out => Control_Word);

      Hilscher_Fieldbus1.Outputs (0) := Control_Word_Byte0;
      Hilscher_Fieldbus1.Outputs (1) := Control_Word_Byte1;

      DWord_To_Bytes (DWord_in => DInt_To_DWord (Target_Velocity_INC),
                      Byte0    => Hilscher_Fieldbus1.Outputs (2),
                      Byte1    => Hilscher_Fieldbus1.Outputs (3),
                      Byte2    => Hilscher_Fieldbus1.Outputs (4),
                      Byte3    => Hilscher_Fieldbus1.Outputs (5)
                      );

   end Map_Outputs;

   procedure Map_HMI_Inputs is
   begin

      null;

   end Map_HMI_Inputs;

   procedure Map_HMI_Outputs is
   begin

      null;

   end Map_HMI_Outputs;

   procedure Target_Velocity_Control is
   begin

      if Input_Bits (3) then

         if Target_Velocity_Slope then
            Target_Velocity_RPM :=
              Target_Velocity_RPM + Target_Velocity_Variation;
         else
            Target_Velocity_RPM :=
              Target_Velocity_RPM - Target_Velocity_Variation;
         end if;

         if Target_Velocity_RPM > Target_Velocity_RPM_Max then
            Target_Velocity_Slope := False;
            Target_Velocity_RPM := Target_Velocity_RPM_Max;
         elsif Target_Velocity_RPM < Target_Velocity_RPM_Min then
            Target_Velocity_Slope := True;
            Target_Velocity_RPM := Target_Velocity_RPM_Min;
         end if;

      else
         Target_Velocity_Slope := True;
         Target_Velocity_RPM := 0;
      end if;

      Target_Velocity_INC :=
        (Target_Velocity_RPM * Increments_Per_Rotation) / 60;

      Actual_Velocity_RPM :=
        (Actual_Velocity_INC * 60) / Increments_Per_Rotation;

   end Target_Velocity_Control;

end A4A.User_Functions;
