
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with A4A.Memory.MBTCP_IOServer;
with A4A.Memory.MBTCP_IOScan;
use A4A.Memory;

with A4A.User_Objects; use A4A.User_Objects;

package body A4A.User_Functions is

   --------------------------------------------------------------------
   --  User functions
   --------------------------------------------------------------------

   procedure Map_Inputs is
--        Temp_Bools : array (0..15) of Boolean := (others => False);
   begin

--        Word_To_Booleans
--          (Word_in       => MBTCP_IOScan.Word_Inputs(0),
--           Boolean_out00 => Auto,
--           Boolean_out01 => Manu,
--           Boolean_out02 => Ack_Faults,
--           Boolean_out03 => Level_Switch_11,
--           Boolean_out04 => Valve_14_Pos_Open,
--           Boolean_out05 => Valve_14_Pos_Closed,
--           Boolean_out06 => Level_Switch_12,
--           Boolean_out07 => MyPump13_FeedBack,
--
--           Boolean_out08 => Temp_Bools(8) , -- Spare
--           Boolean_out09 => Temp_Bools(9) , -- Spare
--           Boolean_out10 => Temp_Bools(10), -- Spare
--           Boolean_out11 => Temp_Bools(11), -- Spare
--           Boolean_out12 => Temp_Bools(12), -- Spare
--           Boolean_out13 => Temp_Bools(13), -- Spare
--           Boolean_out14 => Temp_Bools(14), -- Spare
--           Boolean_out15 => Temp_Bools(15)  -- Spare
--          );

      Auto                := MBTCP_IOScan.Bool_Inputs (32);
      Manu                := MBTCP_IOScan.Bool_Inputs (33);
      Ack_Faults          := MBTCP_IOScan.Bool_Inputs (34);
      Level_Switch_11     := MBTCP_IOScan.Bool_Inputs (35);
      Valve_14_Pos_Open   := MBTCP_IOScan.Bool_Inputs (36);
      Valve_14_Pos_Closed := MBTCP_IOScan.Bool_Inputs (37);
      Level_Switch_12     := MBTCP_IOScan.Bool_Inputs (38);
      MyPump13_FeedBack   := MBTCP_IOScan.Bool_Inputs (39);

      Level_Transmitter_10_Measure := MBTCP_IOScan.Word_Inputs (0);

   end Map_Inputs;

   procedure Map_Outputs is
   begin

--        Booleans_To_Word
--          (Boolean_in00 => MyPump13_Coil,
--           Boolean_in01 => Valve_14_Coil,
--           -- others => Spare
--           Word_out     => MBTCP_IOScan.Word_Outputs(0)
--          );

      MBTCP_IOScan.Bool_Outputs (0) := MyPump13_Coil;
      MBTCP_IOScan.Bool_Outputs (1) := Valve_14_Coil;

   end Map_Outputs;

   procedure Map_HMI_Inputs is
--        Temp_Bools : array (0..15) of Boolean := (others => False);
   begin

--        Word_To_Booleans
--          (Word_in       => MBTCP_IOServer.Registers(0),
--           Boolean_out00 => MyPump13_Manu_Cmd_On,
--           Boolean_out01 => Valve_14_Manu_Cmd_Open,
--           Boolean_out02 => Temp_Bools(2) , -- Spare
--           Boolean_out03 => Temp_Bools(3) , -- Spare
--           Boolean_out04 => Temp_Bools(4) , -- Spare
--           Boolean_out05 => Temp_Bools(5) , -- Spare
--           Boolean_out06 => Temp_Bools(6) , -- Spare
--           Boolean_out07 => Temp_Bools(7) , -- Spare
--
--           Boolean_out08 => Temp_Bools(8) , -- Spare
--           Boolean_out09 => Temp_Bools(9) , -- Spare
--           Boolean_out10 => Temp_Bools(10), -- Spare
--           Boolean_out11 => Temp_Bools(11), -- Spare
--           Boolean_out12 => Temp_Bools(12), -- Spare
--           Boolean_out13 => Temp_Bools(13), -- Spare
--           Boolean_out14 => Temp_Bools(14), -- Spare
--           Boolean_out15 => Temp_Bools(15)  -- Spare
--          );

      MyPump13_Manu_Cmd_On   := MBTCP_IOServer.Bool_Coils (0);
      Valve_14_Manu_Cmd_Open := MBTCP_IOServer.Bool_Coils (1);

   end Map_HMI_Inputs;

   procedure Map_HMI_Outputs is
   begin

--        Booleans_To_Word
--          (Boolean_in00 => MyPump13.is_On,
--           Boolean_in01 => MyPump13.is_Faulty,
--           Boolean_in02 => Valve_14.is_Open,
--           Boolean_in03 => Valve_14.is_Closed,
--           Boolean_in04 => Valve_14.is_Faulty,
--           Boolean_in05 => LS11_AH.is_On,
--           Boolean_in06 => LS11_AH.is_Faulty,
--           Boolean_in07 => LS12_AL.is_On,
--
--           Boolean_in08 => LS12_AL.is_Faulty,
--           -- others => Spare
--           Word_out     => MBTCP_IOServer.Input_Registers(0)
--          );

      MBTCP_IOServer.Bool_Inputs (0) := MyPump13.is_On;
      MBTCP_IOServer.Bool_Inputs (1) := MyPump13.is_Faulty;
      MBTCP_IOServer.Bool_Inputs (2) := Valve_14.is_Open;
      MBTCP_IOServer.Bool_Inputs (3) := Valve_14.is_Closed;
      MBTCP_IOServer.Bool_Inputs (4) := Valve_14.is_Faulty;
      MBTCP_IOServer.Bool_Inputs (5) := LS11_AH.is_On;
      MBTCP_IOServer.Bool_Inputs (6) := LS11_AH.is_Faulty;
      MBTCP_IOServer.Bool_Inputs (7) := LS12_AL.is_On;
      MBTCP_IOServer.Bool_Inputs (8) := LS12_AL.is_Faulty;

      MBTCP_IOServer.Input_Registers (0) := Level_Transmitter_10_Measure;

   end Map_HMI_Outputs;

end A4A.User_Functions;
