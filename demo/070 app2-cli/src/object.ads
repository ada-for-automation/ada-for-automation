package Object is

   type Instance is abstract tagged private;

   function Distance (O : Instance'Class) return Float;

   function Weight (O : Instance'Class; Coef : Float) return Float;
   --  returns Area * Coef

   function Area (O : Instance) return Float is abstract;

private

   type Instance is abstract tagged
      record
         X_Coord : Float;
         Y_Coord : Float;
      end record;

end Object;
