with Object; use Object;

package Circle is

   type Instance is new Object.Instance with private;

private

   type Instance is new Object.Instance with
      record
         Radius : Float;
      end record;

   overriding
   function Area (C : Instance) return Float;

end Circle;
