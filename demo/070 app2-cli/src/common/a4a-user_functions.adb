
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Real_Time;

with A4A.Memory.MBTCP_IOServer;
with A4A.Memory.Hilscher_Fieldbus1;
use A4A.Memory;
with A4A.Library.Conversion; use A4A.Library.Conversion;

with A4A.Log;
with A4A.HilscherX.Fieldbus1; use A4A.HilscherX;

with A4A.User_Objects; use A4A.User_Objects;

package body A4A.User_Functions is

   --------------------------------------------------------------------
   --  User functions
   --------------------------------------------------------------------

   procedure Map_Inputs is
   begin

      null;

   end Map_Inputs;

   procedure Map_Outputs is
   begin

      null;

   end Map_Outputs;

   procedure Map_HMI_Inputs is
   begin

      null;

   end Map_HMI_Inputs;

   procedure Map_HMI_Outputs is
   begin

      Bytes_To_Word (LSB_Byte => Hilscher_Fieldbus1.Inputs (1),
                     MSB_Byte => Hilscher_Fieldbus1.Inputs (0),
                     Word_out => MBTCP_IOServer.Input_Registers (1));

      Bytes_To_Word (LSB_Byte => Hilscher_Fieldbus1.Inputs (3),
                     MSB_Byte => Hilscher_Fieldbus1.Inputs (2),
                     Word_out => MBTCP_IOServer.Input_Registers (0));

      Bytes_To_Word (LSB_Byte => Hilscher_Fieldbus1.Inputs (4),
                     MSB_Byte => 0,
                     Word_out => MBTCP_IOServer.Input_Registers (2));

   end Map_HMI_Outputs;

   procedure Initialise is
   begin

      Initiate_FB.Initialise
        (Channel_Access => Fieldbus1.FB_Channel_Messaging'Access);

      Abort_FB.Initialise
        (Channel_Access => Fieldbus1.FB_Channel_Messaging'Access);

      Read_FB.Initialise
        (Channel_Access => Fieldbus1.FB_Channel_Messaging'Access);

      DPV1_FB.Initialise
        (Channel_Access => Fieldbus1.FB_Channel_Messaging'Access,
         DPV1C2_Closed_Access => DPV1C2_Closed_FB'Access);

   end Initialise;

   procedure Close is
   begin

      DPV1_FB.Quit;

      --  Wait for completion
      loop

         exit when DPV1_FB.Is_Terminated;
         delay 1.0;

      end loop;

   end Close;

   procedure PROFIBUS_DPM_DPV1C2 is

      My_Ident : constant String := "PROFIBUS_DPM_DPV1C2";

   begin

      case Status is

         when X00 => -- Initial state

            if DPV1_FB.Is_Ready then

               Initiate_FB.Set_Parameters
                 (Remote_Address => Remote_Address);

               Status := X01;

            end if;

         when X01 => -- Initiating DPV1C2 connection

            if Initiate_Done then

               if Initiate_Error  then

                  A4A.Log.Logger.Put
                    (Who  => My_Ident,
                     What => "Connection failed");

                  Status := X05;

               else

                  A4A.Log.Logger.Put
                    (Who  => My_Ident,
                     What => "Connected");

                  Initiate_Cnf_Pos_Data := Initiate_FB.Get_Data_Pos;

                  Read_FB.Set_Parameters
                    (Com_Ref => Initiate_Cnf_Pos_Data.Com_Ref,
                     Slot    => Slot,
                     Index   => Index,
                     Length  => Length);

                  Status := X02;

               end if;
            end if;

         when X02 => -- Reading data from slave

            if Read_Done then

               if Read_Error  then

                  A4A.Log.Logger.Put
                    (Who  => My_Ident,
                     What => "Read failed");

               else

                  A4A.Log.Logger.Put
                    (Who  => My_Ident,
                     What => "Data Read");

                  Read_Cnf_Pos_Data := Read_FB.Get_Data_Pos;

                  for I in Software_Revision'Range loop
                     exit when Read_Cnf_Pos_Data.Data (I) = 0;
                     Software_Revision (I) :=
                       Character'Val (Read_Cnf_Pos_Data.Data (I));
                  end loop;

                  A4A.Log.Logger.Put
                    (Who  => My_Ident,
                     What => "Software Revision : " & Software_Revision);

               end if;

               Abort_FB.Set_Parameters
                 (Com_Ref => Initiate_Cnf_Pos_Data.Com_Ref);

               Status := X03;

            end if;

         when X03 => -- Aborting connection

            if Abort_Done then

               if Abort_Error  then

                  A4A.Log.Logger.Put
                    (Who  => My_Ident,
                     What => "Connection Abortion failed");

                  Status := X05;

               else

                  A4A.Log.Logger.Put
                    (Who  => My_Ident,
                     What => "Connection Aborted");

                  Abort_Cnf_Data := Abort_FB.Get_Data;

                  Status := X04;

               end if;

            end if;

         when X04 => -- Waiting for Closed Indication

            if Closed_Indication_Got then

               Closed_Indication_Data := DPV1C2_Closed_FB.Get_Data;

               if Closed_Indication_Data.CRef =
                 Initiate_Cnf_Pos_Data.Com_Ref
               then

                  DPV1C2_Closed_FB.Answer;
                  Status := X05;

               end if;

            end if;

         when X05 => -- Terminating

            null;

      end case;

      Do_Initiate := (Status = X01);

      Initiate_FB.Cyclic
        (Do_Command     => Do_Initiate,
         Done           => Initiate_Done,
         Error          => Initiate_Error);

      Do_Read := (Status = X02);

      Read_FB.Cyclic
        (Do_Command     => Do_Read,
         Done           => Read_Done,
         Error          => Read_Error);

      Do_Abort := (Status = X03);

      Abort_FB.Cyclic
        (Do_Command     => Do_Abort,
         Done           => Abort_Done,
         Error          => Abort_Error);

      DPV1C2_Closed_FB.Cyclic
        (Got_Indication => Closed_Indication_Got);

   end PROFIBUS_DPM_DPV1C2;

   procedure EH_Level is

      My_Ident : constant String := "EH_Level";

      Word_0 : Word := 0;
      Word_1 : Word := 0;

      DWord_0 : DWord := 0;

      Elapsed_TON_1 : Ada.Real_Time.Time_Span;
   begin

      Bytes_To_Word
        (LSB_Byte => Hilscher_Fieldbus1.Inputs (1),
         MSB_Byte => Hilscher_Fieldbus1.Inputs (0),
         Word_out => Word_0);

      Bytes_To_Word
        (LSB_Byte => Hilscher_Fieldbus1.Inputs (3),
         MSB_Byte => Hilscher_Fieldbus1.Inputs (2),
         Word_out => Word_1);

      Words_To_DWord
        (LSW_Word  => Word_1,
         MSW_Word  => Word_0,
         DWord_out => DWord_0);

      LT_Main_Value := DWord_To_Float (DWord_0);

      Tempo_TON_1.Cyclic (Start   => not TON_1_Q,
                          Preset  => Ada.Real_Time.Milliseconds (10000),
                          Elapsed => Elapsed_TON_1,
                          Q       => TON_1_Q);

      if TON_1_Q then

         A4A.Log.Logger.Put
           (Who  => My_Ident,
            What => "LT_Main_Value = " & LT_Main_Value'Img & "%");

      end if;

   end EH_Level;

end A4A.User_Functions;
