
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with A4A.MBRTU_Master; use A4A.MBRTU_Master;
with A4A.Protocols; use A4A.Protocols.Device_Strings;
with A4A.Protocols.LibModbus; use A4A.Protocols.LibModbus;

--  tag::config[]
package A4A.Application.MBRTU_Master_Config is

   --------------------------------------------------------------------
   --  Modbus RTU Master configuration
   --------------------------------------------------------------------

   Config : aliased Master_Configuration :=
     (Command_Number    => 9,

      Enabled           => True,
      Debug_On          => False,
      Task_Period_MS    => 10,
      Retries           => 3,
      Timeout           => 0.2,

      Device            => To_Bounded_String ("/dev/ttyS0"),
      --  "COM1" on Windows
      --  "/dev/ttyS0" or "/dev/ttyUSB0" on Linux

      Baud_Rate         => BR_115200,
      Parity            => Even,
      Data_Bits         => 8,
      Stop_Bits         => 1,

      Commands =>
        (
         --                           Period                      Offset Offset
         --           Action  Enabled Multiple Shift Slave Number Remote Local
         1 =>
           (Read_Input_Registers, True,    10,    0,    2,    10,     0,    0),
         2 =>
           (Read_Registers,       True,    20,    0,    2,    10,     0,   10),
         3 =>
           (Write_Registers,      True,    30,    0,    2,    20,    20,    0),
         4 =>
           (Read_Bits,            True,    30,    1,    2,    16,     0,   16),
         5 =>
           (Read_Input_Bits,      True,    30,    2,    2,    16,     0,    0),
         6 =>
           (Write_Register,       True,    30,    3,    2,     1,    50,   30),
         7 =>
           (Write_Bit,            True,    30,    4,    2,     1,     0,    0),
         8 =>
           (Write_Bits,           True,    30,    5,    2,    15,     1,    1),

         9 => (Action              => Write_Read_Registers,
               Enabled             => False,
               Period_Multiple     => 10,
               Shift               =>  5,
               Slave               =>  2,
               Write_Number        => 10,
               Write_Offset_Remote => 40,
               Write_Offset_Local  => 20,
               Read_Number         => 10,
               Read_Offset_Remote  => 10,
               Read_Offset_Local   => 20),

         10 =>
           (Read_Input_Registers, False,   10,    0,    3,    10,     0,    0),
         11 =>
           (Read_Registers,       False,   20,    0,    3,    10,     0,   10),
         12 =>
           (Write_Registers,      False,   30,    0,    3,    20,    20,    0),
         13 =>
           (Read_Bits,            False,   30,    1,    3,    16,     0,   16),
         14 =>
           (Read_Input_Bits,      False,   30,    2,    3,    16,     0,    0),
         15 =>
           (Write_Register,       False,   30,    3,    3,     1,    50,   30),
         16 =>
           (Write_Bit,            False,   30,    4,    3,     1,     0,    0),
         17 =>
           (Write_Bits,           False,   30,    5,    3,    15,     1,    1),
        )
     );

end A4A.Application.MBRTU_Master_Config;
--  end::config[]
