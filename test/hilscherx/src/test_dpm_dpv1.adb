
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Exceptions; use Ada.Exceptions;

with A4A; use A4A;

with A4A.Log;

with A4A.Protocols.HilscherX.cifX_Errors;
use A4A.Protocols.HilscherX.cifX_Errors;

with A4A.Protocols.HilscherX.Driver;
with A4A.Protocols.HilscherX.cifX_User;

with A4A.Protocols.HilscherX.Channel_Messaging;
use A4A.Protocols.HilscherX.Channel_Messaging;

with A4A.Protocols.HilscherX.Profibus_DPM.DPV1;

with A4A.Protocols.HilscherX.Profibus_DPM.DPV1C2.Initiate;
with A4A.Protocols.HilscherX.Profibus_DPM.DPV1C2.Abort_Req;
with A4A.Protocols.HilscherX.Profibus_DPM.DPV1C2.Read;
with A4A.Protocols.HilscherX.Profibus_DPM.DPV1C2.Closed;

with A4A.Protocols.HilscherX.Request_FB.Profibus_DPM.DPV1C2.Initiate_FB;
with A4A.Protocols.HilscherX.Request_FB.Profibus_DPM.DPV1C2.Abort_FB;
with A4A.Protocols.HilscherX.Request_FB.Profibus_DPM.DPV1C2.Read_FB;
with A4A.Protocols.HilscherX.Indication_FB.Profibus_DPM.DPV1C2.Closed_FB;

use A4A.Protocols.HilscherX.Profibus_DPM;

procedure Test_DPM_DPV1 is

   My_Ident : constant String := "Test_Profibus_DPM_DPV1";

   package cifX renames A4A.Protocols.HilscherX.cifX_User;

   package DPV1C2_Ind_FB renames
     A4A.Protocols.HilscherX.Indication_FB.Profibus_DPM.DPV1C2;

   package DPV1C2_Req_FB renames
     A4A.Protocols.HilscherX.Request_FB.Profibus_DPM.DPV1C2;

   My_Board : constant String := "DPM";
--     My_Board : String := "cifX1";

   Driver_Handle  : aliased cifX.Driver_Handle_Type;
   Channel_Handle : aliased cifX.Channel_Handle_Type;

   Result : DInt;

   cifX_Driver_Init_Done  : Boolean := False;
   cifX_Driver_Open_Done  : Boolean := False;
   cifX_Channel_Open_Done : Boolean := False;

   My_Channel_Messaging : aliased
     A4A.Protocols.HilscherX.Channel_Messaging.Instance;

   DPV1_FB : DPV1.Instance;

   Initiate_FB : DPV1C2_Req_FB.Initiate_FB.Instance;

   Remote_Address : constant DWord := 2;

   Do_Initiate    : Boolean := False;
   Initiate_Done  : Boolean := False;
   Initiate_Error : Boolean := False;

   Initiate_Cnf_Pos_Data  : DPV1C2.Initiate.PROFIBUS_FSPMM2_INITIATE_CNF_POS_T;

   Abort_FB : DPV1C2_Req_FB.Abort_FB.Instance;

   Do_Abort    : Boolean := False;
   Abort_Done  : Boolean := False;
   Abort_Error : Boolean := False;

   Abort_Cnf_Data  : DPV1C2.Abort_Req.PROFIBUS_FSPMM2_ABORT_CNF_T;
   pragma Unreferenced (Abort_Cnf_Data);

   Read_FB : DPV1C2_Req_FB.Read_FB.Instance;

   --  Software revision from E+H Level Meter
   Slot   : constant DWord :=  1;
   Index  : constant DWord := 73;
   Length : constant DWord := 16;

   Do_Read    : Boolean := False;
   Read_Done  : Boolean := False;
   Read_Error : Boolean := False;

   Read_Cnf_Pos_Data  : DPV1C2.Read.PROFIBUS_FSPMM2_READ_CNF_POS_T;

   Software_Revision : String (1 .. 16);

   DPV1C2_Closed_FB : aliased DPV1C2_Ind_FB.Closed_FB.Instance;

   Closed_Indication_Got : Boolean := False;

   Closed_Indication_Data : DPV1C2.Closed.PROFIBUS_FSPMM2_CLOSED_IND_T;

   type Block_Status is
     (X00,
      --  Initial

      X01,
      --  Initiate DPV1C2 connection

      X02,
      --  Read data from slave

      X03,
      --  Abort connection

      X04,
      --  Wait for Closed Indication

      X05
      --  Terminate
     );

   Status : Block_Status := X00;

   procedure cifX_Show_Error (Error : DInt);

   procedure cifX_Show_Error (Error : DInt) is
   begin
      A4A.Log.Logger.Put
        (Who  => My_Ident,
         What => cifX.Driver_Get_Error_Description (Error));
   end cifX_Show_Error;

   procedure Close;

   procedure Close is
   begin

      DPV1_FB.Quit;

      --  Wait for completion
      loop

         exit when DPV1_FB.Is_Terminated;
         delay 1.0;

      end loop;

      My_Channel_Messaging.Quit;

      --  Wait for completion
      loop

         exit when My_Channel_Messaging.Is_Terminated;
         delay 1.0;

      end loop;

      if cifX_Channel_Open_Done then
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Closing Channel");
         Result := cifX.Channel_Close (Channel_Handle);
         if Result /= CIFX_NO_ERROR then
            cifX_Show_Error (Result);
         end if;
         cifX_Channel_Open_Done := False;
      end if;

      if cifX_Driver_Open_Done then
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Closing Driver");
         Result := cifX.Driver_Close (Driver_Handle);
         if Result /= CIFX_NO_ERROR then
            cifX_Show_Error (Result);
         end if;
         cifX_Driver_Open_Done := False;
      end if;

      if cifX_Driver_Init_Done then
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Deinitializing cifX Driver");
         A4A.Protocols.HilscherX.Driver.Driver_Deinit;
         cifX_Driver_Init_Done := False;
      end if;

      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => "Finished !");
      A4A.Log.Quit;

   end Close;

begin

   A4A.Log.Logger.Put (Who  => My_Ident,
                       What => "Test_Messaging...");

   A4A.Log.Logger.Put (Who  => My_Ident,
                       What => "Initializing cifX Driver");
   Result := A4A.Protocols.HilscherX.Driver.Driver_Init;
   if Result /= CIFX_NO_ERROR then
      cifX_Show_Error (Result);
   else
      cifX_Driver_Init_Done := True;
   end if;

   if cifX_Driver_Init_Done then

      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => "Opening Driver");
      Result := cifX.Driver_Open (Driver_Handle'Access);
      if Result /= CIFX_NO_ERROR then
         cifX_Show_Error (Result);
      else
         cifX_Driver_Open_Done := True;
      end if;

   end if;

   if cifX_Driver_Open_Done then

      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => "Opening Channel");
      Result := cifX.Channel_Open
        (Driver_Handle          => Driver_Handle,
         Board_Name             => My_Board,
         Channel_Number         => 0,
         Channel_Handle_Access  => Channel_Handle'Access);

      if Result /= CIFX_NO_ERROR then
         cifX_Show_Error (Result);
      else
         cifX_Channel_Open_Done := True;
      end if;

   end if;

   if cifX_Channel_Open_Done then

      My_Channel_Messaging.Initialise (Channel_Handle => Channel_Handle);

      Initiate_FB.Initialise
        (Channel_Access => My_Channel_Messaging'Unrestricted_Access);

      Abort_FB.Initialise
        (Channel_Access => My_Channel_Messaging'Unrestricted_Access);

      Read_FB.Initialise
        (Channel_Access => My_Channel_Messaging'Unrestricted_Access);

      DPV1_FB.Initialise
        (Channel_Access => My_Channel_Messaging'Unrestricted_Access,
         DPV1C2_Closed_Access => DPV1C2_Closed_FB'Unrestricted_Access);

      loop

         case Status is

            when X00 => -- Initial state

               if DPV1_FB.Is_Ready then

                  Initiate_FB.Set_Parameters
                    (Remote_Address => Remote_Address);

                  Status := X01;

               end if;

            when X01 => -- Initiating DPV1C2 connection

               if Initiate_Done then

                  if Initiate_Error  then

                     A4A.Log.Logger.Put
                       (Who  => My_Ident,
                        What => "Connection failed");

                     Status := X05;

                  else

                     A4A.Log.Logger.Put
                       (Who  => My_Ident,
                        What => "Connected");

                     Initiate_Cnf_Pos_Data := Initiate_FB.Get_Data_Pos;

                     Read_FB.Set_Parameters
                       (Com_Ref => Initiate_Cnf_Pos_Data.Com_Ref,
                        Slot    => Slot,
                        Index   => Index,
                        Length  => Length);

                     Status := X02;

                  end if;
               end if;

            when X02 => -- Reading data from slave

               if Read_Done then

                  if Read_Error  then

                     A4A.Log.Logger.Put
                       (Who  => My_Ident,
                        What => "Read failed");

                  else

                     A4A.Log.Logger.Put
                       (Who  => My_Ident,
                        What => "Data Read");

                     Read_Cnf_Pos_Data := Read_FB.Get_Data_Pos;

                     for I in Software_Revision'Range loop
                        exit when Read_Cnf_Pos_Data.Data (I) = 0;
                        Software_Revision (I) :=
                          Character'Val (Read_Cnf_Pos_Data.Data (I));
                     end loop;

                     A4A.Log.Logger.Put
                       (Who  => My_Ident,
                        What => "Software Revision : " & Software_Revision);

                  end if;

                  Abort_FB.Set_Parameters
                    (Com_Ref => Initiate_Cnf_Pos_Data.Com_Ref);

                  Status := X03;

               end if;

            when X03 => -- Aborting connection

               if Abort_Done then

                  if Abort_Error  then

                     A4A.Log.Logger.Put
                       (Who  => My_Ident,
                        What => "Connection Abortion failed");

                     Status := X05;

                  else

                     A4A.Log.Logger.Put
                       (Who  => My_Ident,
                        What => "Connection Aborted");

                     Abort_Cnf_Data := Abort_FB.Get_Data;

                     Status := X04;

                  end if;

               end if;

            when X04 => -- Waiting for Closed Indication

               if Closed_Indication_Got then

                  Closed_Indication_Data := DPV1C2_Closed_FB.Get_Data;

                  if Closed_Indication_Data.CRef =
                    Initiate_Cnf_Pos_Data.Com_Ref
                  then

                     DPV1C2_Closed_FB.Answer;
                     Status := X05;

                  end if;

               end if;

            when X05 => -- Terminating

               null;

         end case;

         Do_Initiate := (Status = X01);

         Initiate_FB.Cyclic
           (Do_Command     => Do_Initiate,
            Done           => Initiate_Done,
            Error          => Initiate_Error);

         Do_Read := (Status = X02);

         Read_FB.Cyclic
           (Do_Command     => Do_Read,
            Done           => Read_Done,
            Error          => Read_Error);

         Do_Abort := (Status = X03);

         Abort_FB.Cyclic
           (Do_Command     => Do_Abort,
            Done           => Abort_Done,
            Error          => Abort_Error);

         DPV1C2_Closed_FB.Cyclic
           (Got_Indication => Closed_Indication_Got);

         exit when
           ((Status = X05) and DPV1C2_Closed_FB.Is_Ready)
           or DPV1_FB.Is_Faulty;

      end loop;

      --  Let's see if other indications get caught
      delay 5.0;

   end if;

   Close;

exception

   when Error : others =>
      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => Exception_Information (Error));

      Close;

end Test_DPM_DPV1;
