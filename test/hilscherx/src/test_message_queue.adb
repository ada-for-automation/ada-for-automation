
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Text_IO;

with Ada.Exceptions; use Ada.Exceptions;

with Ada.Unchecked_Conversion;

with A4A; use A4A;

with A4A.Protocols.HilscherX.cifX_User;

with A4A.Protocols.HilscherX.Message_Queue;
with A4A.Protocols.HilscherX.Message_Pool;

--  Test with Pool size of 5
--  => Runs out of packets from pool but finishes fine

--  Test with Pool size of 20
--  => Fills up the queue but finishes fine too

--  Test with Queue size of 1 works fine

procedure Test_Message_Queue is

   package cifX renames A4A.Protocols.HilscherX.cifX_User;

   package Pool_Package is new
     A4A.Protocols.HilscherX.Message_Pool (Pool_Size => 20);

   Message_Pool : Pool_Package.Message_Pool_Type;

   package Queue_Package is new
     A4A.Protocols.HilscherX.Message_Queue (Queue_Size => 10);

   Message_Queue : aliased Queue_Package.Message_Queue_Type;

   My_Message_Queue_Access : Queue_Package.Message_Queue_Access;
   My_Message_Queue_Access_LWord : LWord;
   LWord_Img_Buffer : String (1 .. 30);

   package LWord_Text_IO is
     new Ada.Text_IO.Modular_IO (LWord);

   function From_Message_Queue is new Ada.Unchecked_Conversion
     (Source => Queue_Package.Message_Queue_Access,
      Target => LWord);

   Request      : cifX.cifX_Packet_Access;
--     Confirmation : cifX.cifX_Packet_Access;

   Packet_Got : Boolean := False;
   Packet_Put : Boolean := False;

   Request_Id : DWord := 0;

   task Send;

   task body Send is
      Request : cifX.cifX_Packet_Access;

      Quit_Flag : Boolean := False;
   begin

      loop
         Message_Queue.Get (Item => Request);

         Ada.Text_IO.Put_Line (Item => Request.Header.Id'Img & "Sent.");

         Quit_Flag := Request.Header.Id > 30;

         Message_Pool.Return_Packet (Item => Request);

         delay 0.2;

         exit when Quit_Flag;

      end loop;

   end Send;

begin

   Ada.Text_IO.Put_Line (Item => "Test_Message_Queue...");

   My_Message_Queue_Access := Message_Queue'Access;
   My_Message_Queue_Access_LWord :=
     From_Message_Queue (My_Message_Queue_Access);

   LWord_Text_IO.Put (To   => LWord_Img_Buffer,
                      Item => My_Message_Queue_Access_LWord,
                      Base => 16);

   Ada.Text_IO.Put_Line
     (Item =>  "My_Message_Queue_Access : " & LWord_Img_Buffer);

   Message_Pool.Initialise;

   loop

      if not Packet_Got then

         Message_Pool.Get_Packet
           (Item   => Request,
            Result => Packet_Got);

      end if;

      if not Packet_Got then

         Ada.Text_IO.Put_Line
           ("Could not get Request packet from pool...");

      else

         Message_Queue.Put
           (Item   => Request,
            Result => Packet_Put);

         Request.Header.Id := Request_Id;

         if not Packet_Put then

            Ada.Text_IO.Put_Line
              ("Could not put Request packet in queue...");

         else

            Ada.Text_IO.Put_Line (Item => Request.Header.Id'Img & "Queued.");

            Request_Id := Request_Id + 1;

            Packet_Got := False;

         end if;

      end if;

      delay 0.1;

      exit when Request_Id > 31;

   end loop;

exception

   when Error : others =>
      Ada.Text_IO.Put_Line (Exception_Information (Error));

end Test_Message_Queue;
