
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Interfaces.C;

with Ada.Exceptions; use Ada.Exceptions;

with A4A; use A4A;

with A4A.Log;

with A4A.Protocols.HilscherX.cifX_Errors;
use A4A.Protocols.HilscherX.cifX_Errors;

with A4A.Protocols.HilscherX.Driver;
with A4A.Protocols.HilscherX.cifX_User;

with A4A.Protocols.HilscherX.rcX_Public.rcX_Firmware_Identify;
use A4A.Protocols.HilscherX.rcX_Public.rcX_Firmware_Identify;

with A4A.Protocols.HilscherX.Packet_Queue;
use A4A.Protocols.HilscherX.Packet_Queue;

with A4A.Protocols.HilscherX.Channel_Messaging;
use A4A.Protocols.HilscherX.Channel_Messaging;

procedure Test_Channel_Messaging is

   My_Ident : constant String := "Test_Channel_Messaging";

   package cifX renames A4A.Protocols.HilscherX.cifX_User;

   Driver_Handle  : aliased cifX.Driver_Handle_Type;
   Channel_Handle : aliased cifX.Channel_Handle_Type;

   Result : DInt;

   Request      : cifX.cifX_Packet_Access;
   Confirmation : cifX.cifX_Packet_Access;

   cifX_Driver_Init_Done  : Boolean := False;
   cifX_Driver_Open_Done  : Boolean := False;
   cifX_Channel_Open_Done : Boolean := False;

   Fw_Ident_Req : RCX_FIRMWARE_IDENTIFY_REQ_T_Access;
   Fw_Ident_Cnf : RCX_FIRMWARE_IDENTIFY_CNF_T_Access;

   Packet_Got  : Boolean := False;
   Packet_Sent : Boolean := False;
   Answer_Got  : Boolean := False;

   My_Channel_Messaging : A4A.Protocols.HilscherX.Channel_Messaging.Instance;

   My_Receive_Queue : aliased Packet_Queue_Type;
   My_Receive_Queue_App : constant := A4A_App_0;
   My_Receive_Queue_ID  : DWord;

   Queue_ID_Got : Boolean := False;

   procedure cifX_Show_Error (Error : DInt);

   procedure cifX_Show_Error (Error : DInt) is
   begin
      A4A.Log.Logger.Put
        (Who  => My_Ident,
         What => cifX.Driver_Get_Error_Description (Error));
   end cifX_Show_Error;

   procedure cifX_Show_Firmware_Identification;

   procedure cifX_Show_Firmware_Identification is
      What : constant String := CRLF
        & "***********************************************" & CRLF
        & "          Firmware Identification" & CRLF & CRLF
        & "Firmware Version :" & CRLF
        & "Major    : "
        & Fw_Ident_Cnf.Data.Fw_Identification.Fw_Version.Major'Img & CRLF
        & "Minor    : "
        & Fw_Ident_Cnf.Data.Fw_Identification.Fw_Version.Minor'Img & CRLF
        & "Build    : "
        & Fw_Ident_Cnf.Data.Fw_Identification.Fw_Version.Build'Img & CRLF
        & "Revision : "
        & Fw_Ident_Cnf.Data.Fw_Identification.Fw_Version.Revision'Img & CRLF
        & CRLF
        & "Firmware Name : "
        & Interfaces.C.To_Ada
        (Fw_Ident_Cnf.Data.Fw_Identification.Fw_Name.Name) & CRLF
        & CRLF
        & "Firmware Date :" & CRLF
        & "Year  : "
        & Fw_Ident_Cnf.Data.Fw_Identification.Fw_Date.Year'Img & CRLF
        & "Month : "
        & Fw_Ident_Cnf.Data.Fw_Identification.Fw_Date.Month'Img & CRLF
        & "Day   : "
        & Fw_Ident_Cnf.Data.Fw_Identification.Fw_Date.Day'Img & CRLF
        & "***********************************************" & CRLF;
   begin
      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => What);
   end cifX_Show_Firmware_Identification;

   procedure Close;

   procedure Close is
   begin

      My_Channel_Messaging.Quit;

      --  Wait for completion
      loop

         exit when My_Channel_Messaging.Is_Terminated;
         delay 1.0;

      end loop;

      if cifX_Channel_Open_Done then
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Closing Channel");
         Result := cifX.Channel_Close (Channel_Handle);
         if Result /= CIFX_NO_ERROR then
            cifX_Show_Error (Result);
         end if;
         cifX_Channel_Open_Done := False;
      end if;

      if cifX_Driver_Open_Done then
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Closing Driver");
         Result := cifX.Driver_Close (Driver_Handle);
         if Result /= CIFX_NO_ERROR then
            cifX_Show_Error (Result);
         end if;
         cifX_Driver_Open_Done := False;
      end if;

      if cifX_Driver_Init_Done then
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Deinitializing cifX Driver");
         A4A.Protocols.HilscherX.Driver.Driver_Deinit;
         cifX_Driver_Init_Done := False;
      end if;

      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => "finished !");
      A4A.Log.Quit;

   end Close;

begin

   A4A.Log.Logger.Put (Who  => My_Ident,
                       What => "Test_Messaging...");

   A4A.Log.Logger.Put (Who  => My_Ident,
                       What => "Initializing cifX Driver");
   Result := A4A.Protocols.HilscherX.Driver.Driver_Init;
   if Result /= CIFX_NO_ERROR then
      cifX_Show_Error (Result);
   else
      cifX_Driver_Init_Done := True;
   end if;

   if cifX_Driver_Init_Done then

      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => "Opening Driver");
      Result := cifX.Driver_Open (Driver_Handle'Access);
      if Result /= CIFX_NO_ERROR then
         cifX_Show_Error (Result);
      else
         cifX_Driver_Open_Done := True;
      end if;

   end if;

   if cifX_Driver_Open_Done then

      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => "Opening Channel");
      Result := cifX.Channel_Open
        (Driver_Handle          => Driver_Handle,
         Board_Name             => "cifX0",
         Channel_Number         => 0,
         Channel_Handle_Access  => Channel_Handle'Access);

      if Result /= CIFX_NO_ERROR then
         cifX_Show_Error (Result);
      else
         cifX_Channel_Open_Done := True;
      end if;

   end if;

   if cifX_Channel_Open_Done then

      My_Channel_Messaging.Initialise (Channel_Handle => Channel_Handle);

   end if;

   if cifX_Channel_Open_Done then

      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => "Test with default receive queue+++");
      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => "Sending Request");

      My_Channel_Messaging.Get_Packet
        (Item   => Request,
         Result => Packet_Got);

      if not Packet_Got then

         A4A.Log.Logger.Put (Who  => My_Ident,
                             What =>
                               "Could not get Request packet from pool...");

      else

         Fw_Ident_Req := From_cifX_Packet (Request);

         Fw_Ident_Req.Head.Dest    := 16#20#;
         Fw_Ident_Req.Head.Src     := 0;
         Fw_Ident_Req.Head.Dest_Id := 0;
         Fw_Ident_Req.Head.Src_Id  := 0;
         Fw_Ident_Req.Head.Len     := 4;
         Fw_Ident_Req.Head.Id      := 1;
         Fw_Ident_Req.Head.State   := 0;
         Fw_Ident_Req.Head.Cmd     := RCX_FIRMWARE_IDENTIFY_REQ;
         Fw_Ident_Req.Head.Ext     := 0;
         Fw_Ident_Req.Head.Rout    := 0;

         Fw_Ident_Req.Data.Channel_Id := 0;

         My_Channel_Messaging.Send
           (Item   => Request,
            Result => Packet_Sent);

         if not Packet_Sent then

            A4A.Log.Logger.Put (Who  => My_Ident,
                                What =>
                                  "Could not put Request packet in queue...");

            My_Channel_Messaging.Return_Packet (Item => Request);

         else

            delay 3.0;

            My_Channel_Messaging.Receive
              (Item   => Confirmation,
               Result => Answer_Got);

            if Answer_Got then

               Fw_Ident_Cnf := From_cifX_Packet (Confirmation);

               cifX_Show_Firmware_Identification;

               My_Channel_Messaging.Return_Packet (Item => Confirmation);

            else

               A4A.Log.Logger.Put (Who  => My_Ident,
                                   What => "Got no answer...");

            end if;

         end if;

      end if;

   end if;

   Packet_Got  := False;
   Packet_Sent := False;
   Answer_Got  := False;

   My_Channel_Messaging.Put
     (Item   => My_Receive_Queue'Unrestricted_Access,
      Index  => My_Receive_Queue_ID,
      Result => Queue_ID_Got);

   if not Queue_ID_Got then
      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => "Could not Register my Queue !");
   end if;

   if cifX_Channel_Open_Done and Queue_ID_Got then

      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => "Test with own receive queue+++");
      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => "Sending Request");

      My_Channel_Messaging.Get_Packet
        (Item   => Request,
         Result => Packet_Got);

      if not Packet_Got then

         A4A.Log.Logger.Put (Who  => My_Ident,
                             What =>
                               "Could not get Request packet from pool...");

      else

         Fw_Ident_Req := From_cifX_Packet (Request);

         Fw_Ident_Req.Head.Dest    := 16#20#;
         Fw_Ident_Req.Head.Src     := My_Receive_Queue_App;
         Fw_Ident_Req.Head.Dest_Id := 0;

         Fw_Ident_Req.Head.Src_Id  := My_Receive_Queue_ID;

         Fw_Ident_Req.Head.Len     := 4;
         Fw_Ident_Req.Head.Id      := 1;
         Fw_Ident_Req.Head.State   := 0;
         Fw_Ident_Req.Head.Cmd     := RCX_FIRMWARE_IDENTIFY_REQ;
         Fw_Ident_Req.Head.Ext     := 0;
         Fw_Ident_Req.Head.Rout    := 0;

         Fw_Ident_Req.Data.Channel_Id := 0;

         My_Channel_Messaging.Send
           (Item   => Request,
            Result => Packet_Sent);

         if not Packet_Sent then

            A4A.Log.Logger.Put (Who  => My_Ident,
                                What =>
                                  "Could not put Request packet in queue...");

            My_Channel_Messaging.Return_Packet (Item => Request);

         else

            delay 3.0;

            My_Receive_Queue.Get
              (Item   => Confirmation,
               Result => Answer_Got);

            if Answer_Got then

               Fw_Ident_Cnf := From_cifX_Packet (Confirmation);

               cifX_Show_Firmware_Identification;

               My_Channel_Messaging.Return_Packet (Item => Confirmation);

            else

               A4A.Log.Logger.Put (Who  => My_Ident,
                                   What => "Got no answer...");

            end if;

         end if;

      end if;

   end if;

   Close;

exception

   when Error : others =>
      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => Exception_Information (Error));

      Close;

end Test_Channel_Messaging;
