
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Text_IO;

with Ada.Exceptions; use Ada.Exceptions;

with Ada.Unchecked_Conversion;

with A4A; use A4A;

with A4A.Protocols.HilscherX.cifX_User;

with A4A.Protocols.HilscherX.Packet_Pool;
use A4A.Protocols.HilscherX.Packet_Pool;

use A4A.Protocols.HilscherX;

with A4A.Protocols.HilscherX.Packet_Queue;
use A4A.Protocols.HilscherX.Packet_Queue;

with A4A.Protocols.HilscherX.Packet_Queue_Map;
use A4A.Protocols.HilscherX.Packet_Queue_Map;

--  Test with Pool size of 5
--  => Runs out of packets from pool but finishes fine

--  Test with Pool size of 20
--  => Fills up the queue but finishes fine too

--  Test with Queue size of 1 works fine

procedure Test_Packet_Queue is

   package cifX renames cifX_User;

   --  package Pool_Package is new Message_Pool (Pool_Size => 5);

   The_Packet_Pool  : Packet_Pool_Type (Pool_Size => 20);

   --  My_Packet_Queue : aliased Packet_Queue_Type;
   --  Default size is 1

   My_Packet_Queue : aliased Packet_Queue_Type (Queue_Size => 2);

   My_Packet_Queue_Access : Packet_Queue_Access;

   My_Packet_Queue_Access_LWord : LWord;

   LWord_Img_Buffer : String (1 .. 30);

   package LWord_Text_IO is
     new Ada.Text_IO.Modular_IO (LWord);

   function From_Packet_Queue is new Ada.Unchecked_Conversion
     (Source => Packet_Queue_Access,
      Target => LWord);

   My_Packet_Queue_Map : aliased Packet_Queue_Map_Type;
   --  Default size is 100

   Request      : cifX.cifX_Packet_Access;
   --  Confirmation : cifX.cifX_Packet_Access;

   Packet_Got : Boolean := False;
   Packet_Put : Boolean := False;

   Source_Id_Got : Boolean := False;

   Source_Id  : DWord := 0;
   Request_Id : DWord := 0;

   task Send;

   task body Send is
      Request : cifX.cifX_Packet_Access;

      Quit_Flag : Boolean := False;

      Queue_Got : Boolean := False;

      The_Packet_Queue_Access : Packet_Queue_Access;

      The_Packet_Queue_Access_LWord : LWord;

      The_DWord_Img_Buffer : String (1 .. 30);
      The_LWord_Img_Buffer : String (1 .. 30);

   begin

      loop
         My_Packet_Queue.Get (Item => Request);

         DWord_Text_IO.Put (To   => The_DWord_Img_Buffer,
                            Item => Request.Header.Src,
                            Base => 16);

         Ada.Text_IO.Put_Line
           (Item =>
              "Packet received from" & CRLF
            & "Src : " & The_DWord_Img_Buffer & CRLF
            & "Src_Id : " & Request.Header.Src_Id'Img & CRLF
            & "Id : " & Request.Header.Id'Img);

         My_Packet_Queue_Map.Get (Index  => Request.Header.Src_Id,
                                  Item   => The_Packet_Queue_Access,
                                  Result => Queue_Got);

         if not Queue_Got then

            Ada.Text_IO.Put_Line
              ("Could not get Queue from Map...");

         else

            The_Packet_Queue_Access_LWord :=
              From_Packet_Queue (The_Packet_Queue_Access);

            LWord_Text_IO.Put (To   => The_LWord_Img_Buffer,
                               Item => The_Packet_Queue_Access_LWord,
                               Base => 16);

            Ada.Text_IO.Put_Line
              ("Got Queue "
               & "The_Packet_Queue_Access : " & The_LWord_Img_Buffer
               & " from Map...");

         end if;

         Ada.Text_IO.Put_Line (Item => Request.Header.Id'Img & "Sent.");

         Quit_Flag := Request.Header.Id > 30;

         The_Packet_Pool.Return_Packet (Item => Request);

         delay 0.2;

         exit when Quit_Flag;

      end loop;

   end Send;

begin

   Ada.Text_IO.Put_Line (Item => "Test_Message_Queue...");

   My_Packet_Queue_Access := My_Packet_Queue'Unrestricted_Access;
   My_Packet_Queue_Access_LWord :=
     From_Packet_Queue (My_Packet_Queue_Access);

   LWord_Text_IO.Put (To   => LWord_Img_Buffer,
                      Item => My_Packet_Queue_Access_LWord,
                      Base => 16);

   Ada.Text_IO.Put_Line
     (Item =>  "My_Packet_Queue_Access : " & LWord_Img_Buffer
     & " Access'size = " & My_Packet_Queue_Access'Size'Img);

   The_Packet_Pool.Initialise;

   My_Packet_Queue_Map.Initialise;

   My_Packet_Queue_Map.Put (Item   => My_Packet_Queue_Access,
                            Index  => Source_Id,
                            Result => Source_Id_Got);

   if not Source_Id_Got then

      Ada.Text_IO.Put_Line
        ("Could not get Source_Id from Map...");

   else

      Ada.Text_IO.Put_Line
        ("Got Source_Id " & Source_Id'Img & " from Map...");

   end if;

   loop

      if not Packet_Got then

         The_Packet_Pool.Get_Packet
           (Item   => Request,
            Result => Packet_Got);

      end if;

      if not Packet_Got then

         Ada.Text_IO.Put_Line
           ("Could not get Request packet from pool...");

      else

         Request.Header.Src    := 16#ADA00000#;
         Request.Header.Src_Id := Source_Id;

         Request.Header.Id     := Request_Id;

         My_Packet_Queue.Put
           (Item   => Request,
            Result => Packet_Put);

         if not Packet_Put then

            Ada.Text_IO.Put_Line
              ("Could not put Request packet in queue...");

         else

            Ada.Text_IO.Put_Line (Item => Request.Header.Id'Img & "Queued.");

            Request_Id := Request_Id + 1;

            Packet_Got := False;

         end if;

      end if;

      delay 0.1;

      exit when Request_Id > 31;

   end loop;

exception

   when Error : others =>
      Ada.Text_IO.Put_Line (Exception_Information (Error));

end Test_Packet_Queue;
