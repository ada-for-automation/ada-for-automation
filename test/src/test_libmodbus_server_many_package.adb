
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Interfaces.C; use Interfaces.C;

with Ada.Text_IO;
with Ada.Exceptions; use Ada.Exceptions;

with GNAT.Sockets; use GNAT.Sockets;

with GNAT.Ctrl_C; use GNAT.Ctrl_C;

with A4A; use A4A;
with A4A.Protocols.LibModbus;

package body Test_LibModbus_Server_Many_Package is

   Quit       : Boolean := False;

   procedure Ctrl_C is
   begin
      Quit := True;
   end Ctrl_C;

   procedure Test_LibModbus_Server_Many is

      package LibModbus renames A4A.Protocols.LibModbus;

      MyContext         : LibModbus.Context_Type;
      Result            : Interfaces.C.int;
      pragma Unreferenced (Result);
      Req_Len           : Interfaces.C.int;
      Request           : aliased Byte_Array :=
        (0 .. LibModbus.MODBUS_TCP_MAX_ADU_LENGTH => 0);

      Server_Socket     : LibModbus.Socket_Type;
      Master_Socket     : LibModbus.Socket_Type;

      refset            : Socket_Set_Type;
      rdset             : Socket_Set_Type;
      wtset             : Socket_Set_Type;

      Selector          : Selector_Type;
      Status            : Selector_Status;

--        MyMapping_Access  : System.Address;

      My_tab_bits2            : aliased Byte_Array := (0 .. 9 => 0);
      My_tab_input_bits2      : aliased Byte_Array := (0 .. 9 => 0);
      My_tab_input_registers2 : aliased Word_Array := (0 .. 9 => 0);
      My_tab_registers2       : aliased Word_Array := (0 .. 19 => 0);

      MyMapping2 : aliased constant LibModbus.Modbus_Mapping_Type :=
        (nb_bits                => My_tab_bits2'Length,
         start_bits             => 0,
         nb_input_bits          => My_tab_input_bits2'Length,
         start_input_bits       => 0,
         nb_input_registers     => My_tab_input_registers2'Length,
         start_input_registers  => 0,
         nb_registers           => My_tab_registers2'Length,
         start_registers        => 0,
         tab_bits               => My_tab_bits2'Address,
         tab_input_bits         => My_tab_input_bits2'Address,
         tab_input_registers    => My_tab_input_registers2'Address,
         tab_registers          => My_tab_registers2'Address
        );

      Loop1_Count       : Integer := 0;
      Loop2_Count       : Integer := 0;

      Time_Out          : constant := 10.0;     -- 1s

      Context_Ok : Boolean := False;
      Connect_Ok : Boolean := False;
--        Mapping_Ok : Boolean := False;

      procedure Close;
      procedure Close is
      begin
--           if Mapping_Ok then
--              LibModbus.Mapping_Free (Mapping_Access => MyMapping_Access);
--           end if;
         if Connect_Ok then
            LibModbus.Close (Context => MyContext);
         end if;
         if Context_Ok then
            LibModbus.Free (Context => MyContext);
         end if;

         Ada.Text_IO.Put (Item => "Closing gracefully.");
         Uninstall_Handler;
      end Close;

   begin
      Ada.Text_IO.Put (Item => "Test_LibModbus_Server_Many... Waiting");
      Ada.Text_IO.New_Line;

      Install_Handler (Ctrl_C'Access);

      for Index in Request'Range loop
         Request (Index) := 0;
      end loop;

      MyContext := LibModbus.New_TCP (IP_Address => "127.0.0.1",
                                      Port       => 1502);
      Context_Ok := True;

      LibModbus.Set_Debug (Context => MyContext, On => True);

--        MyMapping_Access := LibModbus.Mapping_New
--          (Nb_Coil_Status       => 10,
--           Nb_Input_Status      => 10,
--           Nb_Holding_Registers => 10,
--           Nb_Input_Registers   => 10);
--        Mapping_Ok := True;

--        declare
--           MyMapping : LibModbus.Modbus_Mapping_Type1;
--           for MyMapping'Address use MyMapping_Access;
--
--           My_tab_registers : Word_Array(0..9);
--           for My_tab_registers'Address use MyMapping.tab_registers;
--
--           My_tab_input_registers : Word_Array(0..9);
--        for My_tab_input_registers'Address
--        use MyMapping.tab_input_registers;

--        begin
--           My_tab_registers(0) := 16#0102#;
--           My_tab_registers(1) := 16#0304#;
--           My_tab_registers(2) := 16#0506#;
--           My_tab_registers(3) := 16#0708#;
--           My_tab_registers(4) := 16#090A#;
--           My_tab_registers(5) := 16#0B0C#;
--           My_tab_registers(6) := 16#FF0C#;
--           My_tab_registers(7) := 16#FF0B#;
--           My_tab_registers(8) := 16#FF0A#;
--           My_tab_registers(9) := 16#FFFF#;
--
--           My_tab_input_registers(0) := 16#A1A2#;
--           My_tab_input_registers(1) := 16#A3A4#;
--           My_tab_input_registers(2) := 16#A5A6#;
--           My_tab_input_registers(3) := 16#A7A8#;
--           My_tab_input_registers(4) := 16#A9AA#;
--           My_tab_input_registers(5) := 16#ABAC#;
--           My_tab_input_registers(6) := 16#FF0C#;
--           My_tab_input_registers(7) := 16#FF0B#;
--           My_tab_input_registers(8) := 16#FF0A#;
--           My_tab_input_registers(9) := 16#FFFF#;

         Server_Socket := LibModbus.TCP_Listen (Context       => MyContext,
                                                Nb_Connection => 2);
         --  Wait forever a Client connection
         Connect_Ok := True;

      Ada.Text_IO.Put_Line
        (Item => "TCP_Listen... Server_Socket = " & Image (Server_Socket));
         Empty (refset);

         Set (refset, Server_Socket);

         Create_Selector (Selector);

         loop

            Ada.Text_IO.Put_Line (Item => "Loop1_Count = " & Loop1_Count'Img);

            Copy (refset, rdset);

            Check_Selector (Selector, rdset, wtset, Status, Time_Out);

            case Status is
            when Completed =>

               Ada.Text_IO.Put_Line
                 (Item => "Check_Selector... Status = Completed");

               if Is_Set (rdset, Server_Socket) then
                  --  A client is asking a new connection
                  Ada.Text_IO.Put_Line
                    (Item => "A client is asking a new connection");

                  LibModbus.TCP_Accept
                    (Context       => MyContext,
                     Socket_Access => Server_Socket'Address);

                  Master_Socket := LibModbus.Get_Socket (Context => MyContext);

                  Ada.Text_IO.Put_Line
                    (Item => "TCP_Accept... Master_Socket = "
                     & Image (Master_Socket));

                  Set (refset, Master_Socket);
               else
                  loop
                     Ada.Text_IO.Put_Line
                       (Item => "Loop2_Count = " & Loop2_Count'Img);

                     Get (rdset, Master_Socket);
                     exit when Master_Socket = No_Socket;

                     --  An already connected master has sent a new query
                     Ada.Text_IO.Put_Line
                       (Item => "An already connected master"
                        & " has sent a new query");

                     LibModbus.Set_Socket
                       (Context => MyContext, Socket => Master_Socket);

--                       Req_Len := LibModbus.Receive
--                         (Context => MyContext,
--                          Request => Request'Address);

                     Req_Len := LibModbus.Receive (Context => MyContext,
                                                  Request => Request);
                     if (Req_Len > 0) then
                        Ada.Text_IO.Put_Line (Item => "Request received");

--                          Result := LibModbus.Reply
--                            (Context        => MyContext,
--                             Request        => Request'Address,
--                             Request_Len    => Req_Len,
--                             Mapping_Access => MyMapping_Access);
                        Result := LibModbus.Reply
                          (Context        => MyContext,
                           Request        => Request,
                           Request_Len    => Req_Len,
                           Mapping_Access => MyMapping2);
                     else
                        --  Connection closed by the client
                        Ada.Text_IO.Put_Line
                          (Item => "Connection closed by the client");

                        Close_Socket (Master_Socket);
                        Clear (refset, Master_Socket);
                     end if;

                     Loop2_Count := Loop2_Count + 1;

                  end loop;
               end if;

            when Expired =>
               Ada.Text_IO.Put_Line
                 (Item => "Check_Selector... Status = Expired");
            when Aborted =>
               Ada.Text_IO.Put_Line
                 (Item => "Check_Selector... Status = Aborted");
            end case;

            Loop1_Count := Loop1_Count + 1;

            exit when Quit;

         end loop;
--        end;

      Close;

   exception
      when Error : LibModbus.Context_Error | LibModbus.Connect_Error =>
         Ada.Text_IO.Put_Line ("libmodbus exception: ");
         Ada.Text_IO.Put_Line (Exception_Information (Error));
         Close;

      when Error : others =>
         Ada.Text_IO.Put_Line ("Unexpected exception: ");
         Ada.Text_IO.Put_Line (Exception_Information (Error));
         Close;

   end Test_LibModbus_Server_Many;

end Test_LibModbus_Server_Many_Package;
