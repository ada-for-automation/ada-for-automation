
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Text_IO; use Ada.Text_IO;
with Ada.Characters.Latin_1;

with Interfaces.C; use Interfaces.C;

with A4A; use A4A;
with A4A.Protocols.LibSnap7; use A4A.Protocols.LibSnap7;
with A4A.Library.Conversion; use A4A.Library.Conversion;

procedure Test_libsnap7_client is

   --  Should work on Linux
   --  On Windows, not supported until W10...
   RED_CONSOLE    : constant String := Ada.Characters.Latin_1.ESC & "[41m";
   GREEN_CONSOLE  : constant String := Ada.Characters.Latin_1.ESC & "[42m";
   NORMAL_CONSOLE : constant String := Ada.Characters.Latin_1.ESC & "[0m";

   Client       : aliased S7Object;
   Result       : C.int;
   Connected    : Boolean := False;
   MyUsrDataIn  : aliased Byte_Array (0 .. 19) := (others => 0);
   MyUsrDataOut : Byte_Array (0 .. 19) := (others => 0);
   MW0          : Word  := 0;
   MW2          : Word  := 0;
   MW4          : Word  := 0;
   MD8          : DWord := 0;
   MD8F         : Float := 0.0;
   EW0          : Word  := 0;
   AW0          : Word  := 0;
   DB1_W0       : Word  := 0;
   T1           : Word  := 0;
   C1           : Word  := 0;

   procedure Byte_String
     (Item : Byte; Buffer : in out String; Pos : in out Integer);

   procedure Byte_String
     (Item : Byte; Buffer : in out String; Pos : in out Integer) is

      Hex   : constant array
        (Byte range 0 .. 15) of Character := "0123456789ABCDEF";

   begin

      Buffer (Pos)     := Hex (Item / 16);
      Buffer (Pos + 1) := Hex (Item mod 16);

      Pos := Pos + 2;

   end Byte_String;

   function Data_String (DataIn : Byte_Array) return String;

   function Data_String (DataIn : Byte_Array) return String is

      Data_Count : constant Natural := DataIn'Length;

      --  We want to display the data bytes in hex
      --  00, ff, ...
      --  00 is 2
      --  00, ff is 6
      --  00, aa, ff is 10

      Result : String (1 .. (Data_Count * 4) - 2) := (others => ' ');

      Pos   : Integer := Result'First;

      Index : Integer := DataIn'First;

   begin

      Byte_String
        (Item   => DataIn (Index),
         Buffer => Result,
         Pos    => Pos);

      loop

         Index := Index + 1;

         exit when Index > DataIn'First + Data_Count - 1;

         if (Index - 1) mod 10 = 9 then
            Result (Pos .. Pos + 1) := CRLF;
         else
            Result (Pos .. Pos + 1) := ", ";
         end if;

         Pos := Pos + 2;

         Byte_String
           (Item   => DataIn (Index),
            Buffer => Result,
            Pos    => Pos);

      end loop;

      return Result;

   end Data_String;

------------------------------------------------------------------------------
--  Show_Data                                                               --
------------------------------------------------------------------------------
   procedure Show_Data (DataIn : Byte_Array);

   procedure Show_Data (DataIn : Byte_Array) is

      What : constant String := CRLF & GREEN_CONSOLE
        & "***********************************************" & CRLF
        & "          MyUsrData in Hex" & CRLF & CRLF
        & Data_String (DataIn => DataIn) & CRLF
        & "***********************************************"
        & NORMAL_CONSOLE & CRLF;
   begin
      Put (What);
   end Show_Data;

------------------------------------------------------------------------------
--  Mementos Read / Write tests                                             --
------------------------------------------------------------------------------
   procedure Test_Mementos;

   procedure Test_Mementos is

   begin

      MyUsrDataIn := (others => 0);

      Result := Cli_MBRead (Client  => Client,
                            Start   => 0,
                            Size    => MyUsrDataIn'Length,
                            UsrData => MyUsrDataIn);

      if Result = 0 then
         Show_Data (DataIn => MyUsrDataIn);

         Bytes_To_Word (LSB_Byte => MyUsrDataIn (1),
                        MSB_Byte => MyUsrDataIn (0),
                        Word_out => MW0);
         Put_Line ("Read MW0 : " & MW0'Img & " ! :-)");

         Bytes_To_Word (LSB_Byte => MyUsrDataIn (3),
                        MSB_Byte => MyUsrDataIn (2),
                        Word_out => MW2);
         Put_Line ("Read MW2 : " & MW2'Img & " ! :-)");

         Bytes_To_Word (LSB_Byte => MyUsrDataIn (5),
                        MSB_Byte => MyUsrDataIn (4),
                        Word_out => MW4);
         Put_Line ("Read MW4 : " & MW4'Img & " ! :-)");

         Bytes_To_DWord (Byte0 => MyUsrDataIn (11),
                         Byte1 => MyUsrDataIn (10),
                         Byte2 => MyUsrDataIn (9),
                         Byte3 => MyUsrDataIn (8),
                         DWord_out => MD8);
         Put_Line ("Read MD8 : " & MD8'Img & " ! :-)");

         MD8F := DWord_To_Float (MD8);
         Put_Line ("Read MD8F : " & MD8F'Img & " ! :-)");

      else
         Put_Line ("Read Mementos failed ! :-(");
      end if;

      MyUsrDataOut (0 .. 3) := MyUsrDataIn (8 .. 11);

      Result := Cli_MBWrite (Client  => Client,
                             Start   => MyUsrDataIn'Length,
                             Size    => MyUsrDataOut'Length,
                             UsrData => MyUsrDataOut);

      if Result = 0 then
         Put_Line ("Write MB succeeded! :-)");
      else
         Put_Line ("Write MB failed ! :-(");
      end if;

   end Test_Mementos;

------------------------------------------------------------------------------
--  Inputs Read / Write tests                                             --
------------------------------------------------------------------------------
   procedure Test_Inputs;

   procedure Test_Inputs is

   begin

      MyUsrDataIn := (others => 0);

      Result := Cli_EBRead (Client  => Client,
                            Start   => 0,
                            Size    => MyUsrDataIn'Length,
                            UsrData => MyUsrDataIn);

      if Result = 0 then
         Bytes_To_Word (LSB_Byte => MyUsrDataIn (1),
                        MSB_Byte => MyUsrDataIn (0),
                        Word_out => EW0);
         Put_Line ("Read EW0 : " & EW0'Img & " ! :-)");

         MyUsrDataOut (0 .. 3) := MyUsrDataIn (0 .. 3);
      else
         Put_Line ("Read Inputs failed ! :-(");
      end if;

      Result := Cli_EBWrite (Client  => Client,
                             Start   => 10,
                             Size    => MyUsrDataOut'Length,
                             UsrData => MyUsrDataOut);

      if Result = 0 then
         Put_Line ("Write EB succeeded! :-)");
      else
         Put_Line ("Write EB failed ! :-(");
      end if;

   end Test_Inputs;

------------------------------------------------------------------------------
--  Outputs Read / Write tests                                             --
------------------------------------------------------------------------------
   procedure Test_Outputs;

   procedure Test_Outputs is

   begin

      MyUsrDataIn := (others => 0);

      Result := Cli_ABRead (Client  => Client,
                            Start   => 0,
                            Size    => MyUsrDataIn'Length,
                            UsrData => MyUsrDataIn);

      if Result = 0 then
         Bytes_To_Word (LSB_Byte => MyUsrDataIn (1),
                        MSB_Byte => MyUsrDataIn (0),
                        Word_out => AW0);
         Put_Line ("Read AW0 : " & AW0'Img & " ! :-)");

         MyUsrDataOut (0 .. 3) := MyUsrDataIn (0 .. 3);
      else
         Put_Line ("Read Outputs failed ! :-(");
      end if;

      Result := Cli_ABWrite (Client  => Client,
                             Start   => 10,
                             Size    => MyUsrDataOut'Length,
                             UsrData => MyUsrDataOut);

      if Result = 0 then
         Put_Line ("Write Outputs succeeded! :-)");
      else
         Put_Line ("Write Outputs failed ! :-(");
      end if;

   end Test_Outputs;

------------------------------------------------------------------------------
--  Data Blocks Read / Write tests                                          --
------------------------------------------------------------------------------
   procedure Test_DBs;

   procedure Test_DBs is

   begin

      MyUsrDataIn := (others => 0);

      Result := Cli_DBRead (Client   => Client,
                            DBNumber => 1,
                            Start    => 0,
                            Size     => MyUsrDataIn'Length,
                            UsrData  => MyUsrDataIn);

      if Result = 0 then
         Bytes_To_Word (LSB_Byte => MyUsrDataIn (1),
                        MSB_Byte => MyUsrDataIn (0),
                        Word_out => DB1_W0);
         Put_Line ("Read DB1.DBW0 : " & DB1_W0'Img & " ! :-)");

         MyUsrDataOut (0 .. 3) := MyUsrDataIn (0 .. 3);
      else
         Put_Line ("Read Data Block failed ! :-(");
      end if;

      Result := Cli_DBWrite (Client   => Client,
                             DBNumber => 1,
                             Start    => 10,
                             Size     => MyUsrDataOut'Length,
                             UsrData  => MyUsrDataOut);

      if Result = 0 then
         Put_Line ("Write Data Block succeeded! :-)");
      else
         Put_Line ("Write Data Block failed ! :-(");
      end if;

   end Test_DBs;

------------------------------------------------------------------------------
--  Timers Read / Write tests                                             --
------------------------------------------------------------------------------
   procedure Test_Timers;

   procedure Test_Timers is

   begin

      MyUsrDataIn := (others => 0);

      Result := Cli_TMRead (Client  => Client,
                            Start   => 0,
                            Amount  => MyUsrDataIn'Length / 2,
                            UsrData => MyUsrDataIn);

      if Result = 0 then
         Bytes_To_Word (LSB_Byte => MyUsrDataIn (3),
                        MSB_Byte => MyUsrDataIn (2),
                        Word_out => T1);
         Put_Line ("Read T1 in Hex: "
                   & Data_String (MyUsrDataIn (2 .. 3)) & " ! :-)");

         MyUsrDataOut (0 .. 3) := MyUsrDataIn (0 .. 3);
         MyUsrDataOut (3) := MyUsrDataIn (3) and 16#F0#; -- Let's play !
      else
         Put_Line ("Read Timers failed ! :-(");
      end if;

      Result := Cli_TMWrite (Client  => Client,
                             Start   => 0,
                             Amount  => MyUsrDataOut'Length / 2,
                             UsrData => MyUsrDataOut);

      if Result = 0 then
         Put_Line ("Write Timers succeeded! :-)");
      else
         Put_Line ("Write Timers failed ! :-(");
      end if;

   end Test_Timers;

------------------------------------------------------------------------------
--  Counters Read / Write tests                                             --
------------------------------------------------------------------------------
   procedure Test_Counters;

   procedure Test_Counters is

   begin

      MyUsrDataIn := (others => 0);

      Result := Cli_CTRead (Client  => Client,
                            Start   => 0,
                            Amount  => MyUsrDataIn'Length / 2,
                            UsrData => MyUsrDataIn);

      if Result = 0 then
         Bytes_To_Word (LSB_Byte => MyUsrDataIn (3),
                        MSB_Byte => MyUsrDataIn (2),
                        Word_out => C1);
         Put_Line ("Read C1 in Hex: "
                   & Data_String (MyUsrDataIn (2 .. 3)) & " ! :-)");

         MyUsrDataOut (0 .. 2) := MyUsrDataIn (0 .. 2);
         MyUsrDataOut (3) := MyUsrDataIn (3) and 16#F0#; -- Let's play !
      else
         Put_Line ("Read Counters failed ! :-(");
      end if;

      Result := Cli_CTWrite (Client  => Client,
                             Start   => 0,
                             Amount  => MyUsrDataOut'Length / 2,
                             UsrData => MyUsrDataOut);

      if Result = 0 then
         Put_Line ("Write Counters succeeded! :-)");
      else
         Put_Line ("Write Counters failed ! :-(");
      end if;

   end Test_Counters;

begin

   Client := Cli_Create;

   Result := Cli_ConnectTo (Client     => Client,
                            IP_Address => "192.168.253.240",
                            Rack       => 0,
                            Slot       => 3);

   if Result = 0 then
      Put_Line (GREEN_CONSOLE & "Connected ! :-)" & NORMAL_CONSOLE);
      Connected := True;
   else
      Put_Line (RED_CONSOLE & "Not Connected ! :-(" & NORMAL_CONSOLE);
   end if;

   if Connected then
      for Index in 1 .. 10 loop

         Test_Mementos;

         Test_Inputs;

         Test_Outputs;

         Test_DBs;

         Test_Timers;

         Test_Counters;

         delay 1.0;
      end loop;

      New_Line;

   end if;

   if Connected then
      Result := Cli_Disconnect (Client => Client);

      if Result = 0 then
         Put_Line (GREEN_CONSOLE & "Disconnected ! :-)" & NORMAL_CONSOLE);
         Connected := False;
      else
         Put_Line (RED_CONSOLE & "Not Disconnected ! :-(" & NORMAL_CONSOLE);
      end if;

   end if;

   Cli_Destroy (Client'Access);

   Put_Line ("Test_libsnap7_client finished !");
end Test_libsnap7_client;
