
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Interfaces;

with Ada.Characters.Latin_1; use Ada.Characters.Latin_1;
with Ada.Text_IO;
with Ada.Exceptions; use Ada.Exceptions;

procedure Test_Protected is

   type Byte is new Interfaces.Unsigned_8;
   --  8-bit unsigned integer

   type Byte_Array is array (Integer range <>) of aliased Byte;
   for Byte_Array'Component_Size use Byte'Size;
   pragma Convention (C, Byte_Array);
   type Byte_Array_Access is access all Byte_Array;

   package Byte_Text_IO is
     new Ada.Text_IO.Modular_IO (Byte);

   CRLF : constant String := (CR, LF);

   --  IO Areas
   IO_Size  : constant := 100;
   subtype IO_Range is Natural range 0 .. IO_Size - 1;

   Inputs  : Byte_Array (IO_Range) := (others => 0);
   Outputs : Byte_Array (IO_Range) := (others => 0);

   protected type Dual_Port_Memory (First, Last : Natural) is

      procedure Set_Data
        (Data_In : in Byte_Array;
         Offset  : in Natural);
      function Get_Data
        (Offset  : in Natural;
         Length  : in Natural) return Byte_Array;

   private
      Data : Byte_Array (First .. Last) := (others => 0);
   end Dual_Port_Memory;

   protected body Dual_Port_Memory is

      procedure Set_Data
        (Data_In : in Byte_Array;
         Offset  : in Natural) is
         Data_In_Length : constant Integer := Data_In'Length;
         Data_In_First  : constant Integer := Data_In'First;
         Data_In_Last   : constant Integer := Data_In'Last;
         Data_First : constant Integer := Data'First;
         Data_Last  : constant Integer := Data'Last;
         Offset2 : constant Natural := Offset;
      begin
         Ada.Text_IO.Put_Line ("Data_In'Length : " & Data_In_Length'Img & CRLF
                               & "Data_In'First  : " & Data_In_First'Img & CRLF
                               & "Data_In'Last   : " & Data_In_Last'Img & CRLF
                               & "Data'First : " & Data_First'Img & CRLF
                               & "Data'Last  : " & Data_Last'Img & CRLF
                               & "Offset : " & Offset'Img);
         --           Data (Offset..Offset + Data_In'Length - 1) := Data_In;
         Data (Offset2 .. Offset2 + Data_In'Length - 1) := Data_In;
      end Set_Data;

      function Get_Data
        (Offset  : in Natural;
         Length  : in Natural) return Byte_Array is
      begin
         return Data (Offset .. Offset + Length - 1);
      end Get_Data;

   end Dual_Port_Memory;

   My_DPM_Inputs : aliased Dual_Port_Memory
     (First => IO_Range'First,
      Last  => IO_Range'Last);

   My_DPM_Outputs : aliased Dual_Port_Memory
     (First => IO_Range'First,
      Last  => IO_Range'Last);

   subtype Buffer_Range is Natural range 0 .. 9;

   Quit : Boolean := False;

   task Task_Ping is
   end Task_Ping;

   task body Task_Ping is
      Buffer : Byte_Array (Buffer_Range) := (others => 0);
      Offset : IO_Range := 0;
      Timer  : Natural  := 5;
   begin

      Ada.Text_IO.Put_Line ("Task_Ping : Started");
      delay 2.0;
      loop
         Ada.Text_IO.Put_Line ("Task_Ping : Get Outputs");
         Buffer := My_DPM_Outputs.Get_Data
           (Offset => Offset, Length => Buffer'Length);
         My_DPM_Inputs.Set_Data (Data_In => Buffer, Offset => Offset);
         if Offset + Buffer_Range'Range_Length <= IO_Range'Last then
            Offset := Offset + Buffer'Length;
         end if;

         if Quit then
            Timer := Timer - 1;
         end if;

         exit when Timer = 0;

         delay 1.0;

      end loop;

      Ada.Text_IO.Put_Line ("Task_Ping : Offset = " & Offset'Img);
      Ada.Text_IO.Put_Line ("Task_Ping : Ended");

   exception
      when Error : others =>
         Ada.Text_IO.Put_Line ("Unexpected exception: ");
         Ada.Text_IO.Put_Line (Exception_Information (Error));

   end Task_Ping;

   task Task_Pong is
   end Task_Pong;

   task body Task_Pong is
      Offset  : IO_Range := 0;
      Pattern : Byte     := 1;
      Done    : Boolean  := False;
      Timer   : Natural  := 5;

      procedure Calculate;
      procedure Calculate is
      begin
         Ada.Text_IO.Put_Line ("Task_Pong : Calculate : "
                               & "Pattern = " & Pattern'Img & ", "
                               & "Offset = " & Offset'Img);

         for Index in Buffer_Range loop
            Outputs (Offset + Index) := Pattern;
         end loop;

         if Offset + Buffer_Range'Range_Length > IO_Range'Last then
            Done := True;
         else
            Pattern := Pattern + 1;
            Offset := Offset + Buffer_Range'Range_Length;
         end if;

         Ada.Text_IO.Put_Line ("Task_Pong : Calculate done");
      end Calculate;

      procedure Inputs_Print;

      procedure Inputs_Print is
      begin
         Ada.Text_IO.Put_Line ("Inputs :");
         for Index in Inputs'Range loop
            Byte_Text_IO.Put (Inputs (Index));
            if ((Index + 1) mod 10) = 0 then
               Ada.Text_IO.New_Line;
            else
               Ada.Text_IO.Put (", ");
            end if;
         end loop;
      end Inputs_Print;

   begin

      Ada.Text_IO.Put_Line ("Task_Pong : Started");
      Calculate;
      My_DPM_Outputs.Set_Data (Data_In => Outputs, Offset => Outputs'First);
      Ada.Text_IO.Put_Line ("Task_Pong : Outputs Written");

      loop
         Ada.Text_IO.Put_Line ("Task_Pong : Get Inputs");
         Inputs := My_DPM_Inputs.Get_Data
           (Offset => Inputs'First, Length => Inputs'Length);
         Inputs_Print;

         if not Done then
            Calculate;
            My_DPM_Outputs.Set_Data
              (Data_In => Outputs, Offset => Outputs'First);
            Ada.Text_IO.Put_Line ("Task_Pong : Outputs Written");
         end if;

         if Done then
            Timer := Timer - 1;
            if Timer = 0 then
               Quit := True;
            end if;
         end if;

         exit when Quit;

         delay 1.0;

      end loop;
      Ada.Text_IO.Put_Line ("Task_Pong : Ended");
   exception
      when Error : others =>
         Ada.Text_IO.Put_Line ("Unexpected exception: ");
         Ada.Text_IO.Put_Line (Exception_Information (Error));

   end Task_Pong;

   subtype Offset2_T is Natural range 0 .. 9;
   Offset2 : Offset2_T := 0;
begin

   Ada.Text_IO.Put_Line ("Test_Protected started..");
   --  This loop is expected to raise a CONSTRAINT ERROR
   loop
      Offset2 := Offset2 + 1;
      Ada.Text_IO.Put_Line ("Test_Protected : Offset2 = " & Offset2'Img);
   end loop;

   exception
      when Error : others =>
         Ada.Text_IO.Put_Line ("Unexpected exception: ");
         Ada.Text_IO.Put_Line (Exception_Information (Error));

end Test_Protected;
