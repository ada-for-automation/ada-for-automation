
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Text_IO;

with A4A; use A4A;

procedure Test_Shift is
   MyDWord  : A4A.DWord;
   MyDWord1 : A4A.DWord;
   MyDWord2 : A4A.DWord;
   --  MyWord : A4A.Word;
   --  MyByte : A4A.Byte;

   procedure Print (Text : String; Item : in DWord);

   procedure Print (Text : String; Item : in DWord) is
      Buffer : String (1 .. 30);
   begin

      A4A.DWord_Text_IO.Put (To => Buffer, Item => Item, Base => 16);
      Ada.Text_IO.Put (Item => Text & Buffer);
      Ada.Text_IO.New_Line (2);

   end Print;

begin
   MyDWord := 16#12345678#;

   Print (Text => "MyDWord : ", Item => MyDWord);

   Ada.Text_IO.Put_Line
     (Item => " -- " & "SHL (Value => MyDWord, Amount => 4);");
   MyDWord := A4A.SHL (Value => MyDWord, Amount => 4);
--     MyDWord := A4A.Shift_Left (Value => MyDWord, Amount => 4);

   Print (Text => "MyDWord : ", Item => MyDWord);

   Ada.Text_IO.Put_Line
     (Item => " -- " & "ROL (Value => MyDWord, Amount => 4);");
   MyDWord := A4A.ROL (Value => MyDWord, Amount => 4);
--     MyDWord := A4A.Rotate_Left (Value => MyDWord, Amount => 4);

   Print (Text => "MyDWord : ", Item => MyDWord);

   MyDWord1 := 16#0000ffff#;
   Print (Text => "MyDWord1 : ", Item => MyDWord1);

   MyDWord2 := 16#ffff0000#;
   Print (Text => "MyDWord2 : ", Item => MyDWord2);

   Ada.Text_IO.Put_Line (Item => " -- " & "MyDWord := MyDWord1 and MyDWord2;");
   MyDWord := MyDWord1 and MyDWord2;

   Print (Text => "MyDWord : ", Item => MyDWord);

   Ada.Text_IO.Put_Line (Item => " -- " & "MyDWord := MyDWord1 or MyDWord2;");
   MyDWord := MyDWord1 or MyDWord2;

   Print (Text => "MyDWord : ", Item => MyDWord);

   Ada.Text_IO.Put_Line (Item => " -- " & "MyDWord := MyDWord1 xor MyDWord2;");
   MyDWord := MyDWord1 xor MyDWord2;

   Print (Text => "MyDWord : ", Item => MyDWord);

   Ada.Text_IO.Put_Line (Item => " -- " & "MyDWord := not MyDWord;");
   MyDWord := not MyDWord;

   Print (Text => "MyDWord : ", Item => MyDWord);

   Ada.Text_IO.Put_Line (Item => " -- " & "MyDWord := not MyDWord;");
   MyDWord := not MyDWord;

   Print (Text => "MyDWord : ", Item => MyDWord);

end Test_Shift;
