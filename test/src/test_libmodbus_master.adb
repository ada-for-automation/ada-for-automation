
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Text_IO;
with Ada.Exceptions; use Ada.Exceptions;

with A4A; use A4A;
with A4A.Protocols.LibModbus;

procedure Test_LibModbus_Master is
   package LibModbus renames A4A.Protocols.LibModbus;
   MyContext : LibModbus.Context_Type;
   Registers : Word_Array (0 .. 9) := (others => 0);

   Context_Ok : Boolean := False;
   Connect_Ok : Boolean := False;

   Timeout : Duration := 0.0;

   procedure Close;

   procedure Close is
   begin
      if Connect_Ok then
         LibModbus.Close (Context => MyContext);
      end if;
      if Context_Ok then
         LibModbus.Free (Context => MyContext);
      end if;
      Ada.Text_IO.Put (Item => "Closing gracefully.");
   end Close;

begin
   Ada.Text_IO.Put_Line (Item => "Test_LibModbus_Master...");

   MyContext := LibModbus.New_RTU
     (Device    => "COM1",
      Baud_Rate => LibModbus.BR_115200,
      Parity    => LibModbus.Even,
      Data_Bits => 8,
      Stop_Bits => 1);

   Ada.Text_IO.Put_Line (Item => "Context created...");

   Context_Ok := True;

   LibModbus.Set_Debug
     (Context => MyContext,
      On      => True);

--     LibModbus.RTU_Set_Serial_Mode
--       (Context => MyContext,
--        Mode    => LibModbus.RS232);
--
--     Ada.Text_IO.Put_Line(Item => "Serial Mode set...");

   LibModbus.Connect (Context => MyContext);
   Connect_Ok := True;

   Ada.Text_IO.Put_Line (Item => "Connected...");

   LibModbus.Set_Slave
     (Context => MyContext,
      Slave   => 2);

   Ada.Text_IO.Put_Line (Item => "Slave set...");

   Timeout := LibModbus.Get_Response_Timeout (Context => MyContext);

   Ada.Text_IO.Put_Line (Item => "Old Timeout = " & Timeout'Img);

   LibModbus.Set_Response_Timeout (Context => MyContext, Timeout =>  1.35);

   Timeout := LibModbus.Get_Response_Timeout (Context => MyContext);

   Ada.Text_IO.Put_Line (Item => "New Timeout = " & Timeout'Img);

   for I in 1 .. 10 loop
      LibModbus.Read_Registers
        (Context => MyContext,
         Offset  => 0,
         Number  => Registers'Length,
         Dest    => Registers);
   end loop;

   --   for I in Registers'First .. Registers'Last loop
   for I in Registers'Range loop
      Ada.Text_IO.Put (Item => "Registers(" & I'Img & ") := ");
      A4A.Word_Text_IO.Put (Item => Registers (I), Width => 8, Base => 16);
      Ada.Text_IO.New_Line;
   end loop;

   Close;

exception

   when Error : LibModbus.Context_Error | LibModbus.Connect_Error =>
      Ada.Text_IO.Put_Line ("libmodbus exception: ");
      Ada.Text_IO.Put_Line (Exception_Information (Error));
      Close;

   when Error : others =>
      Ada.Text_IO.Put_Line ("Unexpected exception: ");
      Ada.Text_IO.Put_Line (Exception_Information (Error));
      Close;

end Test_LibModbus_Master;
