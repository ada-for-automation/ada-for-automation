
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Text_IO;
with Ada.Exceptions; use Ada.Exceptions;

procedure Test_Constraint_Error is

   task My_Task1 is
   end My_Task1;

   task body My_Task1 is
      counter : Integer := 10;
   begin

      loop
         counter := counter - 1;

         exit when counter <= 0;

         delay 1.0;

      end loop;
      Ada.Text_IO.Put_Line ("My_Task1 finished");

   end My_Task1;

   task My_Task2 is
   end My_Task2;

   task body My_Task2 is
      counter : Integer := 15;
   begin

      loop
         counter := counter - 1;

         exit when counter <= 0;

         delay 1.0;

      end loop;
      Ada.Text_IO.Put_Line ("My_Task2 finished");

   end My_Task2;

   subtype Offset_T is Integer range 0 .. 9;
   Offset : Offset_T := 0;
begin

   Ada.Text_IO.Put_Line ("Test_Constraint_Error started..");
   loop
      Offset := Offset + 1;
      Ada.Text_IO.Put_Line ("Offset = " & Offset'Img);
   end loop;

   exception
      when Error : others =>
         Ada.Text_IO.Put_Line ("Unexpected exception: ");
         Ada.Text_IO.Put_Line (Exception_Information (Error));

end Test_Constraint_Error;
