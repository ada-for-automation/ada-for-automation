
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with A4A.Library.Conversion;
with Ada.Text_IO;
with A4A;

procedure Test_Convert is
   package Convert renames A4A.Library.Conversion;
   MyDWord : A4A.DWord;
   MyLSW : A4A.Word;
   MyMSW : A4A.Word;
   MyLSB : A4A.Byte;
   MyMSB : A4A.Byte;
   MyBooleans : array (0 .. 31) of Boolean;
begin
   MyDWord := 16#12345678#;
   Ada.Text_IO.Put_Line
     (Item => " -- "
      & "Convert.DWord_To_Words (DWord_in => MyDWord,"
      & " LSW_Word => MyLSW, MSW_Word => MyMSW);");
   Convert.DWord_To_Words
     (DWord_in => MyDWord, LSW_Word => MyLSW, MSW_Word => MyMSW);
   Ada.Text_IO.Put (Item => "MyDWord : ");
   A4A.DWord_Text_IO.Put (Item => MyDWord, Base => 16); Ada.Text_IO.Put (' ');
   Ada.Text_IO.Put (Item => "MyLSW : ");
   A4A.Word_Text_IO.Put (Item => MyLSW, Base => 16); Ada.Text_IO.Put (' ');
   Ada.Text_IO.Put (Item => "MyMSW : ");
   A4A.Word_Text_IO.Put (Item => MyMSW, Base => 16);
   Ada.Text_IO.New_Line (2);

   Ada.Text_IO.Put_Line
     (Item => " -- "
      & "Convert.Words_To_DWord (LSW_Word => MyMSW, MSW_Word => MyLSW,"
      & " DWord_out => MyDWord);");
   Convert.Words_To_DWord
     (LSW_Word => MyMSW, MSW_Word => MyLSW, DWord_out => MyDWord);
   Ada.Text_IO.Put (Item => "MyDWord : ");
   A4A.DWord_Text_IO.Put (Item => MyDWord, Base => 16);
   Ada.Text_IO.New_Line (2);

   Ada.Text_IO.Put_Line
     (Item => " -- "
      & "Convert.Word_To_Bytes (Word_in => MyLSW,"
      & " LSB_Byte => MyLSB, MSB_Byte => MyMSB);");
   Convert.Word_To_Bytes
     (Word_in => MyLSW, LSB_Byte => MyLSB, MSB_Byte => MyMSB);
   Ada.Text_IO.Put (Item => "MyLSB : ");
   A4A.Byte_Text_IO.Put (Item => MyLSB, Base => 16); Ada.Text_IO.Put (' ');
   Ada.Text_IO.Put (Item => "MyMSB : ");
   A4A.Byte_Text_IO.Put (Item => MyMSB, Base => 16);
   Ada.Text_IO.New_Line (2);

   Ada.Text_IO.Put_Line
     (Item => " -- "
      & "Convert.Bytes_To_Word (LSB_Byte => MyMSB, MSB_Byte => MyLSB,"
      & " Word_out => MyLSW);");
   Convert.Bytes_To_Word
     (LSB_Byte => MyMSB, MSB_Byte => MyLSB, Word_out => MyLSW);
   Ada.Text_IO.Put (Item => "MyLSW : ");
   A4A.Word_Text_IO.Put (Item => MyLSW, Base => 16);
   Ada.Text_IO.New_Line (2);

   Ada.Text_IO.Put_Line
     (Item => " -- " & "Convert.Byte_To_Booleans (Byte_in => MyLSB, ...);");
   Convert.Byte_To_Booleans (Byte_in => MyLSB,
                            Boolean_out00 => MyBooleans (0),
                            Boolean_out01 => MyBooleans (1),
                            Boolean_out02 => MyBooleans (2),
                            Boolean_out03 => MyBooleans (3),
                            Boolean_out04 => MyBooleans (4),
                            Boolean_out05 => MyBooleans (5),
                            Boolean_out06 => MyBooleans (6),
                            Boolean_out07 => MyBooleans (7)
                              );
   Ada.Text_IO.Put (Item => "MyLSB : ");
   A4A.Byte_Text_IO.Put (Item => MyLSB, Base => 16);
   Ada.Text_IO.New_Line;
   Ada.Text_IO.Put (Item => "MyBooleans(0 .. 7) reverse : ");
   for Index in reverse 0 .. 7 loop
      Ada.Text_IO.Put (Item => MyBooleans (Index)'Img & ' ');
   end loop;
   Ada.Text_IO.New_Line (2);

   Ada.Text_IO.Put_Line
     (Item => " -- " & "Convert.Booleans_To_Byte (Byte_out => MyLSB, ...);");
   MyBooleans (0) := False;
   MyBooleans (1) := True;
   MyBooleans (2) := True;
   MyBooleans (3) := True;  -- E
   MyBooleans (4) := True;
   MyBooleans (5) := True;
   MyBooleans (6) := True;
   MyBooleans (7) := False; -- 7
   Ada.Text_IO.Put (Item => "MyBooleans(0 .. 7) reverse : ");
   for Index in reverse 0 .. 7 loop
      Ada.Text_IO.Put (Item => MyBooleans (Index)'Img & ' ');
   end loop;
   Ada.Text_IO.New_Line;
   Convert.Booleans_To_Byte (Boolean_in00 => MyBooleans (0),
                             Boolean_in01 => MyBooleans (1),
                             Boolean_in02 => MyBooleans (2),
                             Boolean_in03 => MyBooleans (3),
                             Boolean_in04 => MyBooleans (4),
                             Boolean_in05 => MyBooleans (5),
                             Boolean_in06 => MyBooleans (6),
                             Boolean_in07 => MyBooleans (7),
                             Byte_out => MyLSB);
   Ada.Text_IO.Put (Item => "MyLSB : ");
   A4A.Byte_Text_IO.Put (Item => MyLSB, Base => 16); Ada.Text_IO.Put (' ');
   Ada.Text_IO.New_Line (2);

   Ada.Text_IO.Put_Line
     (Item => " -- " & "Convert.Word_To_Booleans (Word_in => MyLSW, ...);");
   Convert.Word_To_Booleans (Word_in => MyLSW,
                            Boolean_out00 => MyBooleans (0),
                            Boolean_out01 => MyBooleans (1),
                            Boolean_out02 => MyBooleans (2),
                            Boolean_out03 => MyBooleans (3),
                            Boolean_out04 => MyBooleans (4),
                            Boolean_out05 => MyBooleans (5),
                            Boolean_out06 => MyBooleans (6),
                            Boolean_out07 => MyBooleans (7),

                            Boolean_out08 => MyBooleans (8),
                            Boolean_out09 => MyBooleans (9),
                            Boolean_out10 => MyBooleans (10),
                            Boolean_out11 => MyBooleans (11),
                            Boolean_out12 => MyBooleans (12),
                            Boolean_out13 => MyBooleans (13),
                            Boolean_out14 => MyBooleans (14),
                            Boolean_out15 => MyBooleans (15)
                              );
   Ada.Text_IO.Put (Item => "MyLSW : ");
   A4A.Word_Text_IO.Put (Item => MyLSW, Base => 16); Ada.Text_IO.Put (' ');
   Ada.Text_IO.Put_Line (Item => "MyBooleans(0 .. 15) reverse : ");
   for Index in reverse 0 .. 15 loop
      Ada.Text_IO.Put (Item => MyBooleans (Index)'Img & ' ');
      if Index = 8 then
         Ada.Text_IO.New_Line;
      end if;
   end loop;
   Ada.Text_IO.New_Line (2);

   Ada.Text_IO.Put_Line
     (Item => " -- " & "Convert.Booleans_To_Word (Word_out => MyLSW, ...);");
   Convert.Booleans_To_Word (
                             Boolean_in00 => True,
                             Boolean_in01 => True,
                             Boolean_in02 => True,
                             Boolean_in03 => True, -- F
                             Boolean_in04 => False,
                             Boolean_in05 => True,
                             Boolean_in06 => True,
                             Boolean_in07 => True, -- E

                             Boolean_in08 => False,
                             Boolean_in09 => True,
                             Boolean_in10 => True,
                             Boolean_in11 => True, -- E
                             Boolean_in12 => True,
                             Boolean_in13 => True,
                             Boolean_in14 => False,
                             Boolean_in15 => True, -- B
                             Word_out => MyLSW);
   Ada.Text_IO.Put (Item => "MyLSW : ");
   A4A.Word_Text_IO.Put (Item => MyLSW, Base => 16);
   Ada.Text_IO.New_Line (2);

   Ada.Text_IO.Put_Line
     (Item => " -- "
      & "Convert.DWord_To_Booleans (DWord_in => MyDWord, ...);");
   Convert.DWord_To_Booleans (DWord_in => MyDWord,
                            Boolean_out00 => MyBooleans (0),
                            Boolean_out01 => MyBooleans (1),
                            Boolean_out02 => MyBooleans (2),
                            Boolean_out03 => MyBooleans (3),
                            Boolean_out04 => MyBooleans (4),
                            Boolean_out05 => MyBooleans (5),
                            Boolean_out06 => MyBooleans (6),
                            Boolean_out07 => MyBooleans (7),

                            Boolean_out08 => MyBooleans (8),
                            Boolean_out09 => MyBooleans (9),
                            Boolean_out10 => MyBooleans (10),
                            Boolean_out11 => MyBooleans (11),
                            Boolean_out12 => MyBooleans (12),
                            Boolean_out13 => MyBooleans (13),
                            Boolean_out14 => MyBooleans (14),
                            Boolean_out15 => MyBooleans (15),

                            Boolean_out16 => MyBooleans (16),
                            Boolean_out17 => MyBooleans (17),
                            Boolean_out18 => MyBooleans (18),
                            Boolean_out19 => MyBooleans (19),
                            Boolean_out20 => MyBooleans (20),
                            Boolean_out21 => MyBooleans (21),
                            Boolean_out22 => MyBooleans (22),
                            Boolean_out23 => MyBooleans (23),

                            Boolean_out24 => MyBooleans (24),
                            Boolean_out25 => MyBooleans (25),
                            Boolean_out26 => MyBooleans (26),
                            Boolean_out27 => MyBooleans (27),
                            Boolean_out28 => MyBooleans (28),
                            Boolean_out29 => MyBooleans (29),
                            Boolean_out30 => MyBooleans (30),
                            Boolean_out31 => MyBooleans (31)
                             );
   Ada.Text_IO.Put (Item => "MyDWord : ");
   A4A.DWord_Text_IO.Put (Item => MyDWord, Base => 16); Ada.Text_IO.Put (' ');
   Ada.Text_IO.Put_Line (Item => "MyBooleans(0 .. 31) reverse : ");
   for Index in reverse 0 .. 31 loop
      Ada.Text_IO.Put (Item => MyBooleans (Index)'Img & ' ');
      if Index = 8 or Index = 16 or Index = 24 then
         Ada.Text_IO.New_Line;
      end if;
   end loop;
   Ada.Text_IO.New_Line (2);

   Ada.Text_IO.Put_Line
     (Item => " -- "
      & "Convert.Booleans_To_DWord (DWord_out => MyDWord, ...);");
   Convert.Booleans_To_DWord (
                             Boolean_in00 => True,
                             Boolean_in01 => True,
                             Boolean_in02 => True,
                             Boolean_in03 => True, -- F
                             Boolean_in04 => False,
                             Boolean_in05 => True,
                             Boolean_in06 => True,
                             Boolean_in07 => True, -- E

                             Boolean_in08 => False,
                             Boolean_in09 => True,
                             Boolean_in10 => True,
                             Boolean_in11 => True, -- E
                             Boolean_in12 => True,
                             Boolean_in13 => True,
                             Boolean_in14 => False,
                             Boolean_in15 => True, -- B

                             Boolean_in16 => True,
                             Boolean_in17 => False,
                             Boolean_in18 => True,
                             Boolean_in19 => True, -- D
                             Boolean_in20 => False,
                             Boolean_in21 => True,
                             Boolean_in22 => False,
                             Boolean_in23 => True, -- A

                             Boolean_in24 => False,
                             Boolean_in25 => True,
                             Boolean_in26 => True,
                             Boolean_in27 => True, -- E
                             Boolean_in28 => True,
                             Boolean_in29 => False,
                             Boolean_in30 => True,
                             Boolean_in31 => True, -- D
                             DWord_out => MyDWord);
   Ada.Text_IO.Put (Item => "MyDWord : ");
   A4A.DWord_Text_IO.Put (Item => MyDWord, Base => 16);
   Ada.Text_IO.New_Line (2);

end Test_Convert;
