
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with System;

with Interfaces.C; use Interfaces.C;

with Ada.Text_IO;
with Ada.Exceptions; use Ada.Exceptions;

with A4A; use A4A;
with A4A.Protocols.LibModbus;

procedure Test_LibModbus_Server_Single is
   package LibModbus renames A4A.Protocols.LibModbus;

   MyContext         : LibModbus.Context_Type;
   Result            : Interfaces.C.int;
   Req_Len           : Interfaces.C.int;
   Request           : aliased Byte_Array
     (0 .. LibModbus.MODBUS_TCP_MAX_ADU_LENGTH);
   Server_Socket     : aliased LibModbus.Socket_Type;
   MyMapping_Access  : System.Address;

   Context_Ok : Boolean := False;
   Connect_Ok : Boolean := False;
   Mapping_Ok : Boolean := False;

   procedure Close;

   procedure Close is
   begin
      if Mapping_Ok then
         LibModbus.Mapping_Free (Mapping_Access => MyMapping_Access);
      end if;
      if Connect_Ok then
         LibModbus.Close (Context => MyContext);
      end if;
      if Context_Ok then
         LibModbus.Free (Context => MyContext);
      end if;
      Ada.Text_IO.Put (Item => "Closing gracefully.");
   end Close;

begin
   Ada.Text_IO.Put (Item => "Test_LibModbus_Server_Single... Waiting");
   Ada.Text_IO.New_Line;
   MyContext := LibModbus.New_TCP (IP_Address => "127.0.0.1",
                                   Port       => 1502);
   Context_Ok := True;

   LibModbus.Set_Debug (Context => MyContext, On => True);

   MyMapping_Access := LibModbus.Mapping_New (Nb_Coil_Status       => 80,
                                              Nb_Input_Status      => 80,
                                              Nb_Holding_Registers => 10,
                                              Nb_Input_Registers   => 10);
   Mapping_Ok := True;

   declare
      MyMapping : LibModbus.Modbus_Mapping_Type;
      for MyMapping'Address use MyMapping_Access;
      My_tab_registers : Word_Array (0 .. 9);
      for My_tab_registers'Address use MyMapping.tab_registers;
   begin
      My_tab_registers (0) := 16#0102#;
      My_tab_registers (1) := 16#0304#;
      My_tab_registers (2) := 16#0506#;
      My_tab_registers (3) := 16#0708#;
      My_tab_registers (4) := 16#090A#;
      My_tab_registers (5) := 16#0B0C#;
      My_tab_registers (6) := 16#FF0C#;
      My_tab_registers (7) := 16#FF0B#;
      My_tab_registers (8) := 16#FF0A#;
      My_tab_registers (9) := 16#FFFF#;

      Server_Socket := LibModbus.TCP_Listen (Context       => MyContext,
                                             Nb_Connection => 1);
      LibModbus.TCP_Accept (Context       => MyContext,
                            Socket_Access => Server_Socket'Address);
      --  Wait forever a Client connection
      Connect_Ok := True;

      loop
         Req_Len := LibModbus.Receive (Context => MyContext,
                                       Request => Request);
         if (Req_Len > 0) then
            Result := LibModbus.Reply (Context        => MyContext,
                                       Request        => Request,
                                       Request_Len    => Req_Len,
                                       Mapping_Access => MyMapping);
         else
            --  The Client has gone
            exit;
         end if;
      end loop;
   end;

   Close;

exception

   when Error : LibModbus.Context_Error | LibModbus.Connect_Error =>
      Ada.Text_IO.Put_Line ("libmodbus exception: ");
      Ada.Text_IO.Put_Line (Exception_Information (Error));
      Close;

   when Error : others =>
      Ada.Text_IO.Put_Line ("Unexpected exception: ");
      Ada.Text_IO.Put_Line (Exception_Information (Error));
      Close;

end Test_LibModbus_Server_Single;
