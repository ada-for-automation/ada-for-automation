
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

package body A4A.Lock_Mechanisms is

   protected body Simple_RW_Lock_Mechanism is

      entry Write_Lock when not Write_Locked and Read_Count = 0 is
      begin
         Write_Locked := True;
      end Write_Lock;

      entry Write_Unlock when Write_Locked is
      begin
         Write_Locked := False;
      end Write_Unlock;

      entry Read_Lock when not Write_Locked is
      begin
         Read_Count := Read_Count + 1;
      end Read_Lock;

      entry Read_Unlock when Read_Count > 0 is
      begin
         Read_Count := Read_Count - 1;
      end Read_Unlock;

   end Simple_RW_Lock_Mechanism;

end A4A.Lock_Mechanisms;
