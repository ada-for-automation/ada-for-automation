
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package provides a home for timers.
--  </summary>
--  <description>
--  It provides :
--  - TON,
--  - TOFF,
--  - TPULSE.
--  </description>
--  <group>Libraries</group>

with System;

with Ada.Real_Time; use Ada.Real_Time;

with A4A.Tasks.Interfaces; use A4A.Tasks.Interfaces;

package A4A.Library.Timers is

   type Task_Interface is
      record
         Control : Task_Control;
         Status  : Task_Status;
      end record;
   type Task_Itf_Access is access all Task_Interface;

   task type Clock_Handler_Task_Type
     (Task_Priority           : System.Priority;
      Task_Itf                : Task_Itf_Access;
      Period_In_Milliseconds  : Natural
     ) is
      pragma Priority (Task_Priority);
   end Clock_Handler_Task_Type;
   --  This type should have only one instance
   type Clock_Handler_Task_Type_Access is access Clock_Handler_Task_Type;

   function Clock_Time return Time;
   --  returns the Time managed by the Clock_Handler task

private

   Clock_Time_Val : Time;

end A4A.Library.Timers;
