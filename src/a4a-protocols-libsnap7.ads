
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package provides the binding to libsnap7, a library implementing
--  the Siemens S7 communication protocol.
--  </summary>
--  <description>
--  It provides :
--  - libsnap7 data types,
--  - libsnap7 C functions binding.
--  </description>
--  <group>Protocols</group>
--  <c_version>1.4.2</c_version>

with System; use System;
with Interfaces.C;

package A4A.Protocols.LibSnap7 is

   package C renames Interfaces.C;

   type S7Object is private;
   --  typedef uintptr_t  S7Object;
   --  // multi platform/processor object reference
   --  // DON'T CONFUSE IT WITH AN OLE OBJECT, IT'S SIMPLY
   --  // AN INTEGER VALUE (32 OR 64 BIT) USED AS HANDLE.

   type Order_Code_Type is
      record
         Code : String (1 .. 21);
         --  CPU Order Code

         Version_V1 : Byte := 0;
         Version_V2 : Byte := 0;
         Version_V3 : Byte := 0;
         --  Version V1.V2.V3

      end record;
   --  Order code
   --  typedef struct {
   --     char Code[21];
   --     byte V1;
   --     byte V2;
   --     byte V3;
   --  } TS7OrderCode, *PS7OrderCode;

   type CPU_Info_Type is
      record
         Module_Type_Name : String (1 .. 33);
         --  Module Type Name

         Serial_Number : String (1 .. 25);
         --  Serial Number

         AS_Name : String (1 .. 25);
         --  AS Name

         Copyright : String (1 .. 27);
         --  Copyright

         Module_Name : String (1 .. 25);
         --  Module Name

      end record;
   --  CPU Info
   --  typedef struct {
   --     char ModuleTypeName[33];
   --     char SerialNumber[25];
   --     char ASName[25];
   --     char Copyright[27];
   --     char ModuleName[25];
   --  } TS7CpuInfo, *PS7CpuInfo;

------------------------------------------------------------------------------
--  Administrative functions
------------------------------------------------------------------------------
   function Cli_Create return S7Object;
   --  S7Object S7API Cli_Create();

   procedure Cli_Destroy (Client : access S7Object);
   --  void S7API Cli_Destroy(S7Object *Client);

   function Cli_ConnectTo (Client     : in S7Object;
                           IP_Address : in String;
                           Rack       : in Natural;
                           Slot       : in Natural) return C.int;
   --  int S7API Cli_ConnectTo(S7Object Client, const char *Address, int Rack,
   --                          int Slot);

   function Cli_Disconnect (Client    : in S7Object) return C.int;
   --  int S7API Cli_Disconnect(S7Object Client);

   function Cli_GetConnected (Client      : in S7Object;
                              IsConnected : in out C.int) return C.int;
   --  int S7API Cli_GetConnected(S7Object Client, int *Connected);

------------------------------------------------------------------------------
--  Misc
------------------------------------------------------------------------------
   function Cli_Error_Text (Error : C.int) return String;
   --  int S7API Cli_ErrorText(int Error, char *Text, int TextLen);

------------------------------------------------------------------------------
--  Control functions
------------------------------------------------------------------------------
   function Cli_GetPlcStatus (Client : in S7Object;
                              Status : in out C.int) return C.int;
   --  int S7API Cli_GetPlcStatus(S7Object Client, int *Status);

------------------------------------------------------------------------------
--  System Info functions
------------------------------------------------------------------------------
   procedure Cli_Get_Order_Code (Client     : in S7Object;
                                 Order_Code : out Order_Code_Type;
                                 Result     : out C.int);
   --  int S7API Cli_GetOrderCode(S7Object Client, TS7OrderCode *pUsrData);

   procedure Cli_Get_Cpu_Info (Client   : in S7Object;
                               Cpu_Info : out CPU_Info_Type;
                               Result   : out C.int);
   --  int S7API Cli_GetCpuInfo(S7Object Client, TS7CpuInfo *pUsrData);

------------------------------------------------------------------------------
--  Data I/O Lean functions
------------------------------------------------------------------------------
   function Cli_DBRead (Client     : in S7Object;
                        DBNumber   : in Natural;
                        Start      : in Natural;
                        Size       : in Natural;
                        UsrData    : Byte_Array) return C.int;
   --  int S7API Cli_DBRead(S7Object Client, int DBNumber, int Start, int Size,
   --                       void *pUsrData);

   function Cli_DBWrite (Client    : in S7Object;
                         DBNumber  : in Natural;
                         Start     : in Natural;
                         Size      : in Natural;
                         UsrData   : Byte_Array) return C.int;
   --  int S7API Cli_DBWrite(S7Object Client, int DBNumber, int Start,
   --                        int Size, void *pUsrData);

   function Cli_MBRead (Client     : in S7Object;
                        Start      : in Natural;
                        Size       : in Natural;
                        UsrData    : Byte_Array) return C.int;
   --  int S7API Cli_MBRead(S7Object Client, int Start, int Size,
   --                       void *pUsrData);

   function Cli_MBWrite (Client    : in S7Object;
                         Start     : in Natural;
                         Size      : in Natural;
                         UsrData   : Byte_Array) return C.int;
   --  int S7API Cli_MBWrite(S7Object Client, int Start, int Size,
   --                        void *pUsrData);

   function Cli_EBRead (Client     : in S7Object;
                        Start      : in Natural;
                        Size       : in Natural;
                        UsrData    : Byte_Array) return C.int;
   --  int S7API Cli_EBRead(S7Object Client, int Start, int Size,
   --                       void *pUsrData);

   function Cli_EBWrite (Client    : in S7Object;
                         Start     : in Natural;
                         Size      : in Natural;
                         UsrData   : Byte_Array) return C.int;
   --  int S7API Cli_EBWrite(S7Object Client, int Start, int Size,
   --                        void *pUsrData);

   function Cli_ABRead (Client     : in S7Object;
                        Start      : in Natural;
                        Size       : in Natural;
                        UsrData    : Byte_Array) return C.int;
   --  int S7API Cli_ABRead(S7Object Client, int Start, int Size,
   --                       void *pUsrData);

   function Cli_ABWrite (Client    : in S7Object;
                         Start     : in Natural;
                         Size      : in Natural;
                         UsrData   : Byte_Array) return C.int;
   --  int S7API Cli_ABWrite(S7Object Client, int Start, int Size,
   --                        void *pUsrData);

   function Cli_TMRead (Client     : in S7Object;
                        Start      : in Natural;
                        Amount     : in Natural;
                        UsrData    : Byte_Array) return C.int;
   --  int S7API Cli_TMRead(S7Object Client, int Start, int Size,
   --                       void *pUsrData);

   function Cli_TMWrite (Client    : in S7Object;
                         Start     : in Natural;
                         Amount    : in Natural;
                         UsrData   : Byte_Array) return C.int;
   --  int S7API Cli_TMWrite(S7Object Client, int Start, int Size,
   --                        void *pUsrData);

   function Cli_CTRead (Client     : in S7Object;
                        Start      : in Natural;
                        Amount     : in Natural;
                        UsrData    : Byte_Array) return C.int;
   --  int S7API Cli_CTRead(S7Object Client, int Start, int Size,
   --                       void *pUsrData);

   function Cli_CTWrite (Client    : in S7Object;
                         Start     : in Natural;
                         Amount    : in Natural;
                         UsrData   : Byte_Array) return C.int;
   --  int S7API Cli_CTWrite(S7Object Client, int Start, int Size,
   --                        void *pUsrData);

private
   type S7Object is new System.Address;
   --  type S7Object is new C.unsigned_long_long;
   --  typedef uintptr_t  S7Object;
   --  // multi platform/processor object reference
   --  // DON'T CONFUSE IT WITH AN OLE OBJECT, IT'S SIMPLY
   --  // AN INTEGER VALUE (32 OR 64 BIT) USED AS HANDLE.

   function Cli_ErrorText
     (Error         : C.int;
      Buffer        : System.Address;
      Buffer_Length : C.int) return C.int;
   --  int S7API Cli_ErrorText(int Error, char *Text, int TextLen);

   type C_Order_Code_Type is
      record
         Code : Interfaces.C.char_array (1 .. 21) :=
           (others => Interfaces.C.nul);
         --  CPU Order Code

         Version_V1 : Byte := 0;
         Version_V2 : Byte := 0;
         Version_V3 : Byte := 0;
         --  Version V1.V2.V3

      end record;
   --  Order code
   --  // Order code
   --  typedef struct {
   --     char Code[21];
   --     byte V1;
   --     byte V2;
   --     byte V3;
   --  } TS7OrderCode, *PS7OrderCode;
   pragma Convention (C, C_Order_Code_Type);

   type C_CPU_Info_Type is
      record
         Module_Type_Name : Interfaces.C.char_array (1 .. 33) :=
           (others => Interfaces.C.nul);
         --  Module Type Name

         Serial_Number : Interfaces.C.char_array (1 .. 25) :=
           (others => Interfaces.C.nul);
         --  Serial Number

         AS_Name : Interfaces.C.char_array (1 .. 25) :=
           (others => Interfaces.C.nul);
         --  AS Name

         Copyright : Interfaces.C.char_array (1 .. 27) :=
           (others => Interfaces.C.nul);
         --  Copyright

         Module_Name : Interfaces.C.char_array (1 .. 25) :=
           (others => Interfaces.C.nul);
         --  Module Name

      end record;
   --  CPU Info
   --  // CPU Info
   --  typedef struct {
   --     char ModuleTypeName[33];
   --     char SerialNumber[25];
   --     char ASName[25];
   --     char Copyright[27];
   --     char ModuleName[25];
   --  } TS7CpuInfo, *PS7CpuInfo;
   pragma Convention (C, C_CPU_Info_Type);

   function Cli_GetOrderCode (Client     : in S7Object;
                              Order_Code : C_Order_Code_Type) return C.int;
   --  int S7API Cli_GetOrderCode(S7Object Client, TS7OrderCode *pUsrData);

   function Cli_GetCpuInfo (Client   : in S7Object;
                            Cpu_Info : C_CPU_Info_Type) return C.int;
   --  int S7API Cli_GetCpuInfo(S7Object Client, TS7CpuInfo *pUsrData);

   pragma Import (C, Cli_Create,           "Cli_Create");
   pragma Import (C, Cli_Destroy,         "Cli_Destroy");
   pragma Import (C, Cli_Disconnect,   "Cli_Disconnect");
   pragma Import (C, Cli_GetConnected, "Cli_GetConnected");
   pragma Import (C, Cli_ErrorText,     "Cli_ErrorText");
   pragma Import (C, Cli_GetPlcStatus, "Cli_GetPlcStatus");
   pragma Import (C, Cli_GetOrderCode, "Cli_GetOrderCode");
   pragma Import (C, Cli_GetCpuInfo,   "Cli_GetCpuInfo");

   pragma Import (C, Cli_DBRead,           "Cli_DBRead");
   pragma Import (C, Cli_DBWrite,         "Cli_DBWrite");
   pragma Import (C, Cli_MBRead,           "Cli_MBRead");
   pragma Import (C, Cli_MBWrite,         "Cli_MBWrite");
   pragma Import (C, Cli_EBRead,           "Cli_EBRead");
   pragma Import (C, Cli_EBWrite,         "Cli_EBWrite");
   pragma Import (C, Cli_ABRead,           "Cli_ABRead");
   pragma Import (C, Cli_ABWrite,         "Cli_ABWrite");
   pragma Import (C, Cli_TMRead,           "Cli_TMRead");
   pragma Import (C, Cli_TMWrite,         "Cli_TMWrite");
   pragma Import (C, Cli_CTRead,           "Cli_CTRead");
   pragma Import (C, Cli_CTWrite,         "Cli_CTWrite");

end A4A.Protocols.LibSnap7;
