
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Real_Time; use Ada.Real_Time;

with A4A.Library.Timers.TON; use A4A.Library.Timers;

package A4A.Library.Simulation.Contactor is

   Default_TON_Preset : constant := 1000;
   --  Default value in milliseconds for Timer TON Preset

   type Instance is tagged private;

   function Create
     (TON_Preset : in Positive := Default_TON_Preset)
      --  TON Preset in milliseconds
      return Instance;

   procedure Cyclic
     (Device    : in out Instance;
      Auto      : in Boolean;
      FB_Manu   : in Boolean;
      Coil      : in Boolean;
      Feed_Back : out Boolean);

private

   type Instance is tagged
      record
         Preset_Simu : Time_Span;
         Tempo_Simu  : TON.Instance;
      end record;

end A4A.Library.Simulation.Contactor;
