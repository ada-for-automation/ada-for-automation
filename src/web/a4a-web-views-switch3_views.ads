
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Gnoga.Gui.Base;
with Gnoga.Gui.Element;

with A4A.Web.Controllers.Switch3_Controllers; use A4A.Web.Controllers;

package A4A.Web.Views.Switch3_Views is

   type Instance is tagged limited private;
   type Instance_Access is access all Instance;

   procedure Update_View (Object : in out Instance);

   procedure Setup
     (Object     : in out Instance;
      Parent     : in Gnoga.Gui.Base.Base_Type'Class;
      Controller : in Switch3_Controllers.Instance_Access;
      Mobile_Id  : in String;
      Area0_Id   : in String;
      Area1_Id   : in String;
      Area2_Id   : in String);
   --  Attach the View to the HTML element on one hand and to the Controller on
   --  the other hand.

private
   procedure Area0_On_Click (Object : in out Gnoga.Gui.Base.Base_Type'Class);
   procedure Area1_On_Click (Object : in out Gnoga.Gui.Base.Base_Type'Class);
   procedure Area2_On_Click (Object : in out Gnoga.Gui.Base.Base_Type'Class);

   type Area_Instance is new Gnoga.Gui.Element.Element_Type with
      record
         View : Instance_Access;
         --  The View

      end record;

   procedure Setup
     (Object : in out Area_Instance;
      View   : in Instance_Access);

   procedure On_Click0 (Object : in out Instance);
   procedure On_Click1 (Object : in out Instance);
   procedure On_Click2 (Object : in out Instance);

   type Instance is tagged limited
      record
         Mobile : Gnoga.Gui.Element.Element_Type;
         Area0  : Area_Instance;
         Area1  : Area_Instance;
         Area2  : Area_Instance;

         Switch3 : Switch3_Controllers.Instance_Access;
         --  The controller

         Switch3_Status_Previous : Switch3_Controllers.Status_Type;

         First_Time : Boolean := True;
      end record;

end A4A.Web.Views.Switch3_Views;
