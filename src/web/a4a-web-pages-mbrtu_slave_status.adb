
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with A4A.Protocols; use A4A.Protocols.Device_Strings;

with A4A.Web.Controllers.Connection_Stats_Controllers;
use A4A.Web.Controllers;

package body A4A.Web.Pages.MBRTU_Slave_Status is

   procedure Slave_Task_Status_Update
     (Watchdog_Error : Boolean;
      Status         : MBRTU_Slave_Config.Slave.Task_Status_Type) is
   begin
      Slave_Task_Watchdog_Error := Watchdog_Error;
      Slave_Task_Status := Status;
   end Slave_Task_Status_Update;

   overriding
   procedure Update_Page (Web_Page : access Instance) is

   begin

      Web_Page.Common_View.Connection_Stats_View.Update_View;

      Web_Page.Slave_Task_Watchdog_View.Update_Element
        (Status  => Slave_Task_Watchdog_Error,
         If_True => "off", If_False => "on");

      Web_Page.Slave_Connected_To_Master_View.Update_Element
        (Status  => Slave_Task_Status.Commands_Status.Connected_To_Master);

      Web_Page.Slave_FC01_View.Update_Element
        (Slave_Task_Status.Commands_Status.Read_Coils_Count);

      Web_Page.Slave_FC02_View.Update_Element
        (Slave_Task_Status.Commands_Status.Read_Input_Bits_Count);

      Web_Page.Slave_FC03_View.Update_Element
        (Slave_Task_Status.Commands_Status.Read_Holding_Registers_Count);

      Web_Page.Slave_FC04_View.Update_Element
        (Slave_Task_Status.Commands_Status.Read_Input_Registers_Count);

      Web_Page.Slave_FC05_View.Update_Element
        (Slave_Task_Status.Commands_Status.Write_Single_Coil_Count);

      Web_Page.Slave_FC06_View.Update_Element
        (Slave_Task_Status.Commands_Status.Write_Single_Register_Count);

      Web_Page.Slave_FC15_View.Update_Element
        (Slave_Task_Status.Commands_Status.Write_Multiple_Coils_Count);

      Web_Page.Slave_FC16_View.Update_Element
        (Slave_Task_Status.Commands_Status.Write_Multiple_Registers_Count);

      Web_Page.Slave_FC23_View.Update_Element
        (Slave_Task_Status.Commands_Status.Write_Read_Registers_Count);

      Web_Page.Slave_Task_Status_Previous := Slave_Task_Status;

   end Update_Page;

   procedure On_Connect
     (Main_Window : in out Gnoga.Gui.Window.Window_Type'Class;
      Connection  : access
        Gnoga.Application.Multi_Connect.Connection_Holder_Type)
   is
      Web_Page : constant Instance_Access := new Instance;

      My_Ident : aliased String :=
        "A4A.Web.Pages.MBRTU_Slave_Status_View_Updater";
      View_Updater_Task : A4A.Web.Pages.Updater_Task_Type
        (My_Ident                    => My_Ident'Access,
         Web_Page                    => Web_Page);

   begin
      Main_Window.Connection_Data (Web_Page);

      Main_Window.Document.Load_CSS (W3_CSS_URL);
      Main_Window.Document.Load_CSS (Font_Awesome_CSS_URL);
      Main_Window.Document.Load_CSS (APP_CSS_URL);

      Connection_Stats_Controllers.Connection;

      Common_View_Setup (Web_Page.Common_View,
                         Main_Window'Unchecked_Access,
                         "140-mbrtu-slave-status.html");

      --
      --  Modbus RTU Slave View
      --

      Web_Page.Slave_Device_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "slave-device");

      Web_Page.Slave_Device_View.Inner_HTML
        (To_String (MBRTU_Slave_Config.Config1.Device));

      Web_Page.Slave_Address_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "slave-address");

      Web_Page.Slave_Address_View.Inner_HTML
        (MBRTU_Slave_Config.Config1.Slave'Img);

      Web_Page.Slave_Task_Watchdog_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "slave-task-watchdog");

      Web_Page.Slave_Connected_To_Master_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "slave-connected-to-master");

      Web_Page.Slave_FC01_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "slave-fc01");

      Web_Page.Slave_FC02_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "slave-fc02");

      Web_Page.Slave_FC03_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "slave-fc03");

      Web_Page.Slave_FC04_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "slave-fc04");

      Web_Page.Slave_FC05_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "slave-fc05");

      Web_Page.Slave_FC06_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "slave-fc06");

      Web_Page.Slave_FC15_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "slave-fc15");

      Web_Page.Slave_FC16_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "slave-fc16");

      Web_Page.Slave_FC23_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "slave-fc23");

      View_Updater_Task.Start;

      Connection.Hold;

      View_Updater_Task.Stop;

   end On_Connect;

end A4A.Web.Pages.MBRTU_Slave_Status;
