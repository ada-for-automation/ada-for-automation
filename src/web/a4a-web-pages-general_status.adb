
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Calendar.Arithmetic;
use Ada.Calendar.Arithmetic;
with Ada.Calendar.Formatting;

with A4A.Configuration; use A4A.Configuration;
with A4A.Application.Configuration; use A4A.Application.Configuration;

with A4A.Web.Controllers.Connection_Stats_Controllers;
use A4A.Web.Controllers;

with UXStrings; use UXStrings;

package body A4A.Web.Pages.General_Status is

   procedure Main_Task_Data_Update
     (Watchdog_Error   : Boolean;
      Status           : A4A.Tasks.Task_Status_Type;
      Statistics       : A4A.Tasks.Task_Statistics_Type;
      Sched_Statistics : A4A.Tasks.Sched_Stats_Array) is
   begin
      Main_Task_Data.Watchdog_Error   := Watchdog_Error;
      Main_Task_Data.Status           := Status;
      Main_Task_Data.Statistics       := Statistics;
      Main_Task_Data.Sched_Statistics := Sched_Statistics;
   end Main_Task_Data_Update;

   procedure Periodic_Task1_Data_Update
     (Watchdog_Error : Boolean;
      Status         : A4A.Tasks.Task_Status_Type;
      Statistics     : A4A.Tasks.Task_Statistics_Type) is
   begin
      Periodic_Task1_Data.Watchdog_Error := Watchdog_Error;
      Periodic_Task1_Data.Status         := Status;
      Periodic_Task1_Data.Statistics     := Statistics;
   end Periodic_Task1_Data_Update;

   overriding
   procedure Update_Page (Web_Page : access Instance) is
      Days         : Ada.Calendar.Arithmetic.Day_Count;
      Seconds      : Duration;
      Leap_Seconds : Ada.Calendar.Arithmetic.Leap_Seconds_Count;

      Up_Time_Hours   : Integer;
      Up_Time_Minutes : Integer;
      Up_Time_Seconds : Integer;

      Sub_Seconds : Duration;

   begin

      Web_Page.Common_View.Connection_Stats_View.Update_View;

      --
      --  Application View
      --

      Ada.Calendar.Arithmetic.Difference
        (Left         => Ada.Calendar.Clock,
         Right        => Start_Time,
         Days         => Days,
         Seconds      => Seconds,
         Leap_Seconds => Leap_Seconds);

      Ada.Calendar.Formatting.Split
        (Seconds    => Seconds,
         Hour       => Up_Time_Hours,
         Minute     => Up_Time_Minutes,
         Second     => Up_Time_Seconds,
         Sub_Second => Sub_Seconds);

      Up_Time_Hours := Up_Time_Hours + Integer (Days) * 24;

      Web_Page.Application_Up_Time_View.Inner_HTML
        (From_UTF_8
           (Up_Time_Hours'Img & "h, "
            & Up_Time_Minutes'Img & "m,"
            & Up_Time_Seconds'Img & "s"));

      --
      --  Main Task Status View
      --

      Web_Page.Main_Task_View.Watchdog_View.Update_Element
        (Status  => Main_Task_Data.Watchdog_Error,
         If_True => "off", If_False => "on");

      Web_Page.Main_Task_View.Running_View.Update_Element
        (Status  => Main_Task_Data.Status.Running);

      if Main_Task_Data.Statistics.Min_Duration /=
        Web_Page.Main_Task_Data_Previous.Statistics.Min_Duration
      then
         Web_Page.Main_Task_View.Min_View.Inner_HTML
           (From_UTF_8 (Duration'Image
            (Main_Task_Data.Statistics.Min_Duration * 1000)));
      end if;

      if Main_Task_Data.Statistics.Max_Duration /=
        Web_Page.Main_Task_Data_Previous.Statistics.Max_Duration
      then
         Web_Page.Main_Task_View.Max_View.Inner_HTML
           (From_UTF_8 (Duration'Image
            (Main_Task_Data.Statistics.Max_Duration * 1000)));
      end if;

      if Main_Task_Data.Statistics.Avg_Duration /=
        Web_Page.Main_Task_Data_Previous.Statistics.Avg_Duration
      then
         Web_Page.Main_Task_View.Avg_View.Inner_HTML
           (From_UTF_8 (Duration'Image
            (Main_Task_Data.Statistics.Avg_Duration * 1000)));
      end if;

      for Index in Web_Page.Main_Task_View.Stats_View'Range loop
         if Main_Task_Data.Sched_Statistics (Index) /=
           Web_Page.Main_Task_Data_Previous.Sched_Statistics (Index)
         then
            Web_Page.Main_Task_View.Stats_View (Index).Inner_HTML
              (From_UTF_8 (Integer'Image
                 (Main_Task_Data.Sched_Statistics (Index))));
         end if;
      end loop;

      Web_Page.Main_Task_Data_Previous := Main_Task_Data;

      --
      --  Periodic Task1 Status View
      --

      Web_Page.Periodic_Task1_View.Watchdog_View.Update_Element
        (Status  => Periodic_Task1_Data.Watchdog_Error,
         If_True => "off", If_False => "on");

      Web_Page.Periodic_Task1_View.Running_View.Update_Element
        (Status  => Periodic_Task1_Data.Status.Running);

      if Periodic_Task1_Data.Statistics.Min_Duration /=
        Web_Page.Periodic_Task1_Data_Previous.Statistics.Min_Duration
      then
         Web_Page.Periodic_Task1_View.Min_View.Inner_HTML
           (From_UTF_8 (Duration'Image
              (Periodic_Task1_Data.Statistics.Min_Duration * 1000)));
      end if;

      if Periodic_Task1_Data.Statistics.Max_Duration /=
        Web_Page.Periodic_Task1_Data_Previous.Statistics.Max_Duration
      then
         Web_Page.Periodic_Task1_View.Max_View.Inner_HTML
           (From_UTF_8 (Duration'Image
              (Periodic_Task1_Data.Statistics.Max_Duration * 1000)));
      end if;

      if Periodic_Task1_Data.Statistics.Avg_Duration /=
        Web_Page.Periodic_Task1_Data_Previous.Statistics.Avg_Duration
      then
         Web_Page.Periodic_Task1_View.Avg_View.Inner_HTML
           (From_UTF_8 (Duration'Image
              (Periodic_Task1_Data.Statistics.Avg_Duration * 1000)));
      end if;

      Web_Page.Periodic_Task1_Data_Previous := Periodic_Task1_Data;

   end Update_Page;

   procedure On_Connect
     (Main_Window : in out Gnoga.Gui.Window.Window_Type'Class;
      Connection  : access
        Gnoga.Application.Multi_Connect.Connection_Holder_Type)
   is
      Web_Page : constant Instance_Access := new Instance;

      My_Ident : aliased String := "A4A.Web.Pages.General_Status_View_Updater";
      View_Updater_Task : A4A.Web.Pages.Updater_Task_Type
        (My_Ident                    => My_Ident'Access,
         Web_Page                    => Web_Page);

   begin
      Main_Window.Connection_Data (Web_Page);

      Main_Window.Document.Load_CSS (From_UTF_8 (W3_CSS_URL));
      Main_Window.Document.Load_CSS (From_UTF_8 (Font_Awesome_CSS_URL));
      Main_Window.Document.Load_CSS (From_UTF_8 (APP_CSS_URL));

      Connection_Stats_Controllers.Connection;

      Common_View_Setup (Web_Page.Common_View,
                         Main_Window'Unchecked_Access,
                         "100-general-status.html");

      --
      --  Application View
      --

      Web_Page.Application_Start_Time_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "application-start-time");

      Web_Page.Application_Start_Time_View.Inner_HTML
        (From_UTF_8
           (Ada.Calendar.Formatting.Image
           (Date                  => Start_Time,
            Include_Time_Fraction => False,
            Time_Zone             => The_Time_Offset)));

      Web_Page.Application_Up_Time_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "application-up-time");
      Web_Page.Application_Up_Time_View.Inner_HTML ("0h, 0m, 0s");

      --
      --  Main Task Status View
      --

      Web_Page.Main_Task_View.Type_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "main-task-type");

      Web_Page.Main_Task_View.Sched_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "main-task-scheduling");

      if Application_Main_Task_Type = Cyclic then
         Web_Page.Main_Task_View.Type_View.Inner_HTML ("Cyclic type");
         Web_Page.Main_Task_View.Sched_View.Inner_HTML
           (From_UTF_8
              ("Delay : "
               & Integer'Image (Application_Main_Task_Delay_MS)
               & " ms"));
      elsif Application_Main_Task_Type = Periodic then
         Web_Page.Main_Task_View.Type_View.Inner_HTML ("Periodic type");
         Web_Page.Main_Task_View.Sched_View.Inner_HTML
           (From_UTF_8
              ("Period : "
               & Integer'Image (Application_Main_Task_Period_MS)
               & " ms"));
      end if;

      Web_Page.Main_Task_View.Watchdog_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "main-task-watchdog");

      Web_Page.Main_Task_View.Running_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "main-task-running");

      Web_Page.Main_Task_View.Min_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "main-task-min");

      Web_Page.Main_Task_View.Max_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "main-task-max");

      Web_Page.Main_Task_View.Avg_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "main-task-avg");

      for Index in Web_Page.Main_Task_View.Stats_View'Range loop
         Web_Page.Main_Task_View.Stats_View (Index).Attach_Using_Parent
           (Web_Page.Common_View.View, "main-task-th"
            & Gnoga.Left_Trim (From_UTF_8 (Index'Img)));
      end loop;

      --
      --  Periodic Task1 Status View
      --

      Web_Page.Periodic_Task1_View.Period_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "periodic-task1-period");

      Web_Page.Periodic_Task1_View.Period_View.Inner_HTML
        (From_UTF_8
           ("Period : "
            & Integer'Image (Application_Periodic_Task_1_Period_MS)
            & " ms"));

      Web_Page.Periodic_Task1_View.Watchdog_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "periodic-task1-watchdog");

      Web_Page.Periodic_Task1_View.Running_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "periodic-task1-running");

      Web_Page.Periodic_Task1_View.Min_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "periodic-task1-min");

      Web_Page.Periodic_Task1_View.Max_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "periodic-task1-max");

      Web_Page.Periodic_Task1_View.Avg_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "periodic-task1-avg");

      View_Updater_Task.Start;

      Connection.Hold;

      View_Updater_Task.Stop;

   end On_Connect;

end A4A.Web.Pages.General_Status;
