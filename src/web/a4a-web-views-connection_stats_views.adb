
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with UXStrings; use UXStrings;

package body A4A.Web.Views.Connection_Stats_Views is
   use Connection_Stats_Controllers;

   procedure Update_View (Object : in out Instance)
   is
      Status : Connection_Stats_Controllers.Status_Type;
   begin
      Status := Connection_Stats_Controllers.Get_Status;
      if Object.First_Time or Status /= Object.Status_Previous then

         if Object.First_Time then
            Object.First_Time := False;
         end if;

         if Status.Current_Connections_Number_Value /=
           Object.Status_Previous.Current_Connections_Number_Value
         then
            Object.Current_Connections_Number_View.Inner_HTML
              (From_UTF_8 (Status.Current_Connections_Number_Value'Img));
         end if;

         if Status.Connections_Number_Value /=
           Object.Status_Previous.Connections_Number_Value
         then
            Object.Connections_Number_View.Inner_HTML
              (From_UTF_8 (Status.Connections_Number_Value'Img));
         end if;

         if Status.Disconnections_Number_Value /=
           Object.Status_Previous.Disconnections_Number_Value
         then
            Object.Disconnections_Number_View.Inner_HTML
              (From_UTF_8 (Status.Disconnections_Number_Value'Img));
         end if;

         if Status.Connections_Number_Max_Value /=
           Object.Status_Previous.Connections_Number_Max_Value
         then
            Object.Connections_Number_Max_View.Inner_HTML
              (From_UTF_8 (Status.Connections_Number_Max_Value'Img));
         end if;
         Object.Status_Previous := Status;
      end if;
   end Update_View;

   procedure Setup
     (Object : in out Instance;
      Parent     : in Gnoga.Gui.Base.Base_Type'Class;
      Current_Connections_Number_View_Id    : in String;
      Connections_Number_View_Id            : in String;
      Disconnections_Number_View_Id         : in String;
      Connections_Number_Max_View_Id        : in String)
   is
   begin
      Object.Current_Connections_Number_View.Attach_Using_Parent
        (Parent, From_UTF_8 (Current_Connections_Number_View_Id));

      Object.Connections_Number_View.Attach_Using_Parent
        (Parent, From_UTF_8 (Connections_Number_View_Id));

      Object.Disconnections_Number_View.Attach_Using_Parent
        (Parent, From_UTF_8 (Disconnections_Number_View_Id));

      Object.Connections_Number_Max_View.Attach_Using_Parent
        (Parent, From_UTF_8 (Connections_Number_Max_View_Id));

   end Setup;
end A4A.Web.Views.Connection_Stats_Views;
