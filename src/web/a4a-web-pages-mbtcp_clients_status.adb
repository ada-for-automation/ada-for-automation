
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Unchecked_Deallocation;

with A4A.Log;
with A4A.Logger; use A4A.Logger;

with A4A.Protocols; use A4A.Protocols.IP_Address_Strings;
with A4A.Protocols.LibModbus; use A4A.Protocols.LibModbus;
with A4A.Kernel.Main; use A4A.Kernel.Main;

with A4A.Web.Controllers.Connection_Stats_Controllers;
use A4A.Web.Controllers;

with UXStrings; use UXStrings;

package body A4A.Web.Pages.MBTCP_Clients_Status is

   type Side_Type is
     (On_Left,
      On_Right);

   function To_HTML (Command : in Command_Type;
                     Side    : in Side_Type) return String;
   function To_HTML (Command : in Command_Type;
                     Side    : in Side_Type) return String is
      function Action_To_HTML return String;
      function Action_To_HTML return String is
      begin
         case Command.Action is

            when Read_Bits | Read_Input_Bits | Read_Registers |
                 Read_Input_Registers | Write_Bit | Write_Register |
                 Write_Bits | Write_Registers =>

               return
                 "<table class=""w3-table w3-small"">"
                 & "<tr>"
                 & "<th>Number</th>"
                 & "<td class=""w3-right-align"">"
                 & Command.Number'Img & "</td>"
                 & "</tr>"
                 & "<tr>"
                 & "<th>Offset Remote</th>"
                 & "<td class=""w3-right-align"">"
                 & Command.Offset_Remote'Img & "</td>"
                 & "</tr>"
                 & "<tr>"
                 & "<th>Offset Local</th>"
                 & "<td class=""w3-right-align"">"
                 & Command.Offset_Local'Img & "</td>"
                 & "</tr>"
                 & "</table>";

            when Write_Read_Registers =>

               return
                 "<table class=""w3-table w3-small"">"
                 & "<tr>"
                 & "<th>Write Number</th>"
                 & "<td class=""w3-right-align"">"
                 & Command.Write_Number'Img & "</td>"
                 & "</tr>"
                 & "<tr>"
                 & "<th>Write Offset Remote</th>"
                 & "<td class=""w3-right-align"">"
                 & Command.Write_Offset_Remote'Img & "</td>"
                 & "</tr>"
                 & "<tr>"
                 & "<th>Write Offset Local</th>"
                 & "<td class=""w3-right-align"">"
                 & Command.Write_Offset_Local'Img & "</td>"
                 & "</tr>"

                 & "<tr>"
                 & "<th>Read Number</th>"
                 & "<td class=""w3-right-align"">"
                 & Command.Read_Number'Img & "</td>"
                 & "</tr>"
                 & "<tr>"
                 & "<th>Read Offset Remote</th>"
                 & "<td class=""w3-right-align"">"
                 & Command.Read_Offset_Remote'Img & "</td>"
                 & "</tr>"
                 & "<tr>"
                 & "<th>Read Offset Local</th>"
                 & "<td class=""w3-right-align"">"
                 & Command.Read_Offset_Local'Img & "</td>"
                 & "</tr>"
                 & "</table>";

         end case;
      end Action_To_HTML;

      function Tooltip_Class (Side : in Side_Type) return String;
      function Tooltip_Class (Side : in Side_Type) return String is
         function Size_Class return String;
         function Size_Class return String is
         begin
            if Command.Action = Write_Read_Registers then
               return "tooltiptextbig";
            else
               return "tooltiptextsmall";
            end if;
         end Size_Class;

         function Side_Class return String;
         function Side_Class return String is
         begin
            case Side is
            when On_Left => return "tooltiptextleft";
            when On_Right => return "tooltiptextright";
            end case;
         end Side_Class;
      begin
         return Size_Class & " " & Side_Class;
      end Tooltip_Class;

   begin
      return
        "<div class=""" & Tooltip_Class (Side) & """>"
        & "<h3>" & Command.Action'Img & "</h3>"
        & " " & Action_To_HTML
        & "</div>";
   end To_HTML;

   procedure Create_Client_Status_View
     (Index              : Positive;
      Parent             : in out Gnoga.Gui.Element.Element_Type;
      Client_Status_View : in out Client_Status_View_Type) is
      My_Ident : constant String :=
        "A4A.Web.Pages.MBTCP_Clients_Status.Create_Client_Status_View";

      Client_View_Data : constant Client_Status_View_Data_Access :=
        Client_Status_View.Client_View_Data'Unrestricted_Access;

      Config : constant Client_Configuration_Access :=
        MBTCP_Clients_Configuration (Index);

      Command_Index : Positive;

      Index_Str : constant String := To_UTF_8
        (Gnoga.Left_Trim (From_UTF_8 (Index'Img)));
      Index_PN  : Natural;

      TR_Access : Gnoga.Gui.Element.Table.Table_Row_Access;
      TH_Access : Gnoga.Gui.Element.Table.Table_Heading_Access;
      TC_Access : Gnoga.Gui.Element.Table.Table_Column_Access;

      Side : Side_Type;
   begin

      A4A.Log.Logger.Put (Who       => My_Ident,
                          What      => "Trace !",
                          Log_Level => Level_Verbose);

      --  Remember which client we are connected to
      Client_Status_View.Index := Index;

      Client_View_Data.Task_Status :=
        new A4A.MBTCP_Client.Task_Status_Type (Config.Command_Number);

      Client_View_Data.Commands_Status :=
        new A4A.MBTCP_Client.Commands_Status_Type (Config.Commands'Range);
      Client_View_Data.Commands_Status.all := (others => Unknown);

      Client_Status_View.Commands_Status_Indicators :=
        new Commands_Status_Indicators_Type (Config.Commands'Range);

      Client_View_Data.Rows_Number := 1 + ((Config.Command_Number - 1) / 10);

      if Config.Command_Number >= 10 then
         Client_View_Data.Columns_Number := 10;
      else
         Client_View_Data.Columns_Number := Config.Command_Number;
      end if;

      Client_Status_View.Section1.Create
        (Parent  => Parent,
         Section => Gnoga.Gui.Element.Section.Section,
         ID      => From_UTF_8 ("client" & Index_Str));
      Client_Status_View.Section1.Add_Class ("w3-section");
      Client_Status_View.Section1.Place_Inside_Bottom_Of (Parent);

      Client_Status_View.Header1.Create
        (Parent  => Parent,
         Section => Gnoga.Gui.Element.Section.Header);
      Client_Status_View.Header1.Add_Class
        ("w3-container w3-light-blue w3-padding-8");
      Client_Status_View.Header1.Place_Inside_Top_Of
        (Client_Status_View.Section1);

      Client_Status_View.Title_View.Create
        (Parent  => Parent,
         Section => Gnoga.Gui.Element.Section.H1);
      Client_Status_View.Title_View.Inner_HTML
        (From_UTF_8
           ("Client " & Index_Str & " | "
            & To_String (MBTCP_Clients_Configuration (Index).Node)
            & ":")
         & Gnoga.Left_Trim
           (From_UTF_8
                (To_String ((MBTCP_Clients_Configuration (Index).Service)))));
      Client_Status_View.Title_View.Place_Inside_Top_Of
        (Client_Status_View.Header1);

      if Index = MBTCP_Clients_Configuration'First then
         Client_Status_View.A_Previous.Create
           (Parent  => Parent,
            Link    => "#clients",
            Content => "<i class=""fa fa-arrow-up w3-large w3-right""></i>");
      else
         Index_PN := Index - 1;
         Client_Status_View.A_Previous.Create
           (Parent  => Parent,
            Link    => "#client" & Gnoga.Left_Trim (From_UTF_8 (Index_PN'Img)),
            Content => "<i class=""fa fa-arrow-up w3-large w3-right""></i>");
      end if;

      Client_Status_View.A_Previous.Place_Inside_Bottom_Of
        (Client_Status_View.Header1);

      if Index = MBTCP_Clients_Configuration'Last then
         Client_Status_View.A_Next.Create
           (Parent  => Parent,
            Link    => "#bottom",
            Content => "<i class=""fa fa-arrow-down w3-large w3-right""></i>");
      else
         Index_PN := Index + 1;
         Client_Status_View.A_Next.Create
           (Parent  => Parent,
            Link    => "#client" & Gnoga.Left_Trim (From_UTF_8 (Index_PN'Img)),
            Content => "<i class=""fa fa-arrow-down w3-large w3-right""></i>");
      end if;

      Client_Status_View.A_Next.Place_Inside_Bottom_Of
        (Client_Status_View.Header1);

      Client_Status_View.Div1.Create
        (Parent  => Parent);
      Client_Status_View.Div1.Add_Class ("w3-row");
      Client_Status_View.Div1.Place_Inside_Bottom_Of
        (Client_Status_View.Section1);

      Client_Status_View.Div_WD.Create
        (Parent  => Parent,
         Content => "<h3 style=""width:120px; padding: 10px;"
         & "text-align: center"">Watchdog</h3>");
      Client_Status_View.Div_WD.Add_Class ("w3-container w3-half");
      Client_Status_View.Div_WD.Style ("width", "140px");

      Client_Status_View.Watchdog_View.Create
        (Parent  => Parent,
         Section => Gnoga.Gui.Element.Section.P,
         ID      => From_UTF_8 ("client" & Index_Str & "-task-watchdog"));
      Client_Status_View.Watchdog_View.Add_Class ("a4a-status1");
      Client_Status_View.Watchdog_View.Attribute ("status", "off");
      Client_Status_View.Watchdog_View.Inner_HTML ("Status");
      Client_Status_View.Watchdog_View.Place_Inside_Bottom_Of
        (Client_Status_View.Div_WD);

      Client_Status_View.Div_WD.Place_Inside_Top_Of
        (Client_Status_View.Div1);

      Client_Status_View.Div_CNX.Create
        (Parent  => Parent,
         Content => "<h3 style=""width:120px; padding: 10px;"
         & "text-align: center"">Connection</h3>");
      Client_Status_View.Div_CNX.Add_Class ("w3-container w3-half");
      Client_Status_View.Div_CNX.Style ("width", "140px");

      Client_Status_View.CNX_View.Create
        (Parent  => Parent,
         Section => Gnoga.Gui.Element.Section.P,
         ID      => From_UTF_8 ("client" & Index_Str & "-task-connection"));
      Client_Status_View.CNX_View.Add_Class ("a4a-status1");
      Client_Status_View.CNX_View.Attribute ("status", "off");
      Client_Status_View.CNX_View.Inner_HTML ("Status");
      Client_Status_View.CNX_View.Place_Inside_Bottom_Of
        (Client_Status_View.Div_CNX);

      Client_Status_View.Div_CNX.Place_Inside_Bottom_Of
        (Client_Status_View.Div1);

      Client_Status_View.Div2.Create
        (Parent  => Parent,
         Content => "<h3>Commands Status</h3>");
      Client_Status_View.Div2.Add_Class ("w3-container");
      Client_Status_View.Div2.Style ("max-width", "500px");
      Client_Status_View.Div2.Place_Inside_Bottom_Of
        (Client_Status_View.Section1);

      --  One raw for columns labels
      --  One raw for each 10 commands
      --  One column for raws labels
      --  1 to 10 columns for commands

      Client_Status_View.Cmd_Table.Create
        (Parent => Parent);
      Client_Status_View.Cmd_Table.Add_Class
        ("w3-bordered w3-card-4 a4a-status2-table");

      Client_Status_View.Cmd_Table_Head.Create
        (Table => Client_Status_View.Cmd_Table);

      A4A.Log.Logger.Put (Who       => My_Ident,
                          What      => "I'm Fine!",
                          Log_Level => Level_Verbose);

      TR_Access := new Gnoga.Gui.Element.Table.Table_Row_Type;
      TR_Access.Dynamic;
      TR_Access.Create (Client_Status_View.Cmd_Table_Head);

      TH_Access := new Gnoga.Gui.Element.Table.Table_Heading_Type;
      TH_Access.Dynamic;
      TH_Access.Create (TR_Access.all);

      for Col_Index in 1 .. Client_View_Data.Columns_Number loop
         TH_Access := new Gnoga.Gui.Element.Table.Table_Heading_Type;
         TH_Access.Dynamic;
         TH_Access.Create (TR_Access.all,
                           Content =>
                             Gnoga.Left_Trim (From_UTF_8 (Col_Index'Img)));
      end loop;

      Command_Index := 1;
      for Row_Index in 1 .. Client_View_Data.Rows_Number loop

         TR_Access := new Gnoga.Gui.Element.Table.Table_Row_Type;
         TR_Access.Dynamic;
         TR_Access.Create (Client_Status_View.Cmd_Table);

         TH_Access := new Gnoga.Gui.Element.Table.Table_Heading_Type;
         TH_Access.Dynamic;
         TH_Access.Create
           (TR_Access.all,
            Content =>
              Gnoga.Left_Trim
                (From_UTF_8 (Integer'Image ((Row_Index - 1) * 10))));

         for Col_Index in 1 .. Client_View_Data.Columns_Number loop

            TC_Access := new Gnoga.Gui.Element.Table.Table_Column_Type;
            TC_Access.Dynamic;
            TC_Access.Create (TR_Access.all);

            if Col_Index < 8 then
               Side := On_Right;
            else
               Side := On_Left;
            end if;

            Client_Status_View.Commands_Status_Indicators (Command_Index)
              .Create
                (Parent  => Client_Status_View.Cmd_Table,
                 Content => From_UTF_8
                   (To_HTML (Config.Commands (Command_Index), Side)),
                 ID      => "client"
                 & Gnoga.Left_Trim (From_UTF_8 (Client_Status_View.Index'Img))
                 & "-cmd" & Gnoga.Left_Trim (From_UTF_8 (Command_Index'Img)));

            Client_Status_View.Commands_Status_Indicators (Command_Index)
              .Place_Inside_Top_Of (TC_Access.all);

            Client_Status_View.Commands_Status_Indicators (Command_Index)
              .Add_Class ("a4a-status2 tooltip");

            Client_Status_View.Commands_Status_Indicators (Command_Index)
              .Attribute ("status", From_UTF_8 (Command_Status_Unknown));

            Command_Index := Command_Index + 1;

            exit when
              Command_Index >
                Client_Status_View.Commands_Status_Indicators'Last;

         end loop;
      end loop;

      Client_Status_View.Cmd_Table.Place_Inside_Bottom_Of
        (Client_Status_View.Div2);

--  <section id="client1" class="w3-section">
--
--      <header class="w3-container w3-light-blue w3-padding-8">
--          <h1>Client 1 : 192.168.0.100:502</h1>
--          <a href="#clients">
--          <a href="#client2">
--            <i class="fa fa-arrow-down w3-large w3-right"></i></a>
--      </header>
--
--      <div class="w3-row">
--          <div class="w3-container w3-half" style="width:140px;">
--              <h3 style="width:120px; padding: 10px; text-align: center">
--                Watchdog</h3>
--              <p id="client1-task-watchdog" class="a4a-status1" status="off">
--                Status</p>
--          </div>
--
--          <div class="w3-container w3-half" style="width:140px;">
--              <h3 style="width:120px; padding: 10px; text-align: center">
--                Connection</h3>
--              <p id="client1-task-connection"
--                class="a4a-status1" status="off">Status</p>
--          </div>
--      </div>
--
--      <div class="w3-container" style="max-width:300px">
--          <h3>Commands Status</h3>
--          <table class="w3-striped w3-bordered w3-card-4 a4a-status2-table">
--            <tr>
--              <td></td>
--              <td>1</td>
--              <td>2</td>
--              <td>3</td>
--              <td>4</td>
--              <td>5</td>
--            </tr>
--            <tr>
--              <td>0</td>
--              <td>
--                <div id="client1-command1" class="a4a-status2" status="off">
--                </div></td>
--              <td>
--                <div id="client1-command2" class="a4a-status2" status="off">
--                </div></td>
--              <td>
--                <div id="client1-command3" class="a4a-status2" status="off">
--                </div></td>
--              <td>
--                <div id="client1-command4" class="a4a-status2" status="off">
--                </div></td>
--              <td>
--                <div id="client1-command5" class="a4a-status2" status="off">
--                </div></td>
--            </tr>
--          </table>
--      </div>
--
--  </section>

      A4A.Log.Logger.Put (Who       => My_Ident,
                          What      => "Trace End!",
                          Log_Level => Level_Verbose);

   end Create_Client_Status_View;

   procedure Client_Status_View_Update
     (Client_Status_View : in out Client_Status_View_Type;
      Index              : Positive) is

      Client_View_Data : constant Client_Status_View_Data_Access :=
        Client_Status_View.Client_View_Data'Unrestricted_Access;

      Command_Index : Positive;
   begin

      if The_Main_Task_Interface.MClients_Status (Index).WD_Error
        /= Client_View_Data.Watchdog_Error
      then
         if The_Main_Task_Interface.MClients_Status (Index).WD_Error then
            Client_Status_View.Watchdog_View.Attribute ("status", "off");
         else
            Client_Status_View.Watchdog_View.Attribute ("status", "on");
         end if;
      end if;
      Client_View_Data.Watchdog_Error :=
        The_Main_Task_Interface.MClients_Status (Index).WD_Error;

      Client_View_Data.Task_Status.all :=
        MBTCP_Clients_Tasks_Itf (Index).Status.Get_Status;

      if Client_View_Data.Task_Status.Connected /= Client_View_Data.Connected
      then
         if not Client_View_Data.Task_Status.Connected then
            Client_Status_View.CNX_View.Attribute ("status", "off");
         else
            Client_Status_View.CNX_View.Attribute ("status", "on");
         end if;
      end if;
      Client_View_Data.Connected := Client_View_Data.Task_Status.Connected;

      Command_Index := 1;
      for Row_Index in 1 .. Client_View_Data.Rows_Number loop
         for Col_Index in 1 .. Client_View_Data.Columns_Number loop

            if Client_View_Data.Task_Status.Commands_Status (Command_Index)
              /= Client_View_Data.Commands_Status (Command_Index)
            then

               case Client_View_Data
                 .Task_Status.Commands_Status (Command_Index)
               is

                  when Unknown =>
                     Client_Status_View.Commands_Status_Indicators
                       (Command_Index).Attribute
                       ("status", From_UTF_8 (Command_Status_Unknown));

                  when Disabled =>
                     Client_Status_View.Commands_Status_Indicators
                       (Command_Index).Attribute
                       ("status", From_UTF_8 (Command_Status_Disabled));

                  when Fine =>
                     Client_Status_View.Commands_Status_Indicators
                       (Command_Index).Attribute
                       ("status", From_UTF_8 (Command_Status_Fine));

                  when Fault =>
                     Client_Status_View.Commands_Status_Indicators
                       (Command_Index).Attribute
                       ("status", From_UTF_8 (Command_Status_Fault));

               end case;

               Client_View_Data.Commands_Status (Command_Index) :=
                 Client_View_Data.Task_Status.Commands_Status (Command_Index);
            end if;

            Command_Index := Command_Index + 1;

            exit when
              Command_Index >
                Client_Status_View.Commands_Status_Indicators'Last;

         end loop;
      end loop;

   end Client_Status_View_Update;

   procedure Client_Status_View_Finalise
     (Client_Status_View : in out Client_Status_View_Type) is

      procedure Free_Task_Status is new Ada.Unchecked_Deallocation
        (Object => A4A.MBTCP_Client.Task_Status_Type,
         Name   => A4A.MBTCP_Client.Task_Status_Access);

      procedure Free_Commands_Status is new Ada.Unchecked_Deallocation
        (Object => A4A.MBTCP_Client.Commands_Status_Type,
         Name   => A4A.MBTCP_Client.Commands_Status_Access);

      procedure Free_Commands_Status_Indicators is
        new Ada.Unchecked_Deallocation
        (Object => Commands_Status_Indicators_Type,
         Name   => Commands_Status_Indicators_Access);

   begin
      Free_Task_Status
        (Client_Status_View.Client_View_Data.Task_Status);

      Free_Commands_Status
        (Client_Status_View.Client_View_Data.Commands_Status);

      Free_Commands_Status_Indicators
        (Client_Status_View.Commands_Status_Indicators);
   end Client_Status_View_Finalise;

   procedure Finalise_View
     (Clients_Status_View : in out Clients_Status_View_Type) is
      My_Ident : constant String :=
        "A4A.Web.Pages.MBTCP_Clients_Status.Finalise_View";

   begin

      A4A.Log.Logger.Put (Who       => My_Ident,
                          What      => "Trace !",
                          Log_Level => Level_Verbose);

      for Index in Clients_Status_View'Range loop

         Client_Status_View_Finalise (Clients_Status_View (Index));

      end loop;

   end Finalise_View;

   overriding
   procedure Update_Page (Web_Page : access Instance) is

   begin

      Web_Page.Common_View.Connection_Stats_View.Update_View;

      for Index in Web_Page.Clients_Status_View'Range
      loop

         Client_Status_View_Update
           (Web_Page.Clients_Status_View (Index), Index);

      end loop;

   end Update_Page;

   procedure On_Connect
     (Main_Window : in out Gnoga.Gui.Window.Window_Type'Class;
      Connection  : access
        Gnoga.Application.Multi_Connect.Connection_Holder_Type)
   is
      Web_Page : constant Instance_Access := new Instance;

      My_Ident : aliased String :=
        "A4A.Web.Pages.MBTCP_Clients_Status_View_Updater";
      View_Updater_Task : A4A.Web.Pages.Updater_Task_Type
        (My_Ident                    => My_Ident'Access,
         Web_Page                    => Web_Page);

   begin
      Main_Window.Connection_Data (Web_Page);

      Main_Window.Document.Load_CSS (From_UTF_8 (W3_CSS_URL));
      Main_Window.Document.Load_CSS (From_UTF_8 (Font_Awesome_CSS_URL));
      Main_Window.Document.Load_CSS (From_UTF_8 (APP_CSS_URL));
      Main_Window.Document.Load_CSS (From_UTF_8 (TOOLTIP_CSS_URL));

      Connection_Stats_Controllers.Connection;

      Common_View_Setup (Web_Page.Common_View,
                         Main_Window'Unchecked_Access,
                         "120-mbtcp-clients-status.html");
      --
      --  Modbus TCP Clients View
      --

      Web_Page.Clients_Status_Parent_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "mbtcp-clients-status");

      for I in Web_Page.Clients_Status_View'Range loop
         Create_Client_Status_View
           (I, Web_Page.Clients_Status_Parent_View,
            Web_Page.Clients_Status_View (I));
      end loop;

      View_Updater_Task.Start;

      Connection.Hold;

      View_Updater_Task.Stop;

      Finalise_View (Web_Page.Clients_Status_View);

   end On_Connect;

end A4A.Web.Pages.MBTCP_Clients_Status;
