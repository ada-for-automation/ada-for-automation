
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

package A4A.Web.Controllers.Connection_Stats_Controllers is

   type Status_Type is
      record
         Current_Connections_Number_Value : Natural := 0;
         Connections_Number_Value         : Natural := 0;
         Disconnections_Number_Value      : Natural := 0;
         Connections_Number_Max_Value     : Natural := 0;
      end record;

   procedure Connection;

   procedure Disconnection;

   function Get_Status return Status_Type;

private
   type Instance is tagged
      record
         Status : Status_Type;
      end record;

   type Instance_Access is access all Instance;

   procedure Connection (Object : in out Instance);

   procedure Disconnection (Object : in out Instance);

   function Get_Status (Object : in out Instance)
                        return Status_Type;

   procedure Update (Object : in out Instance) is null;

   Connection_Stats_Controller : Connection_Stats_Controllers.Instance;

end A4A.Web.Controllers.Connection_Stats_Controllers;
