
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Gnoga.Gui.Window; use Gnoga.Gui;
with Gnoga.Gui.View;
with Gnoga.Gui.Element; use Gnoga.Gui.Element;

with A4A.Web.Views.Connection_Stats_Views;
use A4A.Web.Views;

package A4A.Web.Pages.Common_View is

   type Common_View_Type is
      record
         Main_Window : Window.Pointer_To_Window_Class;
         View        : aliased Gnoga.Gui.View.View_Type;

         Header_App_Name_View    : Gnoga.Gui.Element.Element_Type;

         About_Box_App_Name_View    : Gnoga.Gui.Element.Element_Type;
         About_Box_App_Desc_View    : Gnoga.Gui.Element.Element_Type;
         About_Box_App_Version_View : Gnoga.Gui.Element.Element_Type;

         Connection_Stats_View      : Connection_Stats_Views.Instance;
      end record;

   procedure Common_View_Setup
     (Common_View  : in out Common_View_Type;
      Main_Window  : in Window.Pointer_To_Window_Class;
      Main_Content : in String);

end A4A.Web.Pages.Common_View;
