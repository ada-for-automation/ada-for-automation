
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Gnoga.Gui.Base;
with Gnoga.Gui.Element;

with A4A.Web.Controllers.Connection_Stats_Controllers; use A4A.Web.Controllers;

package A4A.Web.Views.Connection_Stats_Views is
   type Instance is tagged limited private;

   procedure Update_View (Object : in out Instance);
   procedure Setup
     (Object : in out Instance;
      Parent     : in Gnoga.Gui.Base.Base_Type'Class;
      Current_Connections_Number_View_Id    : in String;
      Connections_Number_View_Id            : in String;
      Disconnections_Number_View_Id         : in String;
      Connections_Number_Max_View_Id        : in String);
private
   type Instance is tagged limited
      record
         Current_Connections_Number_View : Gnoga.Gui.Element.Element_Type;
         Connections_Number_View         : Gnoga.Gui.Element.Element_Type;
         Disconnections_Number_View      : Gnoga.Gui.Element.Element_Type;
         Connections_Number_Max_View     : Gnoga.Gui.Element.Element_Type;

         Status_Previous      :  Connection_Stats_Controllers.Status_Type;

         First_Time : Boolean := True;
      end record;

end A4A.Web.Views.Connection_Stats_Views;
