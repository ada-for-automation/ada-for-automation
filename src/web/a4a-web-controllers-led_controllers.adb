
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

package body A4A.Web.Controllers.LED_Controllers is

   function Get_Status (Object : in out Instance) return Status_Type is
   begin
      return Object.Status;
   end Get_Status;

   procedure Set_Model (Object      : in out Instance;
                        Status_Bit  : access Boolean := null) is
   begin
      Object.Status_Bit  := Status_Bit;
   end Set_Model;

   procedure Update (Object : in out Instance) is
   begin
      if Object.Status_Bit /= null and then Object.Status_Bit.all then
         Object.Status := On;
      else
         Object.Status := Off;
      end if;
   end Update;

end A4A.Web.Controllers.LED_Controllers;
