
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Gnoga.Application.Multi_Connect; use Gnoga;
with Gnoga.Gui.Window; use Gnoga.Gui;
with Gnoga.Gui.Element; use Gnoga.Gui.Element;

with A4A.Tasks;

with A4A.Web.Elements.Bool_Elements;
use A4A.Web.Elements;

with A4A.Web.Pages.Common_View;
use A4A.Web.Pages.Common_View;

package A4A.Web.Pages.General_Status is

   type Instance is new A4A.Web.Pages.Instance with private;

   procedure Main_Task_Data_Update
     (Watchdog_Error   : Boolean;
      Status           : A4A.Tasks.Task_Status_Type;
      Statistics       : A4A.Tasks.Task_Statistics_Type;
      Sched_Statistics : A4A.Tasks.Sched_Stats_Array);

   procedure Periodic_Task1_Data_Update
     (Watchdog_Error : Boolean;
      Status         : A4A.Tasks.Task_Status_Type;
      Statistics     : A4A.Tasks.Task_Statistics_Type);

   procedure On_Connect
     (Main_Window : in out Gnoga.Gui.Window.Window_Type'Class;
      Connection  : access
        Gnoga.Application.Multi_Connect.Connection_Holder_Type);

private

   type Main_Task_Data_Type is
      record
         Watchdog_Error   : Boolean := True;
         Status           : A4A.Tasks.Task_Status_Type;
         Statistics       : A4A.Tasks.Task_Statistics_Type;
         Sched_Statistics : A4A.Tasks.Sched_Stats_Array;
      end record;

   type Main_Task_Stats_View_Type is array (A4A.Tasks.Sched_Stats_Array'Range)
     of Gnoga.Gui.Element.Element_Type;

   type Main_Task_View_Type is
      record
         Type_View     : Gnoga.Gui.Element.Element_Type;
         Sched_View    : Gnoga.Gui.Element.Element_Type;
         Watchdog_View : Bool_Elements.Instance;
         Running_View  : Bool_Elements.Instance;
         Min_View      : Gnoga.Gui.Element.Element_Type;
         Max_View      : Gnoga.Gui.Element.Element_Type;
         Avg_View      : Gnoga.Gui.Element.Element_Type;

         Stats_View    : Main_Task_Stats_View_Type;
      end record;

   type Periodic_Task_Data_Type is
      record
         Watchdog_Error   : Boolean := True;
         Status           : A4A.Tasks.Task_Status_Type;
         Statistics       : A4A.Tasks.Task_Statistics_Type;
      end record;

   type Periodic_Task_View_Type is
      record
         Period_View   : Gnoga.Gui.Element.Element_Type;
         Watchdog_View : Bool_Elements.Instance;
         Running_View  : Bool_Elements.Instance;
         Min_View      : Gnoga.Gui.Element.Element_Type;
         Max_View      : Gnoga.Gui.Element.Element_Type;
         Avg_View      : Gnoga.Gui.Element.Element_Type;
      end record;

   type Instance is new A4A.Web.Pages.Instance with
      record
         Common_View : Common_View_Type;

         Application_Start_Time_View : Gnoga.Gui.Element.Element_Type;
         Application_Up_Time_View    : Gnoga.Gui.Element.Element_Type;

         Main_Task_View          : Main_Task_View_Type;
         Main_Task_Data_Previous : Main_Task_Data_Type;

         Periodic_Task1_View     : Periodic_Task_View_Type;
         Periodic_Task1_Data_Previous : Periodic_Task_Data_Type;

      end record;
   type Instance_Access is access all Instance;
   type Pointer_to_Instance_Class is access all Instance'Class;

   overriding
   procedure Update_Page (Web_Page : access Instance);

   Start_Time : constant Ada.Calendar.Time := Ada.Calendar.Clock;

   Main_Task_Data : Main_Task_Data_Type;

   Periodic_Task1_Data : Periodic_Task_Data_Type;

end A4A.Web.Pages.General_Status;
