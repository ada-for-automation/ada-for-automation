
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

   --  The Switch3 has 3 positions
   --  There are 3 areas allowing the position selection
   --  Two Command bits are driven by the position

package A4A.Web.Controllers.Switch3_Controllers is

   type Status_Type is (Pos0, Pos1, Pos2);

   type Instance is tagged private;
   type Instance_Access is access all Instance;

   procedure Pos0 (Object : in out Instance);
   procedure Pos1 (Object : in out Instance);
   procedure Pos2 (Object : in out Instance);
   function Get_Status (Object : in out Instance) return Status_Type;

   procedure Set_Model (Object       : in out Instance;
                        Command_Bit1 : access Boolean := null;
                        Command_Bit2 : access Boolean := null);

   procedure Update (Object : in out Instance);

private
   My_Ident : constant String := "A4A.Web.Controllers.Switch3_Controllers";
   type Instance is tagged
      record
         Status : Status_Type := Pos0;

         Command_Bit1 : access Boolean;
         Command_Bit2 : access Boolean;

      end record;
end A4A.Web.Controllers.Switch3_Controllers;
