
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with UXStrings; use UXStrings;

package body A4A.Web.Elements.Float_Elements is

   procedure Update_Element
     (Object : in out Instance;
      Value  : in Float;
      Aft    : in Field := Ada.Float_Text_IO.Default_Aft;
      Exp    : in Field := Ada.Float_Text_IO.Default_Exp) is
      Value_Str : String (1 .. 30);
   begin
      if Object.First_Time or Value /= Object.Value_Previous then

         if Object.First_Time then
            Object.First_Time := False;
         end if;

         Ada.Float_Text_IO.Put (To  => Value_Str, Item => Value,
                                Aft => Aft, Exp => Exp);
         Object.Inner_HTML (Gnoga.Left_Trim (From_UTF_8 (Value_Str)));

         Object.Value_Previous := Value;
      end if;
   end Update_Element;

end A4A.Web.Elements.Float_Elements;
