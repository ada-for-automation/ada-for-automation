
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with A4A.Library.Devices.Valve; use A4A.Library.Devices;
with A4A.Web.Controllers.IPB_Controllers; use A4A.Web.Controllers;

package A4A.Web.Controllers.Valve_Controllers is
   use IPB_Controllers;

   type Status_Type is (Closed, Open, Faulty);

   type Valve_Status_Record_Type is
      record
         Valve_Status : Status_Type;
         IO_Status    : Valve.IO_Status_Type;
         Cmd_Status   : IPB_Controllers.Status_Type;
      end record;

   type Instance is tagged private;
   type Instance_Access is access all Instance;

   procedure Toggle (Object : in out Instance);

   function Get_Status (Object : in out Instance)
                        return Status_Type;

   function Get_Status (Object : in out Instance)
                        return Valve_Status_Record_Type;

   function Get_Device_Id (Object : in out Instance)
                           return String;

   function Get_Cmd_Ctrl (Object : in out Instance)
                        return IPB_Controllers.Instance_Access;

   procedure Set_Model (Object : in out Instance;
                        Valve_Model  : access Valve.Instance := null;
                        Cmd_Model    : access Boolean := null);

   procedure Update (Object : in out Instance);

private
   type Instance is tagged
      record
         Status : Valve_Status_Record_Type :=
            (Closed, (False, False, False), Off);

         Valve_Model  : access Valve.Instance := null;
         Cmd          : aliased IPB_Controllers.Instance;
      end record;
end A4A.Web.Controllers.Valve_Controllers;
