
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with A4A.Log;

package body A4A.Web.Controllers.Switch3_Controllers is
   procedure Pos0 (Object : in out Instance) is
   begin
      Object.Status := Pos0;
      if Object.Command_Bit1 /= null then
         Object.Command_Bit1.all := False;
      else
         A4A.Log.Logger.Put (Who  => My_Ident & ".Pos0",
                             What => "Object.Command_Bit1 null !");
      end if;
      if Object.Command_Bit2 /= null then
         Object.Command_Bit2.all := False;
      else
         A4A.Log.Logger.Put (Who  => My_Ident & ".Pos0",
                             What => "Object.Command_Bit2 null !");
      end if;
   end Pos0;

   procedure Pos1 (Object : in out Instance) is
   begin
      Object.Status := Pos1;
      if Object.Command_Bit1 /= null then
         Object.Command_Bit1.all := True;
      else
         A4A.Log.Logger.Put (Who  => My_Ident & ".Pos1",
                             What => "Object.Command_Bit1 null !");
      end if;
      if Object.Command_Bit2 /= null then
         Object.Command_Bit2.all := False;
      else
         A4A.Log.Logger.Put (Who  => My_Ident & ".Pos1",
                             What => "Object.Command_Bit2 null !");
      end if;
   end Pos1;

   procedure Pos2 (Object : in out Instance) is
   begin
      Object.Status := Pos2;
      if Object.Command_Bit1 /= null then
         Object.Command_Bit1.all := False;
      else
         A4A.Log.Logger.Put (Who  => My_Ident & ".Pos2",
                             What => "Object.Command_Bit1 null !");
      end if;
      if Object.Command_Bit2 /= null then
         Object.Command_Bit2.all := True;
      else
         A4A.Log.Logger.Put (Who  => My_Ident & ".Pos2",
                             What => "Object.Command_Bit2 null !");
      end if;
   end Pos2;

   function Get_Status (Object : in out Instance) return Status_Type is
   begin
      return Object.Status;
   end Get_Status;

   procedure Set_Model (Object       : in out Instance;
                        Command_Bit1 : access Boolean := null;
                        Command_Bit2 : access Boolean := null) is
   begin
      Object.Command_Bit1 := Command_Bit1;
      Object.Command_Bit2 := Command_Bit2;
   end Set_Model;

   procedure Update (Object : in out Instance) is
   begin
      if Object.Command_Bit1 /= null and Object.Command_Bit2 /= null then
         if Object.Command_Bit1.all and not Object.Command_Bit2.all then
            Object.Status := Pos1;
         elsif not Object.Command_Bit1.all and Object.Command_Bit2.all then
            Object.Status := Pos2;
         else
            Object.Status := Pos0;
         end if;
      else
         Object.Status := Pos0;
      end if;
   end Update;

end A4A.Web.Controllers.Switch3_Controllers;
