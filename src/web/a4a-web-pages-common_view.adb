
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Gnoga.Server;

with A4A.Application.Identification; use A4A.Application.Identification;

with UXStrings; use UXStrings;

package body A4A.Web.Pages.Common_View is

   procedure Common_View_Setup
     (Common_View  : in out Common_View_Type;
      Main_Window  : in Window.Pointer_To_Window_Class;
      Main_Content : in String)
   is
   begin
      Common_View.Main_Window := Main_Window;

      Common_View.View.Create (Main_Window.all, ID => "main-window-view");
      Common_View.View.Attribute ("style", "display:block");

      Common_View.View.Load_HTML
        (Gnoga.Server.HTML_Directory & From_UTF_8 ("010-top.html"),
         ID => "top");

      Common_View.View.Load_HTML
        (Gnoga.Server.HTML_Directory & From_UTF_8 (Main_Content),
         ID => "main-content");

      Common_View.View.Load_HTML
        (Gnoga.Server.HTML_Directory & From_UTF_8 ("099-bottom.html"),
         ID => "bottom");

      Common_View.Header_App_Name_View.Attach_Using_Parent
        (Common_View.View, "header-appname");

      Common_View.Header_App_Name_View.Inner_HTML
        (From_UTF_8 (Get_Application_Name));

      Common_View.About_Box_App_Name_View.Attach_Using_Parent
        (Common_View.View, "about-box-appname");
      Common_View.About_Box_App_Desc_View.Attach_Using_Parent
        (Common_View.View, "about-box-appdesc");
      Common_View.About_Box_App_Version_View.Attach_Using_Parent
        (Common_View.View, "about-box-appversion");

      Common_View.About_Box_App_Name_View.Inner_HTML
        (From_UTF_8 (Get_Application_Name));
      Common_View.About_Box_App_Desc_View.Inner_HTML
        (From_UTF_8 (Get_Application_Description));
      Common_View.About_Box_App_Version_View.Inner_HTML
        (From_UTF_8 (Get_Application_Version));

      Common_View.Connection_Stats_View.Setup
        (Parent                             => Common_View.View,
         Current_Connections_Number_View_Id => "current_connections_number",
         Connections_Number_View_Id         => "connections_number",
         Disconnections_Number_View_Id      => "disconnections_number",
         Connections_Number_Max_View_Id     => "connections_number_max");

   end Common_View_Setup;

end A4A.Web.Pages.Common_View;
