
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package is part of the binding to Hilscher cifX Device Driver,
--  a library allowing access to the cifX boards with numerous protocols.
--  </summary>
--  <description>
--  It provides, from "cifXUser.h" :
--  - cifX data types,
--  - cifX C functions binding.
--  </description>
--  <group>Protocols</group>
--  <c_version>3.0.1</c_version>

with Interfaces.C; use Interfaces.C;

package A4A.Log_Wrap is

   procedure Work_Around;

private

   --  Maps to A4A.Logger.Log_Level_Type :
   A4A_LOG_LEVEL_ERROR                  : constant DWord := 1;
   A4A_LOG_LEVEL_WARNING                : constant DWord := 2;
   A4A_LOG_LEVEL_INFO                   : constant DWord := 3;
   A4A_LOG_LEVEL_VERBOSE                : constant DWord := 4;

   subtype Who_Buffer_type is Interfaces.C.char_array (0 .. 1023);
   subtype What_Buffer_type is Interfaces.C.char_array (0 .. 1023);

   procedure A4A_Log
     (Who_Buffer         : Who_Buffer_type;
      What_Buffer        : What_Buffer_type;
      Log_Level          : DWord);
   --  void A4A_Log (char* szWhoBuffer, char* szWhatBuffer,
   --                uint32_t ulLog_Level);

   pragma Export (C, A4A_Log, "A4A_Log");

end A4A.Log_Wrap;
