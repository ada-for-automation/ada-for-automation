
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package provides the binding to Hilscher cifX Device Driver,
--  a library allowing access to the cifX boards with numerous protocols.
--  </summary>
--  <description>
--  It provides :
--  - messaging infrastructure.
--  </description>
--  <group>Protocols</group>
--  <c_version>3.0.1</c_version>

with A4A.Protocols.HilscherX.cifX_User;
use A4A.Protocols.HilscherX.cifX_User;

package A4A.Protocols.HilscherX.Packet_Queue is

   type cifX_Packet_Queue is array (Positive range <>) of cifX_Packet_Access;

   protected type Packet_Queue_Type (Queue_Size : Positive := 1) is

      procedure Put
        (Item   : in cifX_Packet_Access;
         Result : out Boolean);
      --  Returns False if the Queue is full

      procedure Get
        (Item   : out cifX_Packet_Access;
         Result : out Boolean);
      --  Returns False if the Queue is empty

      entry Get
        (Item   : out cifX_Packet_Access);

      function Get_Pending return DWord;

   private

      Queue : cifX_Packet_Queue (1 .. Queue_Size);

      Empty : Boolean := True;
      Full  : Boolean := False;

      Pending : DWord := 0;

      Insert_Index : Positive := 1;
      Remove_Index : Positive := 1;

   end Packet_Queue_Type;

   type Packet_Queue_Access is access all Packet_Queue_Type;

end A4A.Protocols.HilscherX.Packet_Queue;
