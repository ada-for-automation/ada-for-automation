
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package provides the binding to Hilscher cifX Device Driver,
--  a library allowing access to the cifX boards with numerous protocols.
--  </summary>
--  <description>
--  It provides :
--  - messaging infrastructure.
--  </description>
--  <group>Protocols</group>
--  <c_version>3.0.1</c_version>

package body A4A.Protocols.HilscherX.Packet_Queue is

   protected body Packet_Queue_Type is

      procedure Put
        (Item   : in cifX_Packet_Access;
         Result : out Boolean) is
      begin

         if Full then
            Result := False;
         else
            Queue (Insert_Index) := Item;

            Pending := Pending + 1;

            if Insert_Index < Queue'Last (1) then
               Insert_Index := Insert_Index + 1;
            else
               Insert_Index := Queue'First (1);
            end if;

            if Insert_Index = Remove_Index then
               Full := True;
            end if;

            Empty := False;
            Result := True;
         end if;

      end Put;

      procedure Get
        (Item   : out cifX_Packet_Access;
         Result : out Boolean) is
      begin

         if Empty then
            Result := False;
         else

            Item := Queue (Remove_Index);

            Pending := Pending - 1;

            if Remove_Index < Queue'Last (1) then
               Remove_Index := Remove_Index + 1;
            else
               Remove_Index := Queue'First (1);
            end if;

            if Insert_Index = Remove_Index then
               Empty := True;
            end if;

            Full := False;
            Result := True;
         end if;

      end Get;

      entry Get
        (Item   : out cifX_Packet_Access)
        when not Empty is
      begin
         Item := Queue (Remove_Index);

         Pending := Pending - 1;

         if Remove_Index < Queue'Last (1) then
            Remove_Index := Remove_Index + 1;
         else
            Remove_Index := Queue'First (1);
         end if;

         if Insert_Index = Remove_Index then
            Empty := True;
         end if;

         Full := False;
      end Get;

      function Get_Pending return DWord is
      begin
         return Pending;
      end Get_Pending;

   end Packet_Queue_Type;

end A4A.Protocols.HilscherX.Packet_Queue;
