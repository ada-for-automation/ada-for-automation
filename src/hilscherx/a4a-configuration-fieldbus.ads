
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Strings.Bounded; use Ada.Strings.Bounded;
with A4A.Protocols.HilscherX.cifX_User;
use A4A.Protocols.HilscherX.cifX_User;

with A4A.Protocols.HilscherX.Channel_Messaging;
use A4A.Protocols.HilscherX;

package A4A.Configuration.Fieldbus is

   package Fieldbus_Name_Strings is new
     Ada.Strings.Bounded.Generic_Bounded_Length (CIFx_MAX_INFO_NAME_LENTH);

   --------------------------------------------------------------------
   --  Fieldbus Configuration
   --------------------------------------------------------------------

   type Fieldbus_Configuration is
      record
         Fieldbus_Name     : Fieldbus_Name_Strings.Bounded_String;
         --  Can be the name (eg. cifX0 / TCP0_cifX0) or the alias

         Fieldbus_Channel : DWord := 0;
         --  Channel number

         Board_Name : Board_Name_Type;
         --  Global board name
         --  Will be filled up automatically

         Board_Alias : Board_Name_Type;
         --  Global board alias name
         --  Will be filled up automatically

         Channel_Handle : Channel_Handle_Type;
         --  Will be filled up automatically

         My_Channel : Channel_Messaging.Instance_Access;
         --  Will be filled up automatically

      end record;
   type Fieldbus_Configuration_Access is access all Fieldbus_Configuration;

   type Fieldbus_Configuration_Access_Array is array (Positive range <>)
     of Fieldbus_Configuration_Access;

end A4A.Configuration.Fieldbus;
