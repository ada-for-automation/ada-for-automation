
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Task_Identification; use Ada.Task_Identification;
with Ada.Calendar.Formatting;
with Ada.Real_Time; use Ada.Real_Time;
with Ada.Exceptions; use Ada.Exceptions;

with A4A.Log;

with A4A.Application.Main_Fieldbus2;
with A4A.Application.Fieldbus2;

with A4A.HilscherX.Fieldbus2; use A4A.HilscherX.Fieldbus2;
with A4A.Protocols.HilscherX; use A4A.Protocols.HilscherX;
with A4A.Protocols.HilscherX.cifX_Errors;
use A4A.Protocols.HilscherX.cifX_Errors;

with A4A.Protocols.HilscherX.rcX_Public.rcX_Get_DPM_IO_Info;

with A4A.Protocols.HilscherX.Request_FB.rcX_Public.rcX_Get_DPM_IO_Info_FB;

use A4A.Protocols.HilscherX.rcX_Public;

with A4A.Library.Timers; use A4A.Library.Timers;
with A4A.Library.Timers.TON;

with A4A.Tasks.Statistics_Manager; use A4A.Tasks;

with A4A.Memory.Hilscher_Fieldbus2; use A4A.Memory;

with A4A.Kernel.Fieldbus1;

package body A4A.Kernel.Fieldbus2 is

   package rcX_Public_Req_FB renames
     A4A.Protocols.HilscherX.Request_FB.rcX_Public;

   procedure Create_Main_Task is
   begin
      if not Main_Task_Created then
         The_Main_Task := new Main_Task
           (Task_Priority  => Fieldbus2_Task_Priority,
            Task_Itf       => The_Main_Task_Interface'Access);

         Main_Task_Created := True;
      end if;
   end Create_Main_Task;

   task body Main_Task is
      My_ID    : constant Task_Id := Current_Task;
      My_Ident : constant String := "A4A.Kernel.Fieldbus2.Main_Task";
      Start_Time : constant Ada.Calendar.Time := Ada.Calendar.Clock;
      Next_Time : Ada.Real_Time.Time;
      My_Period : constant Time_Span :=
        Milliseconds (Fieldbus2_Task_Period_MS);

      Task_Status : Task_Status_Type;
      My_Statistics_Manager : Statistics_Manager.Instance;

      Start_Real_Time : Ada.Real_Time.Time;
      Task_Duration   : Duration;

      First_Cycle     : Boolean := True;

      Main_Watchdog_TON_Q       : Boolean := False;
      Main_Watchdog_Error       : Boolean := False;

      Driver_Handle  : aliased cifX.Driver_Handle_Type;
      Channel_Handle : aliased cifX.Channel_Handle_Type;

      Result         : DInt;

      Driver_Information : cifX.Driver_Information_Type;
      Channel_Information : cifX.Channel_Information_Type;
      Common_Status_Block : cifX.Common_Status_Block_Type;

      cifX_Driver_Init_Done  : Boolean := False;
      cifX_Driver_Open_Done  : Boolean := False;
      cifX_Channel_Open_Done : Boolean := False;

      cifX_Ready_Flag_Not_Set   : Boolean := False;
      cifX_Running_Flag_Not_Set : Boolean := False;
      cifX_COM_Flag_Not_Set     : Boolean := False;

      cifX_OK                : Boolean := False;

      cifX_Host_State        : aliased DWord := 0;
      cifX_Bus_State         : aliased DWord := 0;
      cifX_WD_Trigger        : aliased DWord := 0;

      cifX_Watchdog_Time_MS  : Integer := 1000;

      cifX_WD_Trigger_Previous  : DWord := 0;

      cifX_Watchdog_TON         : TON.Instance;
      cifX_Watchdog_TON_Elapsed : Ada.Real_Time.Time_Span := Time_Span_Zero;
      cifX_Watchdog_TON_Start   : Boolean := False;
      cifX_Watchdog_TON_Q       : Boolean := False;
      cifX_Watchdog_Error       : Boolean := False;

      cifX_Driver_Init_Done_TO : Natural := 5;
      cifX_Driver_Open_Done_TO : Natural := 5;
      FW_Start_Temporisation : constant Duration := 5.0;

      rcX_Get_DPM_IO_Info_FB :
      rcX_Public_Req_FB.rcX_Get_DPM_IO_Info_FB.Instance;

      DPM_IO_Info  : rcX_Get_DPM_IO_Info.RCX_GET_DPM_IO_INFO_CNF_DATA_T;

      Error_String : String (1 .. 20);

      Get_DPM_IO_Info_Do_Command    : Boolean := False;
      Get_DPM_IO_Info_Command_Done  : Boolean := False;
      Get_DPM_IO_Info_Command_Error : Boolean := False;

      Input_Area_Offset : DWord := 0;
      Input_Area_Length : DWord := Hilscher_Fieldbus2.Inputs'Length;

      Output_Area_Offset : DWord := 0;
      Output_Area_Length : DWord := Hilscher_Fieldbus2.Outputs'Length;

      procedure Log_Task_Start;

      procedure Log_Task_Start is
         What : constant String := "Main_Task's ID is " & Image (My_ID) & CRLF
           & "Started at " & Ada.Calendar.Formatting.Image
           (Date                  => Start_Time,
            Include_Time_Fraction => True,
            Time_Zone             => The_Time_Offset);
      begin
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => What);
      end Log_Task_Start;

      procedure Close;

      procedure Close is
      begin

         if cifX_OK then

            A4A.Log.Logger.Put (Who  => My_Ident,
                                What => "Stopping Watchdog...");

            Result := cifX.Channel_Watchdog
              (Channel_Handle      => Channel_Handle,
               Command             => cifX.CIFX_WATCHDOG_STOP,
               Trigger             => cifX_WD_Trigger'Access);

            A4A.Log.Logger.Put (Who  => My_Ident,
                                What => "Resetting Flag Bus On...");

            Result := cifX.Channel_Bus_State
              (Channel_Handle      => Channel_Handle,
               Command             => cifX.CIFX_BUS_STATE_OFF,
               State               => cifX_Bus_State'Access,
               Time_Out            => 1000);

            if Result /= CIFX_NO_ERROR then
               cifX_Show_Error (My_Ident, Result);
            end if;

         end if;

         A4A.Log.Logger.Put
           (Who  => My_Ident,
            What => "Calling Closing...");

         A4A.Application.Fieldbus2.Closing;

         A4A.Log.Logger.Put
           (Who  => My_Ident,
            What => "Terminating Channel Messaging...");

         FB_Channel_Messaging.Quit;

         loop
            exit when
              FB_Channel_Messaging.Is_Terminated;
            delay 1.0;
         end loop;

         if cifX_Channel_Open_Done then

            A4A.Log.Logger.Put (Who  => My_Ident,
                                What => "Resetting Flag Host Ready...");

            Result := cifX.Channel_Host_State
              (Channel_Handle      => Channel_Handle,
               Command             => cifX.CIFX_HOST_STATE_NOT_READY,
               State               => cifX_Host_State'Access,
               Time_Out            => 1000);

            if Result /= CIFX_NO_ERROR then
               cifX_Show_Error (My_Ident, Result);
            end if;

            A4A.Log.Logger.Put (Who  => My_Ident,
                                What => "Closing cifX Channel...");

            Result := cifX.Channel_Close (Channel_Handle);
            if Result /= CIFX_NO_ERROR then
               cifX_Show_Error (My_Ident, Result);
            end if;

            cifX_Channel_Open_Done := False;

         end if;

         if cifX_Driver_Open_Done then

            cifX_Driver_Open_Done := False;

         end if;

         --  DeInit anyway to stop the netXTransport task
         cifX_Driver_Init_Done := True;
         if cifX_Driver_Init_Done then

            A4A.Kernel.Fieldbus1.cifX_Driver_In_Use (False);
            cifX_Driver_Init_Done := False;

         end if;

         Task_Itf.Status.Ready (False);
         Task_Itf.Status.Terminated (Task_Status.Terminated);
         Main_Task_Created := False;

      end Close;

   begin

      Log_Task_Start;

      --------------------------------------------------------------------
      --  Hilscher cifX 0 Management
      --------------------------------------------------------------------

      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => "Initialising cifX Driver...");

      loop
         cifX_Driver_Init_Done :=
           A4A.Kernel.Fieldbus1.is_cifX_Driver_Init_Done;
         cifX_Driver_Init_Done_TO := cifX_Driver_Init_Done_TO - 1;
         exit when cifX_Driver_Init_Done or cifX_Driver_Init_Done_TO = 0;
         delay 1.0;
      end loop;

      if cifX_Driver_Init_Done then

         A4A.Kernel.Fieldbus1.cifX_Driver_In_Use (True);

         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Opening cifX Driver...");

         loop
            cifX_Driver_Open_Done :=
              A4A.Kernel.Fieldbus1.is_cifX_Driver_Open_Done;
            cifX_Driver_Open_Done_TO := cifX_Driver_Open_Done_TO - 1;
            exit when cifX_Driver_Open_Done or cifX_Driver_Open_Done_TO = 0;
            delay 1.0;
         end loop;

      end if;

      if cifX_Driver_Open_Done then

         Driver_Handle := A4A.Kernel.Fieldbus1.Get_Driver_Handle;

         cifX.Driver_Get_Information
           (Driver_Handle      => Driver_Handle,
            Driver_Information => Driver_Information,
            Error              => Result);

         if Result /= CIFX_NO_ERROR then
            cifX_Show_Error (My_Ident, Result);
         else
            cifX_Show_Driver_Information
              (Who                => My_Ident,
               Driver_Information => Driver_Information);
            Task_Itf.cifX_Drv_Status.Set_Driver_Information
              (Driver_Information);
         end if;

         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Waiting for the firmware to start...");

         delay FW_Start_Temporisation;

         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Opening cifX Channel...");

         Result := cifX.Channel_Open
           (Driver_Handle          => Driver_Handle,
            Board_Name             => Fieldbus_Name_Strings.To_String
              (FB_Config.Fieldbus_Name),
            Channel_Number         => FB_Config.Fieldbus_Channel,
            Channel_Handle_Access  => Channel_Handle'Access);

         if Result /= CIFX_NO_ERROR then
            cifX_Show_Error (My_Ident, Result);
         else
            cifX_Channel_Open_Done := True;
            FB_Config.Channel_Handle := Channel_Handle;
         end if;
      end if;

      if cifX_Channel_Open_Done then

         cifX.Channel_Info
           (Channel_Handle      => Channel_Handle,
            Channel_Information => Channel_Information,
            Error               => Result);

         if Result /= CIFX_NO_ERROR then
            cifX_Show_Error (My_Ident, Result);
         else
            cifX_Show_Channel_Information
              (Who                 => My_Ident,
               Channel_Information => Channel_Information);
            Task_Itf.cifX_Board_Status.Set_Channel_Information
              (Channel_Information);
            FB_Config.Board_Name := Channel_Information.Board_Name;
            FB_Config.Board_Alias := Channel_Information.Board_Alias;
         end if;

         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Setting Flag Host Ready...");

         Result := cifX.Channel_Host_State
           (Channel_Handle      => Channel_Handle,
            Command             => cifX.CIFX_HOST_STATE_READY,
            State               => cifX_Host_State'Access,
            Time_Out            => 1000);

         if Result /= CIFX_NO_ERROR then
            cifX_Show_Error (My_Ident, Result);
         end if;

         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Setting Flag Bus On...");

         Result := cifX.Channel_Bus_State
           (Channel_Handle      => Channel_Handle,
            Command             => cifX.CIFX_BUS_STATE_ON,
            State               => cifX_Bus_State'Access,
            Time_Out            => 1000);

         if Result /= CIFX_NO_ERROR then

            cifX_Show_Error (My_Ident, Result);

            if Result = CIFX_DEV_NOT_READY then
               cifX_Ready_Flag_Not_Set := True;
            elsif Result = CIFX_DEV_NOT_RUNNING then
               cifX_Running_Flag_Not_Set := True;
            end if;

         end if;

         cifX_OK := cifX_Channel_Open_Done and
               not cifX_Ready_Flag_Not_Set and not cifX_Running_Flag_Not_Set;

      end if;

      if cifX_OK then

         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Initialising Channel Messaging...");

         FB_Channel_Messaging.Initialise (Channel_Handle => Channel_Handle);

         FB_Config.My_Channel := FB_Channel_Messaging'Access;

         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Getting DPM IO Info...");

         rcX_Get_DPM_IO_Info_FB.Initialise
           (Channel_Access => FB_Channel_Messaging'Unrestricted_Access);

         loop

            Get_DPM_IO_Info_Do_Command := not Get_DPM_IO_Info_Command_Done;

            rcX_Get_DPM_IO_Info_FB.Cyclic
              (Do_Command => Get_DPM_IO_Info_Do_Command,
               Done       => Get_DPM_IO_Info_Command_Done,
               Error      => Get_DPM_IO_Info_Command_Error);

            exit when not Get_DPM_IO_Info_Do_Command;

            delay 0.1;

         end loop;

         if not Get_DPM_IO_Info_Command_Error then

            DPM_IO_Info := rcX_Get_DPM_IO_Info_FB.Get_Data;

            cifX_Show_DPM_IO_Info
              (Who         => My_Ident,
               DPM_IO_Info => DPM_IO_Info);

            Input_Area_Offset := DPM_IO_Info.IO_Block_Info (0).Offset;
            Input_Area_Length := DPM_IO_Info.IO_Block_Info (0).Length;

            Output_Area_Offset := DPM_IO_Info.IO_Block_Info (1).Offset;
            Output_Area_Length := DPM_IO_Info.IO_Block_Info (1).Length;

         else

            DWord_Text_IO.Put
              (To   => Error_String,
               Item => rcX_Get_DPM_IO_Info_FB.Get_Last_Error,
               Base => 16);

            A4A.Log.Logger.Put (Who  => My_Ident & " Get_DPM_IO_Info",
                                What => "Got an error : " & Error_String);

         end if;

         cifX.Channel_Common_Status_Block
           (Channel_Handle      => Channel_Handle,
            Common_Status_Block => Common_Status_Block,
            Error               => Result);

         if Result /= CIFX_NO_ERROR then
            cifX_Show_Error (My_Ident, Result);
         else
            cifX_Watchdog_Time_MS :=
              Integer (Common_Status_Block.Watchdog_Time);
         end if;

      end if;

      A4A.Log.Logger.Put
        (Who  => My_Ident,
         What => "Calling Cold_Start...");

      if cifX_Channel_Open_Done then
         A4A.Application.Fieldbus2.Cold_Start;
      end if;

      Task_Itf.Status.Ready (True);

      --------------------------------------------------------------------
      --  Main loop
      --------------------------------------------------------------------

      Next_Time := Clock;
      Main_Loop :
      loop

         Start_Real_Time := Clock;

         Task_Status.Running := Task_Itf.Control.Run;
         Task_Status.Terminated := Task_Itf.Control.Quit;

         if cifX_OK then

            Result := cifX.Channel_IO_Write
              (Channel_Handle => Channel_Handle,
               Area_Number    => 0,
               Offset         => Output_Area_Offset,
               Data_Length    => Output_Area_Length,
               Data_Out       => Hilscher_Fieldbus2.Outputs,
               Time_Out       => 1000);

            if Result /= CIFX_NO_ERROR then
               if Result = CIFX_DEV_NO_COM_FLAG
                 and not cifX_COM_Flag_Not_Set
               then
                  cifX_Show_Error (My_Ident, Result);
                  cifX_COM_Flag_Not_Set := True;
               end if;
            else
               cifX_COM_Flag_Not_Set := False;
            end if;

         end if;

         --  Here we test the watchdog between the main task and
         --  the console or GUI thread

         Task_Itf.Status_Watchdog.Watchdog
           (Watching         => Task_Itf.Control.Start_Watching,
            Control_Watchdog => Task_Itf.Control_Watchdog.Value,
            Error            => Main_Watchdog_TON_Q);

         if Main_Watchdog_TON_Q and not Main_Watchdog_Error then
            A4A.Log.Logger.Put (Who  => My_Ident,
                                What => "Watchdog Time Out elapsed!");
            Main_Watchdog_Error := True;
         end if;

         if cifX_OK then

            --  Here we test the watchdog between the main task and
            --  Hilscher cifX board

            Result := cifX.Channel_Watchdog
              (Channel_Handle      => Channel_Handle,
               Command             => cifX.CIFX_WATCHDOG_START,
               Trigger             => cifX_WD_Trigger'Access);

            cifX_Watchdog_TON_Start :=
              cifX_WD_Trigger_Previous = cifX_WD_Trigger;

            cifX_Watchdog_TON.Cyclic
              (Start   => cifX_Watchdog_TON_Start,
               Preset  => Ada.Real_Time.Milliseconds (cifX_Watchdog_Time_MS),
               Elapsed => cifX_Watchdog_TON_Elapsed,
               Q       => cifX_Watchdog_TON_Q);

            if cifX_Watchdog_TON_Q
              and not cifX_Watchdog_Error
            then
               A4A.Log.Logger.Put
                 (Who  => My_Ident,
                  What => "Hilscher cifx Watchdog Time Out elapsed!");
               cifX_Watchdog_Error := True;
               Task_Itf.cifX_Board_Status.WD_Error (True);
            end if;

            cifX_WD_Trigger_Previous := cifX_WD_Trigger;

            cifX.Channel_Common_Status_Block
              (Channel_Handle      => Channel_Handle,
               Common_Status_Block => Common_Status_Block,
               Error               => Result);

            if Result /= CIFX_NO_ERROR then
               cifX_Show_Error (My_Ident, Result);
            else
               Task_Itf.cifX_Board_Status.Set_Common_Status_Block
                 (Common_Status_Block);
            end if;

            Result := cifX.Channel_IO_Read
              (Channel_Handle => Channel_Handle,
               Area_Number    => 0,
               Offset         => Input_Area_Offset,
               Data_Length    => Input_Area_Length,
               Data_In        => Hilscher_Fieldbus2.Inputs,
               Time_Out       => 1000);

            if Result /= CIFX_NO_ERROR then
               if Result = CIFX_DEV_NO_COM_FLAG
                 and not cifX_COM_Flag_Not_Set
               then
                  cifX_Show_Error (My_Ident, Result);
                  cifX_COM_Flag_Not_Set := True;
               end if;
            end if;
            Task_Itf.cifX_Board_Status.Communicating (Result = CIFX_NO_ERROR);

         end if;

         if Task_Status.Running
           and not Task_Status.Terminated
           and not A4A.Application.Program_Fault
           and not A4A.Application.Fieldbus2.Program_Fault
         then

            A4A.Application.Main_Fieldbus2.Get_Fieldbus_Inputs;
            A4A.Application.Fieldbus2.Main_Cyclic;
            A4A.Application.Main_Fieldbus2.Set_Fieldbus_Outputs;

         else
            A4A.Memory.Hilscher_Fieldbus2.Outputs := (others => 0);
         end if;

         exit Main_Loop when Task_Status.Terminated;

         Task_Duration := To_Duration (Clock - Start_Real_Time);
         if First_Cycle then
            My_Statistics_Manager.Initialise (Task_Duration);
         else
            My_Statistics_Manager.Update (Task_Duration);
         end if;

         Next_Time := Next_Time + My_Period;
         delay until Next_Time;

         Task_Itf.Status.Running (Task_Status.Running);
         Task_Itf.Statistics.Set_Statistics
           (My_Statistics_Manager.Get_Statistics);

         First_Cycle := False;
      end loop Main_Loop;

      Close;

      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => "Finished !");

   exception

      when Error : others =>
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => Exception_Information (Error));

         Task_Status.Terminated := True;

         Close;

         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Aborted !");
   end Main_Task;

end A4A.Kernel.Fieldbus2;
