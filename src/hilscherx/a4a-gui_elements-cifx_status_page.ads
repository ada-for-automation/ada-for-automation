
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Gtk.Widget;          use Gtk.Widget;
with Gtk.Box;             use Gtk.Box;
with Gtk.Frame;           use Gtk.Frame;
with Gtk.Label;           use Gtk.Label;
with Gtk.Image;           use Gtk.Image;
with Gtk.Size_Group;      use Gtk.Size_Group;
with Gtk.Scrolled_Window; use Gtk.Scrolled_Window;

with A4A.Kernel.Fieldbus; use A4A.Kernel.Fieldbus;

package A4A.GUI_Elements.cifX_Status_Page is

   --------------------------------------------------------------------
   --  Graphical User Interface Elements
   --  Hilscher cifX Status Page
   --------------------------------------------------------------------

   type Instance is new GUI_Element_Type with private;

   overriding function Create
     return Instance;

   overriding function Get_Root_Widget
     (GUI_Element : in Instance)
      return Gtk.Widget.Gtk_Widget;

   overriding function Get_Label
     (GUI_Element : in Instance)
      return Gtk.Widget.Gtk_Widget;

   procedure Update
     (GUI_Element : in out Instance);

   procedure Initialise
     (GUI_Element        : in out Instance;
      cifX_Driver_Status : in cifX_Driver_Status_Access;
      cifX_Status        : in cifX_Status_Access);

private

   RCX_COMM_COS_READY_MASK  : constant := 16#1#;
   RCX_COMM_COS_RUN_MASK    : constant := 16#2#;
   RCX_COMM_COS_BUS_ON_MASK : constant := 16#4#;

   Network_State_Offline    : constant := 16#1#;
   Network_State_Stop       : constant := 16#2#;
   Network_State_Idle       : constant := 16#3#;
   Network_State_Operate    : constant := 16#4#;

   Ready_Image_On           : constant String := "gtk-apply";
   Ready_Image_Off          : constant String := "gtk-media-stop";

   Run_Image_On             : constant String := "gtk-yes";
   Run_Image_Off            : constant String := "gtk-media-stop";

   Bus_On_Image_On          : constant String := "gtk-yes";
   Bus_On_Image_Off         : constant String := "gtk-media-stop";

   Communication_Image_On   : constant String := "gtk-yes";
   Communication_Image_Off  : constant String := "gtk-media-stop";

   Error_Image_On           : constant String := "gtk-no";
   Error_Image_Off          : constant String := "gtk-media-stop";

   Network_Image_On         : constant String := "gtk-apply";
   Network_Image_Off        : constant String := "gtk-media-stop";

   type Driver_Info_UI_Type is
      record
         Frame   : Gtk_Frame;
         HBox    : Gtk_Hbox;

         Items_Label  : Gtk_Label;
         Values_Label : Gtk_Label;
      end record;

   type Channel_Info_UI_Type is
      record
         Frame   : Gtk_Frame;
         VBox    : Gtk_Vbox;
         HBox    : Gtk_Hbox;

         Board_Info_Frame        : Gtk_Frame;
         Board_Info_HBox         : Gtk_Hbox;
         Board_Info_Items_Label  : Gtk_Label;
         Board_Info_Values_Label : Gtk_Label;

         Firmware_Frame        : Gtk_Frame;
         Firmware_HBox         : Gtk_Hbox;
         Firmware_Items_Label  : Gtk_Label;
         Firmware_Values_Label : Gtk_Label;
      end record;

   type Device_State_UI_Type is
      record
         Frame   : Gtk_Frame;
         HBox    : Gtk_Hbox;
         VBox    : Gtk_Vbox;

         Ready_HBox    : Gtk_Hbox;
         Ready_Image   : Gtk_Image;
         Ready_Label   : Gtk_Label;

         Run_HBox    : Gtk_Hbox;
         Run_Image   : Gtk_Image;
         Run_Label   : Gtk_Label;

         Bus_On_HBox    : Gtk_Hbox;
         Bus_On_Image   : Gtk_Image;
         Bus_On_Label   : Gtk_Label;

         Communication_HBox    : Gtk_Hbox;
         Communication_Image   : Gtk_Image;
         Communication_Label   : Gtk_Label;

         Error_HBox    : Gtk_Hbox;
         Error_Image   : Gtk_Image;
         Error_Label   : Gtk_Label;

      end record;

   type Network_State_UI_Type is
      record
         Frame   : Gtk_Frame;
         HBox    : Gtk_Hbox;
         VBox    : Gtk_Vbox;

         Offline_HBox  : Gtk_Hbox;
         Offline_Image : Gtk_Image;
         Offline_Label : Gtk_Label;

         Stop_HBox     : Gtk_Hbox;
         Stop_Image    : Gtk_Image;
         Stop_Label    : Gtk_Label;

         Idle_HBox     : Gtk_Hbox;
         Idle_Image    : Gtk_Image;
         Idle_Label    : Gtk_Label;

         Operate_HBox  : Gtk_Hbox;
         Operate_Image : Gtk_Image;
         Operate_Label : Gtk_Label;

      end record;

   type Watchdog_UI_Type is
      record
         Frame   : Gtk_Frame;
         HBox    : Gtk_Hbox;

         Status_VBox   : Gtk_Vbox;
         Status_Label  : Gtk_Label;
         Status_Image  : Gtk_Image;

         Time_VBox     : Gtk_Vbox;
         Time_Label    : Gtk_Label;
         Time_Value_Label : Gtk_Label;

      end record;

   type Com_Error_UI_Type is
      record
         Frame   : Gtk_Frame;
         HBox    : Gtk_Hbox;

         Items_Label  : Gtk_Label;
         Values_Label : Gtk_Label;

      end record;

   type General_Status_UI_Type is
      record
         Frame   : Gtk_Frame;
         VBox    : Gtk_Vbox;

         HBox1   : Gtk_Hbox;
         Device_State_UI   : aliased Device_State_UI_Type;
         Network_State_UI  : aliased Network_State_UI_Type;

         HBox2   : Gtk_Hbox;
         Watchdog_UI   : aliased Watchdog_UI_Type;
         Com_Error_UI  : aliased Com_Error_UI_Type;

      end record;

   type Master_Status_UI_Type is
      record
         Frame   : Gtk_Frame;
         VBox    : Gtk_Vbox;
         HBox    : Gtk_Hbox;

         Items_Label  : Gtk_Label;
         Values_Label : Gtk_Label;
      end record;

   type Instance is new GUI_Element_Type with
      record
         Scrolled_Window : Gtk_Scrolled_Window;
         Page_Label      : Gtk_Label;
         Page_VBox       : Gtk_Vbox;

         Driver_Info_UI    : aliased Driver_Info_UI_Type;
         Channel_Info_UI   : aliased Channel_Info_UI_Type;
         General_Status_UI : aliased General_Status_UI_Type;
         Master_Status_UI  : aliased Master_Status_UI_Type;
         Size_Group1       : Gtk_Size_Group;

         Driver_Information  : cifX.Driver_Information_Type;
         Channel_Information : cifX.Channel_Information_Type;

         WD_Error : Boolean  := False;
         --  Watchdog error

         Communicating : Boolean  := False;
         --  Communicating

         Common_Status_Block : aliased cifX.Common_Status_Block_Type;
         Common_Status_Block_Previous : cifX.Common_Status_Block_Type;

         cifX_Driver_Status : cifX_Driver_Status_Access;
         cifX_Status        : cifX_Status_Access;

      end record;

   procedure Create_Driver_Info_UI      (GUI_Element : in out Instance);
   procedure Create_Channel_Info_UI     (GUI_Element : in out Instance);

   procedure Create_Device_State_UI     (GUI_Element : in out Instance);
   procedure Create_Network_State_UI    (GUI_Element : in out Instance);
   procedure Create_Watchdog_UI         (GUI_Element : in out Instance);
   procedure Create_Com_Error_UI        (GUI_Element : in out Instance);
   procedure Create_General_Status_UI   (GUI_Element : in out Instance);
   procedure Create_Master_Status_UI    (GUI_Element : in out Instance);

   procedure Device_State_UI_Update     (GUI_Element : in out Instance);
   procedure Network_State_UI_Update    (GUI_Element : in out Instance);

   procedure Watchdog_UI_Update         (GUI_Element : in out Instance);

   procedure Com_Error_UI_Update        (GUI_Element : in out Instance);

   procedure Master_Status_UI_Update    (GUI_Element : in out Instance);

end A4A.GUI_Elements.cifX_Status_Page;
