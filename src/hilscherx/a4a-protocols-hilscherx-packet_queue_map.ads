
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package provides the binding to Hilscher cifX Device Driver,
--  a library allowing access to the cifX boards with numerous protocols.
--  </summary>
--  <description>
--  It provides :
--  - messaging infrastructure.
--  </description>
--  <group>Protocols</group>
--  <c_version>3.0.1</c_version>

with A4A.Protocols.HilscherX.Packet_Queue;
use A4A.Protocols.HilscherX.Packet_Queue;

package A4A.Protocols.HilscherX.Packet_Queue_Map is

   type cifX_Queue_Map is array (Positive range <>)
     of Packet_Queue_Access;

   type cifX_Queue_Map_Index_Array is array (Positive range <>)
     of Positive;

   protected type Packet_Queue_Map_Type (Queue_Map_Size : Positive := 100) is

      procedure Put
        (Item   : in Packet_Queue_Access;
         Index  : out DWord;
         Result : out Boolean);
      --  Registers a Packet Queue in the Map and get corresponding Index
      --  to be used as Source_Id in Packets
      --  Returns False if the Map is full

      procedure Get
        (Index  : in DWord;
         Item   : out Packet_Queue_Access;
         Result : out Boolean);
      --  Retrieves a Packet Queue from Map given its Index got from
      --  Packet Source_Id
      --  Returns False if the Index out of bounds

      procedure Delete
        (Index  : in DWord;
         Result : out Boolean);
      --  Deletes a Packet Queue from Map given its Index
      --  Returns False if Index out of bounds

      procedure Initialise;

   private

      Queue_Map : cifX_Queue_Map (1 .. Queue_Map_Size);

      Queue_Map_Indexes : cifX_Queue_Map_Index_Array (1 .. Queue_Map_Size);

      Empty : Boolean := True;

      Queue_Map_Index : Positive := 1;

   end Packet_Queue_Map_Type;

end A4A.Protocols.HilscherX.Packet_Queue_Map;
