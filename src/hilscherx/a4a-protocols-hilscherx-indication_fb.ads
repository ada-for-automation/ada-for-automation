
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package is part of the binding to Hilscher cifX Device Driver,
--  a library allowing access to the cifX boards with numerous protocols.
--  </summary>
--  <description>
--  It provides :
--  - Indication Function Block
--  </description>
--  <group>Protocols</group>
--  <c_version>3.0.1</c_version>

with A4A.Protocols.HilscherX.cifX_User;
use A4A.Protocols.HilscherX.cifX_User;

with A4A.Protocols.HilscherX.Channel_Messaging;
use A4A.Protocols.HilscherX;

package A4A.Protocols.HilscherX.Indication_FB is

   type Instance is abstract tagged limited private;
   type Instance_Access is access all Instance;

   procedure Initialise
     (Function_Block : in out Instance'Class;
     Channel_Access  : in Channel_Messaging.Instance_Access);

   procedure Cyclic
     (Function_Block : in out Instance'Class;
      Got_Indication : out Boolean);

   function Is_Ready
     (Function_Block : in Instance'Class) return Boolean;

   procedure Handle_Indication
     (Function_Block : in out Instance;
      Indication     : in cifX_Packet_Access) is abstract;

private

   type Block_Status is
     (X00,
      --  Initial

      X01,
      --  Indication Got

      X02
      --  Send packet
     );

   type Instance is abstract tagged limited
      record

         My_Channel   : Channel_Messaging.Instance_Access;

         Status       : Block_Status := X00;

         Response     : cifX_Packet_Access;

         Initialised    : Boolean := False;
         Indication_Got : Boolean := False;
         Answer_Got     : Boolean := False;
         Packet_Sent    : Boolean := False;

      end record;

   function Get_Ident
     (Function_Block : in out Instance) return String;

end A4A.Protocols.HilscherX.Indication_FB;
