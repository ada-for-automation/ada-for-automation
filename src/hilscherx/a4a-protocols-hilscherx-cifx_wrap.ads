
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package is part of the binding to Hilscher cifX Device Driver,
--  a library allowing access to the cifX boards with numerous protocols.
--  </summary>
--  <description>
--  It provides, from "cifXUser.h" :
--  - cifX data types,
--  - cifX C functions binding.
--  </description>
--  <group>Protocols</group>
--  <c_version>3.0.1</c_version>

with System;

with A4A.Protocols.HilscherX.cifX_User;
use A4A.Protocols.HilscherX.cifX_User;

with A4A.Protocols.HilscherX.Channel_Messaging;
use A4A.Protocols.HilscherX.Channel_Messaging;

package A4A.Protocols.HilscherX.cifX_Wrap is

   procedure Set_Driver
     (Driver_Handle  : in Driver_Handle_Type);

private

   Package_Ident : String := "A4A.Protocols.HilscherX.cifX_Wrap";

   function Get_Channel (Channel_Handle : Channel_Handle_Type)
                         return Channel_Messaging.Instance_Access;

   function Driver_Open_Wrap
     (Driver_Handle_Access : access Driver_Handle_Type)
      return DInt;
   --  int32_t xDriverOpenWrapAda    ( CIFXHANDLE* phDriver);

   function Driver_Close_Wrap
     (Driver_Handle : in Driver_Handle_Type)
      return DInt;
   --  int32_t xDriverCloseWrapAda   ( CIFXHANDLE  hDriver);

   function Channel_Get_MBXState_Wrap
     (Channel_Handle : in Channel_Handle_Type;
      RecvPktCount_Access : access DWord;
      SendPktCount_Access : access DWord)
      return DInt;
   --  int32_t xChannelGetMBXStateWrapAda ( CIFXHANDLE  hChannel,
   --                                       uint32_t* pulRecvPktCount,
   --                                       uint32_t* pulSendPktCount);

   function Channel_Put_Packet_Wrap
     (Channel_Handle : in Channel_Handle_Type;
      Send_Packet    : cifX_Packet_Type;
      Time_Out       : in DWord)
      return DInt;
   --  int32_t xChannelPutPacketWrapAda ( CIFXHANDLE  hChannel,
   --                                     CIFX_PACKET*  ptSendPkt,
   --                                     uint32_t ulTimeout);

   function Channel_Get_Packet_Wrap
     (Channel_Handle : in Channel_Handle_Type;
      Size           : in DWord;
      Recv_Packet_Address : System.Address;
      Time_Out       : in DWord)
      return DInt;
   --  int32_t xChannelGetPacketWrapAda ( CIFXHANDLE  hChannel,
   --                                     uint32_t ulSize,
   --                                     CIFX_PACKET* ptRecvPkt,
   --                                     uint32_t ulTimeout);

   The_Driver_Handle  : Driver_Handle_Type;

   Request      : cifX_Packet_Access;
   Confirmation : cifX_Packet_Access;

   Initialised  : Boolean := False;

   pragma Export (StdCall, Driver_Open_Wrap,  "xDriverOpenWrapAda");
   pragma Export (StdCall, Driver_Close_Wrap, "xDriverCloseWrapAda");

   pragma Export
     (StdCall, Channel_Get_MBXState_Wrap, "xChannelGetMBXStateWrapAda");
   pragma Export (StdCall, Channel_Put_Packet_Wrap,
                  "xChannelPutPacketWrapAda");
   pragma Export (StdCall, Channel_Get_Packet_Wrap,
                  "xChannelGetPacketWrapAda");

end A4A.Protocols.HilscherX.cifX_Wrap;
