/*
-----------------------------------------------------------------------
--                       Ada for Automation                          --
--                                                                   --
--              Copyright (C) 2012-2015, Stephane LOS                --
--                                                                   --
-- This library is free software; you can redistribute it and/or     --
-- modify it under the terms of the GNU General Public               --
-- License as published by the Free Software Foundation; either      --
-- version 2 of the License, or (at your option) any later version.  --
--                                                                   --
-- This library is distributed in the hope that it will be useful,   --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of    --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU --
-- General Public License for more details.                          --
--                                                                   --
-- You should have received a copy of the GNU General Public         --
-- License along with this library; if not, write to the             --
-- Free Software Foundation, Inc., 59 Temple Place - Suite 330,      --
-- Boston, MA 02111-1307, USA.                                       --
--                                                                   --
-- As a special exception, if other files instantiate generics from  --
-- this unit, or you link this unit with other files to produce an   --
-- executable, this  unit  does not  by itself cause  the resulting  --
-- executable to be covered by the GNU General Public License. This  --
-- exception does not however invalidate any other reasons why the   --
-- executable file  might be covered by the  GNU Public License.     --
-----------------------------------------------------------------------
*/

#include <stdio.h>
#include "cifxlinux.h"

int32_t a4a_cifXDriverInit()
{

  struct CIFX_LINUX_INIT init =
  {
    .init_options  = CIFX_DRIVER_INIT_AUTOSCAN,
    .base_dir      = NULL,
    .poll_interval = 0,
    .trace_level   = 255,
    .user_card_cnt = 0,
    .user_cards    = NULL,
  };

  /* First of all initialize toolkit */
  return cifXDriverInit(&init);
}

int32_t a4a_cifXDriverDeinit()
{
  /* After using, deinitialize toolkit */
  cifXDriverDeinit();
  return CIFX_NO_ERROR;
}
