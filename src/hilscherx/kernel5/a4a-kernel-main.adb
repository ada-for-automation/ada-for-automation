
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Task_Identification; use Ada.Task_Identification;
with Ada.Calendar.Formatting;
with Ada.Real_Time; use Ada.Real_Time;
with Ada.Exceptions; use Ada.Exceptions;

with A4A.Log;
with A4A.Configuration; use A4A.Configuration;
with A4A.Application.Configuration; use A4A.Application.Configuration;
with A4A.Application.Main_Periodic1;
with A4A.Application.Main_Fieldbus1;
with A4A.Application.Main_Fieldbus2;
with A4A.Memory.MBTCP_IOServer;

package body A4A.Kernel.Main is

   procedure Create_Main_Task is
   begin
      if not Main_Task_Created then
         The_Main_Task := new Main_Task
           (Task_Priority  => Application_Main_Task_Priority,
            Task_Itf       => The_Main_Task_Interface'Access);

         Main_Task_Created := True;
      end if;
   end Create_Main_Task;

   protected body MBTCP_Server_Status is

      procedure WD_Error (Value : in Boolean) is
      begin
         Status.WD_Error := Value;
      end WD_Error;

      function WD_Error return Boolean is
      begin
         return Status.WD_Error;
      end WD_Error;

   end MBTCP_Server_Status;

   protected body MBTCP_Client_Status is

      procedure WD_Error (Value : in Boolean) is
      begin
         Status.WD_Error := Value;
      end WD_Error;

      function WD_Error return Boolean is
      begin
         return Status.WD_Error;
      end WD_Error;

   end MBTCP_Client_Status;

   protected body Main_Task_Status is

      procedure Ready (Value : in Boolean) is
      begin
         Status.Ready := Value;
      end Ready;

      function Ready return Boolean is
      begin
         return Status.Ready;
      end Ready;

      procedure Terminated (Value : in Boolean) is
      begin
         Status.Terminated := Value;
      end Terminated;

      function Terminated return Boolean is
      begin
         return Status.Terminated;
      end Terminated;

      procedure Running (Value : in Boolean) is
      begin
         Status.Running := Value;
      end Running;

      function Running return Boolean is
      begin
         return Status.Running;
      end Running;

      procedure Min_Duration (Value : in Duration) is
      begin
         Status.Min_Duration := Value;
      end Min_Duration;

      procedure Max_Duration (Value : in Duration) is
      begin
         Status.Max_Duration := Value;
      end Max_Duration;

      procedure Avg_Duration (Value : in Duration) is
      begin
         Status.Avg_Duration := Value;
      end Avg_Duration;

      procedure Sched_Stats (Value : in Sched_Stats_Array) is
      begin
         Status.Sched_Stats := Value;
      end Sched_Stats;

      function Get_Status return Main_Task_Status_Type is
      begin
         return Status;
      end Get_Status;

   end Main_Task_Status;

   task body Main_Task is
      My_ID    : constant Task_Id := Current_Task;
      My_Ident : constant String := "A4A.Kernel.Main_Task";

      Start_Time : constant Ada.Calendar.Time := Ada.Calendar.Clock;
      --  if Application_Main_Task_Type = Cyclic then
      My_Delay  : constant Duration :=
        To_Duration (Milliseconds (Application_Main_Task_Delay_MS));
      --  end if;
      --  if Application_Main_Task_Type = Periodic then
      Next_Time : Ada.Real_Time.Time;
      My_Period : constant Time_Span :=
        Milliseconds (Application_Main_Task_Period_MS);
      --  end if;

      Task_Status : Main_Task_Status_Type;

      Start_Real_Time : Ada.Real_Time.Time;
      Sleep_Real_Time : Ada.Real_Time.Time;
      Task_Duration   : Duration;
      Task_Duration_Samples     : array (Byte) of Duration := (others => 0.0);
      Oldest_Sample             : Byte := 0;
      Task_Duration_Moving_Mean : Duration := 0.0;

      Sched_Delay       : Duration;
      Sched_Stats       : Sched_Stats_Array := (others => 0);
      Sched_Thresholds  : constant array (Sched_Stats'Range) of Duration :=
        (1 => To_Duration (Microseconds (100)),
         2 => To_Duration (Milliseconds (1)),
         3 => To_Duration (Milliseconds (10)),
         4 => To_Duration (Milliseconds (20)),
         5 => To_Duration (Milliseconds (30)),
         6 => To_Duration (Milliseconds (40)),
         7 => To_Duration (Milliseconds (50)),
         8 => To_Duration (Milliseconds (60)),
         9 => To_Duration (Milliseconds (70)),
         10 => To_Duration (Milliseconds (80)));

      First_Cycle     : Boolean := True;
      MBTCP_Clients_Terminated : Boolean := False;

      Clock_Handler : Clock_Handler_Task_Type_Access;
      pragma Unreferenced (Clock_Handler);

      Main_Watchdog_TON_Q       : Boolean := False;
      Main_Watchdog_Error       : Boolean := False;

      MBTCP_Server_Task_Watchdog_TON_Q  : Boolean := False;
      MBTCP_Server_Task_Watchdog_Error  : Boolean := False;
      MBTCP_Server_Task_Watchdog_Status : String  := "WD OK";
      pragma Unreferenced (MBTCP_Server_Task_Watchdog_Status);
      --  MBTCP_Server_Task_Status          : Server.Task_Status_Type;

      MBTCP_Clients_Task_Watchdog_TON_Q : array (MBTCP_Clients_Tasks'Range)
        of Boolean := (others => False);
      MBTCP_Clients_Task_Watchdog_Error : array (MBTCP_Clients_Tasks'Range)
        of Boolean := (others => False);

      procedure Log_Task_Start;

      procedure Log_Task_Start is
         What : constant String := "Main_Task's ID is " & Image (My_ID) & CRLF
           & "Started at " & Ada.Calendar.Formatting.Image
           (Date                  => Start_Time,
            Include_Time_Fraction => True,
            Time_Zone             => The_Time_Offset);
      begin
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => What);
      end Log_Task_Start;

      procedure Close;

      procedure Close is
      begin

         A4A.Log.Logger.Put
           (Who  => My_Ident,
            What => "Calling Closing...");

         A4A.Application.Closing;

         MBTCP_Server_Task_Interface.Control.Quit (True);

         for Index in MBTCP_Clients_Tasks'Range loop
            MBTCP_Clients_Tasks_Itf (Index).Control.Quit (True);
         end loop;

         loop
            for Index in MBTCP_Clients_Tasks'Range loop
               MBTCP_Clients_Terminated :=
                 MBTCP_Clients_Tasks_Itf (Index).Status.Terminated;
               exit when not MBTCP_Clients_Terminated;
            end loop;
            exit when MBTCP_Clients_Terminated;
            delay 1.0;
         end loop;

         for Index in MBTCP_Clients_Tasks'Range loop
            Free_Task_Interface (MBTCP_Clients_Tasks_Itf (Index));
         end loop;
         loop
            exit when MBTCP_Server_Task_Interface.Status.Terminated;
            delay 1.0;
         end loop;

         Clock_Handler_Interface.Control.Quit (True);
         loop
            exit when Clock_Handler_Interface.Status.Terminated;
            delay 1.0;
         end loop;

         Task_Itf.Status.Ready (False);
         Task_Itf.Status.Terminated (Task_Status.Terminated);
         Main_Task_Created := False;

      end Close;

   begin

      Log_Task_Start;

      --------------------------------------------------------------------
      --  Clock Management
      --------------------------------------------------------------------

      Clock_Handler := new Clock_Handler_Task_Type
        (Task_Priority          => System.Default_Priority,
         Task_Itf               => Clock_Handler_Interface'Access,
         Period_In_Milliseconds => 10);
      --  This task manages the real time clock calls
      --  so that the system call does not get overwhelmed
      --  Is that true ? It seems to me because a lot of automation
      --  stuff needs time.

      --------------------------------------------------------------------
      --  Modbus TCP Server Management
      --------------------------------------------------------------------

      MBTCP_Server_Task := new Server.Periodic_Task
        (Task_Priority  => System.Default_Priority,
         Task_Itf       => MBTCP_Server_Task_Interface'Access,
         Configuration  => A4A.Application.MBTCP_Server_Config.Config1'Access
        );

      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => "Modbus TCP Server created...");

      --------------------------------------------------------------------
      --  Modbus TCP Clients Management
      --------------------------------------------------------------------

      for Index in MBTCP_Clients_Tasks'Range loop

         MBTCP_Clients_Tasks_Itf (Index) :=
           new A4A.MBTCP_Client.Task_Interface
             (MBTCP_Clients_Configuration (Index).Command_Number);

         MBTCP_Clients_Tasks (Index) := new A4A.MBTCP_Client.Periodic_Task
           (Task_Priority   => System.Default_Priority,
            Task_Itf        => MBTCP_Clients_Tasks_Itf (Index),
            Configuration   => MBTCP_Clients_Configuration (Index),
            Bool_DPM_Access => My_Bool_DPM'Access,
            Word_DPM_Access => My_Word_DPM'Access);
      end loop;

      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => "Modbus TCP Clients created...");

      A4A.Log.Logger.Put
        (Who  => My_Ident,
         What => "Calling Cold_Start...");

      A4A.Application.Cold_Start;

      Task_Itf.Status.Ready (True);

      MBTCP_Server_Task_Interface.Control_Watchdog.Set_Time_Out (2000);
      MBTCP_Server_Task_Interface.Control.Start_Watching (True);

      for Index in MBTCP_Clients_Tasks'Range loop
         MBTCP_Clients_Tasks_Itf (Index).Control_Watchdog.Set_Time_Out (2000);
         MBTCP_Clients_Tasks_Itf (Index).Control.Start_Watching (True);
      end loop;

      --------------------------------------------------------------------
      --  Main loop
      --------------------------------------------------------------------

      Next_Time := Clock;
      Main_Loop :
      loop

         Start_Real_Time := Clock;

         Task_Status.Running := Task_Itf.Control.Run;
         Task_Status.Terminated := Task_Itf.Control.Quit;

         --  Here we test the watchdog between the main task and
         --  the console or GUI thread

         Task_Itf.Status_Watchdog.Watchdog
           (Watching         => Task_Itf.Control.Start_Watching,
            Control_Watchdog => Task_Itf.Control_Watchdog.Value,
            Error            => Main_Watchdog_TON_Q);

         if Main_Watchdog_TON_Q and not Main_Watchdog_Error then
            A4A.Log.Logger.Put (Who  => My_Ident,
                                What => "Watchdog Time Out elapsed!");
            Main_Watchdog_Error := True;
         end if;

         --  Here we test the watchdog between the main task and
         --  Modbus TCP Server

         MBTCP_Server_Task_Interface.Control_Watchdog.Watchdog
           (Watching        => not Task_Status.Terminated,
            Status_Watchdog =>
              MBTCP_Server_Task_Interface.Status_Watchdog.Value,
            Error           => MBTCP_Server_Task_Watchdog_TON_Q);

         if MBTCP_Server_Task_Watchdog_TON_Q
           and not MBTCP_Server_Task_Watchdog_Error
         then
            A4A.Log.Logger.Put
              (Who  => My_Ident,
               What => "MBTCP Server task Watchdog Time Out elapsed!");
            MBTCP_Server_Task_Watchdog_Error := True;
            MBTCP_Server_Task_Watchdog_Status := "WD PB";
            Task_Itf.MServer_Status.WD_Error (True);
         end if;

         --  Here we test the watchdog between the main task and
         --  each of the Modbus TCP Clients

         for Index in MBTCP_Clients_Tasks'Range loop

            MBTCP_Clients_Tasks_Itf (Index).Control_Watchdog.Watchdog
              (Watching        =>
                 MBTCP_Clients_Tasks_Itf (Index).Status.Connected
               and not Task_Status.Terminated,
               Status_Watchdog =>
                 MBTCP_Clients_Tasks_Itf (Index).Status_Watchdog.Value,
               Error           => MBTCP_Clients_Task_Watchdog_TON_Q (Index));

            if MBTCP_Clients_Task_Watchdog_TON_Q (Index)
              and not MBTCP_Clients_Task_Watchdog_Error (Index)
            then
               A4A.Log.Logger.Put
                 (Who  => My_Ident,
                  What => "MBTCP Client "
                  & Index'Img & " task Watchdog Time Out elapsed!");
               MBTCP_Clients_Task_Watchdog_Error (Index) := True;
               Task_Itf.MClients_Status (Index).WD_Error (True);
            end if;

         end loop;

         A4A.Memory.MBTCP_IOScan.Bool_Inputs := My_Bool_DPM.Inputs.Get_Data
           (Offset => A4A.Memory.MBTCP_IOScan.Bool_Inputs'First,
            Number => A4A.Memory.MBTCP_IOScan.Bool_Inputs'Length);

         A4A.Memory.MBTCP_IOScan.Word_Inputs := My_Word_DPM.Inputs.Get_Data
           (Offset => A4A.Memory.MBTCP_IOScan.Word_Inputs'First,
            Number => A4A.Memory.MBTCP_IOScan.Word_Inputs'Length);

         Server.Coils_Read
           (Outputs => A4A.Memory.MBTCP_IOServer.Bool_Coils,
            Offset  => 0);

         Server.Registers_Read
           (Outputs => A4A.Memory.MBTCP_IOServer.Registers,
            Offset  => 0);

         if Task_Status.Running
           and not Task_Status.Terminated
           and not A4A.Application.Program_Fault
         then

            A4A.Application.Main_Periodic1.Get_Main_Inputs;
            A4A.Application.Main_Fieldbus1.Get_Main_Inputs;
            A4A.Application.Main_Fieldbus2.Get_Main_Inputs;
            A4A.Application.Main_Cyclic;
            A4A.Application.Main_Periodic1.Set_Main_Outputs;
            A4A.Application.Main_Fieldbus1.Set_Main_Outputs;
            A4A.Application.Main_Fieldbus2.Set_Main_Outputs;

         else
            A4A.Memory.MBTCP_IOScan.Bool_Outputs := (others => False);
            A4A.Memory.MBTCP_IOScan.Word_Outputs := (others => 0);
         end if;

         My_Bool_DPM.Outputs.Set_Data
           (Data_In => A4A.Memory.MBTCP_IOScan.Bool_Outputs,
            Offset  => A4A.Memory.MBTCP_IOScan.Bool_Outputs'First);

         My_Word_DPM.Outputs.Set_Data
           (Data_In => A4A.Memory.MBTCP_IOScan.Word_Outputs,
            Offset  => A4A.Memory.MBTCP_IOScan.Word_Outputs'First);

         Server.Inputs_Bits_Write
           (Inputs => A4A.Memory.MBTCP_IOScan.Bool_Inputs,
            Offset => 0);

         Server.Inputs_Bits_Write
           (Inputs => A4A.Memory.MBTCP_IOScan.Bool_Outputs,
            Offset => A4A.Memory.MBTCP_IOScan.Bool_Inputs'Length);

         Server.Inputs_Bits_Write
           (Inputs => A4A.Memory.MBTCP_IOServer.Bool_Inputs,
            Offset => (A4A.Memory.MBTCP_IOScan.Bool_Inputs'Length
                       + A4A.Memory.MBTCP_IOScan.Bool_Outputs'Length));

         Server.Inputs_Registers_Write
           (Inputs => A4A.Memory.MBTCP_IOScan.Word_Inputs,
            Offset => 0);

         Server.Inputs_Registers_Write
           (Inputs => A4A.Memory.MBTCP_IOScan.Word_Outputs,
            Offset => A4A.Memory.MBTCP_IOScan.Word_Inputs'Length);

         Server.Inputs_Registers_Write
           (Inputs => A4A.Memory.MBTCP_IOServer.Input_Registers,
            Offset => (A4A.Memory.MBTCP_IOScan.Word_Inputs'Length
                       + A4A.Memory.MBTCP_IOScan.Word_Outputs'Length));

         exit Main_Loop when Task_Status.Terminated;

         Task_Duration := To_Duration (Clock - Start_Real_Time);
         if First_Cycle then
            Task_Status.Min_Duration := Task_Duration;
            Task_Status.Max_Duration := Task_Duration;
         else
            if Task_Status.Min_Duration > Task_Duration then
               Task_Status.Min_Duration := Task_Duration;
            elsif Task_Status.Max_Duration < Task_Duration then
                  Task_Status.Max_Duration := Task_Duration;
            end if;
         end if;

         Task_Duration_Moving_Mean :=
           Task_Duration_Moving_Mean + Task_Duration
             - Task_Duration_Samples (Oldest_Sample);
         Task_Duration_Samples (Oldest_Sample) := Task_Duration;
         Oldest_Sample := Oldest_Sample + 1;
         Task_Status.Avg_Duration :=
           Task_Duration_Moving_Mean / Task_Duration_Samples'Length;

         case Application_Main_Task_Type is
         when Cyclic =>
            Sleep_Real_Time := Clock;
            delay My_Delay;
            Sched_Delay := To_Duration (Clock - Sleep_Real_Time) - My_Delay;
         when Periodic =>
            Next_Time := Next_Time + My_Period;
            delay until Next_Time;
            Sched_Delay := To_Duration (Clock - Next_Time);
         end case;

         if Sched_Delay >= Sched_Thresholds (Sched_Stats'Last) then
            Sched_Stats (Sched_Stats'Last) :=
              Sched_Stats (Sched_Stats'Last) + 1;
            Task_Status.Sched_Stats (Sched_Stats'Last) :=
              Sched_Stats (Sched_Stats'Last);
         else
            for Index in Sched_Stats'Range loop
               if Sched_Delay < Sched_Thresholds (Index) then
                  Sched_Stats (Index) := Sched_Stats (Index) + 1;
                  Task_Status.Sched_Stats (Index) := Sched_Stats (Index);
                  exit;
               end if;
            end loop;
         end if;

         Task_Itf.Status.Running (Task_Status.Running);
         Task_Itf.Status.Min_Duration (Task_Status.Min_Duration);
         Task_Itf.Status.Max_Duration (Task_Status.Max_Duration);
         Task_Itf.Status.Avg_Duration (Task_Status.Avg_Duration);
         Task_Itf.Status.Sched_Stats (Task_Status.Sched_Stats);

         First_Cycle := False;
      end loop Main_Loop;

      Close;

      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => "Finished !");

   exception

      when Error : others =>
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => Exception_Information (Error));

         Task_Status.Terminated := True;

         Close;

         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Aborted !");
   end Main_Task;

end A4A.Kernel.Main;
