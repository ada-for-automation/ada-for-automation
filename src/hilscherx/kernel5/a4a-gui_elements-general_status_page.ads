
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Gtk.Widget;          use Gtk.Widget;
with Gtk.Box;             use Gtk.Box;
with Gtk.Frame;           use Gtk.Frame;
with Gtk.Label;           use Gtk.Label;
with Gtk.Image;           use Gtk.Image;
with Gtk.Scrolled_Window; use Gtk.Scrolled_Window;

with Ada.Calendar; use Ada.Calendar;

with A4A.Task_Interfaces;
with A4A.Kernel.Main; use A4A.Kernel.Main;

package A4A.GUI_Elements.General_Status_Page is

   --------------------------------------------------------------------
   --  Graphical User Interface Elements
   --  General Status Page
   --------------------------------------------------------------------

   type Instance is new GUI_Element_Type with private;

   overriding function Create
     return Instance;

   overriding function Get_Root_Widget
     (GUI_Element : in Instance)
      return Gtk.Widget.Gtk_Widget;

   overriding function Get_Label
     (GUI_Element : in Instance)
      return Gtk.Widget.Gtk_Widget;

   function Create
     (Start_Time : Ada.Calendar.Time) return Instance;

   procedure Update
     (GUI_Element : in out Instance;
      Terminating : in Boolean);

private
   Start_Time : Ada.Calendar.Time;

   Main_Watching                  : Boolean := False;
   Main_Running                   : Boolean := False;
   Main_Task_Watchdog_TON_Q       : Boolean := False;
   Main_Task_Watchdog_Error       : Boolean := False;
   Main_Task_Status               : A4A.Kernel.Main.Main_Task_Status_Type;

   Period1_Watching               : Boolean := False;
   Period1_Running                : Boolean := False;
   Period1_Task_Watchdog_TON_Q    : Boolean := False;
   Period1_Task_Watchdog_Error    : Boolean := False;
   Period1_Task_Watchdog_Status   : String  := "WD OK";
   Period1_Task_Status            : A4A.Task_Interfaces.Task_Status_Type;

   type Fieldbus_Data_Type is
      record
         Main_Watching              : Boolean := False;
         Main_Running               : Boolean := False;
         Communicating              : Boolean := False;
         Main_Task_Watchdog_TON_Q   : Boolean := False;
         Main_Task_Watchdog_Error   : Boolean := False;
         Main_Task_Status           : A4A.Task_Interfaces.Task_Status_Type;
      end record;

   FB1_Data : Fieldbus_Data_Type;
   --  Fieldbus1 Data

   FB2_Data : Fieldbus_Data_Type;
   --  Fieldbus2 Data

   type Application_UI_Type is
      record
         Frame               : Gtk_Frame;
         VBox                : Gtk_Vbox;
         HBox                : Gtk_Hbox;
         Start_Time_Label    : Gtk_Label;
         Up_Time_Label       : Gtk_Label;
      end record;

   type Task_UI_Type is tagged
      record
         Frame          : Gtk_Frame;
         VBox           : Gtk_Vbox;

         Config_HBox    : Gtk_Hbox;
         Config_Label   : Gtk_Label;

         HBox           : Gtk_Hbox;

         Watchdog_VBox  : Gtk_Vbox;
         Watchdog_Label : Gtk_Label;
         Watchdog_Image : Gtk_Image;

         Running_VBox   : Gtk_Vbox;
         Running_Label  : Gtk_Label;
         Running_Image  : Gtk_Image;

         Stats_VBox         : Gtk_Vbox;
         Stats_Frame        : Gtk_Frame;
         Stats_HBox         : Gtk_Hbox;
         Stats_Items_Label  : Gtk_Label;
         Stats_Values_Label : Gtk_Label;

      end record;

   type Main_Task_UI_Type is new Task_UI_Type with
      record
         Sched_Stats_VBox         : Gtk_Vbox;
         Sched_Stats_Frame        : Gtk_Frame;
         Sched_Stats_HBox         : Gtk_Hbox;
         Sched_Stats_Items_Label  : Gtk_Label;
         Sched_Stats_Values_Label : Gtk_Label;

      end record;

   type Periodic_Task1_UI_Type is new Task_UI_Type with null record;

   type Fieldbus_Task_UI_Type is new Task_UI_Type with
      record
         Communicating_VBox   : Gtk_Vbox;
         Communicating_Label  : Gtk_Label;
         Communicating_Image  : Gtk_Image;

      end record;

   type Instance is new GUI_Element_Type with
      record
         Scrolled_Window : Gtk_Scrolled_Window;
         Page_Label      : Gtk_Label;
         Page_VBox       : Gtk_Vbox;

         Application_UI      : aliased Application_UI_Type;
         Main_UI             : aliased Main_Task_UI_Type;
         Periodic1_UI        : aliased Periodic_Task1_UI_Type;
         Fieldbus1_UI        : aliased Fieldbus_Task_UI_Type;
         Fieldbus2_UI        : aliased Fieldbus_Task_UI_Type;

      end record;

   procedure Create_Application_UI
     (GUI_Element   : in out Instance;
      Start_Time_In : in Ada.Calendar.Time);

   procedure Create_Main_Task_UI (GUI_Element : in out Instance);

   procedure Create_Periodic_Task1_UI (GUI_Element : in out Instance);

   procedure Create_Fieldbus1_Task_UI (GUI_Element : in out Instance);

   procedure Create_Fieldbus2_Task_UI (GUI_Element : in out Instance);

   procedure Application_UI_Update
     (GUI_Element : in out Instance);

   procedure Main_Task_UI_Update
     (GUI_Element : in out Instance;
      Terminating  : in Boolean);

   procedure Periodic_Task1_UI_Update
     (GUI_Element : in out Instance;
      Terminating  : in Boolean);

   procedure Fieldbus1_Task_UI_Update
     (GUI_Element : in out Instance;
      Terminating  : in Boolean);

   procedure Fieldbus2_Task_UI_Update
     (GUI_Element : in out Instance;
      Terminating  : in Boolean);

end A4A.GUI_Elements.General_Status_Page;
