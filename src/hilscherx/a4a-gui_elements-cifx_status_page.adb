
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Gtk.Enums;

with A4A.Protocols.HilscherX; use A4A.Protocols.HilscherX;

with A4A.Protocols.HilscherX.cifX_Errors;
use A4A.Protocols.HilscherX.cifX_Errors;

package body A4A.GUI_Elements.cifX_Status_Page is

   --------------------------------------------------------------------
   --  Graphical User Interface Elements
   --  Hilscher cifX Status Page
   --------------------------------------------------------------------

   procedure Create_Driver_Info_UI (GUI_Element : in out Instance) is
      Driver_Info_UI : constant access Driver_Info_UI_Type :=
        GUI_Element.Driver_Info_UI'Access;
   begin
      Gtk_New (Driver_Info_UI.Frame, "Driver Information");
      Gtk_Label (Driver_Info_UI.Frame.Get_Label_Widget).Set_Markup
        ("<b>Driver Information</b>");
      Driver_Info_UI.Frame.Set_Shadow_Type (Gtk.Enums.Shadow_None);

      Gtk_New_Hbox (Driver_Info_UI.HBox);

      Gtk_New (Driver_Info_UI.Items_Label,
               "Version / Board Count");

      Gtk_New (Driver_Info_UI.Values_Label,
               "Values");

      Driver_Info_UI.Items_Label.Set_Markup
        (CRLF
         & "Version :" & CRLF
         & "Board Count :");
      Driver_Info_UI.Items_Label.Set_Justify (Gtk.Enums.Justify_Right);

      Driver_Info_UI.HBox.Pack_Start
        (Driver_Info_UI.Items_Label, Expand => False, Padding => 10);

      Driver_Info_UI.HBox.Pack_Start
        (Driver_Info_UI.Values_Label, Expand => False);

      Driver_Info_UI.Frame.Add (Driver_Info_UI.HBox);

   end Create_Driver_Info_UI;

   procedure Create_Channel_Info_UI (GUI_Element : in out Instance) is
      Channel_Info_UI : constant access Channel_Info_UI_Type :=
        GUI_Element.Channel_Info_UI'Access;
   begin
      Gtk_New (Channel_Info_UI.Frame, "Channel Information");
      Gtk_Label (Channel_Info_UI.Frame.Get_Label_Widget).Set_Markup
        ("<b>Channel Information</b>");
      Channel_Info_UI.Frame.Set_Shadow_Type (Gtk.Enums.Shadow_None);

      Gtk_New_Vbox (Channel_Info_UI.VBox);

      Gtk_New_Hbox (Channel_Info_UI.HBox);

      --  Board Information

      Gtk_New (Channel_Info_UI.Board_Info_Frame, "Board Information");
      Gtk_Label (Channel_Info_UI.Board_Info_Frame.Get_Label_Widget).Set_Markup
        ("<u>Board Information</u>");
      Channel_Info_UI.Board_Info_Frame.Set_Shadow_Type (Gtk.Enums.Shadow_None);

      Gtk_New_Hbox (Channel_Info_UI.Board_Info_HBox);

      Gtk_New (Channel_Info_UI.Board_Info_Items_Label,
               "Items");

      Channel_Info_UI.Board_Info_HBox.Pack_Start
        (Channel_Info_UI.Board_Info_Items_Label,
         Expand => False, Padding => 10);

      Gtk_New (Channel_Info_UI.Board_Info_Values_Label, "Values");

      Channel_Info_UI.Board_Info_HBox.Pack_Start
        (Channel_Info_UI.Board_Info_Values_Label, Expand => False);

      Channel_Info_UI.Board_Info_Frame.Add (Channel_Info_UI.Board_Info_HBox);

      Channel_Info_UI.Board_Info_Items_Label.Set_Markup
        (CRLF
         & "Board_Name :" & CRLF
         & "Board_Alias :" & CRLF
         & "Device Number :" & CRLF
         & "Serial Number :");
      Channel_Info_UI.Board_Info_Items_Label.Set_Justify
        (Gtk.Enums.Justify_Right);

      --  Firmware Information

      Gtk_New (Channel_Info_UI.Firmware_Frame, "Firmware Information");
      Gtk_Label (Channel_Info_UI.Firmware_Frame.Get_Label_Widget).Set_Markup
        ("<u>Firmware Information</u>");
      Channel_Info_UI.Firmware_Frame.Set_Shadow_Type (Gtk.Enums.Shadow_None);

      Gtk_New_Hbox (Channel_Info_UI.Firmware_HBox);

      Gtk_New (Channel_Info_UI.Firmware_Items_Label, "Items");

      Channel_Info_UI.Firmware_HBox.Pack_Start
        (Channel_Info_UI.Firmware_Items_Label, Expand => False, Padding => 10);

      Gtk_New (Channel_Info_UI.Firmware_Values_Label, "Values");

      Channel_Info_UI.Firmware_HBox.Pack_Start
        (Channel_Info_UI.Firmware_Values_Label, Expand => False);

      Channel_Info_UI.Firmware_Frame.Add (Channel_Info_UI.Firmware_HBox);

      Channel_Info_UI.Firmware_Items_Label.Set_Markup
        (CRLF
         & "Name :" & CRLF
         & "Version :" & CRLF
         & "Date :");
      Channel_Info_UI.Firmware_Items_Label.Set_Justify
        (Gtk.Enums.Justify_Right);

      Channel_Info_UI.HBox.Pack_Start
        (Channel_Info_UI.Board_Info_Frame, Expand => False, Padding => 10);

      Channel_Info_UI.HBox.Pack_Start
        (Channel_Info_UI.Firmware_Frame, Expand => False, Padding => 10);

      Channel_Info_UI.VBox.Pack_Start
        (Channel_Info_UI.HBox, Expand => False, Padding => 10);

      Channel_Info_UI.Frame.Add (Channel_Info_UI.VBox);

   end Create_Channel_Info_UI;

   procedure Create_Device_State_UI (GUI_Element : in out Instance) is
      Device_State_UI : constant access Device_State_UI_Type :=
        GUI_Element.General_Status_UI.Device_State_UI'Access;
   begin
      Gtk_New (Device_State_UI.Frame, "Device State");
      Gtk_Label (Device_State_UI.Frame.Get_Label_Widget).Set_Markup
        ("<u>Device State</u>");
      Device_State_UI.Frame.Set_Shadow_Type (Gtk.Enums.Shadow_None);

      Gtk_New_Hbox (Device_State_UI.HBox);

      Gtk_New_Vbox (Device_State_UI.VBox);

      --  Ready

      Gtk_New_Hbox (Device_State_UI.Ready_HBox);

      Gtk_New (Device_State_UI.Ready_Image, Ready_Image_Off,
               Gtk.Enums.Icon_Size_Button);

      Device_State_UI.Ready_HBox.Pack_Start
        (Device_State_UI.Ready_Image, Expand => False, Padding => 10);

      Gtk_New (Device_State_UI.Ready_Label, "Ready");

      Device_State_UI.Ready_HBox.Pack_Start
        (Device_State_UI.Ready_Label, Expand => False);

      Device_State_UI.VBox.Pack_Start
        (Device_State_UI.Ready_HBox, Expand => False);

      --  Run

      Gtk_New_Hbox (Device_State_UI.Run_HBox);

      Gtk_New (Device_State_UI.Run_Image, Run_Image_Off,
               Gtk.Enums.Icon_Size_Button);

      Device_State_UI.Run_HBox.Pack_Start
        (Device_State_UI.Run_Image, Expand => False, Padding => 10);

      Gtk_New (Device_State_UI.Run_Label, "Run");

      Device_State_UI.Run_HBox.Pack_Start
        (Device_State_UI.Run_Label, Expand => False);

      Device_State_UI.VBox.Pack_Start
        (Device_State_UI.Run_HBox, Expand => False);

      --  Bus_On

      Gtk_New_Hbox (Device_State_UI.Bus_On_HBox);

      Gtk_New (Device_State_UI.Bus_On_Image, Bus_On_Image_Off,
               Gtk.Enums.Icon_Size_Button);

      Device_State_UI.Bus_On_HBox.Pack_Start
        (Device_State_UI.Bus_On_Image, Expand => False, Padding => 10);

      Gtk_New (Device_State_UI.Bus_On_Label, "Bus_On");

      Device_State_UI.Bus_On_HBox.Pack_Start
        (Device_State_UI.Bus_On_Label, Expand => False);

      Device_State_UI.VBox.Pack_Start
        (Device_State_UI.Bus_On_HBox, Expand => False);

      --  Communication

      Gtk_New_Hbox (Device_State_UI.Communication_HBox);

      Gtk_New (Device_State_UI.Communication_Image, Communication_Image_Off,
               Gtk.Enums.Icon_Size_Button);

      Device_State_UI.Communication_HBox.Pack_Start
        (Device_State_UI.Communication_Image, Expand => False, Padding => 10);

      Gtk_New (Device_State_UI.Communication_Label, "Communication");

      Device_State_UI.Communication_HBox.Pack_Start
        (Device_State_UI.Communication_Label, Expand => False);

      Device_State_UI.VBox.Pack_Start
        (Device_State_UI.Communication_HBox, Expand => False);

      --  Error

      Gtk_New_Hbox (Device_State_UI.Error_HBox);

      Gtk_New (Device_State_UI.Error_Image, Error_Image_Off,
               Gtk.Enums.Icon_Size_Button);

      Device_State_UI.Error_HBox.Pack_Start
        (Device_State_UI.Error_Image, Expand => False, Padding => 10);

      Gtk_New (Device_State_UI.Error_Label, "Error");

      Device_State_UI.Error_HBox.Pack_Start
        (Device_State_UI.Error_Label, Expand => False);

      Device_State_UI.VBox.Pack_Start
        (Device_State_UI.Error_HBox, Expand => False);

      Device_State_UI.HBox.Pack_Start
        (Device_State_UI.VBox, Expand => False, Padding => 10);

      Device_State_UI.Frame.Add (Device_State_UI.HBox);

   end Create_Device_State_UI;

   procedure Create_Network_State_UI (GUI_Element : in out Instance) is
      Network_State_UI : constant access Network_State_UI_Type :=
        GUI_Element.General_Status_UI.Network_State_UI'Access;
   begin
      Gtk_New (Network_State_UI.Frame, "Network State");
      Gtk_Label (Network_State_UI.Frame.Get_Label_Widget).Set_Markup
        ("<u>Network State</u>");
      Network_State_UI.Frame.Set_Shadow_Type (Gtk.Enums.Shadow_None);

      Gtk_New_Hbox (Network_State_UI.HBox);

      Gtk_New_Vbox (Network_State_UI.VBox);

      --  Offline

      Gtk_New_Hbox (Network_State_UI.Offline_HBox);

      Gtk_New (Network_State_UI.Offline_Image, Network_Image_Off,
               Gtk.Enums.Icon_Size_Button);

      Network_State_UI.Offline_HBox.Pack_Start
        (Network_State_UI.Offline_Image, Expand => False, Padding => 10);

      Gtk_New (Network_State_UI.Offline_Label, "Offline");

      Network_State_UI.Offline_HBox.Pack_Start
        (Network_State_UI.Offline_Label, Expand => False);

      Network_State_UI.VBox.Pack_Start
        (Network_State_UI.Offline_HBox, Expand => False);

      --  Stop

      Gtk_New_Hbox (Network_State_UI.Stop_HBox);

      Gtk_New (Network_State_UI.Stop_Image, Network_Image_Off,
               Gtk.Enums.Icon_Size_Button);

      Network_State_UI.Stop_HBox.Pack_Start
        (Network_State_UI.Stop_Image, Expand => False, Padding => 10);

      Gtk_New (Network_State_UI.Stop_Label, "Stop");

      Network_State_UI.Stop_HBox.Pack_Start
        (Network_State_UI.Stop_Label, Expand => False);

      Network_State_UI.VBox.Pack_Start
        (Network_State_UI.Stop_HBox, Expand => False);

      --  Idle

      Gtk_New_Hbox (Network_State_UI.Idle_HBox);

      Gtk_New (Network_State_UI.Idle_Image, Network_Image_Off,
               Gtk.Enums.Icon_Size_Button);

      Network_State_UI.Idle_HBox.Pack_Start
        (Network_State_UI.Idle_Image, Expand => False, Padding => 10);

      Gtk_New (Network_State_UI.Idle_Label, "Idle");

      Network_State_UI.Idle_HBox.Pack_Start
        (Network_State_UI.Idle_Label, Expand => False);

      Network_State_UI.VBox.Pack_Start
        (Network_State_UI.Idle_HBox, Expand => False);

      --  Operate

      Gtk_New_Hbox (Network_State_UI.Operate_HBox);

      Gtk_New (Network_State_UI.Operate_Image, Network_Image_Off,
               Gtk.Enums.Icon_Size_Button);

      Network_State_UI.Operate_HBox.Pack_Start
        (Network_State_UI.Operate_Image, Expand => False, Padding => 10);

      Gtk_New (Network_State_UI.Operate_Label, "Operate");

      Network_State_UI.Operate_HBox.Pack_Start
        (Network_State_UI.Operate_Label, Expand => False);

      Network_State_UI.VBox.Pack_Start
        (Network_State_UI.Operate_HBox, Expand => False);

      Network_State_UI.HBox.Pack_Start
        (Network_State_UI.VBox, Expand => False, Padding => 10);

      Network_State_UI.Frame.Add (Network_State_UI.HBox);

   end Create_Network_State_UI;

   procedure Create_Watchdog_UI (GUI_Element : in out Instance) is
      Watchdog_UI : constant access Watchdog_UI_Type :=
        GUI_Element.General_Status_UI.Watchdog_UI'Access;
   begin
      Gtk_New (Watchdog_UI.Frame, "Watchdog");
      Gtk_Label (Watchdog_UI.Frame.Get_Label_Widget).Set_Markup
        ("<u>Watchdog</u>");
      Watchdog_UI.Frame.Set_Shadow_Type (Gtk.Enums.Shadow_None);

      Gtk_New_Hbox (Watchdog_UI.HBox);

      --  Status

      Gtk_New_Vbox (Watchdog_UI.Status_VBox);

      Gtk_New (Watchdog_UI.Status_Label, "Status");

      Watchdog_UI.Status_VBox.Pack_Start
        (Watchdog_UI.Status_Label, Expand => False, Padding => 10);

      Gtk_New (Watchdog_UI.Status_Image, "gtk-yes",
               Gtk.Enums.Icon_Size_Button);

      Watchdog_UI.Status_VBox.Pack_Start
        (Watchdog_UI.Status_Image, Expand => False);

      Watchdog_UI.HBox.Pack_Start
        (Watchdog_UI.Status_VBox, Expand => False, Padding => 10);

      --  Time

      Gtk_New_Vbox (Watchdog_UI.Time_VBox);

      Gtk_New (Watchdog_UI.Time_Label, "Configured" & CRLF & "Time Out");
      Watchdog_UI.Time_Label.Set_Justify (Gtk.Enums.Justify_Center);

      Watchdog_UI.Time_VBox.Pack_Start
        (Watchdog_UI.Time_Label, Expand => False, Padding => 10);

      Gtk_New (Watchdog_UI.Time_Value_Label, "1234 ms");

      Watchdog_UI.Time_VBox.Pack_Start
        (Watchdog_UI.Time_Value_Label, Expand => False);

      Watchdog_UI.HBox.Pack_Start
        (Watchdog_UI.Time_VBox, Expand => False);

      Watchdog_UI.Frame.Add (Watchdog_UI.HBox);

   end Create_Watchdog_UI;

   procedure Create_Com_Error_UI (GUI_Element : in out Instance) is
      Com_Error_UI : constant access Com_Error_UI_Type :=
        GUI_Element.General_Status_UI.Com_Error_UI'Access;
   begin
      Gtk_New (Com_Error_UI.Frame, "Communication error");
      Gtk_Label (Com_Error_UI.Frame.Get_Label_Widget).Set_Markup
        ("<u>Communication error</u>");
      Com_Error_UI.Frame.Set_Shadow_Type (Gtk.Enums.Shadow_None);

      Gtk_New_Hbox (Com_Error_UI.HBox);

      Gtk_New (Com_Error_UI.Items_Label,
               "Error :" & CRLF
               & "Count :");
      Com_Error_UI.Items_Label.Set_Justify (Gtk.Enums.Justify_Right);

      Com_Error_UI.HBox.Pack_Start
        (Com_Error_UI.Items_Label, Expand => False, Padding => 10);

      Gtk_New (Com_Error_UI.Values_Label,
               "-" & CRLF
               & "0");

      Com_Error_UI.HBox.Pack_Start
        (Com_Error_UI.Values_Label, Expand => False);

      Com_Error_UI.Frame.Add (Com_Error_UI.HBox);

   end Create_Com_Error_UI;

   procedure Create_General_Status_UI (GUI_Element : in out Instance) is
      General_Status_UI : constant access General_Status_UI_Type :=
        GUI_Element.General_Status_UI'Access;
   begin
      Gtk_New (General_Status_UI.Frame, "General Status");
      Gtk_Label (General_Status_UI.Frame.Get_Label_Widget).Set_Markup
        ("<b>General Status</b>");
      General_Status_UI.Frame.Set_Shadow_Type (Gtk.Enums.Shadow_None);

      Gtk_New_Vbox (General_Status_UI.VBox);

      Gtk_New_Hbox (General_Status_UI.HBox1);

      GUI_Element.Create_Device_State_UI;

      General_Status_UI.HBox1.Pack_Start
        (General_Status_UI.Device_State_UI.Frame,
         Expand => False, Padding => 10);

      GUI_Element.Create_Network_State_UI;

      General_Status_UI.HBox1.Pack_Start
        (General_Status_UI.Network_State_UI.Frame,
         Expand => False, Padding => 10);

      General_Status_UI.VBox.Pack_Start
        (General_Status_UI.HBox1, Expand => False, Padding => 10);

      Gtk_New_Hbox (General_Status_UI.HBox2);

      GUI_Element.Create_Watchdog_UI;

      General_Status_UI.HBox2.Pack_Start
        (General_Status_UI.Watchdog_UI.Frame,
         Expand => False, Padding => 10);

      GUI_Element.Create_Com_Error_UI;

      General_Status_UI.HBox2.Pack_Start
        (General_Status_UI.Com_Error_UI.Frame,
         Expand => False, Padding => 10);

      General_Status_UI.VBox.Pack_Start
        (General_Status_UI.HBox2, Expand => False);

      General_Status_UI.Frame.Add (General_Status_UI.VBox);

   end Create_General_Status_UI;

   procedure Create_Master_Status_UI (GUI_Element : in out Instance) is
      Master_Status_UI : constant access Master_Status_UI_Type :=
        GUI_Element.Master_Status_UI'Access;
   begin
      Gtk_New (Master_Status_UI.Frame, "Master Status");
      Gtk_Label (Master_Status_UI.Frame.Get_Label_Widget).Set_Markup
        ("<b>Master Status</b>");
      Master_Status_UI.Frame.Set_Shadow_Type (Gtk.Enums.Shadow_None);

      Gtk_New_Vbox (Master_Status_UI.VBox);

      Gtk_New_Hbox (Master_Status_UI.HBox);

      Gtk_New (Master_Status_UI.Items_Label,
               "Items");

      Gtk_New (Master_Status_UI.Values_Label,
               "Values");

      Master_Status_UI.Items_Label.Set_Markup
        ("Slave State :" & CRLF
         & "Slave Error Log Indicator :" & CRLF
         & "Configured slaves :" & CRLF
         & "Active slaves :" & CRLF
         & "Slaves with diagnostic :");
      Master_Status_UI.Items_Label.Set_Justify (Gtk.Enums.Justify_Right);

      Master_Status_UI.HBox.Pack_Start
        (Master_Status_UI.Items_Label, Expand => False, Padding => 10);

      Master_Status_UI.HBox.Pack_Start
        (Master_Status_UI.Values_Label, Expand => False);

      Master_Status_UI.VBox.Pack_Start
        (Master_Status_UI.HBox, Expand => False, Padding => 10);

      Master_Status_UI.Frame.Add (Master_Status_UI.VBox);

   end Create_Master_Status_UI;

   procedure Device_State_UI_Update (GUI_Element : in out Instance) is
      Device_State_UI : constant access Device_State_UI_Type :=
        GUI_Element.General_Status_UI.Device_State_UI'Access;
      DWTemp : DWord;
      New_Communicating : Boolean;
   begin

      DWTemp :=
        (GUI_Element.Common_Status_Block.Communication_COS
         xor GUI_Element.Common_Status_Block_Previous.Communication_COS)
        and GUI_Element.Common_Status_Block.Communication_COS;

      if (DWTemp and RCX_COMM_COS_READY_MASK) /= 0 then
         Device_State_UI.Ready_Image.Set
           (Ready_Image_On, Gtk.Enums.Icon_Size_Button);
      end if;

      if (DWTemp and RCX_COMM_COS_RUN_MASK) /= 0 then
         Device_State_UI.Run_Image.Set
           (Run_Image_On, Gtk.Enums.Icon_Size_Button);
      end if;

      if (DWTemp and RCX_COMM_COS_BUS_ON_MASK) /= 0 then
         Device_State_UI.Bus_On_Image.Set
           (Bus_On_Image_On, Gtk.Enums.Icon_Size_Button);
      end if;

      DWTemp :=
        (GUI_Element.Common_Status_Block.Communication_COS
         xor GUI_Element.Common_Status_Block_Previous.Communication_COS)
        and not GUI_Element.Common_Status_Block.Communication_COS;

      if (DWTemp and RCX_COMM_COS_READY_MASK) /= 0 then
         Device_State_UI.Ready_Image.Set
           (Ready_Image_Off, Gtk.Enums.Icon_Size_Button);
      end if;

      if (DWTemp and RCX_COMM_COS_RUN_MASK) /= 0 then
         Device_State_UI.Run_Image.Set
           (Run_Image_Off, Gtk.Enums.Icon_Size_Button);
      end if;

      if (DWTemp and RCX_COMM_COS_BUS_ON_MASK) /= 0 then
         Device_State_UI.Bus_On_Image.Set
           (Bus_On_Image_Off, Gtk.Enums.Icon_Size_Button);
      end if;

      New_Communicating :=
        GUI_Element.cifX_Status.Communicating;
      if New_Communicating /= GUI_Element.Communicating then
         if New_Communicating then
            Device_State_UI.Communication_Image.Set
              (Communication_Image_On, Gtk.Enums.Icon_Size_Button);
         else
            Device_State_UI.Communication_Image.Set
              (Communication_Image_Off, Gtk.Enums.Icon_Size_Button);
         end if;
         GUI_Element.Communicating := New_Communicating;
      end if;

      if GUI_Element.Common_Status_Block.Communication_Error
        /= GUI_Element.Common_Status_Block_Previous.Communication_Error
      then
         if GUI_Element.Common_Status_Block.Communication_Error = CIFX_NO_ERROR
         then
            Device_State_UI.Error_Image.Set
              (Error_Image_Off, Gtk.Enums.Icon_Size_Button);
         else
            Device_State_UI.Error_Image.Set
              (Error_Image_On, Gtk.Enums.Icon_Size_Button);
         end if;
      end if;

   end Device_State_UI_Update;

   procedure Network_State_UI_Update (GUI_Element : in out Instance) is
      Network_State_UI : constant access Network_State_UI_Type :=
        GUI_Element.General_Status_UI.Network_State_UI'Access;
   begin
      if GUI_Element.Common_Status_Block.Communication_State
        /= GUI_Element.Common_Status_Block_Previous.Communication_State
      then

         Network_State_UI.Offline_Image.Set
           (Network_Image_Off, Gtk.Enums.Icon_Size_Button);
         Network_State_UI.Stop_Image.Set
           (Network_Image_Off, Gtk.Enums.Icon_Size_Button);
         Network_State_UI.Idle_Image.Set
           (Network_Image_Off, Gtk.Enums.Icon_Size_Button);
         Network_State_UI.Operate_Image.Set
           (Network_Image_Off, Gtk.Enums.Icon_Size_Button);

         if GUI_Element.Common_Status_Block.Communication_State
           = Network_State_Offline
         then

            Network_State_UI.Offline_Image.Set
              (Network_Image_On, Gtk.Enums.Icon_Size_Button);

         elsif GUI_Element.Common_Status_Block.Communication_State
           = Network_State_Stop
         then

            Network_State_UI.Stop_Image.Set
              (Network_Image_On, Gtk.Enums.Icon_Size_Button);

         elsif GUI_Element.Common_Status_Block.Communication_State
           = Network_State_Idle
         then

            Network_State_UI.Idle_Image.Set
              (Network_Image_On, Gtk.Enums.Icon_Size_Button);

         elsif GUI_Element.Common_Status_Block.Communication_State
           = Network_State_Operate
         then

            Network_State_UI.Operate_Image.Set
              (Network_Image_On, Gtk.Enums.Icon_Size_Button);
         end if;

      end if;
   end Network_State_UI_Update;

   procedure Watchdog_UI_Update (GUI_Element : in out Instance) is
      Watchdog_UI : constant access Watchdog_UI_Type :=
        GUI_Element.General_Status_UI.Watchdog_UI'Access;
   begin
      if GUI_Element.cifX_Status.WD_Error and not GUI_Element.WD_Error
      then
         Watchdog_UI.Status_Image.Set
           ("gtk-no", Gtk.Enums.Icon_Size_Button);
         GUI_Element.WD_Error := True;
      end if;

      Watchdog_UI.Time_Value_Label.Set_Markup
        (GUI_Element.Common_Status_Block.Watchdog_Time'Img & " ms");
   end Watchdog_UI_Update;

   procedure Com_Error_UI_Update (GUI_Element : in out Instance) is
      Com_Error_UI : constant access Com_Error_UI_Type :=
        GUI_Element.General_Status_UI.Com_Error_UI'Access;

      function Get_Error_Description (Error : DWord) return String;

      function Get_Error_Description (Error : DWord)
                                  return String is
         Error_String : String (1 .. 20);
      begin
         if Error = CIFX_NO_ERROR then
            return "-";
         else
            DWord_Text_IO.Put (Error_String, Item => Error, Base => 16);
            return cifX.Driver_Get_Error_Description (DWord_To_DInt (Error))
            & " (" & Error_String & ")";
         end if;
      end Get_Error_Description;

   begin
      Com_Error_UI.Values_Label.Set_Markup
        (Get_Error_Description
           (GUI_Element.Common_Status_Block.Communication_Error)
         & CRLF
         & GUI_Element.Common_Status_Block.Error_Count'Img);

   end Com_Error_UI_Update;

   procedure Master_Status_UI_Update (GUI_Element : in out Instance) is
      Master_Status_UI : constant access Master_Status_UI_Type :=
        GUI_Element.Master_Status_UI'Access;
      Common_Status_Block : constant access cifX.Common_Status_Block_Type :=
        GUI_Element.Common_Status_Block'Access;
      Slave_State : String := "Undefined ";
   begin

      if Common_Status_Block.netX_Master_Status.Slave_State =
        cifX.RCX_SLAVE_STATE_OK
      then
         Slave_State := "OK        ";
      elsif Common_Status_Block.netX_Master_Status.Slave_State =
        cifX.RCX_SLAVE_STATE_FAILED
      then
         Slave_State := "Failed    ";
      end if;

      Master_Status_UI.Values_Label.Set_Markup
        (Slave_State & CRLF
         & Common_Status_Block.netX_Master_Status.Slave_Err_Log_Ind'Img
         & CRLF
         & Common_Status_Block.netX_Master_Status.Num_Of_Config_Slaves'Img
         & CRLF
         & Common_Status_Block.netX_Master_Status.Num_Of_Active_Slaves'Img
         & CRLF
         & Common_Status_Block.netX_Master_Status.Num_Of_Diag_Slaves'Img);

   end Master_Status_UI_Update;

   overriding
   function Create
     return Instance
   is

      GUI_Element : Instance;

   begin
      Gtk_New (GUI_Element.Scrolled_Window);
      GUI_Element.Scrolled_Window.Set_Border_Width (Border_Width => 10);
      GUI_Element.Scrolled_Window.Set_Policy
        (Gtk.Enums.Policy_Automatic, Gtk.Enums.Policy_Automatic);

      GUI_Element.Scrolled_Window.Set_Shadow_Type (Gtk.Enums.Shadow_None);

      Gtk_New (GUI_Element.Page_Label, "cifX Status");

      Gtk_New_Vbox (GUI_Element.Page_VBox);

      GUI_Element.Create_Driver_Info_UI;

      GUI_Element.Page_VBox.Pack_Start
        (GUI_Element.Driver_Info_UI.Frame,
         Expand => False, Padding => 10);

      GUI_Element.Create_Channel_Info_UI;

      GUI_Element.Page_VBox.Pack_Start
        (GUI_Element.Channel_Info_UI.Frame,
         Expand => False);

      GUI_Element.Create_General_Status_UI;

      GUI_Element.Page_VBox.Pack_Start
        (GUI_Element.General_Status_UI.Frame,
         Expand => False);

      GUI_Element.Create_Master_Status_UI;

      GUI_Element.Page_VBox.Pack_Start
        (GUI_Element.Master_Status_UI.Frame,
         Expand => False);

      Gtk_New (GUI_Element.Size_Group1, Horizontal);

      GUI_Element.Size_Group1.Add_Widget
        (GUI_Element.Channel_Info_UI.Board_Info_Frame);

      GUI_Element.Size_Group1.Add_Widget
        (GUI_Element.General_Status_UI.Device_State_UI.Frame);

      GUI_Element.Size_Group1.Add_Widget
        (GUI_Element.General_Status_UI.Watchdog_UI.Frame);

      GUI_Element.Scrolled_Window.Add_With_Viewport
        (GUI_Element.Page_VBox);

      return GUI_Element;

   end Create;

   overriding
   function Get_Root_Widget
     (GUI_Element : in Instance)
      return Gtk.Widget.Gtk_Widget
   is

   begin

      return Gtk_Widget (GUI_Element.Scrolled_Window);

   end Get_Root_Widget;

   overriding
   function Get_Label
     (GUI_Element : in Instance)
      return Gtk.Widget.Gtk_Widget
   is

   begin

      return Gtk_Widget (GUI_Element.Page_Label);

   end Get_Label;

   procedure Update
     (GUI_Element : in out Instance)
   is

   begin

      GUI_Element.Common_Status_Block :=
        GUI_Element.cifX_Status.Get_Common_Status_Block;

      GUI_Element.Device_State_UI_Update;

      GUI_Element.Network_State_UI_Update;

      GUI_Element.Watchdog_UI_Update;

      GUI_Element.Com_Error_UI_Update;

      GUI_Element.Master_Status_UI_Update;

      GUI_Element.Common_Status_Block_Previous :=
        GUI_Element.Common_Status_Block;

   end Update;

   procedure Initialise
     (GUI_Element        : in out Instance;
      cifX_Driver_Status : in cifX_Driver_Status_Access;
      cifX_Status        : in cifX_Status_Access)
   is
   begin
      GUI_Element.cifX_Driver_Status := cifX_Driver_Status;
      GUI_Element.cifX_Status        := cifX_Status;

      GUI_Element.Driver_Information :=
        GUI_Element.cifX_Driver_Status.Get_Driver_Information;

      GUI_Element.Driver_Info_UI.Values_Label.Set_Markup
        (CRLF
         & Trim_String (GUI_Element.Driver_Information.Driver_Version) & CRLF
         & GUI_Element.Driver_Information.Board_Cnt'Img);

      GUI_Element.Channel_Information :=
        GUI_Element.cifX_Status.Get_Channel_Information;

      GUI_Element.Channel_Info_UI.Board_Info_Values_Label.Set_Markup
        (CRLF
         & Trim_String (GUI_Element.Channel_Information.Board_Name) & CRLF
         & Trim_String (GUI_Element.Channel_Information.Board_Alias) & CRLF
         & GUI_Element.Channel_Information.Device_Number'Img & CRLF
         & GUI_Element.Channel_Information.Serial_Number'Img);

      GUI_Element.Channel_Info_UI.Firmware_Values_Label.Set_Markup
        (CRLF
         & Trim_String (GUI_Element.Channel_Information.FW_Name) & CRLF

         & GUI_Element.Channel_Information.FW_Major'Img & "."
         & GUI_Element.Channel_Information.FW_Minor'Img & "."
         & GUI_Element.Channel_Information.FW_Revision'Img
         & "(Build "
         & GUI_Element.Channel_Information.FW_Build'Img & ")" & CRLF

         & GUI_Element.Channel_Information.FW_Year'Img & "-"
         & GUI_Element.Channel_Information.FW_Month'Img & "-"
         & GUI_Element.Channel_Information.FW_Day'Img);

      GUI_Element.Page_Label.Set_Label
        (Trim_String (GUI_Element.Channel_Information.Board_Name) & " Status");

   end Initialise;

end A4A.GUI_Elements.cifX_Status_Page;
