
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package is part of the binding to Hilscher cifX Device Driver,
--  a library allowing access to the cifX boards with numerous protocols.
--  </summary>
--  <description>
--  It provides :
--  - rcX_Public data types
--  </description>
--  <group>Protocols</group>
--  <c_version>3.0.1</c_version>

with Interfaces.C;

with A4A.Protocols.HilscherX.cifX_User;
use A4A.Protocols.HilscherX.cifX_User;

package A4A.Protocols.HilscherX.rcX_Public.rcX_Firmware_Identify is

   --------------------------------------------------------------------
   --  from rcX_Public.h
   --------------------------------------------------------------------

   --  #define RCX_FIRMWARE_IDENTIFY_REQ                     0x00001EB6
   RCX_FIRMWARE_IDENTIFY_REQ             : constant := 16#00001EB6#;

   --  #define RCX_FIRMWARE_IDENTIFY_CNF                     0x00001EB7
   RCX_FIRMWARE_IDENTIFY_CNF             : constant := 16#00001EB7#;

   --------------------------------------------------------------------
   --  Packet : RCX_FIRMWARE_IDENTIFY_REQ / RCX_FIRMWARE_IDENTIFY_CNF
   --  This function identifies the currently running firmware on a given
   --  channel
   --------------------------------------------------------------------

   --  /***** request packet *****/

   type RCX_FIRMWARE_IDENTIFY_REQ_DATA_T is
      record
         Channel_Id       : DWord;
         --  channel id
      end record;
   pragma Convention (C, RCX_FIRMWARE_IDENTIFY_REQ_DATA_T);

   type RCX_FIRMWARE_IDENTIFY_REQ_T is
      record
         Head : TLR_Packet_Header_Type;
         --  packet header

         Data : RCX_FIRMWARE_IDENTIFY_REQ_DATA_T;
         --  packet data
      end record;
   pragma Convention (C, RCX_FIRMWARE_IDENTIFY_REQ_T);

   --  #define RCX_FIRMWARE_IDENTIFY_SYSTEM  0xFFFFFFFF
   RCX_FIRMWARE_IDENTIFY_SYSTEM          : constant := 16#FFFFFFFF#;

   type RCX_FIRMWARE_IDENTIFY_REQ_T_Access is
     access all RCX_FIRMWARE_IDENTIFY_REQ_T;

   function To_cifX_Packet is new Ada.Unchecked_Conversion
     (Source => RCX_FIRMWARE_IDENTIFY_REQ_T_Access,
      Target => cifX_Packet_Access);

   function From_cifX_Packet is new Ada.Unchecked_Conversion
     (Source => cifX_Packet_Access,
      Target => RCX_FIRMWARE_IDENTIFY_REQ_T_Access);

   --  /***** confirmation packet *****/

   type RCX_FW_VERSION_T is
      record
         Major    : Word;
         Minor    : Word;
         Build    : Word;
         Revision : Word;
      end record;
   pragma Convention (C, RCX_FW_VERSION_T);

   type RCX_FW_NAME_T is
      record
         Name_Length : Byte := 0;
         Name : Interfaces.C.char_array (1 .. 63) :=
           (others => Interfaces.C.nul);
      end record;
   pragma Convention (C, RCX_FW_NAME_T);

   type RCX_FW_DATE_T is
      record
         Year  : Word := 0;
         Month : Byte := 0;
         Day   : Byte := 0;
      end record;
   pragma Convention (C, RCX_FW_DATE_T);

   type RCX_FW_IDENTIFICATION_T is
      record
         Fw_Version  : RCX_FW_VERSION_T;
         --  firmware version

         Fw_Name     : RCX_FW_NAME_T;
         --  firmware name

         Fw_Date     : RCX_FW_DATE_T;
         --  firmware date

      end record;
   --  Firmware Identification Structure
   pragma Convention (C, RCX_FW_IDENTIFICATION_T);

   type RCX_FIRMWARE_IDENTIFY_CNF_DATA_T is
      record
         Fw_Identification  : RCX_FW_IDENTIFICATION_T;
         --  firmware identification

      end record;
   --  Firmware Identification Structure
   pragma Convention (C, RCX_FIRMWARE_IDENTIFY_CNF_DATA_T);

   type RCX_FIRMWARE_IDENTIFY_CNF_T is
      record
         Head : TLR_Packet_Header_Type;
         --  packet header

         Data : RCX_FIRMWARE_IDENTIFY_CNF_DATA_T;
         --  packet data

      end record;
   pragma Convention (C, RCX_FIRMWARE_IDENTIFY_CNF_T);

   type RCX_FIRMWARE_IDENTIFY_CNF_T_Access is
     access all RCX_FIRMWARE_IDENTIFY_CNF_T;

   function To_cifX_Packet is new Ada.Unchecked_Conversion
     (Source => RCX_FIRMWARE_IDENTIFY_CNF_T_Access,
      Target => cifX_Packet_Access);

   function From_cifX_Packet is new Ada.Unchecked_Conversion
     (Source => cifX_Packet_Access,
      Target => RCX_FIRMWARE_IDENTIFY_CNF_T_Access);

end A4A.Protocols.HilscherX.rcX_Public.rcX_Firmware_Identify;
