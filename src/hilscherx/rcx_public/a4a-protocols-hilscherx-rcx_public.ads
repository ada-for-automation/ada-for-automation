
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package is part of the binding to Hilscher cifX Device Driver,
--  a library allowing access to the cifX boards with numerous protocols.
--  </summary>
--  <description>
--  It provides :
--  - rcX_Public data types,
--  - cifX C functions binding.
--  </description>
--  <group>Protocols</group>
--  <c_version>3.0.1</c_version>

package A4A.Protocols.HilscherX.rcX_Public is

   --------------------------------------------------------------------
   --  from TLR_Types.h
   --------------------------------------------------------------------

   type TLR_Packet_Header_Type is
      record
         Dest       : DWord;
         --  destination of packet, process queue

         Src        : DWord;
         --  source of packet, process queue

         Dest_Id    : DWord;
         --  destination reference of packet

         Src_Id     : DWord;
         --  source reference of packet

         Len        : DWord;
         --  length of packet data without header

         Id         : DWord;
         --  identification handle of sender

         State      : DWord;
         --  status code of operation

         Cmd        : DWord;
         --  packet command

         Ext        : DWord;
         --  extension

         Rout       : DWord;
         --  router

      end record;
   pragma Convention (C, TLR_Packet_Header_Type);
   --  TLR Packet header

   type TLR_EMPTY_PACKET_T is
      record
         Head : TLR_Packet_Header_Type;
         --  packet header

      end record;
   pragma Convention (C, TLR_EMPTY_PACKET_T);

   --------------------------------------------------------------------
   --  from rcX_User.h
   --------------------------------------------------------------------

   --  /*===================================================================*/
   --  /*                                                                   */
   --  /* RCX Packet Definition                                             */
   --  /*                                                                   */
   --  /*===================================================================*/

   --  /* Structure of the RCX packet header */

   --  #define RCX_MAX_PACKET_SIZE               1596

   RCX_MAX_PACKET_SIZE                   : constant := 1596;
   --  Maximum size of the RCX packet in bytes

   --  #define RCX_PACKET_HEADER_SIZE            40

   RCX_PACKET_HEADER_SIZE                : constant := 40;
   --  Maximum size of the RCX packet header in bytes

   --  #define RCX_MAX_DATA_SIZE (RCX_MAX_PACKET_SIZE - RCX_PACKET_HEADER_SIZE)

   RCX_MAX_DATA_SIZE                     : constant :=
     RCX_MAX_PACKET_SIZE - RCX_PACKET_HEADER_SIZE;
   --  Maximum RCX packet data size

   --------------------------------------------------------------
   --  from rcX_Public.h
   --------------------------------------------------------------------

end A4A.Protocols.HilscherX.rcX_Public;
