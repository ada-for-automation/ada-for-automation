
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package is part of the binding to Hilscher cifX Device Driver,
--  a library allowing access to the cifX boards with numerous protocols.
--  </summary>
--  <description>
--  It provides :
--  - rcX_Public data types
--  </description>
--  <group>Protocols</group>
--  <c_version>3.0.1</c_version>

with A4A.Log;

package body A4A.Protocols.HilscherX.rcX_Public.rcX_Register_App.FB is

   procedure Initialise
     (Function_Block   : in out Instance;
      Channel_Access   : Channel_Messaging.Instance_Access;
      Receive_Queue_ID : in DWord) is
   begin

      Function_Block.My_Channel          := Channel_Access;
      Function_Block.My_Receive_Queue_ID := Receive_Queue_ID;

      Function_Block.Status := X00;

      Function_Block.Error_Flag := False;

   end Initialise;

   procedure Cyclic
     (Function_Block : in out Instance;
      Do_Command     : in Boolean;
      Done           : out Boolean;
      Error          : out Boolean) is
   begin

      case Function_Block.Status is

         when X00 =>

            if Do_Command then
               Function_Block.Error_Flag := False;
               Function_Block.Answer_Got := False;
               Function_Block.Status := X01;
               Function_Block.Message_Id := Function_Block.Message_Id + 1;
            end if;

         when X01 =>

            Function_Block.My_Channel.Get_Packet
              (Item   => Function_Block.Request,
               Result => Function_Block.Packet_Got);

            if Function_Block.Packet_Got then
               Function_Block.Status := X02;
            end if;

         when X02 =>

            Function_Block.Register_App_Req :=
              From_cifX_Packet (Function_Block.Request);

            Function_Block.Register_App_Req.Head.Dest    := 16#20#;

            Function_Block.Register_App_Req.Head.Src     :=
              Function_Block.My_Receive_Queue_App;

            Function_Block.Register_App_Req.Head.Dest_Id := 0;

            Function_Block.Register_App_Req.Head.Src_Id  :=
              Function_Block.My_Receive_Queue_ID;

            Function_Block.Register_App_Req.Head.Len     := 0;

            Function_Block.Register_App_Req.Head.Id      :=
              Function_Block.Message_Id;

            Function_Block.Register_App_Req.Head.State   := 0;

            Function_Block.Register_App_Req.Head.Cmd     :=
              RCX_REGISTER_APP_REQ;

            Function_Block.Register_App_Req.Head.Ext     := 0;
            Function_Block.Register_App_Req.Head.Rout    := 0;

            Function_Block.Status := X03;

         when X03 =>

            Function_Block.My_Channel.Send
              (Item   => Function_Block.Request,
               Result => Function_Block.Packet_Sent);

            if Function_Block.Packet_Sent then
                  A4A.Log.Logger.Put
                    (Who  => My_Ident & ".Cyclic",
                     What => "Request Sent");
               Function_Block.Status := X04;
            end if;

         when X04 =>

            --  Waiting Answer...

            if Function_Block.Answer_Got then

               Function_Block.Register_App_Cnf :=
                 From_cifX_Packet (Function_Block.Confirmation);

               if Function_Block.Register_App_Cnf.Head.Cmd /=
                 RCX_REGISTER_APP_CNF
               then

                  Function_Block.Error_Flag := True;

                  A4A.Log.Logger.Put
                    (Who  => My_Ident & ".Cyclic",
                     What => "Wrong Command received");

               elsif Function_Block.Register_App_Cnf.Head.Id /=
                 Function_Block.Message_Id
               then

                  Function_Block.Error_Flag := True;

                  A4A.Log.Logger.Put
                    (Who  => My_Ident & ".Cyclic",
                     What => "Wrong Message Id received");

               elsif Function_Block.Register_App_Cnf.Head.State /= 0 then

                  Function_Block.Error_Flag := True;

                  Function_Block.Last_Error :=
                    Function_Block.Register_App_Cnf.Head.State;

               else

                  A4A.Log.Logger.Put
                    (Who  => My_Ident & ".Cyclic",
                     What => "Confirmation received fine");

               end if;

               Function_Block.My_Channel.Return_Packet
                 (Item => Function_Block.Confirmation);

               Function_Block.Status := X05;
            end if;

         when X05 =>

            if not Do_Command then
               Function_Block.Status := X00;
            end if;

      end case;

      Done := (Function_Block.Status = X05);

      Error := Function_Block.Error_Flag;

   end Cyclic;

   procedure Handle_Answer
     (Function_Block : in out Instance;
      Answer         : in cifX_Packet_Access) is
   begin

      Function_Block.Confirmation := Answer;
      Function_Block.Answer_Got   := True;

   end Handle_Answer;

   function Get_Last_Error
     (Function_Block : in Instance) return DWord is
   begin

      return Function_Block.Last_Error;

   end Get_Last_Error;

end A4A.Protocols.HilscherX.rcX_Public.rcX_Register_App.FB;
