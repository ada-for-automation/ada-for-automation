
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package is part of the binding to Hilscher cifX Device Driver,
--  a library allowing access to the cifX boards with numerous protocols.
--  </summary>
--  <description>
--  It provides :
--  - rcX_Public data types
--  </description>
--  <group>Protocols</group>
--  <c_version>3.0.1</c_version>

with A4A.Protocols.HilscherX.Channel_Messaging;
use A4A.Protocols.HilscherX;

package A4A.Protocols.HilscherX.rcX_Public.rcX_Unregister_App.FB is

   type Instance is tagged limited private;
   type Instance_Access is access all Instance;

   procedure Initialise
     (Function_Block : in out Instance;
      Channel_Access : in Channel_Messaging.Instance_Access;
      Receive_Queue_ID : in DWord);

   procedure Cyclic
     (Function_Block : in out Instance;
      Do_Command     : in Boolean;
      Done           : out Boolean;
      Error          : out Boolean);

   procedure Handle_Answer
     (Function_Block : in out Instance;
      Answer         : in cifX_Packet_Access);

   function Get_Last_Error
     (Function_Block : in Instance) return DWord;

private

   My_Ident : String :=
     "A4A.Protocols.HilscherX.rcX_Public.rcX_Unregister_App.FB";

   type Block_Status is
     (X00,
      --  Initial

      X01,
      --  Get packet from pool

      X02,
      --  Fill packet

      X03,
      --  Send packet

      X04,
      --  Get answer

      X05
      --  Done
     );

   type Instance is tagged limited
      record

         My_Channel   : Channel_Messaging.Instance_Access;

         Status       : Block_Status := X00;

         Request      : cifX_Packet_Access;
         Confirmation : cifX_Packet_Access;

         Unregister_App_Req : RCX_UNREGISTER_APP_REQ_T_Access;
         Unregister_App_Cnf : RCX_UNREGISTER_APP_CNF_T_Access;

         Message_Id   : DWord := 0;

         Packet_Got   : Boolean := False;
         Packet_Sent  : Boolean := False;
         Answer_Got   : Boolean := False;

         My_Receive_Queue_App : DWord := Channel_Messaging.A4A_App_0;
         My_Receive_Queue_ID  : DWord;

         Error_Flag : Boolean;
         Last_Error : DWord;

      end record;

end A4A.Protocols.HilscherX.rcX_Public.rcX_Unregister_App.FB;
