
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package is part of the binding to Hilscher cifX Device Driver,
--  a library allowing access to the cifX boards with numerous protocols.
--  </summary>
--  <description>
--  It provides :
--  - rcX_Public data types
--  </description>
--  <group>Protocols</group>
--  <c_version>3.0.1</c_version>

with A4A.Protocols.HilscherX.cifX_User;
use A4A.Protocols.HilscherX.cifX_User;

package A4A.Protocols.HilscherX.rcX_Public.rcX_Register_App is

   --------------------------------------------------------------------
   --  from rcX_Public.h
   --------------------------------------------------------------------

   --  #define RCX_REGISTER_APP_REQ                     0x00002F10
   RCX_REGISTER_APP_REQ             : constant := 16#00002F10#;

   --  #define RCX_REGISTER_APP_CNF                     0x00002F11
   RCX_REGISTER_APP_CNF             : constant := 16#00002F11#;

   --------------------------------------------------------------------
   --  Packet : RCX_REGISTER_APP_REQ / RCX_REGISTER_APP_CNF
   --  This packet allows to register an application
   --------------------------------------------------------------------

   --  /***** request packet *****/

   type RCX_REGISTER_APP_REQ_T is new TLR_EMPTY_PACKET_T;
   pragma Convention (C, RCX_REGISTER_APP_REQ_T);

   type RCX_REGISTER_APP_REQ_T_Access is
     access all RCX_REGISTER_APP_REQ_T;

   function To_cifX_Packet is new Ada.Unchecked_Conversion
     (Source => RCX_REGISTER_APP_REQ_T_Access,
      Target => cifX_Packet_Access);

   function From_cifX_Packet is new Ada.Unchecked_Conversion
     (Source => cifX_Packet_Access,
      Target => RCX_REGISTER_APP_REQ_T_Access);

   --  /***** confirmation packet *****/

   type RCX_REGISTER_APP_CNF_T is new TLR_EMPTY_PACKET_T;
   pragma Convention (C, RCX_REGISTER_APP_CNF_T);

   type RCX_REGISTER_APP_CNF_T_Access is
     access all RCX_REGISTER_APP_CNF_T;

   function To_cifX_Packet is new Ada.Unchecked_Conversion
     (Source => RCX_REGISTER_APP_CNF_T_Access,
      Target => cifX_Packet_Access);

   function From_cifX_Packet is new Ada.Unchecked_Conversion
     (Source => cifX_Packet_Access,
      Target => RCX_REGISTER_APP_CNF_T_Access);

end A4A.Protocols.HilscherX.rcX_Public.rcX_Register_App;
