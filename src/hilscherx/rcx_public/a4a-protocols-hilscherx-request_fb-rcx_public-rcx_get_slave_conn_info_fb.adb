
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package is part of the binding to Hilscher cifX Device Driver,
--  a library allowing access to the cifX boards with numerous protocols.
--  </summary>
--  <description>
--  It provides :
--  - rcX_Public data types
--  </description>
--  <group>Protocols</group>
--  <c_version>3.0.1</c_version>

package body
  A4A.Protocols.HilscherX.Request_FB.rcX_Public.rcX_Get_Slave_Conn_Info_FB is

   overriding
   procedure Fill_Request
     (Function_Block : in out Instance;
      Request        : cifX_Packet_Access) is

      Req  : constant RCX_PACKET_GET_SLAVE_CONN_INFO_REQ_T_Access :=
        From_cifX_Packet (Request);

   begin

      Req.Head.Len := DWord'Size / 8;

      Req.Head.Cmd := RCX_GET_SLAVE_CONN_INFO_REQ;

      Req.Data.Handle := Function_Block.Handle;

   end Fill_Request;

   overriding
   procedure Store_Result
     (Function_Block : in out Instance;
      Confirmation   : cifX_Packet_Access;
      Error          : out Boolean;
      Type_Of_Error  : out Error_Type;
      Last_Error     : out DWord) is

      Cnf  : RCX_PACKET_GET_SLAVE_CONN_INFO_CNF_T_Access;

   begin

      if Confirmation.Header.Cmd /= RCX_GET_SLAVE_CONN_INFO_CNF then

         Error := True;

         Type_Of_Error := Error_Cmd;

      elsif Confirmation.Header.State /= 0 then

         Error := True;

         Type_Of_Error := Error_Status;

         Last_Error := Confirmation.Header.State;

      else

         Error := False;

         Type_Of_Error := Error_None;

         Cnf := From_cifX_Packet (Confirmation);

         Function_Block.Bytes_Num := Natural (Cnf.Head.Len - 8);

         Function_Block.Conf_Data.Handle    := Cnf.Data.Handle;

         Function_Block.Conf_Data.Struct_ID := Cnf.Data.Struct_ID;

         for Index
         in Function_Block.Conf_Data.State'First ..
           Function_Block.Conf_Data.State'First
             + Function_Block.Bytes_Num - 1
         loop

            Function_Block.Conf_Data.State (Index) := Cnf.Data.State (Index);

         end loop;

      end if;

   end Store_Result;

   overriding
   function Get_Ident
     (Function_Block : in out Instance) return String is
      pragma Unreferenced (Function_Block);
   begin
      return My_Ident;
   end Get_Ident;

   procedure Set_Parameters
     (Function_Block : in out Instance;
      Handle         : in DWord) is
   begin

      Function_Block.Handle := Handle;

   end Set_Parameters;

   procedure Get_Data
     (Function_Block : in Instance;
      Bytes_Num      : out Natural;
      Handle         : out DWord;
      Struct_ID      : out DWord;
      State          : out Byte_Array) is

      Bytes_Max    : Natural;
      First_In     : constant Natural :=
        Function_Block.Conf_Data.State'First;
      First_Out    : constant Natural := State'First;

   begin

      if Function_Block.Bytes_Num > State'Length then
         Bytes_Max := State'Length;
      else
         Bytes_Max := Function_Block.Bytes_Num;
      end if;

      Bytes_Num := Function_Block.Bytes_Num;

      Handle := Function_Block.Conf_Data.Handle;

      Struct_ID := Function_Block.Conf_Data.Struct_ID;

      for Index in 0 .. Bytes_Max - 1 loop

         State (First_Out + Index) :=
           Function_Block.Conf_Data.State (First_In + Index);

      end loop;

   end Get_Data;

end A4A.Protocols.HilscherX.Request_FB.rcX_Public.rcX_Get_Slave_Conn_Info_FB;
