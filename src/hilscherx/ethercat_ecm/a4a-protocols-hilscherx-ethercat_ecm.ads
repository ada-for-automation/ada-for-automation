
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package is part of the binding to Hilscher cifX Device Driver,
--  a library allowing access to the cifX boards with numerous protocols.
--  </summary>
--  <description>
--  It provides :
--  - EtherCAT Master data types,
--  - cifX C functions binding.
--  </description>
--  <group>Protocols</group>
--  <c_version>3.0.1</c_version>

with Interfaces.C;

package A4A.Protocols.HilscherX.EtherCAT_ECM is

   --------------------------------------------------------------------
   --  from EtherCATMaster_Public.h
   --------------------------------------------------------------------

   --  #define ETHERCAT_MASTER_BUSSTATE_UNKNOWN (0)  /* unknown */
   --  #define ETHERCAT_MASTER_BUSSTATE_INIT    (1)  /* init */
   --  #define ETHERCAT_MASTER_BUSSTATE_PREOP   (2)  /* pre-operational */
   --  #define ETHERCAT_MASTER_BUSSTATE_BOOT    (3)  /* bootstrap mode */
   --  #define ETHERCAT_MASTER_BUSSTATE_SAFEOP  (4)  /* safe operational */
   --  #define ETHERCAT_MASTER_BUSSTATE_OP      (8)  /* operational */

   --------------------------------------------------------------------
   --  from EtherCATMaster_DiagStructDef.h
   --------------------------------------------------------------------

   Diag_Bytes_Num : constant := 5 * 4 + 80;

   type ETHERCAT_MASTER_DIAG_GET_SLAVE_DIAG_T is
      record
         Station_Address          : DWord;
         --  Assigned station address

         Auto_Inc_Address         : DWord;
         --  Position based address

         Current_State            : DWord;
         --  Current State

         Last_Error               : DWord;
         --  Last Error reported by the slave (slave register 0x134)

         Slave_Name               : Interfaces.C.char_array (1 .. 80);
         --  Name of the station (as defined in bus configuration)

         Emergency_Reported       : DWord;
         --  Slave has send an emergency

      end record;
   for ETHERCAT_MASTER_DIAG_GET_SLAVE_DIAG_T'Size
   use Diag_Bytes_Num * Byte'Size;
   pragma Convention (C, ETHERCAT_MASTER_DIAG_GET_SLAVE_DIAG_T);

   type ETHERCAT_MASTER_DIAG_GET_SLAVE_DIAG_T_Access is
     access all ETHERCAT_MASTER_DIAG_GET_SLAVE_DIAG_T;

   type ETHERCAT_MASTER_DIAG_GET_SLAVE_DIAG_BYTES_T is
      record
         Bytes          : Byte_Array (1 .. Diag_Bytes_Num);
         --  Station Status bytes

      end record;
   for ETHERCAT_MASTER_DIAG_GET_SLAVE_DIAG_BYTES_T'Size
   use Diag_Bytes_Num * Byte'Size;
   pragma Convention (C, ETHERCAT_MASTER_DIAG_GET_SLAVE_DIAG_BYTES_T);

   type ETHERCAT_MASTER_DIAG_GET_SLAVE_DIAG_BYTES_T_Access is
     access all ETHERCAT_MASTER_DIAG_GET_SLAVE_DIAG_BYTES_T;

   function From_Byte_Array is new Ada.Unchecked_Conversion
     (Source => ETHERCAT_MASTER_DIAG_GET_SLAVE_DIAG_BYTES_T_Access,
      Target => ETHERCAT_MASTER_DIAG_GET_SLAVE_DIAG_T_Access);

end A4A.Protocols.HilscherX.EtherCAT_ECM;
