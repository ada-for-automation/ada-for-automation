
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package is part of the binding to Hilscher cifX Device Driver,
--  a library allowing access to the cifX boards with numerous protocols.
--  </summary>
--  <description>
--  It provides :
--  - EtherCAT Master data types,
--  </description>
--  <group>Protocols</group>
--  <c_version>3.0.1</c_version>

package body A4A.Protocols.HilscherX.Request_FB.EtherCAT_ECM.SDO_Download_FB is

   overriding
   procedure Fill_Request
     (Function_Block : in out Instance;
      Request        : cifX_Packet_Access) is

      Req : constant ETHERCAT_MASTER_PACKET_SDO_DOWNLOAD_REQ_T_Access :=
        From_cifX_Packet (Request);

      Count_Max     : constant Positive :=
        Positive (Function_Block.Data_Count);
      First_In      : constant Natural := Function_Block.SDO_Data'First;
      First_Out     : constant Natural := Req.Data.SDO_Data'First;

   begin

      Req.Head.Len :=
        ((DWord'Size / 8) * 4) + Function_Block.Data_Count;

      Req.Head.Cmd := ETHERCAT_MASTER_CMD_SDO_DOWNLOAD_REQ;

      Req.Data.Node_Id   := Function_Block.Node_Id;

      Req.Data.Index     := Function_Block.Index;

      Req.Data.Sub_Index := Function_Block.Sub_Index;

      Req.Data.Data_Count := Function_Block.Data_Count;

      for Index in 0 .. Count_Max - 1 loop

         Req.Data.SDO_Data (First_Out + Index) :=
           Function_Block.SDO_Data (First_In + Index);

      end loop;

   end Fill_Request;

   overriding
   procedure Store_Result
     (Function_Block : in out Instance;
      Confirmation   : cifX_Packet_Access;
      Error          : out Boolean;
      Type_Of_Error  : out Error_Type;
      Last_Error     : out DWord) is
      pragma Unreferenced (Function_Block);

   begin

      if Confirmation.Header.Cmd /= ETHERCAT_MASTER_CMD_SDO_DOWNLOAD_CNF then

         Error := True;

         Type_Of_Error := Error_Cmd;

      elsif Confirmation.Header.State /= 0 then

         Error := True;

         Type_Of_Error := Error_Status;

         Last_Error := Confirmation.Header.State;

      else

         Error := False;

         Type_Of_Error := Error_None;

      end if;

   end Store_Result;

   overriding
   function Get_Ident
     (Function_Block : in out Instance) return String is
      pragma Unreferenced (Function_Block);
   begin
      return My_Ident;
   end Get_Ident;

   procedure Set_Parameters
     (Function_Block : in out Instance;
      Node_Id        : in DWord;
      Index          : in DWord;
      Sub_Index      : in DWord;
      Data_Count     : in Positive;
      SDO_Data       : in Byte_Array) is

      Count_Max     : Positive;
      First_In      : constant Natural := SDO_Data'First;
      First_Out     : constant Natural := Function_Block.SDO_Data'First;

   begin

      Function_Block.Node_Id    := Node_Id;
      Function_Block.Index      := Index;
      Function_Block.Sub_Index  := Sub_Index;

      if Data_Count > SDO_Data'Length then

         Count_Max := SDO_Data'Length;

      elsif Data_Count > Function_Block.SDO_Data'Length then

         Count_Max := Function_Block.SDO_Data'Length;

      else

         Count_Max := Data_Count;

      end if;

      Function_Block.Data_Count := DWord (Count_Max);

      for Index in 0 .. Count_Max - 1 loop

         Function_Block.SDO_Data (First_Out + Index) :=
           SDO_Data (First_In + Index);

      end loop;

   end Set_Parameters;

end A4A.Protocols.HilscherX.Request_FB.EtherCAT_ECM.SDO_Download_FB;
