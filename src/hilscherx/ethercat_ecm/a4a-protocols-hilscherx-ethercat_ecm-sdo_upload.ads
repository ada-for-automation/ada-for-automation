
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package is part of the binding to Hilscher cifX Device Driver,
--  a library allowing access to the cifX boards with numerous protocols.
--  </summary>
--  <description>
--  It provides :
--  - EtherCAT Master data types,
--  </description>
--  <group>Protocols</group>
--  <c_version>3.0.1</c_version>

with A4A.Protocols.HilscherX.cifX_User;
use A4A.Protocols.HilscherX.cifX_User;

with A4A.Protocols.HilscherX.rcX_Public;
use A4A.Protocols.HilscherX.rcX_Public;

package A4A.Protocols.HilscherX.EtherCAT_ECM.SDO_Upload is

   --------------------------------------------------------------------
   --  from EtherCATMaster_Public.h
   --------------------------------------------------------------------

   --  #define ETHERCAT_MASTER_CMD_SDO_UPLOAD_REQ            0x00650006
   ETHERCAT_MASTER_CMD_SDO_UPLOAD_REQ       : constant := 16#00650006#;

   --  #define ETHERCAT_MASTER_CMD_SDO_UPLOAD_CNF            0x00650007
   ETHERCAT_MASTER_CMD_SDO_UPLOAD_CNF       : constant := 16#00650007#;

   --  #define  ETHERCAT_MASTER_COE_MAX_SDO_UPLOAD_DATA \
   --                           (RCX_MAX_DATA_SIZE - (sizeof(TLR_UINT32) * 4))

   ETHERCAT_MASTER_COE_MAX_SDO_UPLOAD_DATA : constant :=
     RCX_MAX_DATA_SIZE - ((DWord'Size / 8) * 4);

   --------------------------------------------------------------------
   --  Packet :
   --  ETHERCAT_MASTER_CMD_SDO_UPLOAD_REQ / ETHERCAT_MASTER_CMD_SDO_UPLOAD_CNF
   --  Upload an SDO Object from a Slave
   --------------------------------------------------------------------

   --  /***** request packet *****/

   type ETHERCAT_MASTER_PACKET_SDO_UPLOAD_REQ_DATA_T is
      record
         Node_Id              : DWord;
         --  Station Address, e. g. 1001, 1002, ...

         Index                : DWord;
         --  Object Index

         Sub_Index            : DWord;
         --  Object SubIndex

      end record;
   pragma Convention (C, ETHERCAT_MASTER_PACKET_SDO_UPLOAD_REQ_DATA_T);

   type ETHERCAT_MASTER_PACKET_SDO_UPLOAD_REQ_T is
      record
         Head : TLR_Packet_Header_Type;
         --  packet header

         Data : ETHERCAT_MASTER_PACKET_SDO_UPLOAD_REQ_DATA_T;
         --  packet data
      end record;
   pragma Convention (C, ETHERCAT_MASTER_PACKET_SDO_UPLOAD_REQ_T);

   type ETHERCAT_MASTER_PACKET_SDO_UPLOAD_REQ_T_Access is
     access all ETHERCAT_MASTER_PACKET_SDO_UPLOAD_REQ_T;

   function To_cifX_Packet is new Ada.Unchecked_Conversion
     (Source => ETHERCAT_MASTER_PACKET_SDO_UPLOAD_REQ_T_Access,
      Target => cifX_Packet_Access);

   function From_cifX_Packet is new Ada.Unchecked_Conversion
     (Source => cifX_Packet_Access,
      Target => ETHERCAT_MASTER_PACKET_SDO_UPLOAD_REQ_T_Access);

   --  /***** confirmation packet *****/

   type ETHERCAT_MASTER_PACKET_SDO_UPLOAD_CNF_DATA_POS_T is
      record
         Node_Id              : DWord;
         --  Station Address (same as in request)

         Index                : DWord;
         --  Object Index (same as in request)

         Sub_Index            : DWord;
         --  Object Sub index (same as in request)

         Data_Count           : DWord;
         --  Length of SDO data
         --  (depends on chosen object index and object sub index)

         SDO_Data                 :
         Byte_Array (1 .. ETHERCAT_MASTER_COE_MAX_SDO_UPLOAD_DATA);
         --  SDO data

      end record;
   --  Positive confirmation
   pragma Convention (C, ETHERCAT_MASTER_PACKET_SDO_UPLOAD_CNF_DATA_POS_T);

   type ETHERCAT_MASTER_PACKET_SDO_UPLOAD_CNF_DATA_NEG_T is
      record
         Node_Id              : DWord;
         --  Station Address (same as in request)

         Index                : DWord;
         --  Object Index (same as in request)

         Sub_Index            : DWord;
         --  Object Sub index (same as in request)

      end record;
   --  Negative confirmation
   pragma Convention (C, ETHERCAT_MASTER_PACKET_SDO_UPLOAD_CNF_DATA_NEG_T);

   type ETHERCAT_MASTER_PACKET_SDO_UPLOAD_CNF_POS_T is
      record
         Head : TLR_Packet_Header_Type;
         --  packet header

         Data : ETHERCAT_MASTER_PACKET_SDO_UPLOAD_CNF_DATA_POS_T;
         --  packet data

      end record;
   --  Positive confirmation
   pragma Convention (C, ETHERCAT_MASTER_PACKET_SDO_UPLOAD_CNF_POS_T);

   type ETHERCAT_MASTER_PACKET_SDO_UPLOAD_CNF_POS_T_Access is
     access all ETHERCAT_MASTER_PACKET_SDO_UPLOAD_CNF_POS_T;

   function To_cifX_Packet is new Ada.Unchecked_Conversion
     (Source => ETHERCAT_MASTER_PACKET_SDO_UPLOAD_CNF_POS_T_Access,
      Target => cifX_Packet_Access);

   function From_cifX_Packet is new Ada.Unchecked_Conversion
     (Source => cifX_Packet_Access,
      Target => ETHERCAT_MASTER_PACKET_SDO_UPLOAD_CNF_POS_T_Access);

   type ETHERCAT_MASTER_PACKET_SDO_UPLOAD_CNF_NEG_T is
      record
         Head : TLR_Packet_Header_Type;
         --  packet header

         Data : ETHERCAT_MASTER_PACKET_SDO_UPLOAD_CNF_DATA_NEG_T;
         --  packet data

      end record;
   --  Negative confirmation
   pragma Convention (C, ETHERCAT_MASTER_PACKET_SDO_UPLOAD_CNF_NEG_T);

   type ETHERCAT_MASTER_PACKET_SDO_UPLOAD_CNF_NEG_T_Access is
     access all ETHERCAT_MASTER_PACKET_SDO_UPLOAD_CNF_NEG_T;

   function To_cifX_Packet is new Ada.Unchecked_Conversion
     (Source => ETHERCAT_MASTER_PACKET_SDO_UPLOAD_CNF_NEG_T_Access,
      Target => cifX_Packet_Access);

   function From_cifX_Packet is new Ada.Unchecked_Conversion
     (Source => cifX_Packet_Access,
      Target => ETHERCAT_MASTER_PACKET_SDO_UPLOAD_CNF_NEG_T_Access);

end A4A.Protocols.HilscherX.EtherCAT_ECM.SDO_Upload;
