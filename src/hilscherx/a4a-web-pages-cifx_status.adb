
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with A4A.Protocols.HilscherX.cifX_Errors;
use A4A.Protocols.HilscherX.cifX_Errors;

with A4A.Web.Controllers.Connection_Stats_Controllers;
use A4A.Web.Controllers;

with UXStrings; use UXStrings;

package body A4A.Web.Pages.cifX_Status is

   procedure Initialise
     (cifX_Driver_Status : in cifX_Driver_Status_Access;
      cifX_Status        : in cifX_Status_Access)
   is
   begin
      My_cifX_Driver_Status := cifX_Driver_Status;
      My_cifX_Status        := cifX_Status;

      Driver_Information :=
        My_cifX_Driver_Status.Get_Driver_Information;

      Channel_Information :=
        My_cifX_Status.Get_Channel_Information;

   end Initialise;

   overriding
   procedure Update_Page (Web_Page : access Instance) is

   begin

      Web_Page.Common_View.Connection_Stats_View.Update_View;

      Web_Page.View_Update;

   end Update_Page;

   procedure On_Connect
     (Main_Window : in out Gnoga.Gui.Window.Window_Type'Class;
      Connection  : access
        Gnoga.Application.Multi_Connect.Connection_Holder_Type)
   is
      Web_Page : constant Instance_Access := new Instance;

      My_Ident : aliased String :=
        "A4A.Web.Pages.cifX_Status_View_Updater";
      View_Updater_Task : A4A.Web.Pages.Updater_Task_Type
        (My_Ident                    => My_Ident'Access,
         Web_Page                    => Web_Page);

   begin
      Main_Window.Connection_Data (Web_Page);

      Main_Window.Document.Load_CSS (From_UTF_8 (W3_CSS_URL));
      Main_Window.Document.Load_CSS (From_UTF_8 (Font_Awesome_CSS_URL));
      Main_Window.Document.Load_CSS (From_UTF_8 (APP_CSS_URL));

      Connection_Stats_Controllers.Connection;

      Common_View_Setup (Web_Page.Common_View,
                         Main_Window'Unchecked_Access,
                         "130-cifx-status.html");

      --
      --  cifX Status View
      --

      Web_Page.Header_Label_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "header-label");

      Web_Page.Header_Label_View.Inner_HTML (From_UTF_8 (Label));

      --
      --  Driver Information
      --

      Web_Page.Driver_Info_View_Create;
      --
      --  Channel Information
      --

      Web_Page.Channel_Info_View_Create;

      --
      --  General Status
      --

      Web_Page.Device_View_Create;
      Web_Page.Network_View_Create;
      Web_Page.Watchdog_View_Create;
      Web_Page.Com_Error_View_Create;
      Web_Page.Master_View_Create;

      View_Updater_Task.Start;

      Connection.Hold;

      View_Updater_Task.Stop;

   end On_Connect;

   procedure Driver_Info_View_Create
     (Web_Page : in out Instance)
   is
   begin
      Web_Page.Driver_Version_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "driver-version");

      Web_Page.Driver_Version_View.Inner_HTML
       (From_UTF_8 (Driver_Information.Driver_Version));

      Web_Page.Driver_Board_Count_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "driver-board-count");

      Web_Page.Driver_Board_Count_View.Inner_HTML
       (From_UTF_8 (Driver_Information.Board_Cnt'Img));

   end Driver_Info_View_Create;

   procedure Channel_Info_View_Create
     (Web_Page : in out Instance)
   is
   begin
      Web_Page.Board_Name_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "board-name");

      Web_Page.Board_Name_View.Inner_HTML
        (From_UTF_8 (Channel_Information.Board_Name));

      Web_Page.Board_Alias_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "board-alias");

      Web_Page.Board_Alias_View.Inner_HTML
        (From_UTF_8 (Channel_Information.Board_Alias));

      Web_Page.Device_Number_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "device-number");

      Web_Page.Device_Number_View.Inner_HTML
        (From_UTF_8 (Channel_Information.Device_Number'Img));

      Web_Page.Serial_Number_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "serial-number");

      Web_Page.Serial_Number_View.Inner_HTML
        (From_UTF_8 (Channel_Information.Serial_Number'Img));

      Web_Page.Firmware_Name_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "firmware-name");

      Web_Page.Firmware_Name_View.Inner_HTML
        (From_UTF_8 (Channel_Information.FW_Name));

      Web_Page.Firmware_Version_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "firmware-version");

      Web_Page.Firmware_Version_View.Inner_HTML
        (From_UTF_8
           (Channel_Information.FW_Major'Img & "."
            & Channel_Information.FW_Minor'Img & "."
            & Channel_Information.FW_Revision'Img
            & "(Build "
            & Channel_Information.FW_Build'Img & ")"));

      Web_Page.Firmware_Date_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "firmware-date");

      Web_Page.Firmware_Date_View.Inner_HTML
        (From_UTF_8
           (Channel_Information.FW_Year'Img & "-"
            & Channel_Information.FW_Month'Img & "-"
            & Channel_Information.FW_Day'Img));

   end Channel_Info_View_Create;

   procedure Device_View_Create
     (Web_Page : in out Instance)
   is
   begin
      Web_Page.Device_Ready_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "device-ready");

      Web_Page.Device_Running_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "device-running");

      Web_Page.Device_Bus_On_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "device-bus-on");

      Web_Page.Device_Com_OK_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "device-communicating");

      Web_Page.Device_Error_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "device-error");

   end Device_View_Create;

   procedure Network_View_Create
     (Web_Page : in out Instance)
   is
   begin
      Web_Page.Network_Offline_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "network-offline");

      Web_Page.Network_Stop_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "network-stop");

      Web_Page.Network_Idle_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "network-idle");

      Web_Page.Network_Operate_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "network-operate");

   end Network_View_Create;

   procedure Watchdog_View_Create
     (Web_Page : in out Instance)
   is
   begin
      Web_Page.cifX_Watchdog_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "cifx-watchdog");

      Web_Page.cifX_Watchdog_CTO_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "configured-timeout");

   end Watchdog_View_Create;

   procedure Com_Error_View_Create
     (Web_Page : in out Instance)
   is
   begin
      Web_Page.Com_Error_Desc_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "com-error-description");

      Web_Page.Com_Error_Count_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "com-error-count");

   end Com_Error_View_Create;

   procedure Master_View_Create
     (Web_Page : in out Instance)
   is
   begin
      Web_Page.Master_Slave_State_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "master-slave-state");

      Web_Page.Master_Slave_Err_Log_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "master-slave-error-log-indicator");

      Web_Page.Master_Config_Slaves_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "master-configured-slaves");

      Web_Page.Master_Active_Slaves_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "master-active-slaves");

      Web_Page.Master_Diag_Slaves_View.Attach_Using_Parent
        (Web_Page.Common_View.View, "master-diagnostic-slaves");

   end Master_View_Create;

   procedure Device_View_Update
     (Web_Page : in out Instance)
   is
      DWTemp : DWord;
      New_Communicating : Boolean;
   begin
      DWTemp :=
        (Web_Page.Common_Status_Block.Communication_COS
         xor Web_Page.Common_Status_Block_Previous.Communication_COS)
        and Web_Page.Common_Status_Block.Communication_COS;

      if (DWTemp and RCX_COMM_COS_READY_MASK) /= 0 then
         Web_Page.Device_Ready_View.Attribute ("status", "on");
      end if;

      if (DWTemp and RCX_COMM_COS_RUN_MASK) /= 0 then
         Web_Page.Device_Running_View.Attribute ("status", "on");
      end if;

      if (DWTemp and RCX_COMM_COS_BUS_ON_MASK) /= 0 then
         Web_Page.Device_Bus_On_View.Attribute ("status", "on");
      end if;

      DWTemp :=
        (Web_Page.Common_Status_Block.Communication_COS
         xor Web_Page.Common_Status_Block_Previous.Communication_COS)
        and not Web_Page.Common_Status_Block.Communication_COS;

      if (DWTemp and RCX_COMM_COS_READY_MASK) /= 0 then
         Web_Page.Device_Ready_View.Attribute ("status", "off");
      end if;

      if (DWTemp and RCX_COMM_COS_RUN_MASK) /= 0 then
         Web_Page.Device_Running_View.Attribute ("status", "off");
      end if;

      if (DWTemp and RCX_COMM_COS_BUS_ON_MASK) /= 0 then
         Web_Page.Device_Bus_On_View.Attribute ("status", "off");
      end if;

      New_Communicating := My_cifX_Status.Communicating;
      if New_Communicating /= Web_Page.Communicating then
         if New_Communicating then
            Web_Page.Device_Com_OK_View.Attribute ("status", "on");
         else
            Web_Page.Device_Com_OK_View.Attribute ("status", "off");
         end if;
         Web_Page.Communicating := New_Communicating;
      end if;

      if Web_Page.Common_Status_Block.Communication_Error
        /= Web_Page.Common_Status_Block_Previous.Communication_Error
      then
         if Web_Page.Common_Status_Block.Communication_Error = CIFX_NO_ERROR
         then
            Web_Page.Device_Error_View.Attribute ("status", "off");
         else
            Web_Page.Device_Error_View.Attribute ("status", "on");
         end if;
      end if;

   end Device_View_Update;

   procedure Network_View_Update
     (Web_Page : in out Instance)
   is
   begin
      if Web_Page.Common_Status_Block.Communication_State
        /= Web_Page.Common_Status_Block_Previous.Communication_State
      then

         Web_Page.Network_Offline_View.Attribute ("status", "off");
         Web_Page.Network_Stop_View.Attribute ("status", "off");
         Web_Page.Network_Idle_View.Attribute ("status", "off");
         Web_Page.Network_Operate_View.Attribute ("status", "off");

         if Web_Page.Common_Status_Block.Communication_State
           = Network_State_Offline
         then

            Web_Page.Network_Offline_View.Attribute ("status", "on");

         elsif Web_Page.Common_Status_Block.Communication_State
           = Network_State_Stop
         then

            Web_Page.Network_Stop_View.Attribute ("status", "on");

         elsif Web_Page.Common_Status_Block.Communication_State
           = Network_State_Idle
         then

            Web_Page.Network_Idle_View.Attribute ("status", "on");

         elsif Web_Page.Common_Status_Block.Communication_State
           = Network_State_Operate
         then

            Web_Page.Network_Operate_View.Attribute ("status", "on");
         end if;

      end if;

   end Network_View_Update;

   procedure Watchdog_View_Update
     (Web_Page : in out Instance)
   is
      New_WD_Error : constant Boolean := My_cifX_Status.WD_Error;
   begin
      if New_WD_Error /= Web_Page.WD_Error
      then
         if New_WD_Error then
            Web_Page.cifX_Watchdog_View.Attribute ("status", "off");
         else
            Web_Page.cifX_Watchdog_View.Attribute ("status", "on");
         end if;
         Web_Page.WD_Error := New_WD_Error;
      end if;

      Web_Page.cifX_Watchdog_CTO_View.Inner_HTML
        (From_UTF_8 (Web_Page.Common_Status_Block.Watchdog_Time'Img & " ms"));
   end Watchdog_View_Update;

   procedure Com_Error_View_Update
     (Web_Page : in out Instance)
   is
      function Get_Error_Description (Error : DWord) return String;

      function Get_Error_Description (Error : DWord)
                                  return String is
         Error_String : String (1 .. 20);
      begin
         if Error = CIFX_NO_ERROR then
            return "-";
         else
            DWord_Text_IO.Put (Error_String, Item => Error, Base => 16);
            return cifX.Driver_Get_Error_Description (DWord_To_DInt (Error))
            & " (" & Error_String & ")";
         end if;
      end Get_Error_Description;

   begin
      Web_Page.Com_Error_Desc_View.Inner_HTML
        (From_UTF_8
           (Get_Error_Description
                (Web_Page.Common_Status_Block.Communication_Error)));

      Web_Page.Com_Error_Count_View.Inner_HTML
        (From_UTF_8 (Web_Page.Common_Status_Block.Error_Count'Img));

   end Com_Error_View_Update;

   procedure Master_View_Update
     (Web_Page : in out Instance)
   is
      Common_Status_Block : constant access cifX.Common_Status_Block_Type :=
        Web_Page.Common_Status_Block'Access;
      Slave_State : String := "Undefined ";
   begin
      if Web_Page.Common_Status_Block.netX_Master_Status.Slave_State =
        cifX.RCX_SLAVE_STATE_OK
      then
         Slave_State := "OK        ";
      elsif Web_Page.Common_Status_Block.netX_Master_Status.Slave_State =
        cifX.RCX_SLAVE_STATE_FAILED
      then
         Slave_State := "Failed    ";
      end if;

      Web_Page.Master_Slave_State_View.Inner_HTML (From_UTF_8 (Slave_State));

      Web_Page.Master_Slave_Err_Log_View.Inner_HTML
        (From_UTF_8
           (Common_Status_Block.netX_Master_Status.Slave_Err_Log_Ind'Img));

      Web_Page.Master_Config_Slaves_View.Inner_HTML
        (From_UTF_8
           (Common_Status_Block.netX_Master_Status.Num_Of_Config_Slaves'Img));

      Web_Page.Master_Active_Slaves_View.Inner_HTML
        (From_UTF_8
           (Common_Status_Block.netX_Master_Status.Num_Of_Active_Slaves'Img));

      Web_Page.Master_Diag_Slaves_View.Inner_HTML
        (From_UTF_8
           (Common_Status_Block.netX_Master_Status.Num_Of_Diag_Slaves'Img));

   end Master_View_Update;

   procedure View_Update
     (Web_Page : in out Instance)
   is
   begin
      Web_Page.Common_Status_Block := My_cifX_Status.Get_Common_Status_Block;

      Web_Page.Device_View_Update;

      Web_Page.Network_View_Update;

      Web_Page.Watchdog_View_Update;

      Web_Page.Com_Error_View_Update;

      Web_Page.Master_View_Update;

      Web_Page.Common_Status_Block_Previous := Web_Page.Common_Status_Block;

   end View_Update;

end A4A.Web.Pages.cifX_Status;
