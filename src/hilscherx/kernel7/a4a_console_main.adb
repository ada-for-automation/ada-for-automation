
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Command_Line;
with Ada.Text_IO;
with Ada.Exceptions; use Ada.Exceptions;

with A4A; use A4A;
with A4A.Tasks;
with A4A.Kernel; use A4A.Kernel;
with A4A.Kernel.Main; use A4A.Kernel.Main;
with A4A.Kernel.Fieldbus; use A4A.Kernel.Fieldbus;
with A4A.Kernel.Sig_Handler;
with A4A.Log;
with A4A.Logger;
with A4A.Application.Configuration; use A4A.Application.Configuration;
with A4A.Application.Identification; use A4A.Application.Identification;
with A4A.Configuration.Fieldbus; use A4A.Configuration.Fieldbus;

procedure A4A_Console_Main is
   My_Ident : constant String := "A4A_Console_Main";

   Terminating        : Boolean := False;

   Main_Watching      : Boolean := False;
   Period1_Watching   : Boolean := False;

   Main_Task_Watchdog_TON_Q       : Boolean := False;
   Main_Task_Watchdog_Error       : Boolean := False;
   Main_Task_Watchdog_Status      : String  := "WD OK";
   Main_Task_Status               : A4A.Tasks.Task_Status_Type;
   Main_Task_Statistics           : A4A.Tasks.Task_Statistics_Type;

   Fieldbus1_Common_Status_Block  : cifX.Common_Status_Block_Type;

   Period1_Task_Watchdog_TON_Q    : Boolean := False;
   Period1_Task_Watchdog_Error    : Boolean := False;
   Period1_Task_Watchdog_Status   : String  := "WD OK";
   Period1_Task_Status            : A4A.Tasks.Task_Status_Type;
   Period1_Task_Statistics        : A4A.Tasks.Task_Statistics_Type;

   procedure Show_Application_Identification;

   procedure Show_Application_Identification is
      What : constant String := CRLF
        & "***********************************************" & CRLF
        & "          Application Identification" & CRLF
        & "Name :" & CRLF
        & "    " & Get_Application_Name & CRLF
        & "Version :" & CRLF
        & "    " & Get_Application_Version & CRLF
        & "Description :" & CRLF
        & "    " & Get_Application_Description & CRLF
        & "***********************************************" & CRLF;
   begin
      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => What);
   end Show_Application_Identification;

   procedure Show_Common_Status_Block;

   procedure Show_Common_Status_Block is
      Communication_COS_Img   : String (1 .. 20);
      Communication_State_Img : String (1 .. 20);
      Communication_Error_Img : String (1 .. 20);

      What : constant String :=
        Fieldbus_Name_Strings.To_String
          (Fieldbuses_Configuration (1).Fieldbus_Name)
        & " Channel " & Fieldbuses_Configuration (1).Fieldbus_Channel'Img
        & " (COS, State, Err) : ";
   begin
      DWord_Text_IO.Put
        (To   => Communication_COS_Img,
         Item => Fieldbus1_Common_Status_Block.Communication_COS,
         Base => 16);

      DWord_Text_IO.Put
        (To   => Communication_State_Img,
         Item => Fieldbus1_Common_Status_Block.Communication_State,
         Base => 16);

      DWord_Text_IO.Put
        (To   => Communication_Error_Img,
         Item => Fieldbus1_Common_Status_Block.Communication_Error,
         Base => 16);

      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => What
                          & Communication_COS_Img & ", "
                          & Communication_State_Img & ", "
                          & Communication_Error_Img);
   end Show_Common_Status_Block;

begin

   --  Let's print our Command Line (not that useful at the moment)
   A4A.Log.Logger.Put (Who  => My_Ident,
                       What => "started as : "
                       & Ada.Command_Line.Command_Name & " ");
   for Index in 1 .. Ada.Command_Line.Argument_Count loop
      Ada.Text_IO.Put (Ada.Command_Line.Argument (Index) & " ");
   end loop;
   Ada.Text_IO.New_Line;

   A4A.Log.Logger.Set_Level (Log_Level => A4A.Logger.Level_Info);

   Show_Application_Identification;

   --------------------------------------------------------------------
   --  Create necessary tasks
   --------------------------------------------------------------------
   --  This one waits for Ctrl+C and terminates the application
   A4A.Kernel.Sig_Handler.Create_Sig_Handler;

   --  Well, the main automation task, either cyclic or periodic, which runs
   --  the A4A.Application.Main_Cyclic procedure
   A4A.Kernel.Main.Create_Main_Task;

   --  A periodic task, running the A4A.Application.Periodic1_Run procedure
   A4A.Kernel.Create_Generic_Periodic_Task_1;

   loop
      exit when
        The_Main_Task_Interface.Status.Ready
        and The_GP_Task1_Interface.Status.Ready;
      delay 1.0;
   end loop;

   --  Start tasks
   The_Main_Task_Interface.Control.Run (True);
   The_GP_Task1_Interface.Control.Run (True);

   --------------------------------------------------------------------
   --  Monitoring the tasks
   --------------------------------------------------------------------
   loop

      if not Main_Watching then
         The_Main_Task_Interface.Status_Watchdog.Set_Time_Out
           (Time_Out_MS => 1500);
         The_Main_Task_Interface.Control.Start_Watching (True);
         Main_Watching := True;
      end if;

      The_Main_Task_Interface.Control_Watchdog.Watchdog
        (Watching        => not Terminating,
         Status_Watchdog => The_Main_Task_Interface.Status_Watchdog.Value,
         Error           => Main_Task_Watchdog_TON_Q);

      if Main_Task_Watchdog_TON_Q and not Main_Task_Watchdog_Error then
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Main task Watchdog Time Out elapsed!");
         Main_Task_Watchdog_Error := True;
         Main_Task_Watchdog_Status := "WD PB";
      end if;

      if not Period1_Watching then
         The_GP_Task1_Interface.Status_Watchdog.Set_Time_Out
           (Time_Out_MS => 1500);
         The_GP_Task1_Interface.Control.Start_Watching (True);
         Period1_Watching := True;
      end if;

      The_GP_Task1_Interface.Control_Watchdog.Watchdog
        (Watching        => not Terminating,
         Status_Watchdog => The_GP_Task1_Interface.Status_Watchdog.Value,
         Error           => Period1_Task_Watchdog_TON_Q);

      if Period1_Task_Watchdog_TON_Q and not Period1_Task_Watchdog_Error then
         A4A.Log.Logger.Put
           (Who  => My_Ident,
            What => "Periodic task 1 Watchdog Time Out elapsed!");
         Period1_Task_Watchdog_Error := True;
         Period1_Task_Watchdog_Status := "WD PB";
      end if;

      Main_Task_Status := The_Main_Task_Interface.Status.Get_Status;
      Main_Task_Statistics :=
        The_Main_Task_Interface.Statistics.Get_Statistics;
      A4A.Log.Logger.Put
        (Who  => My_Ident,
         What => "Main Task (Running, WD_Status, Min, Max, Avg) : "
         & Main_Task_Status.Running'Image & " "
         & Main_Task_Watchdog_Status & " "
         & Duration'Image (Main_Task_Statistics.Min_Duration) & " "
         & Duration'Image (Main_Task_Statistics.Max_Duration) & " "
         & Duration'Image (Main_Task_Statistics.Avg_Duration));

      Fieldbus1_Common_Status_Block :=
        The_Main_Task_Interface.cifX_Board_Status.Get_Common_Status_Block;
      Show_Common_Status_Block;

      Period1_Task_Status := The_GP_Task1_Interface.Status.Get_Status;
      Period1_Task_Statistics :=
        The_GP_Task1_Interface.Statistics.Get_Statistics;
      A4A.Log.Logger.Put
        (Who  => My_Ident,
         What => "Periodic Task 1 (Running, WD_Status, Min, Max, Avg) : "
         & Period1_Task_Status.Running'Image & " "
         & Period1_Task_Watchdog_Status & " "
         & Duration'Image (Period1_Task_Statistics.Min_Duration) & " "
         & Duration'Image (Period1_Task_Statistics.Max_Duration) & " "
         & Duration'Image (Period1_Task_Statistics.Avg_Duration));

      if A4A.Kernel.Quit then
         Terminating := True;
         The_Main_Task_Interface.Control.Quit (True);
         The_GP_Task1_Interface.Control.Quit (True);
      end if;

      exit when Terminating
        and The_Main_Task_Interface.Status.Terminated
        and The_GP_Task1_Interface.Status.Terminated;

      delay 1.0;
   end loop;

   A4A.Log.Logger.Put (Who  => My_Ident,
                       What => "Finished !");
   A4A.Log.Quit;

   loop
      delay 1.0;
      exit when A4A.Log.is_Terminated;
   end loop;

exception

   when Error : others =>
      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => Exception_Information (Error));

      The_Main_Task_Interface.Control.Quit (True);
      The_GP_Task1_Interface.Control.Quit (True);

      delay 5.0;

      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => "Aborted !");
      A4A.Log.Quit;

   loop
      delay 1.0;
      exit when A4A.Log.is_Terminated;
   end loop;

end A4A_Console_Main;
