
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Calendar; use Ada.Calendar;

with A4A.GUI_Elements.Ident_Page; use A4A.GUI_Elements.Ident_Page;

with A4A.GUI_Elements.General_Status_Page;
use A4A.GUI_Elements.General_Status_Page;

with A4A.GUI_Elements.MClients_Status_Page;
use A4A.GUI_Elements.MClients_Status_Page;

with A4A.GUI_Elements.cifX_Status_Page;
use A4A.GUI_Elements.cifX_Status_Page;

package A4A.Application.GUI is

   --------------------------------------------------------------------
   --  Graphical User Interface
   --------------------------------------------------------------------

   procedure Create_GUI;

private
   Start_Time : constant Ada.Calendar.Time := Ada.Calendar.Clock;

   Update_Rate : constant := 1000;

   Ident_Page    : aliased A4A.GUI_Elements.Ident_Page.Instance;

   General_Status_Page : aliased A4A.GUI_Elements.General_Status_Page.Instance;

   MClients_Status_Page :
   aliased A4A.GUI_Elements.MClients_Status_Page.Instance;

   cifX_Status_Page1    : aliased A4A.GUI_Elements.cifX_Status_Page.Instance;

   procedure Update;

   procedure Finalise;

end A4A.Application.GUI;
