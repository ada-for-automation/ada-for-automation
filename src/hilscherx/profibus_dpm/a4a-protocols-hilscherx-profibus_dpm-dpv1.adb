
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package is part of the binding to Hilscher cifX Device Driver,
--  a library allowing access to the cifX boards with numerous protocols.
--  </summary>
--  <description>
--  It provides :
--  - PROFIBUS DP Master data types,
--  - cifX C functions binding.
--  </description>
--  <group>Protocols</group>
--  <c_version>3.0.1</c_version>

with A4A.Log;

with A4A.Protocols.HilscherX.Profibus_DPM.DPV1C2.Closed;
use A4A.Protocols.HilscherX.Profibus_DPM;

package body A4A.Protocols.HilscherX.Profibus_DPM.DPV1 is

   procedure Initialise
     (Self           : in out Instance;
      Channel_Access : Channel_Messaging.Instance_Access;
      DPV1C2_Closed_Access : in DPV1C2_Ind_FB.Closed_FB.Instance_Access) is

      My_Ident : constant String := Package_Ident & ".Initialise";

   begin

      Self.My_Channel := Channel_Access;

      if Self.My_Channel.Is_Initialised then

         Self.My_Channel.Put
           (Item   => Self.My_Receive_Queue'Unrestricted_Access,
            Index  => Self.My_Receive_Queue_ID,
            Result => Self.Queue_ID_Got);

         if not Self.Queue_ID_Got then
            A4A.Log.Logger.Put (Who  => My_Ident & ".Initialise",
                                What => "Could not Register my Queue !");
         else

            Self.DPV1C2_Closed_FB := DPV1C2_Closed_Access;

            Self.Register_App_FB.Initialise
              (Channel_Access => Channel_Access,
               Receive_Queue_ID  => Self.My_Receive_Queue_ID);

            Self.Unregister_App_FB.Initialise
              (Channel_Access => Channel_Access,
               Receive_Queue_ID  => Self.My_Receive_Queue_ID);

               Self.DPV1C2_Closed_FB.Initialise
                 (Channel_Access => Channel_Access);

               Self.Status := X00;
               Self.Error_Flag := False;

               Self.Init_Flag := True;

         end if;

      else

         A4A.Log.Logger.Put
           (Who  => My_Ident,
            What => "Channel Not Initialised !");

      end if;

   end Initialise;

   procedure Quit
     (Self : in out Instance) is
   begin
      Self.Quit_Flag := True;
   end Quit;

   function Is_Ready
     (Self : in Instance) return Boolean is
   begin

      return Self.Status = X02;

   end Is_Ready;

   function Is_Faulty
     (Self : in Instance) return Boolean is
   begin

      return Self.Status = X05;

   end Is_Faulty;

   function Is_Terminated
     (Self : in Instance) return Boolean is
   begin

      return Self.Status = X06;

   end Is_Terminated;

   task body DPV1_Task_Type is

      My_Ident : constant String := Package_Ident & ".DPV1_Task";

      Error_String : String (1 .. 20);
      Cmd_String   : String (1 .. 20);

   begin

      loop

         case Self.Status is

         when X00 =>

            if Self.Init_Flag then

               A4A.Log.Logger.Put
                 (Who  => My_Ident,
                  What => "Initialised !");

               Self.Status  := X01;

            end if;

         when X01 =>

            if Self.Register_App_Done then

               if Self.Register_App_Error then

                  DWord_Text_IO.Put
                    (To   => Error_String,
                     Item => Self.Register_App_FB.Get_Last_Error,
                     Base => 16);

                  A4A.Log.Logger.Put
                    (Who  => My_Ident,
                     What => "Got an error : " & Error_String);

                  A4A.Log.Logger.Put
                    (Who  => My_Ident,
                     What => "App Not Registered !");

                  Self.Status  := X05;

               else

                  A4A.Log.Logger.Put
                    (Who  => My_Ident,
                     What => "App Registered !");

                  Self.Status  := X02;

               end if;

            end if;

         when X02 =>

            if Self.Quit_Flag then

               Self.Status  := X03;

            end if;

         when X03 =>

            if Self.Unregister_App_Done then

               if Self.Unregister_App_Error then

                  DWord_Text_IO.Put
                    (To   => Error_String,
                     Item => Self.Unregister_App_FB.Get_Last_Error,
                     Base => 16);

                  A4A.Log.Logger.Put
                    (Who  => My_Ident,
                     What => "Got an error : " & Error_String);

                  A4A.Log.Logger.Put
                    (Who  => My_Ident,
                     What => "App Not Unregistered !");

                  Self.Status  := X05;

               else

                  A4A.Log.Logger.Put
                    (Who  => My_Ident,
                     What => "App Unregistered !");

                  Self.Status  := X04;

               end if;

            end if;

         when X04 =>

            null;

         when X05 =>

            null;

         when X06 =>

            null;

         end case;

         --  Register the Application
         Self.Do_Register_App := (Self.Status = X01);

         Self.Register_App_FB.Cyclic
           (Do_Command => Self.Do_Register_App,
            Done       => Self.Register_App_Done,
            Error      => Self.Register_App_Error);

         --  Unregister the Application
         Self.Do_Unregister_App := (Self.Status = X03);

         Self.Unregister_App_FB.Cyclic
           (Do_Command => Self.Do_Unregister_App,
            Done       => Self.Unregister_App_Done,
            Error      => Self.Unregister_App_Error);

         if not Self.Packet_Got then
            Self.My_Receive_Queue.Get
              (Item   => Self.Received_Packet,
               Result => Self.Packet_Got);
         end if;

         if Self.Packet_Got then

            DWord_Text_IO.Put
              (To   => Cmd_String,
               Item => Self.Received_Packet.Header.Cmd,
               Base => 16);

            A4A.Log.Logger.Put
              (Who  => My_Ident,
               What => "Got a message : " & Cmd_String);

            case Self.Received_Packet.Header.Cmd is

               when rcX_Register_App.RCX_REGISTER_APP_CNF =>

                  Self.Register_App_FB.Handle_Answer (Self.Received_Packet);
                  Self.Packet_Got := False;

               when rcX_Unregister_App.RCX_UNREGISTER_APP_CNF =>

                  Self.Unregister_App_FB.Handle_Answer (Self.Received_Packet);
                  Self.Packet_Got := False;

               when DPV1C2.Closed.PROFIBUS_FSPMM2_CMD_CLOSED_IND =>

                  if Self.DPV1C2_Closed_FB.Is_Ready then
                     Self.DPV1C2_Closed_FB.Handle_Indication
                       (Self.Received_Packet);
                     Self.Packet_Got := False;
                  end if;

               when others =>

                  A4A.Log.Logger.Put
                    (Who  => My_Ident,
                     What => "Received unmanaged message !!!");
                     Self.Packet_Got := False;

            end case;

         end if;

         exit when
           ((Self.Status = X00) or (Self.Status = X04) or (Self.Status = X05))
           and Self.Quit_Flag;

         delay 0.1;

      end loop;

      A4A.Log.Logger.Put
        (Who  => My_Ident,
         What => "Terminated !");

      Self.Status  := X06;

   end DPV1_Task_Type;

end A4A.Protocols.HilscherX.Profibus_DPM.DPV1;
