
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with A4A.Protocols.HilscherX.cifX_Errors;
use A4A.Protocols.HilscherX.cifX_Errors;

package body A4A.Protocols.HilscherX.Profibus_DPM is

   procedure Channel_Extended_Status_Block
     (Channel_Handle        : in  Channel_Handle_Type;
      Extended_Status_Block : out Extended_Status_Block_Type;
      Error                 : out DInt) is

      ESB    : aliased Extended_Status_Block_Type;
      Result : DInt;
   begin

      Result := cifX_Channel_Extended_Status_Block
        (Channel_Handle => Channel_Handle,
         Cmd            => CIFX_CMD_READ_DATA,
         Offset         => 0,
         Data_Len       => ESB'Size / 8,
         Data           => ESB'Address);

      if Result = CIFX_NO_ERROR then
         Extended_Status_Block := ESB;
      end if;

      Error := Result;
   end Channel_Extended_Status_Block;

end A4A.Protocols.HilscherX.Profibus_DPM;
