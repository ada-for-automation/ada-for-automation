
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package is part of the binding to Hilscher cifX Device Driver,
--  a library allowing access to the cifX boards with numerous protocols.
--  </summary>
--  <description>
--  It provides :
--  - PROFIBUS DP V1 Master Class 2 data types,
--  </description>
--  <group>Protocols</group>
--  <c_version>3.0.1</c_version>

package A4A.Protocols.HilscherX.Profibus_DPM.DPV1C2 is

   --------------------------------------------------------------------
   --  from ProfibusFspmm2_Public.h
   --------------------------------------------------------------------

   --  Abort instance
   --  #define PROFIBUS_FSPMM2_INSTANCE_DLL         0
   PROFIBUS_FSPMM2_INSTANCE_DLL             : constant := 0;

   --  #define PROFIBUS_FSPMM2_INSTANCE_MSAC2M      1
   PROFIBUS_FSPMM2_INSTANCE_MSAC2M          : constant := 1;

   --  #define PROFIBUS_FSPMM2_INSTANCE_USER        2
   PROFIBUS_FSPMM2_INSTANCE_USER            : constant := 2;

   --  Subnet
   --  #define PROFIBUS_FSPMM2_SUBNET_NO            0
   PROFIBUS_FSPMM2_SUBNET_NO                : constant := 0;

   --  #define PROFIBUS_FSPMM2_SUBNET_LOCAL         1
   PROFIBUS_FSPMM2_SUBNET_LOCAL             : constant := 1;

   --  #define PROFIBUS_FSPMM2_SUBNET_REMOTE        2
   PROFIBUS_FSPMM2_SUBNET_REMOTE            : constant := 2;

   --  Live List
   --  #define PROFIBUS_FSPMM2_STATON_TYPE_PASSIVE  1
   PROFIBUS_FSPMM2_STATION_TYPE_PASSIVE     : constant := 1;

   --  #define PROFIBUS_FSPMM2_STATON_TYPE_ACTIVE   3
   PROFIBUS_FSPMM2_STATION_TYPE_ACTIVE      : constant := 3;

   --  maximum abort indications
   --  #define PROFIBUS_FSPMM2_MAX_ABORT_IND        8
   PROFIBUS_FSPMM2_MAX_ABORT_IND            : constant := 8;

end A4A.Protocols.HilscherX.Profibus_DPM.DPV1C2;
