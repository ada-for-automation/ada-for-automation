
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package is part of the binding to Hilscher cifX Device Driver,
--  a library allowing access to the cifX boards with numerous protocols.
--  </summary>
--  <description>
--  It provides :
--  - PROFIBUS DP V1 Master Class 2 data types,
--  </description>
--  <group>Protocols</group>
--  <c_version>3.0.1</c_version>

with A4A.Protocols.HilscherX.rcX_Public;
use A4A.Protocols.HilscherX.rcX_Public;

package A4A.Protocols.HilscherX.Profibus_DPM.DPV1C2.Abort_Req is

   --------------------------------------------------------------------
   --  from ProfibusFspmm2_Public.h
   --------------------------------------------------------------------

   --  #define PROFIBUS_FSPMM2_CMD_ABORT_REQ                     0x0000440C
   PROFIBUS_FSPMM2_CMD_ABORT_REQ             : constant := 16#0000440C#;

   --  #define PROFIBUS_FSPMM2_CMD_ABORT_CNF                     0x0000440D
   PROFIBUS_FSPMM2_CMD_ABORT_CNF             : constant := 16#0000440D#;

   --------------------------------------------------------------------
   --  Packet : PROFIBUS_FSPMM2_ABORT_REQ / PROFIBUS_FSPMM2_ABORT_CNF
   --  This function requests an abort of a DPV1 Class 2 Connection
   --------------------------------------------------------------------

   --  /***** request packet *****/

   type PROFIBUS_FSPMM2_ABORT_REQ_T is
      record
         Com_Ref              : DWord;
         --  Communication Reference

         Subnet               : Byte;
         --  Subnet

         Instance             : Byte;
         --  Instance number (4 bits)

         Reason_Code          : Byte;
         --  Reason code why an abort is requested

         Reserved             : Byte;
         --  Reserved field

      end record;
   pragma Convention (C, PROFIBUS_FSPMM2_ABORT_REQ_T);

   type PROFIBUS_FSPMM2_PACKET_ABORT_REQ_T is
      record
         Head : TLR_Packet_Header_Type;
         --  packet header

         Data : PROFIBUS_FSPMM2_ABORT_REQ_T;
         --  packet data
      end record;
   pragma Convention (C, PROFIBUS_FSPMM2_PACKET_ABORT_REQ_T);

   type PROFIBUS_FSPMM2_PACKET_ABORT_REQ_T_Access is
     access all PROFIBUS_FSPMM2_PACKET_ABORT_REQ_T;

   function To_cifX_Packet is new Ada.Unchecked_Conversion
     (Source => PROFIBUS_FSPMM2_PACKET_ABORT_REQ_T_Access,
      Target => cifX_Packet_Access);

   function From_cifX_Packet is new Ada.Unchecked_Conversion
     (Source => cifX_Packet_Access,
      Target => PROFIBUS_FSPMM2_PACKET_ABORT_REQ_T_Access);

   --  /***** confirmation packet *****/

   type PROFIBUS_FSPMM2_ABORT_CNF_T is
      record
         Com_Ref              : DWord;
         --  Communication Reference

      end record;
   pragma Convention (C, PROFIBUS_FSPMM2_ABORT_CNF_T);

   type PROFIBUS_FSPMM2_PACKET_ABORT_CNF_T is
      record
         Head : TLR_Packet_Header_Type;
         --  packet header

         Data : PROFIBUS_FSPMM2_ABORT_CNF_T;
         --  packet data

      end record;
   pragma Convention (C, PROFIBUS_FSPMM2_PACKET_ABORT_CNF_T);

   type PROFIBUS_FSPMM2_PACKET_ABORT_CNF_T_Access is
     access all PROFIBUS_FSPMM2_PACKET_ABORT_CNF_T;

   function To_cifX_Packet is new Ada.Unchecked_Conversion
     (Source => PROFIBUS_FSPMM2_PACKET_ABORT_CNF_T_Access,
      Target => cifX_Packet_Access);

   function From_cifX_Packet is new Ada.Unchecked_Conversion
     (Source => cifX_Packet_Access,
      Target => PROFIBUS_FSPMM2_PACKET_ABORT_CNF_T_Access);

end A4A.Protocols.HilscherX.Profibus_DPM.DPV1C2.Abort_Req;
