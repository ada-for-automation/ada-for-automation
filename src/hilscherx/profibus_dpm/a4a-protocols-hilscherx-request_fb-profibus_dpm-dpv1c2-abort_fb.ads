
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package is part of the binding to Hilscher cifX Device Driver,
--  a library allowing access to the cifX boards with numerous protocols.
--  </summary>
--  <description>
--  It provides :
--  - PROFIBUS DP V1 Master Class 2 data types,
--  </description>
--  <group>Protocols</group>
--  <c_version>3.0.1</c_version>

with A4A.Protocols.HilscherX.Profibus_DPM.DPV1C2.Abort_Req;
use A4A.Protocols.HilscherX.Profibus_DPM.DPV1C2.Abort_Req;
use A4A.Protocols.HilscherX.Profibus_DPM.DPV1C2;

package A4A.Protocols.HilscherX.Request_FB.Profibus_DPM.DPV1C2.Abort_FB is

   type Instance is new Request_FB.Instance with private;
   type Instance_Access is access all Instance;

   procedure Set_Parameters
     (Function_Block : in out Instance;
      Com_Ref        : in DWord);

   function Get_Data
     (Function_Block : in Instance)
      return PROFIBUS_FSPMM2_ABORT_CNF_T;

private

   My_Ident : String :=
     "A4A.Protocols.HilscherX.Request_FB.Profibus_DPM.DPV1C2.Abort_FB";

   type Instance is new Request_FB.Instance with
      record

         Com_Ref        : DWord := 0;

         Cnf_Data  : PROFIBUS_FSPMM2_ABORT_CNF_T;

      end record;

   overriding
   procedure Fill_Request
     (Function_Block : in out Instance;
      Request        : cifX_Packet_Access);

   overriding
   procedure Store_Result
     (Function_Block : in out Instance;
      Confirmation   : cifX_Packet_Access;
      Error          : out Boolean;
      Type_Of_Error  : out Error_Type;
      Last_Error     : out DWord);

   overriding
   function Get_Ident
     (Function_Block : in out Instance) return String;

end A4A.Protocols.HilscherX.Request_FB.Profibus_DPM.DPV1C2.Abort_FB;
