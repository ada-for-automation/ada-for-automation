
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package provides the binding to Hilscher cifX Device Driver,
--  a library allowing access to the cifX boards with numerous protocols.
--  </summary>
--  <description>
--  It provides :
--  - messaging infrastructure.
--  </description>
--  <group>Protocols</group>
--  <c_version>3.0.1</c_version>

package body A4A.Protocols.HilscherX.Message_Pool is

   protected body Message_Pool_Type is

      procedure Get_Packet
        (Item   : out cifX_Packet_Access;
         Result : out Boolean) is
      begin

         if Empty then
            Result := False;
         else
            Item := Pool_Accesses (Pool_Access_Index);
            Result := True;

            if Pool_Access_Index > Pool_Index'First then
               Pool_Access_Index := Pool_Access_Index - 1;
            else
               Empty := True;
            end if;

         end if;

      end Get_Packet;

      procedure Return_Packet
        (Item   : in cifX_Packet_Access) is
      begin

         if Empty then
            Empty := False;
         else
            Pool_Access_Index := Pool_Access_Index + 1;
         end if;

         Pool_Accesses (Pool_Access_Index) := Item;

      end Return_Packet;

      procedure Initialise is
      begin

         for Index in Pool'Range loop
            Pool_Accesses (Index) := Pool (Index)'Unrestricted_Access;
         end loop;

         Pool_Access_Index := Pool_Index'Last;
         Empty := False;

      end Initialise;

   end Message_Pool_Type;

end A4A.Protocols.HilscherX.Message_Pool;
