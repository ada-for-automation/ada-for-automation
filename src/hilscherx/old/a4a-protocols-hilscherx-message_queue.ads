
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package provides the binding to Hilscher cifX Device Driver,
--  a library allowing access to the cifX boards with numerous protocols.
--  </summary>
--  <description>
--  It provides :
--  - messaging infrastructure.
--  </description>
--  <group>Protocols</group>
--  <c_version>3.0.1</c_version>

with Ada.Unchecked_Conversion;

with A4A.Protocols.HilscherX.cifX_User;
use A4A.Protocols.HilscherX.cifX_User;

generic

   Queue_Size : Positive;

package A4A.Protocols.HilscherX.Message_Queue is

   type Queue_Index is new Positive range 1 .. Queue_Size;

   type cifX_Packet_Queue is private;

   protected type Message_Queue_Type is

      procedure Put
        (Item   : in cifX_Packet_Access;
         Result : out Boolean);
      --  Returns False if the Queue is full

      procedure Get
        (Item   : out cifX_Packet_Access;
         Result : out Boolean);
      --  Returns False if the Queue is empty

      entry Get
        (Item   : out cifX_Packet_Access);

      function Get_Pending return DWord;

   private

      Queue : cifX_Packet_Queue;

      Empty : Boolean := True;
      Full  : Boolean := False;

      Pending : DWord := 0;

      Insert_Index : Queue_Index := Queue_Index'First;
      Remove_Index : Queue_Index := Queue_Index'First;

   end Message_Queue_Type;

   type Message_Queue_Access is access all Message_Queue_Type;

   function To_Message_Queue is new Ada.Unchecked_Conversion
     (Source => DWord,
      Target => Message_Queue_Access);

   function From_Message_Queue is new Ada.Unchecked_Conversion
     (Source => Message_Queue_Access,
      Target => DWord);

private

   type cifX_Packet_Queue is array (Queue_Index) of cifX_Packet_Access;

end A4A.Protocols.HilscherX.Message_Queue;
