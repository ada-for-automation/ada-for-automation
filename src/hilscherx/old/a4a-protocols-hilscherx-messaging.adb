
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package provides the binding to Hilscher cifX Device Driver,
--  a library allowing access to the cifX boards with numerous protocols.
--  </summary>
--  <description>
--  It provides :
--  - messaging infrastructure.
--  </description>
--  <group>Protocols</group>
--  <c_version>3.0.1</c_version>

with Ada.Text_IO;

with A4A.Protocols.HilscherX.cifX_Errors;
use A4A.Protocols.HilscherX.cifX_Errors;

package body A4A.Protocols.HilscherX.Messaging is

   procedure Quit is
   begin
      Quit_Flag := True;
   end Quit;

   procedure Initialise
     (Channel_Handle : in Channel_Handle_Type) is
   begin

      The_Channel_Handle := Channel_Handle;

      Message_Pool.Initialise;

      Init_Flag := True;

   end Initialise;

   task body Send_Msg is

      Packet    : cifX_Packet_Access;

      Result    : DInt;

   begin

      loop

         delay 1.0;
         exit when Init_Flag or Quit_Flag;

      end loop;

      loop

         exit when Quit_Flag;

         select

            Send_Queue.Get (Item => Packet);

            Result := Channel_Put_Packet
              (Channel_Handle => The_Channel_Handle,
               Send_Packet    => Packet.all,
               Time_Out       => 10);
            if Result /= CIFX_NO_ERROR then
               Show_Error (Result);
            end if;

            Message_Pool.Return_Packet (Item => Packet);

         or

            delay 1.0;

         end select;

      end loop;

   end Send_Msg;

   task body Receive_Msg is

      Packet     : cifX_Packet_Access;

      Result     : DInt;

      Packet_Got : Boolean := False;
      Packet_Put : Boolean := False;

   begin

      loop

         delay 1.0;
         exit when Init_Flag or Quit_Flag;

      end loop;

      loop

         exit when Quit_Flag;

         if not Packet_Got then

            Message_Pool.Get_Packet
              (Item   => Packet,
               Result => Packet_Got);

         end if;

         if not Packet_Got then

            Ada.Text_IO.Put_Line
              ("Receive_Msg : Could not get packet from pool...");

         else

            Result := Channel_Get_Packet
              (Channel_Handle => The_Channel_Handle,
               Size           => cifX_Packet_Type'Size / 8,
               Recv_Packet    => Packet.all,
               Time_Out       => 1000);
            if Result = CIFX_NO_ERROR then

               Receive_Queue.Put
                 (Item   => Packet,
                  Result => Packet_Put);

               Packet_Got := False;

            elsif Result = CIFX_DEV_GET_TIMEOUT
              or Result = CIFX_DEV_GET_NO_PACKET
            then

               Ada.Text_IO.Put_Line
                 ("Zut !");

            else

               Show_Error (Result);

            end if;

         end if;

      end loop;

   end Receive_Msg;

end A4A.Protocols.HilscherX.Messaging;
