
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package is part of the binding to Hilscher cifX Device Driver,
--  a library allowing access to the cifX boards with numerous protocols.
--  </summary>
--  <description>
--  It provides, from "cifXUser.h" :
--  - cifX data types,
--  - cifX C functions binding.
--  </description>
--  <group>Protocols</group>
--  <c_version>3.0.1</c_version>

package A4A.Protocols.HilscherX.netXTransport is

   function Initialise return DInt;
   --  int32_t a4a_netXTransportInit();

   procedure De_Initialise;
   --  int32_t a4a_netXTransportDeinit();

   procedure Start;
   procedure Quit;

   function is_Terminated return Boolean;

private

   Start_Flag : Boolean := False;
   Quit_Flag : Boolean := False;
   Terminated_Flag : Boolean := False;

   --  imported from "a4a_netXTransportInit.c"

   procedure Cyclic_Function;
   --  void netXTransportCyclicFunction (void);

   pragma Import (C, Initialise, "a4a_netXTransportInit");
   pragma Import (C, De_Initialise, "a4a_netXTransportDeinit");
   pragma Import (C, Cyclic_Function, "netXTransportCyclicFunction");

   task netXTransport;

end A4A.Protocols.HilscherX.netXTransport;
