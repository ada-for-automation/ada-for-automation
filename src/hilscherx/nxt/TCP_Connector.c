/**************************************************************************************

   Copyright (c) Hilscher GmbH. All Rights Reserved.

 **************************************************************************************

   Filename:
    $Id: Main.c 3194 2011-12-14 16:36:28Z Robert $
   Last Modification:
    $Author: Robert $
    $Date: 2011-12-14 17:36:28 +0100 (Mi, 14 Dez 2011) $
    $Revision: 3194 $

   Targets:
     linux    : yes

   Description:
    TCP connector example

   Changes:

     Version   Date        Author   Description
     ----------------------------------------------------------------------------------
      1        23.02.13    SD       initial version

**************************************************************************************/

#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <string.h>
#include <pthread.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <linux/tcp.h>
#include <limits.h>
#include <asm/ioctls.h>
#include <sys/queue.h>
#include "ConnectorAPI.h"
#include "netXTransport.h"
#include "OS_Dependent.h"
#include "a4a_LogWrapAda.h"

#define WHAT_BUFFER_LEN        256
#define WHO_BUFFER_LEN         256
#define PACKAGE_NAME           "NXT_TCP_CONNECTOR."

/* global defines */
/*#define DEBUG printf*/
#define SOCKET_TIMEOUT           10000 /* timeout before closing socket           */
#define PARSER_BUFFER_SIZE       256   /* required for parsing configuration file */
#define INTERFACE_EVENT_ATTACHED 0     /* event definition                        */
#define INTERFACE_EVENT_DETACHED 1     /* event definition                        */
#define NETX_TRANSPORT_DEF_PORT  50111 /* netXTransport default port number       */
#define TCP_SEND_TIMEOUT         100
#define TCP_KEEP_ALIVE_TIMEOUT   1000
#define TCP_RESET_TIMEOUT        20000

/* global variables */
static int              g_fConnectorAdded = 0;
static NETX_CONNECTOR_T s_tTCPConnector   = {{0}};

TAILQ_HEAD(CONNECTOR_L, TCP_IF_DATA_Ttag);

typedef struct CONNECTOR_CONFIG_Ttag
{
  char     szTCPIP[16];        /* TCP connector configuration information */
  char     szIPConfig[15]; /* TCP connector configuration information */
  uint32_t ulPort;
  uint32_t ulSendTimeout;
  uint32_t ulKeepAliveTimeout;
  uint32_t ulResetTimeout;

} CONNECTOR_CONFIG_T;

/* connector instance */
typedef struct TCP_CONNECTOR_INFO_Ttag
{
  uint32_t           ulConfigCount;  /* number registered connector interfaces */
  struct CONNECTOR_L tConnectorList; /* connector interfaces                   */

} TCP_CONNECTOR_INFO_T;

/* structure definitions               */
/* TCP connector information structure */
typedef struct TCP_IF_DATA_Ttag
{
  TAILQ_ENTRY(TCP_IF_DATA_Ttag) tList;

  CONNECTOR_CONFIG_T      tConnectorConfig;
  pthread_t               tRXThread;                     /* Thread info of receive thread           */
  void*                   pvRXThreadStopped;             /* Event for receive thread termination    */
  pthread_t               tEventThread;                  /* Thread info of event handler thread     */
  int                     fEventThread;
  void*                   pvEventThreadStopped;          /* Event for event thread termination      */
  int                     fThreadRunning;
  char                    szInterfaceName[NXT_MAX_PATH]; /* Interface name                          */
  uint32_t                ulState;                       /* State of the interface                  */
  void*                   pvSendEvent;                   /* Send event (currently not used)         */
  void*                   pvEvent;                       /* Event handling                          */
  uint32_t                ulEvent;                       /* Event that occurs                       */
  int                     iSocket;                       /* socket number                           */
  struct sockaddr_in      tSockIn;                       /* socket information structure            */
  void*                   pvConnectorLock;

  PFN_NETXCON_DEVICE_RECEIVE_CALLBACK   pfnRxCallback;        /* Receive data Callback                            */
  void*                                 pvRxCallbackData;     /* User data (for receive data callback)            */
  PFN_NETXCON_INTERFACE_NOTIFY_CALLBACK pfnDevNotifyCallback; /* Interface state change Callback                  */
  void*                                 pvUser;               /* User parameter (interface state change callback) */

} TCP_IF_DATA_T, *PTCP_IF_DATA_T;

/* function declaration */
int32_t APIENTRY TCP_GetIdentifier ( char* szIdentifier, NETX_TRANSPORT_UUID_T* pvUUID);
int32_t APIENTRY TCP_Open          ( PFN_NETXCON_INTERFACE_NOTIFY_CALLBACK pfnDevNotifyCallback, void* pvUser);
int32_t APIENTRY TCP_Close         ( void);
void*   APIENTRY TCP_CreateIF      ( const char* szDeviceName);
int32_t APIENTRY TCP_GetInformation( NETX_CONNECTOR_INFO_E eCmd, uint32_t ulSize, void* pvConnectorInfo);
int32_t APIENTRY TCP_GetConfig     ( NETX_CONNECTOR_CONFIG_CMD_E eCmd, void* pvConfig);
int32_t APIENTRY TCP_SetConfig     ( NETX_CONNECTOR_CONFIG_CMD_E eCmd, const char* szConfig);

int32_t APIENTRY TCP_IF_Start         ( void* pvInterface, PFN_NETXCON_DEVICE_RECEIVE_CALLBACK pfnReceiveData, void* pvUser);
int32_t APIENTRY TCP_IF_Stop          ( void* pvInterface);
int32_t APIENTRY TCP_IF_Send          ( void* pvInterface, unsigned char* pabData, uint32_t ulDataLen);
int32_t APIENTRY TCP_IF_GetInformation( void* pvInterface, NETX_INTERFACE_INFO_E eCmd, uint32_t ulSize, void* pvInterfaceInfo);

static void*     CommThread     ( void* pvParam);
static void*     EventThread    ( void* pvParam);
static int       GetConfigString( const char* szFile, const char* szKey, char** szValue);

int32_t APIENTRY TCP_ReadConfigFile( TCP_CONNECTOR_INFO_T* ptConnectorInfo);


static TCP_CONNECTOR_INFO_T s_tConnectorInfo;

/*****************************************************************************/
/*! Connector initialization and registration
*   \return 0 on success                                                     */
/*****************************************************************************/
int32_t TCP_Connector_Init(void)
{
  int32_t lRet = NXT_NO_ERROR;
  char szWhatBuffer [WHAT_BUFFER_LEN];
  char szWhoBuffer  [WHO_BUFFER_LEN];

  strcpy(szWhoBuffer, PACKAGE_NAME);
  strcat(szWhoBuffer, __func__);

  sprintf(szWhatBuffer, "Start");
  A4A_Log(szWhoBuffer, szWhatBuffer, A4A_LOG_LEVEL_INFO);

  /* initialize function pointer table */
  memset(&s_tTCPConnector, 0, sizeof(s_tTCPConnector));
  s_tTCPConnector.tFunctions.pfnConGetIdentifier       = TCP_GetIdentifier;
  s_tTCPConnector.tFunctions.pfnConOpen                = TCP_Open;
  s_tTCPConnector.tFunctions.pfnConClose               = TCP_Close;
  s_tTCPConnector.tFunctions.pfnConCreateInterface     = TCP_CreateIF;
  s_tTCPConnector.tFunctions.pfnConGetInformation      = TCP_GetInformation;
  s_tTCPConnector.tFunctions.pfnConGetConfig           = TCP_GetConfig;
  s_tTCPConnector.tFunctions.pfnConSetConfig           = TCP_SetConfig;

  s_tTCPConnector.tFunctions.pfnConIntfStart           = TCP_IF_Start;
  s_tTCPConnector.tFunctions.pfnConIntfStop            = TCP_IF_Stop;
  s_tTCPConnector.tFunctions.pfnConIntfSend            = TCP_IF_Send;
  s_tTCPConnector.tFunctions.pfnConIntfGetInformation  = TCP_IF_GetInformation;

  s_tTCPConnector.tFunctions.pfnConCreateDialog        = NULL;
  s_tTCPConnector.tFunctions.pfnConEndDialog           = NULL;

  /* add TCP connector interface */
  if (NXT_NO_ERROR == (lRet = netXTransportAddConnector(&s_tTCPConnector)))
  {
    g_fConnectorAdded = 1;
  }
  sprintf(szWhatBuffer, "End : g_fConnectorAdded = %d", g_fConnectorAdded);
  A4A_Log(szWhoBuffer, szWhatBuffer, A4A_LOG_LEVEL_INFO);
  return lRet;
}

/*****************************************************************************/
/*! De-registers connector                                                   */
/*****************************************************************************/
void TCP_Connector_Deinit(void)
{
  char szWhatBuffer [WHAT_BUFFER_LEN];
  char szWhoBuffer  [WHO_BUFFER_LEN];

  strcpy(szWhoBuffer, PACKAGE_NAME);
  strcat(szWhoBuffer, __func__);

  sprintf(szWhatBuffer, "Start : g_fConnectorAdded = %d", g_fConnectorAdded);
  A4A_Log(szWhoBuffer, szWhatBuffer, A4A_LOG_LEVEL_INFO);

  if (g_fConnectorAdded == 1)
  {
    /* remove connector interface */
    netXTransportRemoveConnector(&s_tTCPConnector);
  }
  sprintf(szWhatBuffer, "End : g_fConnectorAdded = %d", g_fConnectorAdded);
  A4A_Log(szWhoBuffer, szWhatBuffer, A4A_LOG_LEVEL_INFO);
}

/*****************************************************************************/
/*! Returns the UU-Id (currently not used)                                   */
/*****************************************************************************/
int32_t TCP_GetIdentifier(char* szIdentifier, NETX_TRANSPORT_UUID_T* ptUUID)
{
  char szWhatBuffer [WHAT_BUFFER_LEN];
  char szWhoBuffer  [WHO_BUFFER_LEN];

  strcpy(szWhoBuffer, PACKAGE_NAME);
  strcat(szWhoBuffer, __func__);

  sprintf(szWhatBuffer, "Trace");
  A4A_Log(szWhoBuffer, szWhatBuffer, A4A_LOG_LEVEL_VERBOSE);

  UNREFERENCED_PARAMETER(szIdentifier);
  UNREFERENCED_PARAMETER(ptUUID);

  return 0;
}

/*****************************************************************************/
/*! Retrieve connector configuration and initializes connector resources
*   \param pfnDevNotifyCallback Callback (interface detection)
*   \param pvUser               User data
*   \return 0 on success                                                     */
/*****************************************************************************/
int32_t APIENTRY TCP_Open( PFN_NETXCON_INTERFACE_NOTIFY_CALLBACK pfnDevNotifyCallback, void* pvUser)
{
  int32_t        lRet            = NXT_NO_ERROR;
  PTCP_IF_DATA_T ptInterfaceData = NULL;
  char szWhatBuffer [WHAT_BUFFER_LEN];
  char szWhoBuffer  [WHO_BUFFER_LEN];

  strcpy(szWhoBuffer, PACKAGE_NAME);
  strcat(szWhoBuffer, __func__);

  sprintf(szWhatBuffer, "Trace");
  A4A_Log(szWhoBuffer, szWhatBuffer, A4A_LOG_LEVEL_INFO);

  TAILQ_INIT(&s_tConnectorInfo.tConnectorList);

  TCP_ReadConfigFile( &s_tConnectorInfo);

  TAILQ_FOREACH( ptInterfaceData, &s_tConnectorInfo.tConnectorList, tList)
  {
    ptInterfaceData->pvUser               = pvUser;
    ptInterfaceData->pfnDevNotifyCallback = pfnDevNotifyCallback;

    /* Notify netXTransport-Layer that new interface is created */
    pfnDevNotifyCallback( ptInterfaceData->szInterfaceName, eNXT_DEVICE_ATTACHED, ptInterfaceData, pvUser);
  }
  return lRet;
}

/*****************************************************************************/
/*! Closes connector (and all related interfaces)
*   \return 0 success                                                        */
/*****************************************************************************/
int32_t APIENTRY TCP_Close( void)
{
  PTCP_IF_DATA_T ptInterfaceData = NULL;
  char szWhatBuffer [WHAT_BUFFER_LEN];
  char szWhoBuffer  [WHO_BUFFER_LEN];

  strcpy(szWhoBuffer, PACKAGE_NAME);
  strcat(szWhoBuffer, __func__);

  sprintf(szWhatBuffer, "Trace");
  A4A_Log(szWhoBuffer, szWhatBuffer, A4A_LOG_LEVEL_INFO);

  while(NULL != (ptInterfaceData = TAILQ_FIRST( &s_tConnectorInfo.tConnectorList)))
  {
    TAILQ_REMOVE( &s_tConnectorInfo.tConnectorList, ptInterfaceData, tList);

    sprintf(szWhatBuffer, "Removing : %s", ptInterfaceData->szInterfaceName);
    A4A_Log(szWhoBuffer, szWhatBuffer, A4A_LOG_LEVEL_INFO);

    /* check if we have to stop the interface */
    if (ptInterfaceData->pvConnectorLock != NULL)
    {
      OS_EnterLock( ptInterfaceData->pvConnectorLock);
      if (ptInterfaceData->ulState != eINTERFACE_STATE_STOPPED)
        TCP_IF_Stop( ptInterfaceData);
      OS_LeaveLock( ptInterfaceData->pvConnectorLock);
    }

    if (ptInterfaceData->pvRXThreadStopped != NULL)
      OS_DeleteEvent( ptInterfaceData->pvRXThreadStopped);

    free( ptInterfaceData);
    ptInterfaceData = NULL;
  }
  sprintf(szWhatBuffer, "End");
  A4A_Log(szWhoBuffer, szWhatBuffer, A4A_LOG_LEVEL_INFO);

  OS_Sleep( 1000 );

  return 0;
}

/*****************************************************************************/
/*! Create interface and initializes connection
*   \param szDeviceName  Name of the interface
*   \return Pointer to the interface                                         */
/*****************************************************************************/
void* APIENTRY TCP_CreateIF(const char* szDeviceName)
{
  int32_t        lRet            = -1;
  PTCP_IF_DATA_T ptInterfaceData = NULL;
  int            fFound          = 0;
  int            iTmpRet         = 0;
  char szWhatBuffer [WHAT_BUFFER_LEN];
  char szWhoBuffer  [WHO_BUFFER_LEN];

  strcpy(szWhoBuffer, PACKAGE_NAME);
  strcat(szWhoBuffer, __func__);

  sprintf(szWhatBuffer, "szDeviceName : %s", szDeviceName);
  A4A_Log(szWhoBuffer, szWhatBuffer, A4A_LOG_LEVEL_INFO);

  TAILQ_FOREACH( ptInterfaceData, &s_tConnectorInfo.tConnectorList, tList)
  {
    if (strcmp( ptInterfaceData->szInterfaceName, szDeviceName) == 0)
    {
      /* interface found! */
      fFound = 1;
      break;
    }
  }

  if (1 == fFound)
  {
    int iNoDelay = 1;

    ptInterfaceData->pvEvent = OS_CreateEvent();

    ptInterfaceData->fEventThread = 1;
    if ((iTmpRet = pthread_create( &ptInterfaceData->tEventThread, NULL, EventThread, ptInterfaceData)))
    {
      sprintf(szWhatBuffer, "Error creating Com-Thread (Error=%d)", iTmpRet);
      A4A_Log(szWhoBuffer, szWhatBuffer, A4A_LOG_LEVEL_ERROR);
      OS_DeleteEvent( ptInterfaceData->pvEvent);
      return NULL;
    }

    /* create socket */
    ptInterfaceData->iSocket = socket(AF_INET, SOCK_STREAM, 0);

    if ( setsockopt( ptInterfaceData->iSocket, IPPROTO_TCP, TCP_NODELAY, (void*)&iNoDelay, sizeof(iNoDelay)) != 0 )
    {
      sprintf(szWhatBuffer, "The server is not able to send small packets.\n"
              "So the communication could be very slow!");
      A4A_Log(szWhoBuffer, szWhatBuffer, A4A_LOG_LEVEL_WARNING);
    }

    if (ptInterfaceData->iSocket < 0)
    {
      sprintf(szWhatBuffer, "ERROR opening socket");
      A4A_Log(szWhoBuffer, szWhatBuffer, A4A_LOG_LEVEL_ERROR);
   } else
    {
      ptInterfaceData->tSockIn.sin_family = AF_INET;                               /* Adressfamilie AF_XXX */
      ptInterfaceData->tSockIn.sin_port   = htons(ptInterfaceData->tConnectorConfig.ulPort);        /* Portnummer           */
      inet_aton( ptInterfaceData->tConnectorConfig.szIPConfig, &ptInterfaceData->tSockIn.sin_addr); /* IP-Adresse           */

      if (connect( ptInterfaceData->iSocket,(struct sockaddr*)&ptInterfaceData->tSockIn, sizeof(ptInterfaceData->tSockIn)) == 0)
      {
        lRet = NXT_NO_ERROR;
      } else
      {
        /* This might be no error in case of no running server */
        sprintf(szWhatBuffer,
                "No server detected on interface %s (IP:%s) (Error=%d)",
                ptInterfaceData->szInterfaceName,
                ptInterfaceData->tConnectorConfig.szIPConfig, errno);
        A4A_Log(szWhoBuffer, szWhatBuffer, A4A_LOG_LEVEL_WARNING);
      }
    }
    if (lRet != NXT_NO_ERROR)
    {
      if (ptInterfaceData->iSocket > 0)
      {
        close(ptInterfaceData->iSocket);
        ptInterfaceData = NULL;
      }
    }
  }
  return ptInterfaceData;
}

/*****************************************************************************/
/*! Returns connector information (currently not used)
*   \param eCmd             Command to execute
*   \param ulSize           Size of buffer pvConnectorInfo
*   \param pvConnectorInfo  Pointer to buffer
*   \return 0 on success                                                     */
/*****************************************************************************/
int32_t APIENTRY TCP_GetInformation( NETX_CONNECTOR_INFO_E eCmd, uint32_t ulSize, void* pvConnectorInfo)
{
  int32_t lRet = NXT_NO_ERROR;
  char szWhatBuffer [WHAT_BUFFER_LEN];
  char szWhoBuffer  [WHO_BUFFER_LEN];

  strcpy(szWhoBuffer, PACKAGE_NAME);
  strcat(szWhoBuffer, __func__);

  sprintf(szWhatBuffer, "Trace");
  A4A_Log(szWhoBuffer, szWhatBuffer, A4A_LOG_LEVEL_INFO);

  UNREFERENCED_PARAMETER(eCmd);
  UNREFERENCED_PARAMETER(ulSize);
  UNREFERENCED_PARAMETER(pvConnectorInfo);

  return lRet;
}

/*****************************************************************************/
/*! Returns connector configuration (currently not used)
*   \param eCmd      Command to execute
*   \param pvConfig  Pointer to buffer
*   \return 0 on success                                                     */
/*****************************************************************************/
int32_t APIENTRY TCP_GetConfig( NETX_CONNECTOR_CONFIG_CMD_E eCmd, void* pvConfig)
{
  int32_t lRet = NXT_NO_ERROR;
  char szWhatBuffer [WHAT_BUFFER_LEN];
  char szWhoBuffer  [WHO_BUFFER_LEN];

  strcpy(szWhoBuffer, PACKAGE_NAME);
  strcat(szWhoBuffer, __func__);

  sprintf(szWhatBuffer, "Trace");
  A4A_Log(szWhoBuffer, szWhatBuffer, A4A_LOG_LEVEL_INFO);

  UNREFERENCED_PARAMETER(eCmd);
  UNREFERENCED_PARAMETER(pvConfig);

  return lRet;
}

/*****************************************************************************/
/*! Sets connector configuration (currently not used)
*   \param eCmd      Command to execute
*   \param pvConfig  Pointer to buffer
*   \return 0 on success                                                     */
/*****************************************************************************/
int32_t APIENTRY TCP_SetConfig( NETX_CONNECTOR_CONFIG_CMD_E eCmd, const char* szConfig)
{
  int32_t lRet = NXT_NO_ERROR;
  char szWhatBuffer [WHAT_BUFFER_LEN];
  char szWhoBuffer  [WHO_BUFFER_LEN];

  strcpy(szWhoBuffer, PACKAGE_NAME);
  strcat(szWhoBuffer, __func__);

  sprintf(szWhatBuffer, "Trace");
  A4A_Log(szWhoBuffer, szWhatBuffer, A4A_LOG_LEVEL_INFO);

  UNREFERENCED_PARAMETER(eCmd);
  UNREFERENCED_PARAMETER(szConfig);

  return lRet;
}

/*****************************************************************************/
/*! Starts interface (Com-Thread)
*   \param pvInterface     Pointer to the interface info structure
*   \param pfnReceiveData  Callback (receive data callback)
*   \param pvUser          User data
*   \return 0 on success                                                     */
/*****************************************************************************/
int32_t APIENTRY TCP_IF_Start( void* pvInterface, PFN_NETXCON_DEVICE_RECEIVE_CALLBACK pfnReceiveData, void* pvUser)
{
  PTCP_IF_DATA_T ptInterfaceData     = (PTCP_IF_DATA_T)pvInterface;
  pthread_attr_t polling_thread_attr = {{0}};
  int            iTmpRet             = 0;
  int32_t        iRet                = 0;
  char szWhatBuffer [WHAT_BUFFER_LEN];
  char szWhoBuffer  [WHO_BUFFER_LEN];

  strcpy(szWhoBuffer, PACKAGE_NAME);
  strcat(szWhoBuffer, __func__);

  sprintf(szWhatBuffer, "Trace");
  A4A_Log(szWhoBuffer, szWhatBuffer, A4A_LOG_LEVEL_INFO);

  /* set receive callback data */
  ptInterfaceData->pfnRxCallback     = pfnReceiveData;
  ptInterfaceData->pvRxCallbackData  = pvUser;
  ptInterfaceData->pvRXThreadStopped = OS_CreateEvent();
  ptInterfaceData->pvConnectorLock   = OS_CreateLock();

  pthread_attr_setstacksize( &polling_thread_attr, PTHREAD_STACK_MIN);

  ptInterfaceData->fThreadRunning = 1;

  /* create thread for read processing */
  if ((iTmpRet = pthread_create( &ptInterfaceData->tRXThread, &polling_thread_attr, CommThread, ptInterfaceData)))
  {
    sprintf(szWhatBuffer, "Error createing Com-Thread (Error=%d)", iTmpRet);
    A4A_Log(szWhoBuffer, szWhatBuffer, A4A_LOG_LEVEL_ERROR);
    iRet = -1;
  } else
  {
    /* set interface state to running */
    ptInterfaceData->ulState = eINTERFACE_STATE_RUNNING;
  }

  return iRet;
}

/*****************************************************************************/
/*! Stops interface and de-initializes connection
*   \param pvInterface  Pointer to interface info structure
*   \return 0 on success                                                     */
/*****************************************************************************/
int32_t APIENTRY TCP_IF_Stop( void* pvInterface)
{
  PTCP_IF_DATA_T ptInterfaceData = (PTCP_IF_DATA_T)pvInterface;
  char szWhatBuffer [WHAT_BUFFER_LEN];
  char szWhoBuffer  [WHO_BUFFER_LEN];

  strcpy(szWhoBuffer, PACKAGE_NAME);
  strcat(szWhoBuffer, __func__);

  sprintf(szWhatBuffer, "Trace");
  A4A_Log(szWhoBuffer, szWhatBuffer, A4A_LOG_LEVEL_INFO);

  if (ptInterfaceData->pvConnectorLock == NULL)
    return 0;

  OS_EnterLock( ptInterfaceData->pvConnectorLock);
  if (ptInterfaceData->ulState != eINTERFACE_STATE_STOPPED)
  {
    /* stop rX-Thread */
    ptInterfaceData->fThreadRunning = 0;

    if (ptInterfaceData->pvRXThreadStopped != NULL)
    {
      /* wait for terminating thread */
      if (OS_WaitEvent(ptInterfaceData->pvRXThreadStopped, 1000))
      {
        pthread_cancel( ptInterfaceData->tRXThread);
      }
    }
    OS_DeleteEvent( ptInterfaceData->pvRXThreadStopped);
    ptInterfaceData->pvRXThreadStopped = NULL;

    /* Close open TCP Socket */
    if( -1 != ptInterfaceData->iSocket)
    {
      close( ptInterfaceData->iSocket);
      ptInterfaceData->iSocket = -1;
    }

    /* Set interface state to stopped */
    ptInterfaceData->ulState = eINTERFACE_STATE_STOPPED;
  }
  OS_LeaveLock( ptInterfaceData->pvConnectorLock);

  return NXT_NO_ERROR;
}

/*****************************************************************************/
/*! Send data
*   \param pvInterface  Pointer to interface info structure
*   \param pabData      Pointer to the data to send
*   \param pvInterface  Length of buffer pabData
*   \return 0 on success                                                     */
/*****************************************************************************/
int32_t APIENTRY TCP_IF_Send( void* pvInterface, unsigned char* pabData, uint32_t ulDataLen)
{
  PTCP_IF_DATA_T ptInterfaceData = (PTCP_IF_DATA_T)pvInterface;
  int32_t        ulRet           = 0;
  size_t         SendSize        = 0;
  char szWhatBuffer [WHAT_BUFFER_LEN];
  char szWhoBuffer  [WHO_BUFFER_LEN];

  strcpy(szWhoBuffer, PACKAGE_NAME);
  strcat(szWhoBuffer, __func__);

/*  sprintf(szWhatBuffer, "Trace");
  A4A_Log(szWhoBuffer, szWhatBuffer, A4A_LOG_LEVEL_INFO);
*/
  /* Check if interface is running */
  if (eINTERFACE_STATE_RUNNING != ptInterfaceData->ulState)
  {
    sprintf(szWhatBuffer, "Send error: %s (Interface not running)",
            ptInterfaceData->szInterfaceName);
    A4A_Log(szWhoBuffer, szWhatBuffer, A4A_LOG_LEVEL_ERROR);
    return NXCON_DRV_NOT_START;
  }

  if (0 > (SendSize = send (ptInterfaceData->iSocket, (char*)pabData, ulDataLen, 0)))
  {
    ulRet = -1;
  }

  if (0 != ulRet)
  {
    sprintf(szWhatBuffer, "Send error: %s -> %s (Error code %u)",
            ptInterfaceData->szInterfaceName, strerror(errno), errno);
    A4A_Log(szWhoBuffer, szWhatBuffer, A4A_LOG_LEVEL_ERROR);
  } else if (SendSize < ulDataLen)
  {
    sprintf(szWhatBuffer, "Error sending all data error: "
            "%s -> %s (Error code %u)",
            ptInterfaceData->szInterfaceName, strerror(errno), errno);
    A4A_Log(szWhoBuffer, szWhatBuffer, A4A_LOG_LEVEL_ERROR);
  }

  return ulRet;
}

/*****************************************************************************/
/*! Returns interface information (currently not used)
*   \param eCmd             Command to execute
*   \param ulSize           Size of buffer pvConnectorInfo
*   \param pvInterfaceInfo  Pointer to buffer
*   \return 0 on success                                                     */
/*****************************************************************************/
int32_t APIENTRY TCP_IF_GetInformation( void* pvInterface, NETX_INTERFACE_INFO_E eCmd, uint32_t ulSize, void* pvInterfaceInfo)
{
  int32_t lRet = NXT_NO_ERROR;
  char szWhatBuffer [WHAT_BUFFER_LEN];
  char szWhoBuffer  [WHO_BUFFER_LEN];

  strcpy(szWhoBuffer, PACKAGE_NAME);
  strcat(szWhoBuffer, __func__);

  sprintf(szWhatBuffer, "Trace");
  A4A_Log(szWhoBuffer, szWhatBuffer, A4A_LOG_LEVEL_INFO);

  UNREFERENCED_PARAMETER(pvInterface);
  UNREFERENCED_PARAMETER(eCmd);
  UNREFERENCED_PARAMETER(ulSize);
  UNREFERENCED_PARAMETER(pvInterfaceInfo);

  return lRet;
}

/*****************************************************************************/
/*! Thread processes incoming receive data
*   \param pvParam  User data (pointer to interface info structure)
*   \return NULL                                                             */
/*****************************************************************************/
void* CommThread(void* pvParam)
{
  PTCP_IF_DATA_T ptInterfaceData = (PTCP_IF_DATA_T)pvParam;
  char szWhatBuffer [WHAT_BUFFER_LEN];
  char szWhoBuffer  [WHO_BUFFER_LEN];

  strcpy(szWhoBuffer, PACKAGE_NAME);
  strcat(szWhoBuffer, __func__);

  sprintf(szWhatBuffer, "Trace");
  A4A_Log(szWhoBuffer, szWhatBuffer, A4A_LOG_LEVEL_INFO);

  while(ptInterfaceData->fThreadRunning)
  {
    fd_set  tRead, tExcept;
    struct timeval tTimeout = {SOCKET_TIMEOUT, 0};

    FD_ZERO(&tRead);
    FD_ZERO(&tExcept);

    FD_SET(ptInterfaceData->iSocket, &tRead);
    FD_SET(ptInterfaceData->iSocket, &tExcept);

    int iErr = select( ptInterfaceData->iSocket + 1, &tRead, NULL, &tExcept, NULL);

    if(0 < iErr)
    {
      if(FD_ISSET(ptInterfaceData->iSocket, &tRead))
      {
        /* We have data to read */
        unsigned long  ulDataLen;
        unsigned char* pbData;
        size_t         iRecv;

        ioctl( ptInterfaceData->iSocket, FIONREAD, &ulDataLen);

        pbData = (unsigned char*)OS_Memalloc(ulDataLen);

        OS_Memset(pbData,0,ulDataLen);


        iRecv = recv(ptInterfaceData->iSocket, (char*)pbData, ulDataLen, 0);

        if((-1 == iRecv) || (0 == iRecv))
        {
          ptInterfaceData->ulEvent = INTERFACE_EVENT_DETACHED;
          OS_SetEvent( ptInterfaceData->pvEvent);

          /* Gracefully closed socket */
          free(pbData);
          close(ptInterfaceData->iSocket);
          ptInterfaceData->iSocket = -1;
          break;
        } else
        {
          unsigned long ulDataLen = (unsigned long)iRecv;

          /* notify data to HilTransport-Layer */
          ptInterfaceData->pfnRxCallback( pbData, ulDataLen, ptInterfaceData->pvRxCallbackData);

          OS_Memfree(pbData);
        }
      }
      if(FD_ISSET(ptInterfaceData->iSocket, &tExcept))
      {
        /* Socket has been closed */
        close(ptInterfaceData->iSocket);
        sprintf(szWhatBuffer, "Exception on socket %d: %s (Error code %i)",
                ptInterfaceData->iSocket, strerror(errno), errno);
        A4A_Log(szWhoBuffer, szWhatBuffer, A4A_LOG_LEVEL_ERROR);
        ptInterfaceData->iSocket = -1;
        break;
      }
    } else if (iErr == 0)
    {
      /* Timeout -> close socket */
      close(ptInterfaceData->iSocket);
      sprintf(szWhatBuffer, "Timeout on socket %d", ptInterfaceData->iSocket);
      A4A_Log(szWhoBuffer, szWhatBuffer, A4A_LOG_LEVEL_ERROR);
      ptInterfaceData->iSocket = -1;
      break;
    }
  }
  /* signal thread termination */
  OS_SetEvent( ptInterfaceData->pvRXThreadStopped);

  return 0;
}

/*****************************************************************************/
/*! Interface statet notify thread
*   \param pvParam  User data (pointer to interface info structure)
*   \return NULL                                                             */
/*****************************************************************************/
void* EventThread(void* pvParam)
{
  PTCP_IF_DATA_T ptInterfaceData = (PTCP_IF_DATA_T)pvParam;

  char szWhatBuffer [WHAT_BUFFER_LEN];
  char szWhoBuffer  [WHO_BUFFER_LEN];

  strcpy(szWhoBuffer, PACKAGE_NAME);
  strcat(szWhoBuffer, __func__);

  sprintf(szWhatBuffer, "Trace");
  A4A_Log(szWhoBuffer, szWhatBuffer, A4A_LOG_LEVEL_INFO);

  while( ptInterfaceData->fEventThread)
  {
    if (OS_EVENT_SIGNALLED == OS_WaitEvent( ptInterfaceData->pvEvent, 10))
    {
      switch ( ptInterfaceData->ulEvent)
      {
        case INTERFACE_EVENT_ATTACHED:
          ptInterfaceData->pfnDevNotifyCallback( ptInterfaceData->szInterfaceName, eNXT_DEVICE_DETACHED, NULL, ptInterfaceData->pvUser);
          break;
        case INTERFACE_EVENT_DETACHED:
          ptInterfaceData->pfnDevNotifyCallback( ptInterfaceData->szInterfaceName, eNXT_DEVICE_DETACHED, NULL, ptInterfaceData->pvUser);
          break;
        default:
          break;
      }
    }
  }

  sprintf(szWhatBuffer, "Thread Stopped");
  A4A_Log(szWhoBuffer, szWhatBuffer, A4A_LOG_LEVEL_INFO);

  /* signal thread termination */
  OS_SetEvent( ptInterfaceData->pvEventThreadStopped);

  sprintf(szWhatBuffer, "Thread Stopped Event sent");
  A4A_Log(szWhoBuffer, szWhatBuffer, A4A_LOG_LEVEL_INFO);

  return NULL;
}

/*****************************************************************************/
/*! Reads and interprets TCP connector configuration files
*   \param ptConnectorInfo  Pointer to global connector information structure
*   \return 0 on success                                                     */
/*****************************************************************************/
int32_t APIENTRY TCP_ReadConfigFile( TCP_CONNECTOR_INFO_T* ptConnectorInfo)
{
  int32_t     lRet           = NXT_NO_ERROR;
  uint32_t    ulConfigCount  = 0;
  char*       szTmp          = NULL;
  char        szConfigFile[256];
  char        szInterfaceName[16];
  struct stat tStat;
  char szWhatBuffer [WHAT_BUFFER_LEN];
  char szWhoBuffer  [WHO_BUFFER_LEN];

  strcpy(szWhoBuffer, PACKAGE_NAME);
  strcat(szWhoBuffer, __func__);

  sprintf(szWhatBuffer, "Trace");
  A4A_Log(szWhoBuffer, szWhatBuffer, A4A_LOG_LEVEL_INFO);

  /* check if file exists */
  sprintf(szConfigFile, "./tcpconfig%d", ulConfigCount);
  while (0 == stat( szConfigFile, &tStat))
  {
    if (GetConfigString(szConfigFile, "IP=", &szTmp))
    {
      PTCP_IF_DATA_T ptInterfaceData = NULL;
      if (NULL == (ptInterfaceData = malloc(sizeof(TCP_IF_DATA_T))))
        break;

      memset( ptInterfaceData, 0, sizeof(TCP_IF_DATA_T));

      sprintf( szInterfaceName, "TCP%d", s_tConnectorInfo.ulConfigCount);

      strcpy( ptInterfaceData->szInterfaceName, szInterfaceName);

      /* allocate new configuration */
      strcpy( ptInterfaceData->tConnectorConfig.szIPConfig, szTmp);
      free( szTmp);

      /* add all interfaces (TCP0 (e.g. 192.168.178.12), TCP1 (e.g. 192.168.178.13), TCPx (...), ...) */
      TAILQ_INSERT_TAIL(&s_tConnectorInfo.tConnectorList, ptInterfaceData, tList);
      s_tConnectorInfo.ulConfigCount++;

      ptInterfaceData->tConnectorConfig.ulPort = NETX_TRANSPORT_DEF_PORT;
      if (GetConfigString(szConfigFile, "Port=", &szTmp))
      {
        ptInterfaceData->tConnectorConfig.ulPort = atoi(szTmp);
        free( szTmp);
      }

      ptInterfaceData->tConnectorConfig.ulSendTimeout = TCP_SEND_TIMEOUT;
      if (GetConfigString(szConfigFile, "SendTimeout=", &szTmp))
      {
        ptInterfaceData->tConnectorConfig.ulSendTimeout = atoi(szTmp);
        free( szTmp);
      }

      ptInterfaceData->tConnectorConfig.ulKeepAliveTimeout = TCP_KEEP_ALIVE_TIMEOUT;
      if (GetConfigString(szConfigFile, "KeepAliveTimeout=", &szTmp))
      {
        ptInterfaceData->tConnectorConfig.ulKeepAliveTimeout = atoi(szTmp);
        free( szTmp);
      }

      ptInterfaceData->tConnectorConfig.ulResetTimeout = TCP_RESET_TIMEOUT;
      if (GetConfigString(szConfigFile, "ResetTimeout=", &szTmp))
      {
        ptInterfaceData->tConnectorConfig.ulResetTimeout = atoi(szTmp);
        free( szTmp);
      }
    }
    sprintf(szConfigFile, "./tcpconfig%d", ++ulConfigCount);
  }
  if (ulConfigCount == 0)
  {
    sprintf(szWhatBuffer, "Error reading TCP-Connector config file!"
	    " tcpconfig file must reside in the execution path!");
    A4A_Log(szWhoBuffer, szWhatBuffer, A4A_LOG_LEVEL_ERROR);
  }

  return lRet;
}

/*****************************************************************************/
/*! Parses connector configuration file
*   \param szFile   File name
*   \param szKey    Key to search for
*   \param szValue  Returned value (if key was found)
*   \return != 0 on success                                                  */
/*****************************************************************************/
static int GetConfigString(const char* szFile, const char* szKey, char** szValue)
{
  int   ret = 0;
  FILE* fd  = fopen(szFile, "r");
  char szWhatBuffer [WHAT_BUFFER_LEN];
  char szWhoBuffer  [WHO_BUFFER_LEN];

  strcpy(szWhoBuffer, PACKAGE_NAME);
  strcat(szWhoBuffer, __func__);

  sprintf(szWhatBuffer, "Trace");
  A4A_Log(szWhoBuffer, szWhatBuffer, A4A_LOG_LEVEL_INFO);

  if(NULL != fd)
  {
    /* File is open */
    char* buffer = malloc(PARSER_BUFFER_SIZE);

    /* Read file line by line */
    while(NULL != fgets(buffer, PARSER_BUFFER_SIZE, fd))
    {
      char* key;

      /* '#' marks a comment line in the device.conf file */
      if(buffer[0] == '#')
        continue;

      /* Search for key in the input buffer */
      /*key = (char*)strcasestr(buffer, szKey);*/
      key = (char*)strstr(buffer, szKey);

      if(NULL != key)
      {
        /* We've found the key */
        int   allocsize  = strlen(key + strlen(szKey)) + 1;
        int   valuelen;
        char* tempstring = (char*)malloc(allocsize);

        strcpy(tempstring, key + strlen(szKey));
        valuelen = strlen(tempstring);

        /* strip all trailing whitespaces */
        while( (tempstring[valuelen - 1] == '\n') ||
               (tempstring[valuelen - 1] == '\r') ||
               (tempstring[valuelen - 1] == ' ') )
        {
          tempstring[valuelen - 1] = '\0';
          --valuelen;
        }

        *szValue = tempstring;
        ret = 1;
        break;
      }
    }

    free(buffer);
    fclose(fd);
  }

  return ret;
}
