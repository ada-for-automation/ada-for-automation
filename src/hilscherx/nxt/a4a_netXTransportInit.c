/*
-----------------------------------------------------------------------
--                       Ada for Automation                          --
--                                                                   --
--              Copyright (C) 2012-2015, Stephane LOS                --
--                                                                   --
-- This library is free software; you can redistribute it and/or     --
-- modify it under the terms of the GNU General Public               --
-- License as published by the Free Software Foundation; either      --
-- version 2 of the License, or (at your option) any later version.  --
--                                                                   --
-- This library is distributed in the hope that it will be useful,   --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of    --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU --
-- General Public License for more details.                          --
--                                                                   --
-- You should have received a copy of the GNU General Public         --
-- License along with this library; if not, write to the             --
-- Free Software Foundation, Inc., 59 Temple Place - Suite 330,      --
-- Boston, MA 02111-1307, USA.                                       --
--                                                                   --
-- As a special exception, if other files instantiate generics from  --
-- this unit, or you link this unit with other files to produce an   --
-- executable, this  unit  does not  by itself cause  the resulting  --
-- executable to be covered by the GNU General Public License. This  --
-- exception does not however invalidate any other reasons why the   --
-- executable file  might be covered by the  GNU Public License.     --
-----------------------------------------------------------------------
*/

#include <stdio.h>
#include "cifXUser.h"
#include "netXTransport.h"
#include "TL_Marshaller.h"
#include "TL_rcXPacket.h"
#include "OS_Dependent.h"
#include "a4a_LogWrapAda.h"

/* function declaration */
/* netXTransport specific functions */
int32_t TCP_Connector_Init    (void);
void    TCP_Connector_Deinit  (void);

static TL_INIT_T      tDataLayerInit;

#define WHAT_BUFFER_LEN        100

/*****************************************************************************/
/*****************************************************************************/
/*! FUNCTIONS REQUIRED FOR NETXTRANSPORT INITIALIZATION                      */
/*****************************************************************************/
/*****************************************************************************/

/*****************************************************************************/
/*! Initializes the cifX Marshaller Translation-Layer and the rcX-Packet
*   Translation-Layer.
*   \param pvParam    User param (currently not used)
*   \return NXT_NO_ERROR on success                                          */
/*****************************************************************************/
int32_t TLLayerInit( void* pvParam)
{
  int32_t lRet = NXT_NO_ERROR;

  char szWhatBuffer [WHAT_BUFFER_LEN];

  sprintf(szWhatBuffer, "TLLayerInit Start");
  A4A_Log("a4a_netXTransportInit", szWhatBuffer, A4A_LOG_LEVEL_INFO);

  lRet = cifX_Marshaller_Init( pvParam);
  if (NXT_NO_ERROR == lRet)
  {
    lRet = rcXPacket_Init( pvParam);
  }
  return lRet;
}

/*****************************************************************************/
/*! De-initializes translation layer
*   \param pvParam    User param (currently not used)                        */
/*****************************************************************************/
void TLLayerDeInit( void* pvParam)
{
  char szWhatBuffer [WHAT_BUFFER_LEN];

  sprintf(szWhatBuffer, "TLLayerDeInit Start");
  A4A_Log("a4a_netXTransportInit", szWhatBuffer, A4A_LOG_LEVEL_INFO);

  cifX_Marshaller_DeInit( pvParam);
  rcXPacket_DeInit( pvParam);

  sprintf(szWhatBuffer, "TLLayerDeInit Done");
  A4A_Log("a4a_netXTransportInit", szWhatBuffer, A4A_LOG_LEVEL_INFO);
}

int32_t a4a_netXTransportInit()
{
  char szWhatBuffer [WHAT_BUFFER_LEN];
  int32_t        lRet                = NXT_NO_ERROR;

  sprintf(szWhatBuffer, "a4a_netXTransportInit Start");
  A4A_Log("a4a_netXTransportInit", szWhatBuffer, A4A_LOG_LEVEL_INFO);
  /* setup netXTransport initialization structure */

  /* function pointer the Translation-Layer initialization    */
  tDataLayerInit.pfnTLInit   = TLLayerInit;

  /* function pointer the Translation-Layer de-initialization */
  tDataLayerInit.pfnTLDeInit = TLLayerDeInit;

  /* Private data (currently  not used)                       */
  tDataLayerInit.pvData      = NULL;

  /***************************************************************************/
  /* 1. Initialize netXTransport Toolkit (add Translation-Layer)             */
  /***************************************************************************/
  sprintf(szWhatBuffer, "Initializing netXTransport Toolkit");
  A4A_Log("a4a_netXTransportInit", szWhatBuffer, A4A_LOG_LEVEL_INFO);
  lRet = netXTransportInit( &tDataLayerInit, sizeof(tDataLayerInit));
  if (NXT_NO_ERROR != lRet)
  {
    sprintf(szWhatBuffer,
            "Error during toolkit initialization. lRet=0x%X\n", lRet);
    A4A_Log("a4a_netXTransportInit", szWhatBuffer, A4A_LOG_LEVEL_ERROR);
  }

  /***************************************************************************/
  /* 2. Add a connector                                                      */
  /***************************************************************************/
  if (lRet == NXT_NO_ERROR)
  {
    sprintf(szWhatBuffer, "Add example connector TCP...");
    A4A_Log("a4a_netXTransportInit", szWhatBuffer, A4A_LOG_LEVEL_INFO);
    if (NXT_NO_ERROR != (lRet = TCP_Connector_Init()))
    {
      sprintf(szWhatBuffer,
              "Error during TCP-Connector initialization. lRet=0x%X\n", lRet);
      A4A_Log("a4a_netXTransportInit", szWhatBuffer, A4A_LOG_LEVEL_ERROR);
    }
  }

  /***************************************************************************/
  /* 3. Start the netXTransport Toolkit                                      */
  /***************************************************************************/
  if (lRet == NXT_NO_ERROR)
  {
    sprintf(szWhatBuffer, "Start netXTransport Toolkit...");
    A4A_Log("a4a_netXTransportInit", szWhatBuffer, A4A_LOG_LEVEL_INFO);

    /* start netXTransport (starts device discovering and registration) */
    lRet = netXTransportStart( NULL, NULL);
    if (NXT_NO_ERROR != lRet)
    {
      sprintf(szWhatBuffer,
              "Error while trying to start. lRet=0x%X\n", lRet);
      A4A_Log("a4a_netXTransportInit", szWhatBuffer, A4A_LOG_LEVEL_ERROR);
    }
  }

  if (lRet != NXT_NO_ERROR)
  {
    sprintf(szWhatBuffer, "Aborting netXTransport!");
    A4A_Log("a4a_netXTransportInit", szWhatBuffer, A4A_LOG_LEVEL_ERROR);
    /* de-initialize netXTransport */
    netXTransportDeinit();
    TCP_Connector_Deinit();

    return lRet;
  }

}

int32_t a4a_netXTransportDeinit()
{
  char szWhatBuffer [WHAT_BUFFER_LEN];

  sprintf(szWhatBuffer, "TCP_Connector_Deinit");
  A4A_Log("a4a_netXTransportDeinit", szWhatBuffer, A4A_LOG_LEVEL_INFO);

  TCP_Connector_Deinit();

  sprintf(szWhatBuffer, "TCP_Connector_Deinit done");
  A4A_Log("a4a_netXTransportDeinit", szWhatBuffer, A4A_LOG_LEVEL_INFO);

  /* After using, deinitialize netXTransport */
  sprintf(szWhatBuffer, "netXTransportDeinit");
  A4A_Log("a4a_netXTransportDeinit", szWhatBuffer, A4A_LOG_LEVEL_INFO);

  netXTransportDeinit();

  sprintf(szWhatBuffer, "netXTransportDeinit done");
  A4A_Log("a4a_netXTransportDeinit", szWhatBuffer, A4A_LOG_LEVEL_INFO);

  return NXT_NO_ERROR;
}
