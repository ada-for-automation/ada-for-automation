
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package provides the binding to Hilscher cifX Device Driver,
--  a library allowing access to the cifX boards with numerous protocols.
--  </summary>
--  <description>
--  It provides :
--  - messaging infrastructure.
--  </description>
--  <group>Protocols</group>
--  <c_version>3.0.1</c_version>

with A4A.Protocols.HilscherX.cifX_User;
use A4A.Protocols.HilscherX.cifX_User;

with A4A.Protocols.HilscherX.Packet_Pool;
use A4A.Protocols.HilscherX.Packet_Pool;

with A4A.Protocols.HilscherX.Packet_Queue;
use A4A.Protocols.HilscherX.Packet_Queue;

with A4A.Protocols.HilscherX.Packet_Queue_Map;
use A4A.Protocols.HilscherX.Packet_Queue_Map;

package A4A.Protocols.HilscherX.Channel_Messaging is

   Default_Pool_Size  : constant := 20;
   Default_Queue_Size : constant := 10;

   Default_App  : constant := 0;
   A4A_App_0    : constant := 16#A4A00000#;

   type Instance is tagged limited private;
   type Instance_Access is access all Instance;

   procedure Initialise
     (Channel        : in out Instance;
      Channel_Handle : in Channel_Handle_Type);

   procedure Quit
     (Channel : in out Instance);

   function Is_Initialised
     (Channel : in Instance) return Boolean;

   function Is_Terminated
     (Channel : in Instance) return Boolean;

   procedure Get_Packet
     (Channel : in out Instance;
      Item    : out cifX_Packet_Access;
      Result  : out Boolean);
   --  Get a packet from the Packet Pool
   --  Returns False if the Pool is empty

   procedure Return_Packet
     (Channel : in out Instance;
      Item    : in cifX_Packet_Access);
   --  Return a packet to the Packet Pool

   procedure Put
     (Channel : in out Instance;
      Item   : in Packet_Queue_Access;
      Index  : out DWord;
      Result : out Boolean);
   --  Registers a Packet Queue in the Map and get corresponding Index
   --  to be used as Source_Id in Packets
   --  Returns False if the Map is full

   procedure Get
     (Channel : in out Instance;
      Index  : in DWord;
      Item   : out Packet_Queue_Access;
      Result : out Boolean);
   --  Retrieves a Packet Queue from Map given its Index got from
   --  Packet Source_Id
   --  Returns False if the Index out of bounds

   procedure Delete
     (Channel : in out Instance;
      Index  : in DWord;
      Result : out Boolean);
   --  Deletes a Packet Queue from Map given its Index
   --  Returns False if Index out of bounds

   procedure Send
     (Channel : in out Instance;
      Item    : in cifX_Packet_Access;
      Result  : out Boolean);
   --  Send a packet via the Packet Queue
   --  Returns False if the Queue is full

   procedure Receive
     (Channel : in out Instance;
      Item    : out cifX_Packet_Access;
      Result  : out Boolean);
   --  Receive a packet from the default receive queue
   --  Returns False if no packet was available

   function Get_Pending
     (Channel : in Instance) return DWord;
   --  Returns the number of packets in the default receive queue

   function Get_Send_Pkt_Count
     (Channel : in Instance) return DWord;
   --  Returns the Number of packets the Host is able to send at once

private

   Package_Ident : String := "A4A.Protocols.HilscherX.Channel_Messaging";

   task type Send_Msg_Task_Type
     (Channel : access Instance);

   task type Receive_Msg_Task_Type
     (Channel : access Instance);

   type Instance is tagged limited
      record

         Quit_Flag : Boolean := False;
         Init_Flag : Boolean := False;

         Send_Task_Terminated    : Boolean := False;
         Receive_Task_Terminated : Boolean := False;

         The_Channel_Handle  : Channel_Handle_Type;

         --  The real thing
         Recv_Pkt_Count : aliased DWord := 0;
         Send_Pkt_Count : aliased DWord := 0;

         The_Packet_Pool  : Packet_Pool_Type (Pool_Size => Default_Pool_Size);

         The_Queue_Map : aliased Packet_Queue_Map_Type (Queue_Map_Size => 100);
         --  Default size is 100

         Send_Queue : aliased Packet_Queue_Type
           (Queue_Size => Default_Queue_Size);

         Receive_Queue : aliased Packet_Queue_Type
           (Queue_Size => Default_Queue_Size);

         Send_Msg_Task    : Send_Msg_Task_Type (Instance'Access);
         Receive_Msg_Task : Receive_Msg_Task_Type (Instance'Access);

      end record;

end A4A.Protocols.HilscherX.Channel_Messaging;
