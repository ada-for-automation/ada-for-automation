
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Gtk.Enums;
with Gtk.Main;
with Gtk.Dialog;           use Gtk.Dialog;
with Gtk.Message_Dialog;   use Gtk.Message_Dialog;

--  with Gtkada.Handlers; use Gtkada.Handlers;
with A4A.GUI.Handlers;

with A4A.Application.Identification; use A4A.Application.Identification;
with A4A.Kernel; use A4A.Kernel;
with A4A.Kernel.Main; use A4A.Kernel.Main;
with A4A.Kernel.Fieldbus1;

package body A4A.GUI is

   procedure Connect_Handlers is
      package App_Handlers renames A4A.GUI.Handlers;
   begin
      --  When the window is given the "delete_event" signal (this is given
      --  by the window manager, usually by the "close" option, or on the
      --  titlebar), we ask it to call the Delete_Event function.
      App_Handlers.Return_Handlers.Connect
        (Main_Window, "delete_event",
         App_Handlers.Return_Handlers.To_Marshaller
           (App_Handlers.Delete_Event'Access));

      --  Here we connect the "destroy" event to a signal handler.
      --  This event occurs when we call Gtk.Widget.Destroy on the window,
      --  or if we return False in the "delete_event" callback.
      --        App_Handlers.Handlers.Connect
      --          (Main_Window, "destroy",
      --           App_Handlers.Handlers.To_Marshaller
      --             (App_Handlers.Destroy'Access));

      App_Handlers.Handlers.Connect
        (Button_Bar.Quit_Button, "clicked",
         App_Handlers.Handlers.To_Marshaller
           (App_Handlers.Quit_Callback'Access));

      --  This will cause the window to be destroyed by calling
      --  Gtk.Widget.Destroy (Window) when "clicked".  Again, the destroy
      --  signal could come from here, or the window manager.
      --        App_Handlers.Handlers.Object_Connect
      --          (Button_Bar.Quit_Button,
      --           "clicked",
      --           App_Handlers.Handlers.To_Marshaller
      --           (Gtk.Widget.Destroy_Cb'Access), Main_Window);

      App_Handlers.Handlers.Connect
        (Button_Bar.Stop_Button, "clicked",
         App_Handlers.Handlers.To_Marshaller
           (App_Handlers.Stop_Callback'Access));

      App_Handlers.Handlers.Connect
        (Button_Bar.Start_Button, "clicked",
         App_Handlers.Handlers.To_Marshaller
           (App_Handlers.Start_Callback'Access));

      App_Handlers.Notebook_Cb.Connect
        (Notebook, "switch_page",
         App_Handlers.Page_Switch_Callback'Access);

   end Connect_Handlers;

   function On_Timeout return Boolean is

   begin

      if The_Main_Task_Interface.Status.Terminated
        and The_GP_Task1_Interface.Status.Terminated
          and Fieldbus1.The_Main_Task_Interface.Status.Terminated
      then
         Gtk.Main.Main_Quit;
      end if;

      My_Update_Cb.all;

      return True;

   end On_Timeout;

   procedure Stop_Timeout is
   begin
      if Timeout /= 0 then
         Remove (Timeout);
         Timeout := 0;
      end if;
   end Stop_Timeout;

   procedure Start_Timeout is
   begin
      if Timeout = 0 then
         Timeout := Timeout_Add (Update_Rate, On_Timeout'Access);
      end if;
   end Start_Timeout;

   procedure Quit
   is
      Message_Dialog : Gtk_Message_Dialog;
   begin

      Gtk_New (Dialog   => Message_Dialog,
               Parent   => Main_Window,
               Flags    => 0,
               The_Type => Gtk.Message_Dialog.Message_Warning,
               Buttons  => Gtk.Message_Dialog.Buttons_Ok_Cancel,
               Message  =>
                 "This will terminate the application." & CRLF
                 & "Are you sure ?"
               );

      if Gtk.Dialog.Gtk_Response_OK = Message_Dialog.Run then

         Terminating := True;
         The_Main_Task_Interface.Control.Quit (True);
         The_GP_Task1_Interface.Control.Quit (True);
         Fieldbus1.The_Main_Task_Interface.Control.Quit (True);

      end if;

      Message_Dialog.Destroy;

   end Quit;

   procedure Stop is
      Message_Dialog : Gtk_Message_Dialog;
   begin

      if not The_Main_Task_Interface.Status.Running then
         return;
      end if;

      Gtk_New (Dialog   => Message_Dialog,
               Parent   => Main_Window,
               Flags    => 0,
               The_Type => Gtk.Message_Dialog.Message_Warning,
               Buttons  => Gtk.Message_Dialog.Buttons_Ok_Cancel,
               Message  =>
                 "This will stop the user program." & CRLF
               & "Are you sure ?"
              );

      if Gtk.Dialog.Gtk_Response_OK = Message_Dialog.Run then

         The_Main_Task_Interface.Control.Run (False);
         The_GP_Task1_Interface.Control.Run (False);
         Fieldbus1.The_Main_Task_Interface.Control.Run (False);

      end if;

      Message_Dialog.Destroy;

   end Stop;

   procedure Start is
      Message_Dialog : Gtk_Message_Dialog;
   begin

      if The_Main_Task_Interface.Status.Running then
         return;
      end if;

      Gtk_New (Dialog   => Message_Dialog,
               Parent   => Main_Window,
               Flags    => 0,
               The_Type => Gtk.Message_Dialog.Message_Warning,
               Buttons  => Gtk.Message_Dialog.Buttons_Ok_Cancel,
               Message  =>
                 "This will start the user program." & CRLF
               & "Are you sure ?"
              );

      if Gtk.Dialog.Gtk_Response_OK = Message_Dialog.Run then

         The_Main_Task_Interface.Control.Run (True);
         The_GP_Task1_Interface.Control.Run (True);
         Fieldbus1.The_Main_Task_Interface.Control.Run (True);

      end if;

      Message_Dialog.Destroy;

   end Start;

   procedure Create_GUI
     (Update_Cb   : Update_Callback;
      Finalise_Cb : Finalise_Callback) is
   begin

      --  Problem in GtkAda 3
      --  Set_Locale functions are no longer needed and have been removed.
      --  Gtk.Main.Set_Locale;
      Gtk.Main.Init;

      Gtk_New (Main_Window);
      Main_Window.Set_Default_Size (600, 400);
      Main_Window.Set_Title ("A4A : " & Get_Application_Name);

      Gtk_New_Vbox (VBox1);

      Button_Bar := Create_Button_Bar;

      Gtk_New_Hbox (HBox2);

      Gtk_New (Notebook);
      Notebook.Set_Tab_Pos (Gtk.Enums.Pos_Left);

      HBox2.Pack_Start (Notebook, Padding => 10);

      VBox1.Pack_Start (Button_Bar.HBox1, Expand => False);

      VBox1.Pack_Start (HBox2, Padding => 10);

      Main_Window.Add (VBox1);

      Connect_Handlers;

      My_Update_Cb := Update_Cb;

      My_Finalise_Cb := Finalise_Cb;

   end Create_GUI;

   procedure Add
     (GUI_Element : access A4A.GUI_Elements.GUI_Element_Type'Class)
   is
   begin

      Notebook.Append_Page
        (GUI_Element.Get_Root_Widget, GUI_Element.Get_Label);

   end Add;

   procedure Main_Loop is
   begin

      Main_Window.Show_All;

      Start_Timeout;

      Gtk.Main.Main;

      Stop_Timeout;

      My_Finalise_Cb.all;

   end Main_Loop;

   function is_Terminating return Boolean is
   begin

      return Terminating;

   end is_Terminating;

end A4A.GUI;
