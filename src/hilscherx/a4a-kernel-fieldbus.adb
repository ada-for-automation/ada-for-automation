
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with A4A.Log;
with A4A.Logger; use A4A.Logger;

package body A4A.Kernel.Fieldbus is

   protected body cifX_Driver_Status is

      procedure Set_Driver_Information
        (Value : in cifX.Driver_Information_Type) is
      begin
         Driver_Information := Value;
      end Set_Driver_Information;

      function Get_Driver_Information return cifX.Driver_Information_Type is
      begin
         return Driver_Information;
      end Get_Driver_Information;

   end cifX_Driver_Status;

   protected body cifX_Status is

      procedure WD_Error (Value : in Boolean) is
      begin
         Status.WD_Error := Value;
      end WD_Error;

      function WD_Error return Boolean is
      begin
         return Status.WD_Error;
      end WD_Error;

      procedure Communicating (Value : in Boolean) is
      begin
         Status.Communicating := Value;
      end Communicating;

      function Communicating return Boolean is
      begin
         return Status.Communicating;
      end Communicating;

      procedure Set_Channel_Information
        (Value : in cifX.Channel_Information_Type) is
      begin
         Channel_Information := Value;
      end Set_Channel_Information;

      function Get_Channel_Information return cifX.Channel_Information_Type is
      begin
         return Channel_Information;
      end Get_Channel_Information;

      procedure Set_Common_Status_Block
        (Value : in cifX.Common_Status_Block_Type) is
      begin
         Common_Status_Block := Value;
      end Set_Common_Status_Block;

      function Get_Common_Status_Block return cifX.Common_Status_Block_Type is
      begin
         return Common_Status_Block;
      end Get_Common_Status_Block;

   end cifX_Status;

   procedure cifX_Show_Error (Who   : in String;
                              Error : in DInt) is
      Error_Image : String (1 .. 12);
   begin
      DWord_Text_IO.Put
        (To   => Error_Image,
         Item => DInt_To_DWord (Error),
         Base => 16);

      A4A.Log.Logger.Put
        (Who       => Who,
         What      => "(" & Error_Image & ")"
         & cifX.Driver_Get_Error_Description (Error),
         Log_Level => Level_Error);
   end cifX_Show_Error;

   procedure cifX_Show_Driver_Information
     (Who                : in String;
      Driver_Information : in cifX.Driver_Information_Type) is
      What : constant String := CRLF
        & "***********************************************" & CRLF
        & "          Driver Information" & CRLF
        & "Version     : "
        & Trim_String (Driver_Information.Driver_Version) & CRLF
        & "Board Count : " & Driver_Information.Board_Cnt'Img & CRLF
        & "***********************************************" & CRLF;
   begin
      A4A.Log.Logger.Put (Who       => Who,
                          What      => What,
                          Log_Level => Level_Info);
   end cifX_Show_Driver_Information;

   procedure cifX_Show_Channel_Information
     (Who                 : in String;
      Channel_Information : in cifX.Channel_Information_Type) is

      What : constant String := CRLF
        & "***********************************************" & CRLF
        & "          Channel Information" & CRLF
        & "Board_Name    : "
        & Trim_String (Channel_Information.Board_Name) & CRLF
        & "Board Alias   : "
        & Trim_String (Channel_Information.Board_Alias) & CRLF
        & "Device Number : "  & Channel_Information.Device_Number'Img & CRLF
        & "Serial Number : "  & Channel_Information.Serial_Number'Img & CRLF
        & CRLF
        & "Firmware : "
        & Trim_String (Channel_Information.FW_Name) & CRLF
        & "Version  : "
        & Channel_Information.FW_Major'Img & "."
        & Channel_Information.FW_Minor'Img & "."
        & Channel_Information.FW_Revision'Img
        & "(Build "
        & Channel_Information.FW_Build'Img & ")" & CRLF
        & "Date     : "
        & Channel_Information.FW_Year'Img & "-"
        & Channel_Information.FW_Month'Img & "-"
        & Channel_Information.FW_Day'Img & CRLF
        & "***********************************************" & CRLF;
   begin
      A4A.Log.Logger.Put (Who       => Who,
                          What      => What,
                          Log_Level => Level_Info);
   end cifX_Show_Channel_Information;

   procedure cifX_Show_DPM_IO_Info
     (Who         : in String;
      DPM_IO_Info : rcX_Get_DPM_IO_Info.RCX_GET_DPM_IO_INFO_CNF_DATA_T) is
      What : constant String := CRLF
        & "***********************************************" & CRLF
        & "          DPM IO_Info" & CRLF & CRLF
        & "Number of IO Block Info    : "
        & DPM_IO_Info.Num_IO_Block_Info'Img & CRLF
        & CRLF
        & "Input Area : " & CRLF
        & "Type of sub block    : "
        & DPM_IO_Info.IO_Block_Info (0).Type_Of_Sub_Block'Img & CRLF
        & "Flags                : "
        & DPM_IO_Info.IO_Block_Info (0).Flags'Img & CRLF
        & "Offset               : "
        & DPM_IO_Info.IO_Block_Info (0).Offset'Img & CRLF
        & "Length               : "
        & DPM_IO_Info.IO_Block_Info (0).Length'Img & CRLF
        & CRLF
        & "Output Area : " & CRLF
        & "Type of sub block    : "
        & DPM_IO_Info.IO_Block_Info (1).Type_Of_Sub_Block'Img & CRLF
        & "Flags                : "
        & DPM_IO_Info.IO_Block_Info (1).Flags'Img & CRLF
        & "Offset               : "
        & DPM_IO_Info.IO_Block_Info (1).Offset'Img & CRLF
        & "Length               : "
        & DPM_IO_Info.IO_Block_Info (1).Length'Img & CRLF
        & "***********************************************" & CRLF;
   begin
      A4A.Log.Logger.Put (Who       => Who,
                          What      => What,
                          Log_Level => Level_Info);
   end cifX_Show_DPM_IO_Info;

end A4A.Kernel.Fieldbus;
