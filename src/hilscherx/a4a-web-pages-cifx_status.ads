
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Gnoga.Application.Multi_Connect; use Gnoga;
with Gnoga.Gui.Window; use Gnoga.Gui;
with Gnoga.Gui.Element; use Gnoga.Gui.Element;

with A4A.Kernel.Fieldbus; use A4A.Kernel.Fieldbus;

with A4A.Web.Pages.Common_View;
use A4A.Web.Pages.Common_View;

generic
   Label : String;
package A4A.Web.Pages.cifX_Status is

   procedure Initialise
     (cifX_Driver_Status : in cifX_Driver_Status_Access;
      cifX_Status        : in cifX_Status_Access);

   procedure On_Connect
     (Main_Window : in out Gnoga.Gui.Window.Window_Type'Class;
      Connection  : access
        Gnoga.Application.Multi_Connect.Connection_Holder_Type);

private

   RCX_COMM_COS_READY_MASK  : constant := 16#1#;
   RCX_COMM_COS_RUN_MASK    : constant := 16#2#;
   RCX_COMM_COS_BUS_ON_MASK : constant := 16#4#;

   Network_State_Offline    : constant := 16#1#;
   Network_State_Stop       : constant := 16#2#;
   Network_State_Idle       : constant := 16#3#;
   Network_State_Operate    : constant := 16#4#;

   My_cifX_Driver_Status : cifX_Driver_Status_Access;
   My_cifX_Status        : cifX_Status_Access;

   Driver_Information  : cifX.Driver_Information_Type;
   Channel_Information : cifX.Channel_Information_Type;

   type Instance is new A4A.Web.Pages.Instance with
      record
         Common_View : Common_View_Type;

         Header_Label_View  : Gnoga.Gui.Element.Element_Type;

         --
         --  Driver Information
         --

         Driver_Version_View     : Gnoga.Gui.Element.Element_Type;
         Driver_Board_Count_View : Gnoga.Gui.Element.Element_Type;

         --
         --  Channel Information
         --

         Board_Name_View         : Gnoga.Gui.Element.Element_Type;
         Board_Alias_View        : Gnoga.Gui.Element.Element_Type;
         Device_Number_View      : Gnoga.Gui.Element.Element_Type;
         Serial_Number_View      : Gnoga.Gui.Element.Element_Type;

         Firmware_Name_View      : Gnoga.Gui.Element.Element_Type;
         Firmware_Version_View   : Gnoga.Gui.Element.Element_Type;
         Firmware_Date_View      : Gnoga.Gui.Element.Element_Type;

         --
         --  General Status
         --

         Device_Ready_View       : Gnoga.Gui.Element.Element_Type;
         Device_Running_View     : Gnoga.Gui.Element.Element_Type;
         Device_Bus_On_View      : Gnoga.Gui.Element.Element_Type;
         Device_Com_OK_View      : Gnoga.Gui.Element.Element_Type;
         Device_Error_View       : Gnoga.Gui.Element.Element_Type;

         Network_Offline_View    : Gnoga.Gui.Element.Element_Type;
         Network_Stop_View       : Gnoga.Gui.Element.Element_Type;
         Network_Idle_View       : Gnoga.Gui.Element.Element_Type;
         Network_Operate_View    : Gnoga.Gui.Element.Element_Type;

         cifX_Watchdog_View      : Gnoga.Gui.Element.Element_Type;
         cifX_Watchdog_CTO_View  : Gnoga.Gui.Element.Element_Type;

         Com_Error_Desc_View     : Gnoga.Gui.Element.Element_Type;
         Com_Error_Count_View    : Gnoga.Gui.Element.Element_Type;

         Master_Slave_State_View   : Gnoga.Gui.Element.Element_Type;
         Master_Slave_Err_Log_View : Gnoga.Gui.Element.Element_Type;
         Master_Config_Slaves_View : Gnoga.Gui.Element.Element_Type;
         Master_Active_Slaves_View : Gnoga.Gui.Element.Element_Type;
         Master_Diag_Slaves_View   : Gnoga.Gui.Element.Element_Type;

         WD_Error : Boolean  := True;
         --  Watchdog error

         Communicating : Boolean  := False;
         --  Communicating

         Common_Status_Block : aliased cifX.Common_Status_Block_Type;
         Common_Status_Block_Previous : cifX.Common_Status_Block_Type;

      end record;
   type Instance_Access is access all Instance;
   type Pointer_to_Instance_Class is access all Instance'Class;

   --
   --  Views creation
   --

   procedure Driver_Info_View_Create
     (Web_Page : in out Instance);

   procedure Channel_Info_View_Create
     (Web_Page : in out Instance);

   procedure Device_View_Create
     (Web_Page : in out Instance);

   procedure Network_View_Create
     (Web_Page : in out Instance);

   procedure Watchdog_View_Create
     (Web_Page : in out Instance);

   procedure Com_Error_View_Create
     (Web_Page : in out Instance);

   procedure Master_View_Create
     (Web_Page : in out Instance);

   --
   --  Views update
   --

   procedure Device_View_Update
     (Web_Page : in out Instance);

   procedure Network_View_Update
     (Web_Page : in out Instance);

   procedure Watchdog_View_Update
     (Web_Page : in out Instance);

   procedure Com_Error_View_Update
     (Web_Page : in out Instance);

   procedure Master_View_Update
     (Web_Page : in out Instance);

   procedure View_Update
     (Web_Page : in out Instance);

   overriding
   procedure Update_Page (Web_Page : access Instance);

end A4A.Web.Pages.cifX_Status;
