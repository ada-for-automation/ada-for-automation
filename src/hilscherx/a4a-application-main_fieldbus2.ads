
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with A4A.Generic_Dual_Port_Memory; use A4A;

package A4A.Application.Main_Fieldbus2 is

   --------------------------------------------------------------------
   --  Main / Fieldbus1 Tasks interface
   --------------------------------------------------------------------

   type Inputs is
      record
         A : Boolean := False;
         B : Word    :=     0;
         C : Byte    :=     0;
      end record;

   type Outputs is
      record
         X : Boolean := False;
         Y : Word    :=     0;
         Z : Byte    :=     0;
      end record;

   --------------------------------------------------------------------
   --  Memory areas available to Main task
   --------------------------------------------------------------------

   Main_Inputs  : Inputs;
   --  These are the Inputs from task Fieldbus to Main

   Main_Outputs : Outputs;
   --  These are the Outputs from task Main to Fieldbus

   procedure Get_Main_Inputs;
   --  This procedure updates Main_Inputs from DPM
   --  Main task should call it before calling Main_Cyclic

   procedure Set_Main_Outputs;
   --  This procedure updates DPM from Main_Outputs
   --  Main task should call it after calling Main_Cyclic

   --------------------------------------------------------------------
   --  Memory areas available to Fieldbus1 task
   --------------------------------------------------------------------

   Fieldbus_Inputs  : Outputs;
   --  These are the Inputs from task Main to Fieldbus

   Fieldbus_Outputs : Inputs;
   --  These are the Outputs from task Fieldbus to Main

   procedure Get_Fieldbus_Inputs;
   --  This procedure updates Fieldbus1_Inputs from DPM
   --  Fieldbus1 task should call it before calling Fieldbus_Cyclic

   procedure Set_Fieldbus_Outputs;
   --  This procedure updates DPM from Periodic1_Outputs
   --  Fieldbus1 task should call it after calling Fieldbus_Cyclic

private

   package DPM is new
     A4A.Generic_Dual_Port_Memory
       (Inputs_Type  => Inputs,
        Outputs_Type => Outputs);

   The_DPM : DPM.Dual_Port_Memory;

end A4A.Application.Main_Fieldbus2;
