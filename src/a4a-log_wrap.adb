
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

--  <summary>
--  This package is part of the binding to Hilscher cifX Device Driver,
--  a library allowing access to the cifX boards with numerous protocols.
--  </summary>
--  <description>
--  It provides, from "cifXUser.h" :
--  - cifX data types,
--  - cifX C functions binding.
--  </description>
--  <group>Protocols</group>
--  <c_version>3.0.1</c_version>

with A4A.Log;
with A4A.Logger; use A4A.Logger;

package body A4A.Log_Wrap is

   procedure Work_Around is
   begin
      return;
   end Work_Around;

   procedure A4A_Log
     (Who_Buffer         : Who_Buffer_type;
      What_Buffer        : What_Buffer_type;
      Log_Level          : DWord) is

      Level : A4A.Logger.Log_Level_Type;

   begin

      case Log_Level is
         when A4A_LOG_LEVEL_ERROR =>
            Level := A4A.Logger.Level_Error;
         when A4A_LOG_LEVEL_WARNING =>
            Level := A4A.Logger.Level_Warning;
         when A4A_LOG_LEVEL_INFO =>
            Level := A4A.Logger.Level_Info;
         when A4A_LOG_LEVEL_VERBOSE =>
            Level := A4A.Logger.Level_Verbose;
         when others =>
            Level := A4A.Logger.Level_Error;
      end case;

      A4A.Log.Logger.Put
        (Who  => Interfaces.C.To_Ada (Who_Buffer),
         What => Interfaces.C.To_Ada (What_Buffer),
         Log_Level => Level);

   end A4A_Log;

end A4A.Log_Wrap;
