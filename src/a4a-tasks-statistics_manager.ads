
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

package A4A.Tasks.Statistics_Manager is

   type Instance is tagged private;

   procedure Initialise
     (Stats         : in out Instance;
      Task_Duration : Duration);

   procedure Update
     (Stats         : in out Instance;
      Task_Duration : Duration);

   function Get_Statistics
     (Stats         : in out Instance) return Task_Statistics_Type;

private

   type Duration_Samples is array (Byte) of Duration;
   type Instance is tagged
      record
         Task_Statistics           : Task_Statistics_Type;
         Task_Duration_Samples     : Duration_Samples := (others => 0.0);
         Oldest_Sample             : Byte := 0;
         Task_Duration_Moving_Mean : Duration := 0.0;
      end record;

end A4A.Tasks.Statistics_Manager;
