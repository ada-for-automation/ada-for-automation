
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

package body A4A.Dual_Port_Memory is

   protected body Bool_Dual_Port_Memory_Area is

      procedure Set_Data
        (Data_In : in Bool_Array;
         Offset  : in Natural) is
         Offset2 : Natural := Offset;
      begin
         --  Data (Offset .. Offset + Data_In'Length - 1) := Data_In;
         Data (Offset2 .. Offset2 + Data_In'Length - 1) := Data_In;
      end Set_Data;

      function Get_Data
        (Offset  : in Natural;
         Number  : in Natural) return Bool_Array is
      begin
         return Data (Offset .. Offset + Number - 1);
      end Get_Data;

   end Bool_Dual_Port_Memory_Area;

   protected body Word_Dual_Port_Memory_Area is

      procedure Set_Data
        (Data_In : in Word_Array;
         Offset  : in Natural) is
         Offset2 : Natural := Offset;
      begin
         --  Data (Offset .. Offset + Data_In'Length - 1) := Data_In;
         Data (Offset2 .. Offset2 + Data_In'Length - 1) := Data_In;
      end Set_Data;

      function Get_Data
        (Offset  : in Natural;
         Number  : in Natural) return Word_Array is
      begin
         return Data (Offset .. Offset + Number - 1);
      end Get_Data;

   end Word_Dual_Port_Memory_Area;

end A4A.Dual_Port_Memory;
