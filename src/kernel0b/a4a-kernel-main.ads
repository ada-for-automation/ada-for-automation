
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with System;

with A4A.Tasks.Interfaces; use A4A.Tasks.Interfaces;
with A4A.Tasks.Watchdog; use A4A.Tasks.Watchdog;

with A4A.Application;

with A4A.Application.MBRTU_Slave_Config;
use  A4A.Application.MBRTU_Slave_Config;
with A4A.Library.Timers; use A4A.Library.Timers;

with A4A.Kernel.MBRTU_Slave; use A4A.Kernel.MBRTU_Slave;

package A4A.Kernel.Main is

   Watchdog_Time_Out_MS : constant Natural := 2000;

   procedure Create_Main_Task;

   type Main_Task_Interface is
      record
         Control : Task_Control;
         Status  : Task_Status;
         Statistics  : Task_Statistics;
         Sched_Statistics  : Task_Sched_Statistics;

         Control_Watchdog  : Control_Watchdog_Type;
         Status_Watchdog   : Status_Watchdog_Type;

         MSlave_Status    : MBRTU_Slave_Status;
      end record;

   type Main_Task_Itf_Access is access all Main_Task_Interface;

   task type Main_Task
     (Task_Priority           : System.Priority;
      Task_Itf                : Main_Task_Itf_Access
     ) is
      pragma Priority (Task_Priority);
   end Main_Task;
   type Main_Task_Access is access Main_Task;

   --------------------------------------------------------------------
   --  Main Task Management
   --------------------------------------------------------------------

   The_Main_Task_Interface     : aliased Main_Task_Interface;

   --------------------------------------------------------------------
   --  Clock Management
   --------------------------------------------------------------------

   Clock_Handler_Interface : aliased A4A.Library.Timers.Task_Interface;

   --------------------------------------------------------------------
   --  Modbus RTU Slave Management
   --------------------------------------------------------------------

   MBRTU_Slave_Task_Interface : aliased Slave.Task_Interface;

private

   The_Main_Task       : Main_Task_Access;
   Main_Task_Created   : Boolean := False;

   MBRTU_Slave_Task   : Slave.Periodic_Task_Access;

end A4A.Kernel.Main;
