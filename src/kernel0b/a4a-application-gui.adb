
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with A4A.GUI;

package body A4A.Application.GUI is

   procedure Create_GUI is

   begin

      --  Create Main frame
      A4A.GUI.Create_GUI (Update'Access, Finalise'Access);

      --  and Add items to it

      Ident_Page := A4A.GUI_Elements.Ident_Page.Create;
      A4A.GUI.Add (Ident_Page'Access);

      General_Status_Page :=
        A4A.GUI_Elements.General_Status_Page.Create (Start_Time);
      A4A.GUI.Add (General_Status_Page'Access);

      MSlave_Status_Page := A4A.GUI_Elements.MSlave_Status_Page.Create;
      A4A.GUI.Add (MSlave_Status_Page'Access);

      A4A.GUI.Main_Loop;

   end Create_GUI;

   procedure Update is

   begin

      General_Status_Page.Update (A4A.GUI.is_Terminating);

      if not A4A.GUI.is_Terminating then

         MSlave_Status_Page.Update;

      end if;

   end Update;

   procedure Finalise is

   begin

      null;

   end Finalise;

end A4A.Application.GUI;
