
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with A4A.Log;
with A4A.Logger; use A4A.Logger;
with Ada.Real_Time; use Ada.Real_Time;

with A4A.Tasks.Statistics_Manager; use A4A.Tasks;

package body A4A.Generic_Periodic_Task is

   task body Periodic_Task is
      Next_Time : Ada.Real_Time.Time := Clock;
      Period : constant Time_Span    := Milliseconds (Period_In_Milliseconds);
      My_Ident : constant String := "A4A.Generic_Periodic_Task";

      Task_Status : Task_Status_Type;
      My_Statistics_Manager : Statistics_Manager.Instance;

      Start_Real_Time : Ada.Real_Time.Time;
      Task_Duration   : Duration;

      First_Cycle     : Boolean := True;

      Watchdog_TON_Q       : Boolean := False;
      Watchdog_Error       : Boolean := False;

   begin
      A4A.Log.Logger.Put (Who       => My_Ident,
                          What      => "Started !",
                          Log_Level => Level_Info);

      Task_Itf.Status.Ready (True);

      loop

         Start_Real_Time := Clock;

         Next_Time := Next_Time + Period;

         Task_Status.Running := Task_Itf.Control.Run;
         Task_Status.Terminated := Task_Itf.Control.Quit;

         Task_Itf.Status_Watchdog.Watchdog
           (Watching         => Task_Itf.Control.Start_Watching,
            Control_Watchdog => Task_Itf.Control_Watchdog.Value,
            Error            => Watchdog_TON_Q);

         if Watchdog_TON_Q and not Watchdog_Error then
            A4A.Log.Logger.Put (Who  => My_Ident,
                                What => "Watchdog Time Out elapsed!");
            Watchdog_Error := True;
         end if;

         if Task_Status.Running then
            Run;
         end if;

         Task_Duration := To_Duration (Clock - Start_Real_Time);
         if First_Cycle then
            My_Statistics_Manager.Initialise (Task_Duration);
         else
            My_Statistics_Manager.Update (Task_Duration);
         end if;

         Task_Itf.Status.Running (Task_Status.Running);
         Task_Itf.Statistics.Set_Statistics
           (My_Statistics_Manager.Get_Statistics);

         First_Cycle := False;

         delay until Next_Time;

         exit when Task_Status.Terminated;

      end loop;

      A4A.Log.Logger.Put (Who       => My_Ident,
                          What      => "Finished !",
                          Log_Level => Level_Info);

      Task_Itf.Status.Ready (False);
      Task_Itf.Status.Terminated (Task_Status.Terminated);
   end Periodic_Task;

end A4A.Generic_Periodic_Task;
