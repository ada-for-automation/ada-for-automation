
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Glib.Main;       use Glib.Main;

with Gtk.Widget;      use Gtk.Widget;
with Gtk.Window;      use Gtk.Window;
with Gtk.Box;         use Gtk.Box;
with Gtk.Notebook;    use Gtk.Notebook;

with A4A.GUI_Elements;
with A4A.GUI_Elements.Button_Bar; use A4A.GUI_Elements.Button_Bar;

package A4A.GUI is

   --------------------------------------------------------------------
   --  Graphical User Interface
   --------------------------------------------------------------------

   type Update_Callback is access procedure;

   type Finalise_Callback is access procedure;

   procedure Create_GUI
     (Update_Cb   : Update_Callback;
      Finalise_Cb : Finalise_Callback);

   procedure Add
     (GUI_Element : access A4A.GUI_Elements.GUI_Element_Type'Class);

   procedure Main_Loop;

   function is_Terminating return Boolean;

private
   procedure Quit;
   procedure Stop;
   procedure Start;

   function On_Timeout return Boolean;
   procedure Stop_Timeout;
   procedure Start_Timeout;

   Update_Rate    : constant := 1000;

   My_Update_Cb   : Update_Callback;

   My_Finalise_Cb : Finalise_Callback;

   Timeout : G_Source_Id;

   Terminating   : Boolean := False;

   procedure Connect_Handlers;

   Main_Window   : Gtk_Window;
   VBox1         : Gtk_Vbox;

   Button_Bar    : Button_Bar_Type;

   HBox2         : Gtk_Hbox;
   Notebook      : Gtk_Notebook;

end A4A.GUI;
