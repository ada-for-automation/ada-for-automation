
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Gtk.Widget;         use Gtk.Widget;

package A4A.GUI_Elements is

   --------------------------------------------------------------------
   --  Graphical User Interface Elements
   --------------------------------------------------------------------
   type GUI_Element_Type is abstract tagged private;
   type GUI_Element is access GUI_Element_Type'Class;

   function Create
     return GUI_Element_Type
     is abstract;

   function Get_Root_Widget
     (GUI_Element : in GUI_Element_Type)
      return Gtk.Widget.Gtk_Widget
      is abstract;

   function Get_Label
     (GUI_Element : in GUI_Element_Type)
      return Gtk.Widget.Gtk_Widget
      is abstract;

private

   type GUI_Element_Type is abstract tagged null record;

end A4A.GUI_Elements;
