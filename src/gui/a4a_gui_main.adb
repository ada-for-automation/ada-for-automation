
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Command_Line;
with Ada.Exceptions; use Ada.Exceptions;
with Ada.Strings.Unbounded;

with A4A; use A4A;
with A4A.Command_Line;

with A4A.Kernel; use A4A.Kernel;
with A4A.Kernel.Main; use A4A.Kernel.Main;

with A4A.Log;
with A4A.Logger; use A4A.Logger;

with A4A.Application.Identification; use A4A.Application.Identification;

with A4A.Application.GUI;

procedure A4A_GUI_Main is
   My_Ident : constant String := "A4A_GUI_Main";

   CL_Arguments : Ada.Strings.Unbounded.Unbounded_String;

   procedure Show_Application_Identification;

   procedure Show_Application_Identification is
      What : constant String := CRLF
        & "***********************************************" & CRLF
        & "          Application Identification" & CRLF
        & "Name :" & CRLF
        & "    " & Get_Application_Name & CRLF
        & "Version :" & CRLF
        & "    " & Get_Application_Version & CRLF
        & "Description :" & CRLF
        & "    " & Get_Application_Description & CRLF
        & "***********************************************" & CRLF;
   begin
      A4A.Log.Logger.Put (Who       => My_Ident,
                          What      => What,
                          Log_Level => A4A.Logger.Level_Info);
   end Show_Application_Identification;

begin

   A4A.Command_Line.Parse_Arguments;

   --  Let's print our Command Line (not that useful at the moment)
   if A4A.Log.Logger.Get_Level = A4A.Logger.Level_Info then
      for Index in 1 .. Ada.Command_Line.Argument_Count loop
         Ada.Strings.Unbounded.Append
           (CL_Arguments, Ada.Command_Line.Argument (Index) & " ");
      end loop;
   end if;
   A4A.Log.Logger.Put (Who       => My_Ident,
                       What      => "started as : "
                       & Ada.Command_Line.Command_Name & " "
                       & Ada.Strings.Unbounded.To_String (CL_Arguments),
                       Log_Level => A4A.Logger.Level_Info);

   Show_Application_Identification;

   --------------------------------------------------------------------
   --  Create necessary tasks
   --------------------------------------------------------------------

   --  Well, the main automation task, either cyclic or periodic, which runs
   --  the A4A.Application.Main_Cyclic procedure
   A4A.Kernel.Main.Create_Main_Task;

   --  A periodic task, running the A4A.Application.Periodic_Run_1 procedure
   A4A.Kernel.Create_Generic_Periodic_Task_1;

   loop
      exit when
        The_Main_Task_Interface.Status.Ready
        and The_GP_Task1_Interface.Status.Ready;
      delay 1.0;
   end loop;

   --------------------------------------------------------------------
   --  Monitoring the tasks
   --------------------------------------------------------------------

   A4A.Application.GUI.Create_GUI;

   A4A.Log.Logger.Put (Who       => My_Ident,
                       What      => "Finished !",
                       Log_Level => Level_Info);
   A4A.Log.Quit;

   loop
      delay 1.0;
      exit when A4A.Log.is_Terminated;
   end loop;

exception

   when Error : others =>
      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => Exception_Information (Error));

      The_Main_Task_Interface.Control.Quit (True);
      The_GP_Task1_Interface.Control.Quit (True);

      delay 5.0;

      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => "Aborted !");
      A4A.Log.Quit;

   loop
      delay 1.0;
      exit when A4A.Log.is_Terminated;
   end loop;

end A4A_GUI_Main;
