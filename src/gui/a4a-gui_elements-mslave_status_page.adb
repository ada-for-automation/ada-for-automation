
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Gtk.Enums;

with A4A.Protocols; use A4A.Protocols.Device_Strings;
with A4A.Kernel.Main; use A4A.Kernel.Main;

package body A4A.GUI_Elements.MSlave_Status_Page is

   --------------------------------------------------------------------
   --  Graphical User Interface Elements
   --  Modbus RTU Slave Status Page
   --------------------------------------------------------------------

   function Create_MSlave_Status_UI return MSlave_Status_UI_Type is
      MSlave_Status_UI    : MSlave_Status_UI_Type;
   begin
      Gtk_New (MSlave_Status_UI.Frame, "Modbus RTU Slave");
      Gtk_Label (MSlave_Status_UI.Frame.Get_Label_Widget).Set_Markup
        ("<b>Modbus RTU Slave : " & CRLF
         & "Device : " & To_String (Config1.Device) & CRLF
         & "Address : " & Config1.Slave'Img
         & "</b>");
      MSlave_Status_UI.Frame.Set_Shadow_Type (Gtk.Enums.Shadow_None);

      Gtk_New_Vbox (MSlave_Status_UI.VBox);

      Gtk_New_Hbox (MSlave_Status_UI.HBox);

      --  Watchdog

      Gtk_New_Vbox (MSlave_Status_UI.Watchdog_VBox);

      Gtk_New (MSlave_Status_UI.Watchdog_Label, "Watchdog");

      Gtk_New (MSlave_Status_UI.Watchdog_Image, "./green-led.png");

      MSlave_Status_UI.Watchdog_VBox.Pack_Start
        (MSlave_Status_UI.Watchdog_Label, Expand => False, Padding => 10);

      MSlave_Status_UI.Watchdog_VBox.Pack_Start
        (MSlave_Status_UI.Watchdog_Image, Expand => False);

      --  General Stats

      Gtk_New_Vbox (MSlave_Status_UI.Stats_VBox);

      Gtk_New (MSlave_Status_UI.General_Stats_Frame, "General Stats");
      Gtk_Label
        (MSlave_Status_UI.General_Stats_Frame.Get_Label_Widget).Set_Markup
        ("<u>General Stats</u>" & CRLF);
      MSlave_Status_UI.General_Stats_Frame.Set_Shadow_Type
        (Gtk.Enums.Shadow_None);

      Gtk_New_Hbox (MSlave_Status_UI.General_Stats_HBox);

      Gtk_New (MSlave_Status_UI.General_Stats_Items_Label,
               "Connected to Master :");
      MSlave_Status_UI.General_Stats_Items_Label.Set_Justify
        (Gtk.Enums.Justify_Right);

      Gtk_New (MSlave_Status_UI.General_Stats_Values_Label,
               "Values");

      MSlave_Status_UI.General_Stats_HBox.Pack_Start
        (MSlave_Status_UI.General_Stats_Items_Label,
         Expand => False, Padding => 10);

      MSlave_Status_UI.General_Stats_HBox.Pack_Start
        (MSlave_Status_UI.General_Stats_Values_Label, Expand => False);

      MSlave_Status_UI.General_Stats_Frame.Add
        (MSlave_Status_UI.General_Stats_HBox);

      MSlave_Status_UI.Stats_VBox.Pack_Start
        (MSlave_Status_UI.General_Stats_Frame,
         Expand => False, Padding => 10);

      --  Requests stats

      Gtk_New (MSlave_Status_UI.Requests_Stats_Frame, "Requests served");
      Gtk_Label
        (MSlave_Status_UI.Requests_Stats_Frame.Get_Label_Widget).Set_Markup
        ("<u>Requests served</u>" & CRLF);
      MSlave_Status_UI.Requests_Stats_Frame.Set_Shadow_Type
        (Gtk.Enums.Shadow_None);

      Gtk_New_Hbox (MSlave_Status_UI.Requests_Stats_HBox);

      Gtk_New (MSlave_Status_UI.Requests_Stats_Items_Label,
               "Read Coils (FC 01) :" & CRLF
               & "Read Input Bits (FC 02) :" & CRLF
               & "Read Holding Registers (FC 03) :" & CRLF
               & "Read Input Registers (FC 04) :" & CRLF
               & "Write Single Coil (FC 05) :" & CRLF
               & "Write Single Register (FC 06) :" & CRLF
               & "Write Multiple Coils (FC 15) :" & CRLF
               & "Write Multiple Registers (FC 16) :" & CRLF
               & "Write Read Registers (FC 23) :" & CRLF
               & "Unmanaged Requests :"
              );
      MSlave_Status_UI.Requests_Stats_Items_Label.Set_Justify
        (Gtk.Enums.Justify_Right);

      Gtk_New (MSlave_Status_UI.Requests_Stats_Values_Label,
               "Values");

      MSlave_Status_UI.Requests_Stats_HBox.Pack_Start
        (MSlave_Status_UI.Requests_Stats_Items_Label,
         Expand => False, Padding => 10);

      MSlave_Status_UI.Requests_Stats_HBox.Pack_Start
        (MSlave_Status_UI.Requests_Stats_Values_Label, Expand => False);

      MSlave_Status_UI.Requests_Stats_Frame.Add
        (MSlave_Status_UI.Requests_Stats_HBox);

      MSlave_Status_UI.Stats_VBox.Pack_Start
        (MSlave_Status_UI.Requests_Stats_Frame,
         Expand => False, Padding => 10);

      --  Last Error

      Gtk_New (MSlave_Status_UI.Last_Error_Frame, "Last error");
      Gtk_Label
        (MSlave_Status_UI.Last_Error_Frame.Get_Label_Widget).Set_Markup
        ("<u>Last error</u>" & CRLF);
      MSlave_Status_UI.Last_Error_Frame.Set_Shadow_Type
        (Gtk.Enums.Shadow_None);

      Gtk_New_Hbox (MSlave_Status_UI.Last_Error_HBox);

      Gtk_New (MSlave_Status_UI.Last_Error_Label,
               "None");

      MSlave_Status_UI.Last_Error_HBox.Pack_Start
        (MSlave_Status_UI.Last_Error_Label,
         Expand => False, Padding => 10);

      MSlave_Status_UI.Last_Error_Frame.Add
        (MSlave_Status_UI.Last_Error_HBox);

      MSlave_Status_UI.Stats_VBox.Pack_Start
        (MSlave_Status_UI.Last_Error_Frame, Expand => False, Padding => 10);

      MSlave_Status_UI.HBox.Pack_Start
        (MSlave_Status_UI.Watchdog_VBox, Expand => False, Padding => 10);

      MSlave_Status_UI.HBox.Pack_Start
        (MSlave_Status_UI.Stats_VBox, Expand => False, Padding => 10);

      MSlave_Status_UI.VBox.Pack_Start
        (MSlave_Status_UI.HBox, Expand => False, Padding => 10);

      MSlave_Status_UI.Frame.Add (MSlave_Status_UI.VBox);

      return MSlave_Status_UI;

   end Create_MSlave_Status_UI;

   procedure MSlave_Status_UI_Update
     (MSlave_Status_UI : in MSlave_Status_UI_Type) is
   begin

      if The_Main_Task_Interface.MSlave_Status.WD_Error
        and not MBRTU_Slave_Watchdog_Error
      then
         MSlave_Status_UI.Watchdog_Image.Set ("./red-led.png");
         MBRTU_Slave_Watchdog_Error := True;
      end if;

      MSlave_Task_Status := MBRTU_Slave_Task_Interface.Status.Get_Status;

      MSlave_Status_UI.General_Stats_Values_Label.Set_Markup
        (Boolean'Image
           (MSlave_Task_Status.Commands_Status.Connected_To_Master));

      MSlave_Status_UI.Requests_Stats_Values_Label.Set_Markup
        (Long_Integer'Image (Long_Integer
         (MSlave_Task_Status.Commands_Status.Read_Coils_Count))
         & CRLF
         & Long_Integer'Image (Long_Integer
           (MSlave_Task_Status.Commands_Status.Read_Input_Bits_Count))
         & CRLF
         & Long_Integer'Image (Long_Integer
           (MSlave_Task_Status.Commands_Status.Read_Holding_Registers_Count))
         & CRLF
         & Long_Integer'Image (Long_Integer
           (MSlave_Task_Status.Commands_Status.Read_Input_Registers_Count))
         & CRLF
         & Long_Integer'Image (Long_Integer
           (MSlave_Task_Status.Commands_Status.Write_Single_Coil_Count))
         & CRLF
         & Long_Integer'Image (Long_Integer
           (MSlave_Task_Status.Commands_Status.Write_Single_Register_Count))
         & CRLF
         & Long_Integer'Image (Long_Integer
           (MSlave_Task_Status.Commands_Status.Write_Multiple_Coils_Count))
         & CRLF
         & Long_Integer'Image (Long_Integer
           (MSlave_Task_Status.Commands_Status.
                  Write_Multiple_Registers_Count))
         & CRLF
         & Long_Integer'Image (Long_Integer
           (MSlave_Task_Status.Commands_Status.Write_Read_Registers_Count))
         & CRLF
         & Long_Integer'Image (Long_Integer
           (MSlave_Task_Status.Commands_Status.Unmanaged_Requests_Count))
        );

   end MSlave_Status_UI_Update;

   overriding function Create
     return Instance
   is

      GUI_Element : Instance;

   begin
      Gtk_New (GUI_Element.Scrolled_Window);
      GUI_Element.Scrolled_Window.Set_Border_Width (Border_Width => 10);
      GUI_Element.Scrolled_Window.Set_Policy
        (Gtk.Enums.Policy_Automatic, Gtk.Enums.Policy_Automatic);

      GUI_Element.Scrolled_Window.Set_Shadow_Type (Gtk.Enums.Shadow_None);

      Gtk_New (GUI_Element.Page_Label, "Slave Status");

      Gtk_New_Vbox (GUI_Element.Page_VBox);

      GUI_Element.MSlave_Status_UI := Create_MSlave_Status_UI;

      GUI_Element.Page_VBox.Pack_Start
        (GUI_Element.MSlave_Status_UI.Frame,
         Expand => False, Padding => 10);

      GUI_Element.Scrolled_Window.Add (GUI_Element.Page_VBox);

      return GUI_Element;

   end Create;

   overriding function Get_Root_Widget
     (GUI_Element : in Instance)
      return Gtk.Widget.Gtk_Widget
   is

   begin

      return Gtk_Widget (GUI_Element.Scrolled_Window);

   end Get_Root_Widget;

   overriding function Get_Label
     (GUI_Element : in Instance)
      return Gtk.Widget.Gtk_Widget
   is

   begin

      return Gtk_Widget (GUI_Element.Page_Label);

   end Get_Label;

   procedure Update
     (GUI_Element : in out Instance)
   is

   begin

      MSlave_Status_UI_Update (GUI_Element.MSlave_Status_UI);

   end Update;

end A4A.GUI_Elements.MSlave_Status_Page;
