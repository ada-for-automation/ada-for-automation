
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Gtk.Enums;
with A4A.Application.Identification; use A4A.Application.Identification;

package body A4A.GUI_Elements.Ident_Page is

   --------------------------------------------------------------------
   --  Graphical User Interface Elements
   --  Identity Page
   --------------------------------------------------------------------

   function Create_Application_Info_UI
     return Application_Info_UI_Type
   is

      Application_Info_UI    : Application_Info_UI_Type;

   begin

      Gtk_New (Application_Info_UI.Frame, "Application Identification");
      Gtk_Label (Application_Info_UI.Frame.Get_Label_Widget).Set_Markup
        ("<b>Application Identification</b>");
      Application_Info_UI.Frame.Set_Shadow_Type (Gtk.Enums.Shadow_None);

      Gtk_New_Vbox (Application_Info_UI.VBox);

      Gtk_New_Hbox (Application_Info_UI.HBox);

      Gtk_New (Application_Info_UI.Label, "Identification");

      Application_Info_UI.Label.Set_Line_Wrap (True);
      Application_Info_UI.Label.Set_Markup
        ("<big><u>Name</u> :</big>" & CRLF
         & Get_Application_Name & CRLF
         & CRLF
         & "<big><u>Version</u> :</big>" & CRLF
         & Get_Application_Version & CRLF
         & CRLF
         & "<big><u>Description</u> :</big>" & CRLF
         & Get_Application_Description);

      Application_Info_UI.HBox.Pack_Start
        (Application_Info_UI.Label, Expand => False, Padding => 10);

      Application_Info_UI.VBox.Pack_Start
        (Application_Info_UI.HBox, Expand => False, Padding => 10);

      Application_Info_UI.Frame.Add (Application_Info_UI.VBox);

      return Application_Info_UI;

   end Create_Application_Info_UI;

   overriding function Create
     return Instance
   is

      GUI_Element : Instance;

   begin

      Gtk_New (GUI_Element.Scrolled_Window);
      GUI_Element.Scrolled_Window.Set_Border_Width (Border_Width => 10);
      GUI_Element.Scrolled_Window.Set_Policy
        (Gtk.Enums.Policy_Automatic, Gtk.Enums.Policy_Automatic);

      GUI_Element.Scrolled_Window.Set_Shadow_Type (Gtk.Enums.Shadow_None);

      Gtk_New (GUI_Element.Page_Label, "Identity");

      GUI_Element.Application_Info_UI := Create_Application_Info_UI;

      GUI_Element.Scrolled_Window.Add (GUI_Element.Application_Info_UI.Frame);

      return GUI_Element;

   end Create;

   overriding function Get_Root_Widget
     (GUI_Element : in Instance)
      return Gtk.Widget.Gtk_Widget
   is

   begin

      return Gtk_Widget (GUI_Element.Scrolled_Window);

   end Get_Root_Widget;

   overriding function Get_Label
     (GUI_Element : in Instance)
      return Gtk.Widget.Gtk_Widget
   is

   begin

      return Gtk_Widget (GUI_Element.Page_Label);

   end Get_Label;

end A4A.GUI_Elements.Ident_Page;
