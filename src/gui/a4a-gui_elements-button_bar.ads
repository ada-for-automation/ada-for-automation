
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Gtk.Box;         use Gtk.Box;
with Gtk.Button;      use Gtk.Button;
with Gtk.Label;       use Gtk.Label;
with Gtk.Image;       use Gtk.Image;

package A4A.GUI_Elements.Button_Bar is

   --------------------------------------------------------------------
   --  Graphical User Interface Elements
   --  Button Bar
   --------------------------------------------------------------------

   type Button_Bar_Type is
      record
         HBox1              : Gtk_Hbox;
         Quit_Button        : Gtk_Button;
         Quit_Button_Image  : Gtk_Image;
         Stop_Button        : Gtk_Button;
         Stop_Button_Image  : Gtk_Image;
         Start_Button       : Gtk_Button;
         Start_Button_Image : Gtk_Image;
         App_Status_Label   : Gtk_Label;
         App_Status_Image   : Gtk_Image;
      end record;

   function Create_Button_Bar return Button_Bar_Type;

end A4A.GUI_Elements.Button_Bar;
