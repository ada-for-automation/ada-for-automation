
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Glib; use Glib;
with Gtk.Enums;

with Ada.Unchecked_Deallocation;

with A4A.Log;
with A4A.Logger;
with A4A.Protocols; use A4A.Protocols.IP_Address_Strings;
with A4A.Kernel.Main; use A4A.Kernel.Main;

package body A4A.GUI_Elements.MClients_Status_Page is

   --------------------------------------------------------------------
   --  Graphical User Interface Elements
   --  Modbus TCP Clients Status Page
   --------------------------------------------------------------------

   function Create_MClient_Status_UI
     (Index : Positive) return MClient_Status_UI_Type is
      MClient_Status_UI : MClient_Status_UI_Type;

      Label : Gtk_Label;
      Image : Gtk_Image;

      Command_Index : Positive;

      Client_UI_Data : constant MClient_Status_UI_Data_Access :=
        Clients_UI_Data (Index)'Access;

      Config : constant Client_Configuration_Access :=
        MBTCP_Clients_Configuration (Index);
   begin
      --  Remember which client we are connected to
      MClient_Status_UI.Index := Index;

      Client_UI_Data.Task_Status :=
        new A4A.MBTCP_Client.Task_Status_Type (Config.Command_Number);

      Client_UI_Data.Commands_Status :=
        new A4A.MBTCP_Client.Commands_Status_Type (Config.Commands'Range);
      Client_UI_Data.Commands_Status.all := (others => Unknown);

      MClient_Status_UI.Commands_Status_Images :=
        new Commands_Status_Images_Type (Config.Commands'Range);

      Client_UI_Data.Raws_Number := 1 + ((Config.Command_Number - 1) / 10);

      if Config.Command_Number >= 10 then
         Client_UI_Data.Columns_Number := 10;
      else
         Client_UI_Data.Columns_Number := Config.Command_Number mod 10;
      end if;

      Gtk_New (MClient_Status_UI.Frame, "Modbus TCP Client");
      Gtk_Label (MClient_Status_UI.Frame.Get_Label_Widget).Set_Markup
        ("<b>Modbus Client " & Index'Img & " : "
         & To_String (Config.Server_IP_Address)
         & ":"
         & Config.Server_TCP_Port'Img
         & "</b>");
      MClient_Status_UI.Frame.Set_Shadow_Type (Gtk.Enums.Shadow_None);

      Gtk_New_Vbox (MClient_Status_UI.VBox);

      Gtk_New_Hbox (MClient_Status_UI.HBox);

      --  Watchdog

      Gtk_New_Vbox (MClient_Status_UI.Watchdog_VBox);

      Gtk_New (MClient_Status_UI.Watchdog_Label, "Watchdog");

      Gtk_New (MClient_Status_UI.Watchdog_Image, "./green-led.png");

      MClient_Status_UI.Watchdog_VBox.Pack_Start
        (MClient_Status_UI.Watchdog_Label, Expand => False, Padding => 10);

      MClient_Status_UI.Watchdog_VBox.Pack_Start
        (MClient_Status_UI.Watchdog_Image, Expand => False);

      --  Connection

      Gtk_New_Vbox (MClient_Status_UI.Connection_VBox);

      Gtk_New (MClient_Status_UI.Connection_Label, "Connection");

      Gtk_New (MClient_Status_UI.Connection_Image, "./red-led.png");

      MClient_Status_UI.Connection_VBox.Pack_Start
        (MClient_Status_UI.Connection_Label, Expand => False, Padding => 10);

      MClient_Status_UI.Connection_VBox.Pack_Start
        (MClient_Status_UI.Connection_Image, Expand => False);

      --  Commands Status

      Gtk_New (MClient_Status_UI.Commands_Status_Frame, "Commands Status");
      Gtk_Label (MClient_Status_UI.Commands_Status_Frame.Get_Label_Widget)
        .Set_Markup ("<b>Commands Status</b>");
      MClient_Status_UI.Commands_Status_Frame.Set_Shadow_Type
        (Gtk.Enums.Shadow_None);

      Gtk_New_Vbox (MClient_Status_UI.Commands_Status_VBox);

      Gtk_New_Hbox (MClient_Status_UI.Commands_Status_HBox);

      Gtk_New (MClient_Status_UI.Commands_Status_Table);
      --  One raw for columns labels
      --  One raw for each 10 commands
      --  One column for raws labels
      --  1 to 10 columns for commands

      for Col_Index in 1 .. Client_UI_Data.Columns_Number loop
         Gtk_New (Label);
         Label.Set_Markup ("<b>" & Integer'Image (Col_Index) & "</b>");
         MClient_Status_UI.Commands_Status_Table.Attach
           (Child  => Label,
            Left   => Gint (Col_Index),
            Top    => 0,
            Width  => 1,
            Height => 1);
      end loop;

      for Raw_Index in 1 .. Client_UI_Data.Raws_Number loop
         Gtk_New (Label);
         Label.Set_Markup ("<b>"
                           & Integer'Image ((Raw_Index - 1) * 10) & "</b>");
         MClient_Status_UI.Commands_Status_Table.Attach
           (Child  => Label,
            Left   => 0,
            Top    => Gint (Raw_Index),
            Width  => 1,
            Height => 1);
      end loop;

      Command_Index := 1;
      for Raw_Index in 1 .. Client_UI_Data.Raws_Number loop
         for Col_Index in 1 .. Client_UI_Data.Columns_Number loop

            Gtk_New (Image, Command_Status_Image_Unknown);

            MClient_Status_UI.Commands_Status_Table.Attach
              (Child  => Image,
               Left   => Gint (Col_Index),
               Top    => Gint (Raw_Index),
               Width  => 1,
               Height => 1);

            MClient_Status_UI.Commands_Status_Images (Command_Index) := Image;
            Command_Index := Command_Index + 1;

            exit when
              Command_Index > MClient_Status_UI.Commands_Status_Images'Last;

         end loop;
      end loop;

      MClient_Status_UI.Commands_Status_HBox.Pack_Start
        (MClient_Status_UI.Commands_Status_Table,
         Expand => False, Padding => 10);

      MClient_Status_UI.Commands_Status_VBox.Pack_Start
        (MClient_Status_UI.Commands_Status_HBox,
         Expand => False, Padding => 10);

      MClient_Status_UI.Commands_Status_Frame.Add
        (MClient_Status_UI.Commands_Status_VBox);

      --  HBox

      MClient_Status_UI.HBox.Pack_Start
        (MClient_Status_UI.Watchdog_VBox, Expand => False, Padding => 10);

      MClient_Status_UI.HBox.Pack_Start
        (MClient_Status_UI.Connection_VBox, Expand => False, Padding => 10);

      --  VBox

      MClient_Status_UI.VBox.Pack_Start
        (MClient_Status_UI.HBox, Expand => False, Padding => 10);

      MClient_Status_UI.VBox.Pack_Start
        (MClient_Status_UI.Commands_Status_Frame,
         Expand => False, Padding => 10);

      --  Frame

      MClient_Status_UI.Frame.Add (MClient_Status_UI.VBox);

      return MClient_Status_UI;

   end Create_MClient_Status_UI;

   function Create_MClients_Status_UI return MClients_Status_UI_Type is
      MClients_Status_UI : MClients_Status_UI_Type;
   begin
      Gtk_New_Vbox (MClients_Status_UI.VBox);

      for Index in MClients_Status_UI.Clients_UI'Range
      loop
         MClients_Status_UI.Clients_UI (Index) :=
           Create_MClient_Status_UI (Index);

         MClients_Status_UI.VBox.Pack_Start
           (MClients_Status_UI.Clients_UI (Index).Frame, Expand => False);

      end loop;

      return MClients_Status_UI;

   end Create_MClients_Status_UI;

   procedure MClient_Status_UI_Update
     (MClient_Status_UI : in MClient_Status_UI_Type) is
      Index : constant Positive := MClient_Status_UI.Index;
      Image : Gtk_Image;
      Command_Index : Positive;

      Client_UI_Data : constant MClient_Status_UI_Data_Access :=
        Clients_UI_Data (Index)'Access;

   begin

      if The_Main_Task_Interface.MClients_Status (Index).WD_Error
        and not Client_UI_Data.Watchdog_Error
      then
         MClient_Status_UI.Watchdog_Image.Set ("./red-led.png");
         Client_UI_Data.Watchdog_Error := True;
      end if;

      Client_UI_Data.Task_Status.all :=
        MBTCP_Clients_Tasks_Itf (Index).Status.Get_Status;

      if Client_UI_Data.Task_Status.Connected then
         if not Client_UI_Data.Connected then
            MClient_Status_UI.Connection_Image.Set ("./green-led.png");
         end if;
      else
         if Client_UI_Data.Connected then
            MClient_Status_UI.Connection_Image.Set ("./red-led.png");
         end if;
      end if;
      Client_UI_Data.Connected :=
        Client_UI_Data.Task_Status.Connected;

      Command_Index := 1;
      for Raw_Index in 1 .. Client_UI_Data.Raws_Number loop
         for Col_Index in 1 .. Client_UI_Data.Columns_Number loop

            if Client_UI_Data.Task_Status.Commands_Status (Command_Index)
              /= Client_UI_Data.Commands_Status (Command_Index)
            then

               Image := MClient_Status_UI.Commands_Status_Images
                 ((Raw_Index - 1) * 10 + Col_Index);

               case Client_UI_Data.Task_Status.Commands_Status (Command_Index)
               is

                  when Unknown => Image.Set (Command_Status_Image_Unknown);

                  when Disabled => Image.Set (Command_Status_Image_Disabled);

                  when Fine => Image.Set (Command_Status_Image_Fine);

                  when Fault => Image.Set (Command_Status_Image_Fault);

               end case;

               Client_UI_Data.Commands_Status (Command_Index) :=
                 Client_UI_Data.Task_Status.Commands_Status (Command_Index);
            end if;

            Command_Index := Command_Index + 1;

            exit when
              Command_Index > MClient_Status_UI.Commands_Status_Images'Last;

         end loop;
      end loop;

   end MClient_Status_UI_Update;

   overriding function Create
     return Instance
   is

      GUI_Element : Instance;

   begin
      Gtk_New (GUI_Element.Scrolled_Window);
      GUI_Element.Scrolled_Window.Set_Border_Width (Border_Width => 10);
      GUI_Element.Scrolled_Window.Set_Policy
        (Gtk.Enums.Policy_Automatic, Gtk.Enums.Policy_Automatic);

      GUI_Element.Scrolled_Window.Set_Shadow_Type (Gtk.Enums.Shadow_None);

      Gtk_New (GUI_Element.Page_Label, "Clients Status");

      Gtk_New_Vbox (GUI_Element.Page_VBox);

      GUI_Element.MClients_Status_UI := Create_MClients_Status_UI;

      GUI_Element.Page_VBox.Pack_Start
        (GUI_Element.MClients_Status_UI.VBox,
         Expand => False, Padding => 10);

      GUI_Element.Scrolled_Window.Add (GUI_Element.Page_VBox);

      return GUI_Element;

   end Create;

   overriding function Get_Root_Widget
     (GUI_Element : in Instance)
      return Gtk.Widget.Gtk_Widget
   is

   begin

      return Gtk_Widget (GUI_Element.Scrolled_Window);

   end Get_Root_Widget;

   overriding function Get_Label
     (GUI_Element : in Instance)
      return Gtk.Widget.Gtk_Widget
   is

   begin

      return Gtk_Widget (GUI_Element.Page_Label);

   end Get_Label;

   procedure Update
     (GUI_Element : in out Instance)
   is

   begin

      for Index in GUI_Element.MClients_Status_UI.Clients_UI'Range
      loop

         MClient_Status_UI_Update
           (GUI_Element.MClients_Status_UI.Clients_UI (Index));

      end loop;

   end Update;

   procedure Finalise
     (GUI_Element : in out Instance)
   is

      procedure Free_Task_Status is new Ada.Unchecked_Deallocation
        (Object => A4A.MBTCP_Client.Task_Status_Type,
         Name   => A4A.MBTCP_Client.Task_Status_Access);

      procedure Free_Commands_Status is new Ada.Unchecked_Deallocation
        (Object => A4A.MBTCP_Client.Commands_Status_Type,
         Name   => A4A.MBTCP_Client.Commands_Status_Access);

      procedure Free_Commands_Status_Images is new Ada.Unchecked_Deallocation
        (Object => Commands_Status_Images_Type,
         Name   => Commands_Status_Images_Access);

   begin

      for Index in Clients_UI_Data'Range
      loop

         Free_Task_Status (Clients_UI_Data (Index).Task_Status);

         Free_Commands_Status (Clients_UI_Data (Index).Commands_Status);

         Free_Commands_Status_Images
           (GUI_Element.MClients_Status_UI.Clients_UI (Index).
                Commands_Status_Images);

      end loop;

      A4A.Log.Logger.Put
        (Who  => "A4A.GUI_Elements.MClients_Status_Page.Finalise",
         What => "Done", Log_Level => A4A.Logger.Level_Verbose);

   end Finalise;

end A4A.GUI_Elements.MClients_Status_Page;
