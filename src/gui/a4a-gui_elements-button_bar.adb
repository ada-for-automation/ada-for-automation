
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Gtk.Enums;

package body A4A.GUI_Elements.Button_Bar is

   --------------------------------------------------------------------
   --  Graphical User Interface Elements
   --  Button Bar
   --------------------------------------------------------------------

   function Create_Button_Bar return Button_Bar_Type is
      Button_Bar : Button_Bar_Type;
   begin
      Gtk_New_Hbox (Button_Bar.HBox1);

      Gtk_New (Button_Bar.Quit_Button, "Quit");
      Gtk_New (Button_Bar.Quit_Button_Image, "gtk-stop",
               Gtk.Enums.Icon_Size_Button);

      Gtk_New (Button_Bar.Stop_Button, "Stop");
      Gtk_New (Button_Bar.Stop_Button_Image, "gtk-media-pause",
               Gtk.Enums.Icon_Size_Button);

      Gtk_New (Button_Bar.Start_Button, "Start");
      Gtk_New (Button_Bar.Start_Button_Image, "gtk-media-play",
               Gtk.Enums.Icon_Size_Button);

      Gtk_New (Button_Bar.App_Status_Label, "Application Status");

      Gtk_New (Button_Bar.App_Status_Image, "./green-led.png");

      Button_Bar.HBox1.Pack_Start
        (Button_Bar.Quit_Button, Expand => False, Padding => 10);
      Button_Bar.HBox1.Pack_Start
        (Button_Bar.Stop_Button, Expand => False);
      Button_Bar.HBox1.Pack_Start
        (Button_Bar.Start_Button, Expand => False);
      Button_Bar.HBox1.Pack_End
        (Button_Bar.App_Status_Image, Expand => False, Padding => 10);
      Button_Bar.HBox1.Pack_End
        (Button_Bar.App_Status_Label, Expand => False);

      Button_Bar.Quit_Button.Set_Image (Button_Bar.Quit_Button_Image);
      Button_Bar.Quit_Button.Set_Always_Show_Image (Always_Show => True);

      Button_Bar.Stop_Button.Set_Image (Button_Bar.Stop_Button_Image);
      Button_Bar.Stop_Button.Set_Always_Show_Image (Always_Show => True);

      Button_Bar.Start_Button.Set_Image (Button_Bar.Start_Button_Image);
      Button_Bar.Start_Button.Set_Always_Show_Image (Always_Show => True);

      return Button_Bar;

   end Create_Button_Bar;

end A4A.GUI_Elements.Button_Bar;
