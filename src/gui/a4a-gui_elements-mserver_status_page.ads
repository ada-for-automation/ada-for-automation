
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Gtk.Widget;          use Gtk.Widget;
with Gtk.Box;             use Gtk.Box;
with Gtk.Frame;           use Gtk.Frame;
with Gtk.Label;           use Gtk.Label;
with Gtk.Image;           use Gtk.Image;
with Gtk.Scrolled_Window; use Gtk.Scrolled_Window;

with A4A.Application.MBTCP_Server_Config;
use A4A.Application.MBTCP_Server_Config;

package A4A.GUI_Elements.MServer_Status_Page is

   --------------------------------------------------------------------
   --  Graphical User Interface Elements
   --  Modbus TCP Server Status Page
   --------------------------------------------------------------------

   type Instance is new GUI_Element_Type with private;

   overriding function Create
     return Instance;

   overriding function Get_Root_Widget
     (GUI_Element : in Instance)
      return Gtk.Widget.Gtk_Widget;

   overriding function Get_Label
     (GUI_Element : in Instance)
      return Gtk.Widget.Gtk_Widget;

   procedure Update
     (GUI_Element : in out Instance);

private

   MServer_Task_Status            : Server.Task_Status_Type;
   MBTCP_Server_Watchdog_Error    : Boolean  := False;

   type MServer_Status_UI_Type is
      record
         Frame        : Gtk_Frame;
         VBox         : Gtk_Vbox;
         HBox         : Gtk_Hbox;

         Watchdog_VBox  : Gtk_Vbox;
         Watchdog_Label : Gtk_Label;
         Watchdog_Image : Gtk_Image;

         Stats_VBox : Gtk_Vbox;

         General_Stats_Frame        : Gtk_Frame;
         General_Stats_HBox         : Gtk_Hbox;
         General_Stats_Items_Label  : Gtk_Label;
         General_Stats_Values_Label : Gtk_Label;

         Requests_Stats_Frame        : Gtk_Frame;
         Requests_Stats_HBox         : Gtk_Hbox;
         Requests_Stats_Items_Label  : Gtk_Label;
         Requests_Stats_Values_Label : Gtk_Label;

         Last_Error_Frame        : Gtk_Frame;
         Last_Error_HBox         : Gtk_Hbox;
         Last_Error_Label        : Gtk_Label;
      end record;

   type Instance is new GUI_Element_Type with
      record
         Scrolled_Window : Gtk_Scrolled_Window;
         Page_Label      : Gtk_Label;
         Page_VBox       : Gtk_Vbox;

         MServer_Status_UI : MServer_Status_UI_Type;
      end record;

   function Create_MServer_Status_UI return MServer_Status_UI_Type;

   procedure MServer_Status_UI_Update
     (MServer_Status_UI : in MServer_Status_UI_Type);

end A4A.GUI_Elements.MServer_Status_Page;
