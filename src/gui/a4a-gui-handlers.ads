
with Gdk.Event; use Gdk.Event;
with Gtk.Widget, Gtk.Handlers; use Gtk.Widget, Gtk.Handlers;
with Gtk.Arguments;       use Gtk.Arguments;
with Gtk.Notebook;        use Gtk.Notebook;

package A4A.GUI.Handlers is

   package Handlers is new Gtk.Handlers.Callback
     (Widget_Type => Gtk_Widget_Record);

   package Return_Handlers is new Gtk.Handlers.Return_Callback
     (Widget_Type => Gtk_Widget_Record,
      Return_Type => Boolean);

   package Notebook_Cb is new Gtk.Handlers.Callback (Gtk_Notebook_Record);

   procedure Quit_Callback (Widget : access Gtk_Widget_Record'Class);

   procedure Stop_Callback (Widget : access Gtk_Widget_Record'Class);

   procedure Start_Callback (Widget : access Gtk_Widget_Record'Class);

   procedure Page_Switch_Callback
     (Notebook : access Gtk_Notebook_Record'Class;
      Params   : Gtk.Arguments.Gtk_Args);

   function Delete_Event
     (Widget : access Gtk_Widget_Record'Class;
      Event  : Gdk_Event) return Boolean;

   procedure Destroy (Widget : access Gtk_Widget_Record'Class);

end A4A.GUI.Handlers;
