
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

package body A4A.Library.Analog is

   function Scale_In
     (X    : Integer;
      Xmin : Integer;
      Xmax : Integer;
      Ymin : Float;
      Ymax : Float)
      return Float is
   begin
      return Ymax + ((Float (X - Xmax) * (Ymax - Ymin)) / Float (Xmax - Xmin));
   end Scale_In;

   function Scale_Out
     (X    : Float;
      Xmin : Float;
      Xmax : Float;
      Ymin : Integer;
      Ymax : Integer)
      return Integer is
   begin
      return Ymax + Integer (Float'Floor (
        ((X - Xmax) * Float (Ymax - Ymin)) / (Xmax - Xmin)));
   end Scale_Out;

   function Limits
     (X    : Float;
      Ymin : Float;
      Ymax : Float)
      return Float is
   begin
      if X < Ymin then
         return Ymin;
      elsif X > Ymax then
         return Ymax;
      else
         return X;
      end if;
   end Limits;

   procedure Ramp
     (Value_In    : in Float;
      Gradient    : in Gradient_Type;
      Value_Out   : in out Float
     ) is
   begin
      if (Value_In - Value_Out) > Float (Gradient) then
         Value_Out := Value_Out + Float (Gradient);
      elsif (-Value_In + Value_Out) > Float (Gradient) then
         Value_Out := Value_Out - Float (Gradient);
      else
         Value_Out := Value_In;
      end if;
   end Ramp;

end A4A.Library.Analog;
