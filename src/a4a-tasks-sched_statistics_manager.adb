
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

package body A4A.Tasks.Sched_Statistics_Manager is

   procedure Update
     (Stats       : in out Instance;
      Sched_Delay : Duration) is
   begin
      if Sched_Delay >= Sched_Thresholds (Stats.Sched_Stats'Last) then
         Stats.Sched_Stats (Stats.Sched_Stats'Last) :=
           Stats.Sched_Stats (Stats.Sched_Stats'Last) + 1;
      else
         for Index in Stats.Sched_Stats'Range loop
            if Sched_Delay < Sched_Thresholds (Index) then
               Stats.Sched_Stats (Index) := Stats.Sched_Stats (Index) + 1;
               exit;
            end if;
         end loop;
      end if;
   end Update;

   function Get_Statistics
     (Stats : in out Instance) return Sched_Stats_Array is
   begin
      return Stats.Sched_Stats;
   end Get_Statistics;

end A4A.Tasks.Sched_Statistics_Manager;
