/*
-----------------------------------------------------------------------
--                       Ada for Automation                          --
--                                                                   --
--              Copyright (C) 2012-2015, Stephane LOS                --
--                                                                   --
-- This library is free software; you can redistribute it and/or     --
-- modify it under the terms of the GNU General Public               --
-- License as published by the Free Software Foundation; either      --
-- version 2 of the License, or (at your option) any later version.  --
--                                                                   --
-- This library is distributed in the hope that it will be useful,   --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of    --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU --
-- General Public License for more details.                          --
--                                                                   --
-- You should have received a copy of the GNU General Public         --
-- License along with this library; if not, write to the             --
-- Free Software Foundation, Inc., 59 Temple Place - Suite 330,      --
-- Boston, MA 02111-1307, USA.                                       --
--                                                                   --
-- As a special exception, if other files instantiate generics from  --
-- this unit, or you link this unit with other files to produce an   --
-- executable, this  unit  does not  by itself cause  the resulting  --
-- executable to be covered by the GNU General Public License. This  --
-- exception does not however invalidate any other reasons why the   --
-- executable file  might be covered by the  GNU Public License.     --
-----------------------------------------------------------------------
*/

#ifndef __A4A_LOG_WRAP_ADA_H
#define __A4A_LOG_WRAP_ADA_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>

  /* Maps to A4A.Logger.Log_Level_Type :*/
#define A4A_LOG_LEVEL_ERROR            1
#define A4A_LOG_LEVEL_WARNING          2
#define A4A_LOG_LEVEL_INFO             3
#define A4A_LOG_LEVEL_VERBOSE          4

  void A4A_Log (const char* szWhoBuffer, const char* szWhatBuffer,
                uint32_t ulLog_Level);

#ifdef __cplusplus
}
#endif

#endif /* __A4A_LOG_WRAP_ADA_H */
