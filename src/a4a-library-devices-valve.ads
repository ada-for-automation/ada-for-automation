
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Real_Time; use Ada.Real_Time;

with A4A.Library.Timers.TON; use A4A.Library.Timers;

package A4A.Library.Devices.Valve is

   Default_TON_Preset : constant := 1000;
   --  Default value in milliseconds for Timer TON Preset

   type IO_Status_Type is
      record
         Pos_Open    : Boolean;
         Pos_Closed  : Boolean;
         Coil        : Boolean;
      end record;

   type Instance is new Device_Type with private;

   overriding function Create
     (Id         : in String)
      return Instance;

   function Create
     (Id         : in String;
      TON_Preset : in Positive)
      --  TON Preset in milliseconds
      return Instance;

   procedure Cyclic
     (Device      : in out Instance;
      Pos_Open    : in Boolean;
      Pos_Closed  : in Boolean;
      Ack         : in Boolean;
      Cmd_Open    : in Boolean;
      Coil        : out Boolean);

   function is_Open
     (Device    : in Instance) return Boolean;

   function is_Closed
     (Device    : in Instance) return Boolean;

   function is_Faulty
     (Device    : in Instance) return Boolean;

   function Get_IO_Status
     (Device    : in Instance) return IO_Status_Type;

private

   type Device_Status is
     (Status_Closed,
      Status_Opening,
      Status_Open,
      Status_Closing,
      Status_Fault);

   type Instance is new Device_Type with
      record
         Status     : Device_Status := Status_Closed;
         IO_Status  : IO_Status_Type;
         Preset_Dis : Time_Span;
         Preset_Mov : Time_Span;
         Tempo_Dis  : TON.Instance;
         Tempo_Mov  : TON.Instance;
      end record;

end A4A.Library.Devices.Valve;
