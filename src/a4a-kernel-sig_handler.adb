
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with A4A.Sigint_Handler;
with A4A.Log;
with A4A.Logger; use A4A.Logger;

package body A4A.Kernel.Sig_Handler is

   procedure Create_Sig_Handler is
   begin
      if not Sig_Handler_Task_Created then
         The_Sig_Handler := new Sig_Handler;
         Sig_Handler_Task_Created := True;
      end if;
   end Create_Sig_Handler;

   task body Sig_Handler is
      My_Ident : constant String := "A4A.Kernel.Sig_Handler";
   begin
      A4A.Log.Logger.Put
        (Who       => My_Ident,
         What      => "Waiting Signal Interrupt... Use Ctrl+C to exit",
         Log_Level => Level_Info);

      A4A.Sigint_Handler.Handler.Wait;

      Quit_Flag := True;

      A4A.Log.Logger.Put
        (Who       => My_Ident,
         What      => "Signal Interrupt caught !",
         Log_Level => Level_Info);

      Sig_Handler_Task_Created := False;
   end Sig_Handler;

end A4A.Kernel.Sig_Handler;
