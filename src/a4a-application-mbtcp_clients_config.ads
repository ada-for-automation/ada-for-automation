
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with A4A.MBTCP_Client; use A4A.MBTCP_Client;
with A4A.Protocols; use A4A.Protocols.IP_Address_Strings;
with A4A.Protocols.LibModbus; use A4A.Protocols.LibModbus;

--  tag::config[]
package A4A.Application.MBTCP_Clients_Config is

   --------------------------------------------------------------------
   --  Modbus TCP Clients configuration
   --------------------------------------------------------------------

   --  For each Modbus TCP Server define one client configuration task

   Config1 : aliased Client_Configuration :=
     (Command_Number    => 2, --  <3>
      Enabled           => True,
      Debug_On          => False,
      Task_Period_MS    => 10,
      Retries           => 3,
      Timeout           => 0.2,

      --  Using an IP Address
      --  Node    => To_Bounded_String ("127.0.0.1"), --  <1>
      --  or a Node name, i.e. example.com
      --  Node    => To_Bounded_String ("localhost"), --  <1>
      --  This one for the Docker demo
      Node    => To_Bounded_String ("a4a_piano"), --  <1>
      Service => To_Bounded_String ("1504"), --  <2>
      --  Standard : 502

      Commands => --  <4>
        (
         --                                Period              Offset Offset
         --               Action Enabled Multiple Shift Number Remote  Local
         1 =>
           (Read_Input_Bits,        True,      10,    0,    16,     0,     0),
         2 =>
           (Write_Bits,             True,      10,    5,    16,     0,     0)
        )
     );

   Config2 : aliased Client_Configuration :=
     (Command_Number    => 2,
      Enabled           => True,
      Debug_On          => False,
      Task_Period_MS    => 100,
      Retries           => 3,
      Timeout           => 0.2,

      Node    => To_Bounded_String ("127.0.0.1"),
      Service => To_Bounded_String ("1503"), -- My own MBTCP server

      Commands =>
        (
         --                                Period              Offset Offset
         --               Action Enabled Multiple Shift Number Remote  Local
         1 =>
           (Read_Registers,         True,      10,    0,    10,     0,     0),
         2 =>
           (Write_Registers,        True,      30,    1,    10,     0,     0)
        )
     );

   --  Declare all clients configurations in the array
   --  The kernel will create those clients accordingly

   MBTCP_Clients_Configuration : Client_Configuration_Access_Array :=
     (1 => Config1'Access, --  <5>
      2 => Config2'Access);

end A4A.Application.MBTCP_Clients_Config;
--  end::config[]
