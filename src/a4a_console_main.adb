
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Command_Line;
with Ada.Exceptions; use Ada.Exceptions;
with Ada.Strings.Unbounded;

with A4A; use A4A;
with A4A.Command_Line;
with A4A.Tasks;
with A4A.Kernel; use A4A.Kernel;
with A4A.Kernel.Main; use A4A.Kernel.Main;
with A4A.Kernel.Sig_Handler;

with A4A.Log;
with A4A.Logger; use A4A.Logger;

with A4A.Application.Identification; use A4A.Application.Identification;

procedure A4A_Console_Main is
   My_Ident : constant String := "A4A_Console_Main";

   CL_Arguments : Ada.Strings.Unbounded.Unbounded_String;

   Terminating      : Boolean := False;

   Main_Watching    : Boolean := False;

   Period1_Watching : Boolean := False;

   Main_Task_Watchdog_TON_Q       : Boolean := False;
   Main_Task_Watchdog_Error       : Boolean := False;
   Main_Task_Watchdog_Status      : String  := "WD OK";
   Our_Main_Task_Status           : A4A.Tasks.Task_Status_Type;
   Our_Main_Task_Statistics       : A4A.Tasks.Task_Statistics_Type;

   Period1_Task_Watchdog_TON_Q    : Boolean := False;
   Period1_Task_Watchdog_Error    : Boolean := False;
   Period1_Task_Watchdog_Status   : String  := "WD OK";
   Period1_Task_Status            : A4A.Tasks.Task_Status_Type;
   Period1_Task_Statistics        : A4A.Tasks.Task_Statistics_Type;

   procedure Show_Application_Identification;

   procedure Show_Application_Identification is
      What : constant String := CRLF
        & "***********************************************" & CRLF
        & "          Application Identification" & CRLF
        & "Name :" & CRLF
        & "    " & Get_Application_Name & CRLF
        & "Version :" & CRLF
        & "    " & Get_Application_Version & CRLF
        & "Description :" & CRLF
        & "    " & Get_Application_Description & CRLF
        & "***********************************************" & CRLF;
   begin
      A4A.Log.Logger.Put (Who       => My_Ident,
                          What      => What,
                          Log_Level => A4A.Logger.Level_Info);
   end Show_Application_Identification;

begin

   A4A.Command_Line.Parse_Arguments;

   --  Let's print our Command Line (not that useful at the moment)
   if A4A.Log.Logger.Get_Level = A4A.Logger.Level_Info then
      for Index in 1 .. Ada.Command_Line.Argument_Count loop
         Ada.Strings.Unbounded.Append
           (CL_Arguments, Ada.Command_Line.Argument (Index) & " ");
      end loop;
   end if;
   A4A.Log.Logger.Put (Who       => My_Ident,
                       What      => "started as : "
                       & Ada.Command_Line.Command_Name & " "
                       & Ada.Strings.Unbounded.To_String (CL_Arguments),
                       Log_Level => A4A.Logger.Level_Info);

   Show_Application_Identification;

   --------------------------------------------------------------------
   --  Create necessary tasks
   --------------------------------------------------------------------
   --  This one waits for Ctrl+C and terminates the application
   A4A.Kernel.Sig_Handler.Create_Sig_Handler;

   --  Well, the main automation task, either cyclic or periodic, which runs
   --  the A4A.Application.Main_Cyclic procedure
   A4A.Kernel.Main.Create_Main_Task;

   --  A periodic task, running the A4A.Application.Periodic_Run_1 procedure
   A4A.Kernel.Create_Generic_Periodic_Task_1;

   loop
      exit when
        The_Main_Task_Interface.Status.Ready
        and The_GP_Task1_Interface.Status.Ready;
      delay 1.0;
   end loop;

   --  Start tasks
   The_Main_Task_Interface.Control.Run (True);
   The_GP_Task1_Interface.Control.Run (True);

   --------------------------------------------------------------------
   --  Monitoring the tasks
   --------------------------------------------------------------------
   loop

      if not Main_Watching then
         The_Main_Task_Interface.Status_Watchdog.Set_Time_Out
           (Time_Out_MS => 1500);
         The_Main_Task_Interface.Control.Start_Watching (True);
         Main_Watching := True;
      end if;

      The_Main_Task_Interface.Control_Watchdog.Watchdog
        (Watching        => not Terminating,
         Status_Watchdog => The_Main_Task_Interface.Status_Watchdog.Value,
         Error           => Main_Task_Watchdog_TON_Q);

      if Main_Task_Watchdog_TON_Q and not Main_Task_Watchdog_Error then
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Main task Watchdog Time Out elapsed!");
         Main_Task_Watchdog_Error := True;
         Main_Task_Watchdog_Status := "WD PB";
      end if;

      if not Period1_Watching then
         The_GP_Task1_Interface.Status_Watchdog.Set_Time_Out
           (Time_Out_MS => 1500);
         The_GP_Task1_Interface.Control.Start_Watching (True);
         Period1_Watching := True;
      end if;

      The_GP_Task1_Interface.Control_Watchdog.Watchdog
        (Watching        => not Terminating,
         Status_Watchdog => The_GP_Task1_Interface.Status_Watchdog.Value,
         Error           => Period1_Task_Watchdog_TON_Q);

      if Period1_Task_Watchdog_TON_Q and not Period1_Task_Watchdog_Error then
         A4A.Log.Logger.Put
           (Who  => My_Ident,
            What => "Periodic task 1 Watchdog Time Out elapsed!");
         Period1_Task_Watchdog_Error := True;
         Period1_Task_Watchdog_Status := "WD PB";
      end if;

      Our_Main_Task_Status := The_Main_Task_Interface.Status.Get_Status;
      Our_Main_Task_Statistics :=
        The_Main_Task_Interface.Statistics.Get_Statistics;
      A4A.Log.Logger.Put
        (Who       => My_Ident,
         What      => "Main Task (Running, WD_Status, Min, Max, Avg) : "
         & Boolean'Image (Our_Main_Task_Status.Running) & " "
         & Main_Task_Watchdog_Status & " "
         & Duration'Image (Our_Main_Task_Statistics.Min_Duration) & " "
         & Duration'Image (Our_Main_Task_Statistics.Max_Duration) & " "
         & Duration'Image (Our_Main_Task_Statistics.Avg_Duration),
         Log_Level => A4A.Logger.Level_Verbose);

      Period1_Task_Status := The_GP_Task1_Interface.Status.Get_Status;
      Period1_Task_Statistics :=
        The_GP_Task1_Interface.Statistics.Get_Statistics;
      A4A.Log.Logger.Put
        (Who       => My_Ident,
         What      => "Periodic Task 1 (Running, WD_Status, Min, Max, Avg) : "
         & Boolean'Image (Period1_Task_Status.Running) & " "
         & Period1_Task_Watchdog_Status & " "
         & Duration'Image (Period1_Task_Statistics.Min_Duration) & " "
         & Duration'Image (Period1_Task_Statistics.Max_Duration) & " "
         & Duration'Image (Period1_Task_Statistics.Avg_Duration),
         Log_Level => A4A.Logger.Level_Verbose);

      if A4A.Kernel.Quit then
         Terminating := True;

         --  Stop tasks
         The_Main_Task_Interface.Control.Run (False);
         The_GP_Task1_Interface.Control.Run (False);

         --  Terminate tasks
         The_Main_Task_Interface.Control.Quit (True);
         The_GP_Task1_Interface.Control.Quit (True);
      end if;

      exit when Terminating
        and The_Main_Task_Interface.Status.Terminated
        and The_GP_Task1_Interface.Status.Terminated;

      delay 1.0;
   end loop;

   A4A.Log.Logger.Put (Who       => My_Ident,
                       What      => "Finished !",
                       Log_Level => Level_Info);
   A4A.Log.Quit;

   loop
      delay 1.0;
      exit when A4A.Log.is_Terminated;
   end loop;

exception

   when Error : others =>
      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => Exception_Information (Error));

      The_Main_Task_Interface.Control.Quit (True);
      The_GP_Task1_Interface.Control.Quit (True);

      delay 5.0;

      A4A.Log.Logger.Put (Who  => My_Ident,
                          What => "Aborted !");
      A4A.Log.Quit;

   loop
      delay 1.0;
      exit when A4A.Log.is_Terminated;
   end loop;

end A4A_Console_Main;
