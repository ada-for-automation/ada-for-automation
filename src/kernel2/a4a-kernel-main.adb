
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Task_Identification; use Ada.Task_Identification;
with Ada.Calendar.Formatting;
with Ada.Real_Time; use Ada.Real_Time;
with Ada.Exceptions; use Ada.Exceptions;

with A4A.Log;
with A4A.Logger; use A4A.Logger;

with A4A.Configuration; use A4A.Configuration;
with A4A.Application.Configuration; use A4A.Application.Configuration;
with A4A.Application.Main_Periodic1;
with A4A.Memory.MBTCP_IOServer;

with A4A.Tasks.Statistics_Manager;
with A4A.Tasks.Sched_Statistics_Manager; use A4A.Tasks;

package body A4A.Kernel.Main is

   procedure Create_Main_Task is
   begin
      if not Main_Task_Created then
         The_Main_Task := new Main_Task
           (Task_Priority  => Application_Main_Task_Priority,
            Task_Itf       => The_Main_Task_Interface'Access);

         Main_Task_Created := True;
      end if;
   end Create_Main_Task;

   protected body MBRTU_Master_Status is

      procedure WD_Error (Value : in Boolean) is
      begin
         Status.WD_Error := Value;
      end WD_Error;

      function WD_Error return Boolean is
      begin
         return Status.WD_Error;
      end WD_Error;

   end MBRTU_Master_Status;

   task body Main_Task is
      My_ID    : constant Task_Id := Current_Task;
      My_Ident : constant String := "A4A.Kernel.Main_Task";

      Start_Time : constant Ada.Calendar.Time := Ada.Calendar.Clock;
      --  if Application_Main_Task_Type = Cyclic then
      My_Delay  : constant Duration :=
        To_Duration (Milliseconds (Application_Main_Task_Delay_MS));
      --  end if;
      --  if Application_Main_Task_Type = Periodic then
      Next_Time : Ada.Real_Time.Time := Clock;
      My_Period : constant Time_Span :=
        Milliseconds (Application_Main_Task_Period_MS);
      --  end if;

      Task_Status : Tasks.Task_Status_Type;
      My_Statistics_Manager : Statistics_Manager.Instance;
      My_Sched_Statistics_Manager : Sched_Statistics_Manager.Instance;

      Start_Real_Time : Ada.Real_Time.Time;
      Sleep_Real_Time : Ada.Real_Time.Time;
      Task_Duration   : Duration;

      Sched_Delay       : Duration;

      First_Cycle     : Boolean := True;

      Clock_Handler : Clock_Handler_Task_Type_Access;
      pragma Unreferenced (Clock_Handler);

      Main_Watchdog_TON_Q       : Boolean := False;
      Main_Watchdog_Error       : Boolean := False;

      MBTCP_Server_Task_Watchdog_TON_Q  : Boolean := False;
      MBTCP_Server_Task_Watchdog_Error  : Boolean := False;
      MBTCP_Server_Task_Watchdog_Status : String  := "WD OK";
      pragma Unreferenced (MBTCP_Server_Task_Watchdog_Status);
      --  MBTCP_Server_Task_Status          : Server.Task_Status_Type;

      MBRTU_Master_Task_Watchdog_TON_Q  : Boolean := False;
      MBRTU_Master_Task_Watchdog_Error  : Boolean := False;

      procedure Log_Task_Start;

      procedure Log_Task_Start is
         What : constant String := "Main_Task's ID is " & Image (My_ID) & CRLF
           & "Started at " & Ada.Calendar.Formatting.Image
           (Date                  => Start_Time,
            Include_Time_Fraction => True,
            Time_Zone             => The_Time_Offset);
      begin
         A4A.Log.Logger.Put
           (Who       => My_Ident,
            What      => What,
            Log_Level => Level_Info);
      end Log_Task_Start;

      procedure Close;

      procedure Close is
      begin

         A4A.Log.Logger.Put
           (Who       => My_Ident,
            What      => "Calling Closing...",
            Log_Level => Level_Info);

         A4A.Application.Closing;

         MBTCP_Server_Task_Interface.Control.Quit (True);

         MBRTU_Master_Task_Itf.Control.Quit (True);

         loop
            exit when MBRTU_Master_Task_Itf.Status.Terminated;
            delay 1.0;
         end loop;

         loop
            exit when MBTCP_Server_Task_Interface.Status.Terminated;
            delay 1.0;
         end loop;

         Clock_Handler_Interface.Control.Quit (True);
         loop
            exit when Clock_Handler_Interface.Status.Terminated;
            delay 1.0;
         end loop;

         Task_Itf.Status.Ready (False);
         Task_Itf.Status.Terminated (Task_Status.Terminated);
         Main_Task_Created := False;

      end Close;

   begin

      Log_Task_Start;

      --------------------------------------------------------------------
      --  Clock Management
      --------------------------------------------------------------------

      Clock_Handler := new Clock_Handler_Task_Type
        (Task_Priority          => System.Default_Priority,
         Task_Itf               => Clock_Handler_Interface'Access,
         Period_In_Milliseconds => 10);
      --  This task manages the real time clock calls
      --  so that the system call does not get overwhelmed
      --  Is that true ? It seems to me because a lot of automation
      --  stuff needs time.

      --------------------------------------------------------------------
      --  Modbus TCP Server Management
      --------------------------------------------------------------------

      MBTCP_Server_Task := new Server.Periodic_Task
        (Task_Priority  => System.Default_Priority,
         Task_Itf       => MBTCP_Server_Task_Interface'Access,
         Configuration  => A4A.Application.MBTCP_Server_Config.Config1'Access
        );

      A4A.Log.Logger.Put
        (Who       => My_Ident,
         What      => "Modbus TCP Server created...",
         Log_Level => Level_Info);

      --------------------------------------------------------------------
      --  Modbus RTU Master Management
      --------------------------------------------------------------------

      MBRTU_Master_Task := new A4A.MBRTU_Master.Periodic_Task
        (Task_Priority   => System.Default_Priority,
         Task_Itf        => MBRTU_Master_Task_Itf'Access,
         Configuration   => A4A.Application.MBRTU_Master_Config.Config'Access,
         Bool_DPM_Access => My_Bool_DPM'Access,
         Word_DPM_Access => My_Word_DPM'Access);

      A4A.Log.Logger.Put
        (Who       => My_Ident,
         What      => "Modbus RTU Master created...",
         Log_Level => Level_Info);

      A4A.Log.Logger.Put
        (Who       => My_Ident,
         What      => "Calling Cold_Start...",
         Log_Level => Level_Info);

      A4A.Application.Cold_Start;

      Task_Itf.Status.Ready (True);

      MBTCP_Server_Task_Interface.Control_Watchdog.Set_Time_Out (2000);
      MBTCP_Server_Task_Interface.Control.Start_Watching (True);

      MBRTU_Master_Task_Itf.Control_Watchdog.Set_Time_Out (2000);
      MBRTU_Master_Task_Itf.Control.Start_Watching (True);

      --------------------------------------------------------------------
      --  Main loop
      --------------------------------------------------------------------

      Main_Loop :
      loop

         Start_Real_Time := Clock;

         Task_Status.Running := Task_Itf.Control.Run;
         Task_Status.Terminated := Task_Itf.Control.Quit;

         --  Here we test the watchdog between the main task and
         --  the console or GUI thread

         Task_Itf.Status_Watchdog.Watchdog
           (Watching         => Task_Itf.Control.Start_Watching,
            Control_Watchdog => Task_Itf.Control_Watchdog.Value,
            Error            => Main_Watchdog_TON_Q);

         if Main_Watchdog_TON_Q and not Main_Watchdog_Error then
            A4A.Log.Logger.Put
              (Who       => My_Ident,
               What      => "Watchdog Time Out elapsed!",
               Log_Level => Level_Error);
            Main_Watchdog_Error := True;
         end if;

         --  Here we test the watchdog between the main task and
         --  Modbus TCP Server

         MBTCP_Server_Task_Interface.Control_Watchdog.Watchdog
           (Watching        => not Task_Status.Terminated,
            Status_Watchdog =>
              MBTCP_Server_Task_Interface.Status_Watchdog.Value,
            Error           => MBTCP_Server_Task_Watchdog_TON_Q);

         if MBTCP_Server_Task_Watchdog_TON_Q
           and not MBTCP_Server_Task_Watchdog_Error
         then
            A4A.Log.Logger.Put
              (Who       => My_Ident,
               What      => "MBTCP Server task Watchdog Time Out elapsed!",
               Log_Level => Level_Error);

            MBTCP_Server_Task_Watchdog_Error := True;
            MBTCP_Server_Task_Watchdog_Status := "WD PB";
            Task_Itf.MServer_Status.WD_Error (True);
         end if;

         --  Here we test the watchdog between the main task and
         --  the Modbus RTU Master

         MBRTU_Master_Task_Itf.Control_Watchdog.Watchdog
           (Watching        =>
              MBRTU_Master_Task_Itf.Status.Connected
            and not Task_Status.Terminated,
            Status_Watchdog => MBRTU_Master_Task_Itf.Status_Watchdog.Value,
            Error           => MBRTU_Master_Task_Watchdog_TON_Q);

         if MBRTU_Master_Task_Watchdog_TON_Q
           and not MBRTU_Master_Task_Watchdog_Error
         then
            A4A.Log.Logger.Put
              (Who       => My_Ident,
               What      => "MBRTU Master task Watchdog Time Out elapsed!",
               Log_Level => Level_Error);

            MBRTU_Master_Task_Watchdog_Error := True;
            Task_Itf.MMaster_Status.WD_Error (True);
         end if;

         A4A.Memory.MBRTU_Master.Bool_Inputs := My_Bool_DPM.Inputs.Get_Data
           (Offset => A4A.Memory.MBRTU_Master.Bool_Inputs'First,
            Number => A4A.Memory.MBRTU_Master.Bool_Inputs'Length);

         A4A.Memory.MBRTU_Master.Word_Inputs := My_Word_DPM.Inputs.Get_Data
           (Offset => A4A.Memory.MBRTU_Master.Word_Inputs'First,
            Number => A4A.Memory.MBRTU_Master.Word_Inputs'Length);

         Server.Registers_Read
           (Outputs => A4A.Memory.MBTCP_IOServer.Registers,
            Offset  => 0);

         if Task_Status.Running
           and not Task_Status.Terminated
           and not A4A.Application.Program_Fault
         then

            A4A.Application.Main_Periodic1.Get_Main_Inputs;
            A4A.Application.Main_Cyclic;
            A4A.Application.Main_Periodic1.Set_Main_Outputs;

         else
            A4A.Memory.MBRTU_Master.Bool_Outputs := (others => False);
            A4A.Memory.MBRTU_Master.Word_Outputs := (others => 0);
         end if;

         My_Bool_DPM.Outputs.Set_Data
           (Data_In => A4A.Memory.MBRTU_Master.Bool_Outputs,
            Offset  => A4A.Memory.MBRTU_Master.Bool_Outputs'First);

         My_Word_DPM.Outputs.Set_Data
           (Data_In => A4A.Memory.MBRTU_Master.Word_Outputs,
            Offset  => A4A.Memory.MBRTU_Master.Word_Outputs'First);

         Server.Inputs_Registers_Write
           (Inputs => A4A.Memory.MBRTU_Master.Word_Inputs,
            Offset => 0);

         Server.Inputs_Registers_Write
           (Inputs => A4A.Memory.MBRTU_Master.Word_Outputs,
            Offset => A4A.Memory.MBRTU_Master.Word_Inputs'Length);

         Server.Inputs_Registers_Write
           (Inputs => MBTCP_IOServer.Input_Registers,
            Offset => (A4A.Memory.MBRTU_Master.Word_Inputs'Length
                       + A4A.Memory.MBRTU_Master.Word_Outputs'Length));

         exit Main_Loop when Task_Status.Terminated;

         Task_Duration := To_Duration (Clock - Start_Real_Time);
         if First_Cycle then
            My_Statistics_Manager.Initialise (Task_Duration);
         else
            My_Statistics_Manager.Update (Task_Duration);
         end if;

         case Application_Main_Task_Type is
         when Cyclic =>
            Sleep_Real_Time := Clock;
            delay My_Delay;
            Sched_Delay := To_Duration (Clock - Sleep_Real_Time) - My_Delay;
         when Periodic =>
            Next_Time := Next_Time + My_Period;
            delay until Next_Time;
            Sched_Delay := To_Duration (Clock - Next_Time);
         end case;

         My_Sched_Statistics_Manager.Update (Sched_Delay);

         Task_Itf.Status.Running (Task_Status.Running);
         Task_Itf.Statistics.Set_Statistics
           (My_Statistics_Manager.Get_Statistics);
         Task_Itf.Sched_Statistics.Set_Sched_Stats
           (My_Sched_Statistics_Manager.Get_Statistics);

         First_Cycle := False;
      end loop Main_Loop;

      Close;

      A4A.Log.Logger.Put (Who       => My_Ident,
                          What      => "Finished !",
                          Log_Level => Level_Info);

   exception

      when Error : others =>
         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => Exception_Information (Error));

         Task_Status.Terminated := True;

         Close;

         A4A.Log.Logger.Put (Who  => My_Ident,
                             What => "Aborted !");
   end Main_Task;

end A4A.Kernel.Main;
