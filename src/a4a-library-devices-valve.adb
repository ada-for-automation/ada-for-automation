
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

package body A4A.Library.Devices.Valve is

   overriding function Create
     (Id : in String)
      return Instance is
      Tempo_Dis : TON.Instance;
      Tempo_Mov : TON.Instance;
   begin
      return (Limited_Controlled with Id => new String'(Id),
              Status     => Status_Closed,
              Preset_Dis => Milliseconds (Default_TON_Preset),
              Preset_Mov => Milliseconds (Default_TON_Preset),
              Tempo_Dis  => Tempo_Dis,
              Tempo_Mov  => Tempo_Mov,
              IO_Status  => (False, False, False));
   end Create;

   function Create
     (Id         : in String;
      TON_Preset : in Positive)
      --  TON Preset in milliseconds
      return Instance is
      Tempo_Dis : TON.Instance;
      Tempo_Mov : TON.Instance;
   begin
      return (Limited_Controlled with Id => new String'(Id),
              Status     => Status_Closed,
              Preset_Dis => Milliseconds (Default_TON_Preset),
              Preset_Mov => Milliseconds (TON_Preset),
              Tempo_Dis  => Tempo_Dis,
              Tempo_Mov  => Tempo_Mov,
              IO_Status  => (False, False, False));
   end Create;

   procedure Cyclic
     (Device      : in out Instance;
      Pos_Open    : in Boolean;
      Pos_Closed  : in Boolean;
      Ack         : in Boolean;
      Cmd_Open    : in Boolean;
      Coil        : out Boolean) is
      Elapsed_Dis : Time_Span;
      Elapsed_Mov : Time_Span;
      Tempo_Dis_Q : Boolean;
      Tempo_Mov_Q : Boolean;
      Static_Failure_Condition  : Boolean;
      Dynamic_Failure_Condition : Boolean;
   begin

      Static_Failure_Condition :=
        ((Device.Status = Status_Closed) and not Pos_Closed)
        or ((Device.Status = Status_Open) and not Pos_Open)
        or (Pos_Closed and Pos_Open);

      Device.Tempo_Dis.Cyclic
        (Start      => Static_Failure_Condition,
         Preset     => Device.Preset_Dis,
         Elapsed    => Elapsed_Dis,
         Q          => Tempo_Dis_Q);

      Dynamic_Failure_Condition :=
        (Device.Status = Status_Opening) or (Device.Status = Status_Closing);

      Device.Tempo_Mov.Cyclic
        (Start      => Dynamic_Failure_Condition,
         Preset     => Device.Preset_Mov,
         Elapsed    => Elapsed_Mov,
         Q          => Tempo_Mov_Q);

      if Tempo_Dis_Q or Tempo_Mov_Q then
         Device.Status := Status_Fault;
      end if;

      case Device.Status is
         when Status_Closed =>
            if Cmd_Open then
               Device.Status := Status_Opening;
            end if;
         when Status_Opening =>
            if Pos_Open then
               Device.Status := Status_Open;
            end if;
         when Status_Open =>
            if not Cmd_Open then
               Device.Status := Status_Closing;
            end if;
         when Status_Closing =>
            if Pos_Closed then
               Device.Status := Status_Closed;
            end if;
         when Status_Fault =>
            if Ack then
               Device.Status := Status_Closed;
            end if;
      end case;

      Coil :=
        (Device.Status = Status_Opening) or (Device.Status = Status_Open);

      Device.IO_Status.Pos_Open := Pos_Open;
      Device.IO_Status.Pos_Closed := Pos_Closed;
      Device.IO_Status.Coil := Coil;

   end Cyclic;

   function is_Open
     (Device    : in Instance) return Boolean is
   begin
      return (Device.Status = Status_Open);
   end is_Open;

   function is_Closed
     (Device    : in Instance) return Boolean is
   begin
      return (Device.Status = Status_Closed);
   end is_Closed;

   function is_Faulty
     (Device    : in Instance) return Boolean is
   begin
      return (Device.Status = Status_Fault);
   end is_Faulty;

   function Get_IO_Status
     (Device    : in Instance) return IO_Status_Type is
   begin
      return Device.IO_Status;
   end Get_IO_Status;

end A4A.Library.Devices.Valve;
