
------------------------------------------------------------------------------
--                            Ada for Automation                            --
--                                                                          --
--                   Copyright (C) 2012-2023, Stephane LOS                  --
--                                                                          --
-- This library is free software;  you can redistribute it and/or modify it --
-- under terms of the  GNU General Public License  as published by the Free --
-- Software  Foundation;  either version 3,  or (at your  option) any later --
-- version. This library is distributed in the hope that it will be useful, --
-- but WITHOUT ANY WARRANTY;  without even the implied warranty of MERCHAN- --
-- TABILITY or FITNESS FOR A PARTICULAR PURPOSE.                            --
--                                                                          --
-- As a special exception under Section 7 of GPL version 3, you are granted --
-- additional permissions described in the GCC Runtime Library Exception,   --
-- version 3.1, as published by the Free Software Foundation.               --
--                                                                          --
-- You should have received a copy of the GNU General Public License and    --
-- a copy of the GCC Runtime Library Exception along with this program;     --
-- see the files COPYING3 and COPYING.RUNTIME respectively.  If not, see    --
-- <http://www.gnu.org/licenses/>.                                          --
--                                                                          --
------------------------------------------------------------------------------

with A4A.Library.Timers.TON;

package A4A.Tasks.Watchdog is

   Default_Watchdog_Time_Out_MS : constant Natural := 1000;

   --------------------------------------------------------------------
   --  Control Watchdog
   --------------------------------------------------------------------

   protected type Control_Watchdog_Type is

      procedure Watchdog
        (Watching        : in Boolean;
         Status_Watchdog : in DWord;
         Error           : out Boolean);

      procedure Set_Time_Out
        (Time_Out_MS : Natural := Default_Watchdog_Time_Out_MS);

      function Value return DWord;

   private

      Watching   : Boolean := False;

      Watchdog_Value    : DWord   := 0;

      Watchdog_Time_Out : Ada.Real_Time.Time_Span :=
        Ada.Real_Time.Milliseconds (Default_Watchdog_Time_Out_MS);

      Watchdog_TON         : A4A.Library.Timers.TON.Instance;
      Watchdog_TON_Elapsed : Ada.Real_Time.Time_Span := Time_Span_Zero;
      Watchdog_TON_Start   : Boolean := False;
      Watchdog_TON_Q       : Boolean := False;

   end Control_Watchdog_Type;

   --------------------------------------------------------------------
   --  Status Watchdog
   --------------------------------------------------------------------

   protected type Status_Watchdog_Type is

      procedure Watchdog
        (Watching         : in Boolean;
         Control_Watchdog : in DWord;
         Error            : out Boolean);

      procedure Set_Time_Out
        (Time_Out_MS : Natural := Default_Watchdog_Time_Out_MS);

      function Value return DWord;

   private

      Watching   : Boolean := False;

      Watchdog_Value    : DWord   := 0;

      Watchdog_Time_Out : Ada.Real_Time.Time_Span :=
        Ada.Real_Time.Milliseconds (Default_Watchdog_Time_Out_MS);

      Watchdog_TON         : A4A.Library.Timers.TON.Instance;
      Watchdog_TON_Elapsed : Ada.Real_Time.Time_Span := Time_Span_Zero;
      Watchdog_TON_Start   : Boolean := False;
      Watchdog_TON_Q       : Boolean := False;

   end Status_Watchdog_Type;

end A4A.Tasks.Watchdog;
