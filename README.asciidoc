= Ada for Automation Read Me
Stéphane LOS
v2022.02, 2022-02-22
:doctype: book
:lang: en
:toc: left
:toclevels: 4
:icons: font
:numbered:
:Author Initials: SLO
:source-highlighter: rouge
:imagesdir: images
:title-logo-image: image:../book/images/A4A_logo1-4 TakeOff.png[Logo, pdfwidth=80%]

include::./book/A4A_Links.asciidoc[]

== Description

include::./book/A4A_Description-en.asciidoc[]

== General idea

[plantuml, a4a-mindmap-applications, png]     
----
@startuml

@startmindmap
caption figure 1
title Ada For Automation Applications

+ A4A
-- Size
--- Simple applications
--- Complex applications

-- Type
--- Slave applications
--- Master applications
--- Gateway applications
--- Control and Monitoring

-- Communication
--- Legacy field buses
--- Real Time Ethernet
--- IoT / IIoT
--- S7 Communication

++ User Interface
+++ Without UI (CLI)
+++ Graphical UI
+++ Web UI

++ OS Support
+++ Linux Prempt-RT
+++ Microsoft Windows
+++ QNX
+++ VxWorks

++ Platform
+++ Industrial PC
+++ Raspberry Pi
+++ Embedded
+++ Cloud

header
A4A Applications Mindmap
endheader

center footer Applications one can think of

legend right
  What do You
  have in mind ?
endlegend

@endmindmap

@enduml
----


[plantuml, a4a-mindmap, png]     
----
@startuml

@startmindmap
caption figure 2
title Ada For Automation

+ A4A
++ Ada

++ GtkAda
+++ Graphical User Interface

++ Simple Components
+++ Gnoga
++++ Web User Interface
+++ MQTT
++++_ Client
++++_ Broker
+++ SMTP
++++_ Client

++ Libmodbus
+++ IO Scanning
+++ Modbus TCP
++++_ Client
++++_ Server
+++ Modbus RTU
++++_ Master
++++_ Slave

++ LibSnap7
+++ S7 Communication
++++_ Client

++ Hilscher cifX API

header
Ada for Automation Mindmap
endheader

center footer This is the General Idea

legend right
  What else
  will you add ?
endlegend

@endmindmap

@enduml
----


[plantuml, a4a-mindmap-hilscherx, png]     
----
@startuml

@startmindmap
caption figure 3
title Hilscher cifX API

+ Hilscher cifX API
++ Dual Port Memory Access
+++ Parallel bus
++++_ netX 100
++++_ netX 51/52
++++_ netX 90
++++_ comX 100
++++_ comX 51/52
+++ SPI
++++_ netX 90
++++_ netX 51/52
++++_ comX 51/52
+++ ISA
++++_ cifX
+++ PCI
++++_ cifX
+++ PCI Express
++++_ cifX
+++ Ethernet
++++_ netHOST

++ Real Time Ethernet
+++ PROFINET IO RT / IRT
++++_ Controller
++++_ Device
+++ EtherCAT
++++_ Master
++++_ Slave
+++ Ethernet/IP
++++_ Scanner
++++_ Adapter
+++ Modbus TCP
++++_ Client
++++_ Server
+++ Sercos III
++++_ Master
++++_ Slave
+++ POWERLINK
++++_ Controlled Node
+++ VARAN
++++_ Master

++ Legacy Fieldbus
+++ AS-Interface
++++_ Master
+++ CANopen
++++_ Master
++++_ Slave
+++ PROFIBUS DP
++++_ Master
++++_ Slave
+++ DeviceNet
++++_ Master
++++_ Slave
+++ CC-Link
++++_ Slave

header
Hilscher cifX API Mindmap
endheader

center footer This is Hilscher cifX API contribution

legend right
  What's next ?
endlegend

@endmindmap

@enduml
----

== License

*COPYING3* +
The GPL License you should read carefully.
GNU GENERAL PUBLIC LICENSE                       Version 3, 29 June 2007

*COPYING.RUNTIME* +
GCC RUNTIME LIBRARY EXCEPTION                    Version 3.1, 31 March 2009

== Directories

*a4a_apps* +
This is the basic "Ada for Automation" application folder. +
Contains historic build artifacts. +
Nothing interesting, is going to disappear soon.

*book* +
The book "Ada for Automation" is elaborated here. +
This book is composed in plain text in the Asciidoc format and processed to provide HTML or PDF files.

*demo* +
This is the Demo applications folder.

*exp* +
Contains historic build artifacts. +
Nothing interesting, is going to disappear soon.

*gpr* +
Contains shared projects files. +

*hilscherx* +
The directory for the Hilscher cifX API binding. +
You will need it if you would use a Hilscher cifX board that can provide the connectivity you need to manage your equipment on all major fieldbuses Real Time Ethernet or legacy. +
Contains historic stuff. Most is now in src/hilscherx except test items. +
Nothing interesting, is going to disappear soon.

*icons* +
GUI Applications icons.

*src* +
Ada for Automation framework source files.

*test* +
The place to place all sorts of tests and experiments or examples.

*tutorial* +
Everyone starts some day.

*www* +
Web related files like documentation portal.

== Kernels

Kernels feature the core functionality needed by applications.

Following are available ones :

*kernel0* : Modbus TCP Server

*kernel0b* : Modbus RTU Slave

*kernel1* : Modbus TCP Server + Modbus TCP IO Scanning

*kernel2* : Modbus TCP Server + Modbus RTU IO Scanning

*kernel3* : Modbus TCP Server + one Hilscher cifX channel

*kernel4* : Modbus TCP Server + Modbus TCP IO Scanning + one Hilscher cifX channel

*kernel5* : Modbus TCP Server + Modbus TCP IO Scanning + two Hilscher cifX channels

*kernel6* : One Hilscher cifX channel

*kernel7* : Modbus TCP IO Scanning + One Hilscher cifX channel

== Applications

An application is built around the kernel, with application logic added by the framework user.

Thanks to {GtkAda}, it can provide a Graphic User Interface.

Thanks to {Gnoga}, it can provide a Web User Interface.

== GNAT Pro Studio Projects

GNAT Pro Studio allows to arrange the projects in a project tree with inheriting other projects. +
This will expand the project by adding or substituting source code files. +
You will please refer to {gprbuild} documentation regarding the projects.

=== Shared projects

The following projects are common to all CLI, GUI or WUI applications.

*shared.gpr* +
An abstract project, shared by the other ones and containing common elements.

*shared_hilscherx.gpr* +
An abstract project, shared by the other ones and containing common Hilscher related elements.

*libmodbus.gpr* +
The project for the libmodbus library. +
It is possible that you have to adjust it to suit your installation.

*libsnap7.gpr* +
The project for the Snap7 library. +
It is possible that you have to adjust it to suit your installation.

*libcifx.gpr* +
The project for the Hilscher cifX Device Driver library. +
It is possible that you have to adjust it to suit your installation. +
Only required if using a Hilscher cifX card.

*libnetxtransport.gpr* +
The project for the Hilscher netXTransport library. +
It is possible that you have to adjust it to suit your installation. +
Only required if using a Hilscher netHOST.

*gtkada.gpr* +
The project for the gtkada library. +
It comes with installed library. +

*gnoga.gpr* +
The project for the gnoga library. +
It comes with installed library.

=== Basic projects

The following projects are inherited by applications projects.

==== A4A Project diagram

This project brings in most of the functionality offered by the framework.

The tool chain will only pull in what is needed by the application itself.

The following picture shows the diagram of the project :

[plantuml, a4a-diagram-classes, png]     
----
@startuml

class Shared 		<< Project >>
note left: Shared\nsettings

class A4A 		  << Project >>

Shared - A4A : uses <

@enduml
----

==== A4A Kernel 0 Project diagram

This project inherits from the A4A project adding the Kernel 0 featuring a Modbus TCP Server.

The following picture shows the diagram of the project :

[plantuml, a4a-k0-diagram-classes, png]     
----
@startuml

class Shared 		  << Project >>
note left: Shared\nsettings

class Libmodbus 	<< Project >>
note left: Modbus\nlibrary

class A4A 		    << Project >>

class A4A_K0 		  << Project >>
note right: Kernel 0\nModbus TCP Server

Shared - A4A : uses <
A4A <|-- A4A_K0
Shared - A4A_K0 : uses <
Libmodbus - A4A_K0 : uses <

@enduml
----

==== A4A Kernel 0b Project diagram

This project inherits from the A4A project adding the Kernel 0b featuring a Modbus RTU Slave.

The following picture shows the diagram of the project :

[plantuml, a4a-k0b-diagram-classes, png]     
----
@startuml

class Shared 		  << Project >>
note left: Shared\nsettings

class Libmodbus 	<< Project >>
note left: Modbus\nlibrary

class A4A 		    << Project >>

class A4A_K0b 		  << Project >>
note right: Kernel 0b\nModbus RTU Slave

Shared - A4A : uses <
A4A <|-- A4A_K0b
Shared - A4A_K0b : uses <
Libmodbus - A4A_K0b : uses <

@enduml
----

==== A4A Kernel 1 Project diagram

This project inherits from the A4A project adding the Kernel 1 featuring a Modbus TCP Server and Modbus TCP IO Scanning.

The following picture shows the diagram of the project :

[plantuml, a4a-k1-diagram-classes, png]     
----
@startuml

class Shared 		  << Project >>
note left: Shared\nsettings

class Libmodbus 	<< Project >>
note left: Modbus\nlibrary

class A4A 		    << Project >>

class A4A_K1 		  << Project >>
note right: Kernel 1\nModbus TCP Server\nModbus TCP IO Scanning

Shared - A4A : uses <
A4A <|-- A4A_K1
Shared - A4A_K1 : uses <
Libmodbus - A4A_K1 : uses <

@enduml
----

==== A4A Kernel 2 Project diagram

This project inherits from the A4A project adding the Kernel 2 featuring a Modbus TCP Server and Modbus RTU IO Scanning.

The following picture shows the diagram of the project :

[plantuml, a4a-k2-diagram-classes, png]     
----
@startuml

class Shared 		  << Project >>
note left: Shared\nsettings

class Libmodbus 	<< Project >>
note left: Modbus\nlibrary

class A4A 		    << Project >>

class A4A_K2 		  << Project >>
note right: Kernel 2\nModbus TCP Server\nModbus RTU IO Scanning

Shared - A4A : uses <
A4A <|-- A4A_K2
Shared - A4A_K2 : uses <
Libmodbus - A4A_K2 : uses <

@enduml
----

==== A4A Kernel 3 Project diagrams

This project inherits from the A4A project adding the Kernel 3 featuring a Modbus TCP Server and one Hilscher cifX channel.

There are two variants :

* one using the standard Hilscher cifX API for use with most Hilscher products with ISA, PCI, PCI Express, DPM, SPI interfaces,

* one using the standard  Hilscher netXTransport API which has the same cifX API but via a TCP/IP connection for use with Hilscher netHOST.

The following picture shows the diagram of the project using cifX API :

[plantuml, a4a-k3-cifx-diagram-classes, png]     
----
@startuml

class Shared 		  << Project >>
note left: Shared\nsettings

class Shared_HilscherX  << Project >>
note left: Shared HilscherX\nsettings

class Libmodbus 	<< Project >>
note left: Modbus\nlibrary

class LibcifX 	  << Project >>
note right: cifX\nlibrary

class A4A 		    << Project >>

class A4A_K3_cifX << Project >>
note right: Kernel 3\nModbus TCP Server\none Hilscher cifX channel

Shared - A4A : uses <
A4A <|-- A4A_K3_cifX
Shared -- A4A_K3_cifX : uses <
Shared_HilscherX - A4A_K3_cifX : uses <
Libmodbus -- A4A_K3_cifX : uses <
A4A_K3_cifX -- LibcifX : uses >

@enduml
----

The following picture shows the diagram of the project using netXTransport API :

[plantuml, a4a-k3-nxt-diagram-classes, png]     
----
@startuml

class Shared 		  << Project >>
note left: Shared\nsettings

class Shared_HilscherX  << Project >>
note left: Shared HilscherX\nsettings

class Libmodbus 	<< Project >>
note left: Modbus\nlibrary

class LibnetXTransport  << Project >>
note right: netXTransport\nlibrary

class A4A 		    << Project >>

class A4A_K3_nxt 	<< Project >>
note right: Kernel 3\nModbus TCP Server\none Hilscher cifX channel

Shared - A4A : uses <
A4A <|-- A4A_K3_nxt
Shared -- A4A_K3_nxt : uses <
Shared_HilscherX - A4A_K3_nxt : uses <
Libmodbus -- A4A_K3_nxt : uses <
A4A_K3_nxt -- LibnetXTransport : uses >

@enduml
----

==== A4A Kernel 4 Project diagram

This project inherits from the A4A project adding the Kernel 4 featuring a Modbus TCP Server, Modbus TCP IO Scanning and one Hilscher cifX channel.

The following picture shows the diagram of the project using cifX API :

[plantuml, a4a-k4-cifx-diagram-classes, png]     
----
@startuml

class Shared 		  << Project >>
note left: Shared\nsettings

class Shared_HilscherX  << Project >>
note left: Shared HilscherX\nsettings

class Libmodbus 	<< Project >>
note left: Modbus\nlibrary

class LibcifX 	  << Project >>
note right: cifX\nlibrary

class A4A 		    << Project >>

class A4A_K4 		  << Project >>
note right: Kernel 4\nModbus TCP Server\nModbus TCP IO Scanning\none Hilscher cifX channel

Shared - A4A : uses <
A4A <|-- A4A_K4
Shared -- A4A_K4 : uses <
Shared_HilscherX - A4A_K4 : uses <
Libmodbus -- A4A_K4 : uses <
A4A_K4 -- LibcifX : uses >

@enduml
----

==== A4A Kernel 5 Project diagram

This project inherits from the A4A project adding the Kernel 5 featuring a Modbus TCP Server, Modbus TCP IO Scanning and two Hilscher cifX channels.

The following picture shows the diagram of the project using cifX API :

[plantuml, a4a-k5-cifx-diagram-classes, png]     
----
@startuml

class Shared 		  << Project >>
note left: Shared\nsettings

class Shared_HilscherX  << Project >>
note left: Shared HilscherX\nsettings

class Libmodbus 	<< Project >>
note left: Modbus\nlibrary

class LibcifX 	  << Project >>
note right: cifX\nlibrary

class A4A 		    << Project >>

class A4A_K5 		  << Project >>
note right: Kernel 5\nModbus TCP Server\nModbus TCP IO Scanning\ntwo Hilscher cifX channels

Shared - A4A : uses <
A4A <|-- A4A_K5
Shared -- A4A_K5 : uses <
Shared_HilscherX - A4A_K5 : uses <
Libmodbus -- A4A_K5 : uses <
A4A_K5 -- LibcifX : uses >

@enduml
----

==== A4A Kernel 6 Project diagram

This project inherits from the A4A project adding the Kernel 6 featuring one Hilscher cifX channel.

Project is having no dependency on libmodbus.

[plantuml, a4a-k6-cifx-diagram-classes, png]     
----
@startuml

class Shared 		  << Project >>
note left: Shared\nsettings

class Shared_HilscherX  << Project >>
note left: Shared HilscherX\nsettings

class LibcifX 	  << Project >>
note right: cifX\nlibrary

class A4A 		    << Project >>

class A4A_K6 		  << Project >>
note right: Kernel 6\none Hilscher cifX channel

Shared - A4A : uses <
A4A <|-- A4A_K6
Shared -- A4A_K6 : uses <
Shared_HilscherX - A4A_K6 : uses <
A4A_K6 -- LibcifX : uses >

@enduml
----

==== A4A Kernel 7 Project diagram

This project inherits from the A4A project adding the Kernel 7 featuring Modbus TCP IO Scanning and one Hilscher cifX channel.

The following picture shows the diagram of the project using cifX API :

[plantuml, a4a-k7-cifx-diagram-classes, png]     
----
@startuml

class Shared 		  << Project >>
note left: Shared\nsettings

class Shared_HilscherX  << Project >>
note left: Shared HilscherX\nsettings

class Libmodbus 	<< Project >>
note left: Modbus\nlibrary

class LibcifX 	  << Project >>
note right: cifX\nlibrary

class A4A 		    << Project >>

class A4A_K7 		  << Project >>
note right: Kernel 7\nModbus TCP IO Scanning\none Hilscher cifX channel

Shared - A4A : uses <
A4A <|-- A4A_K7
Shared -- A4A_K7 : uses <
Shared_HilscherX - A4A_K7 : uses <
Libmodbus -- A4A_K7 : uses <
A4A_K7 -- LibcifX : uses >

@enduml
----

== Demo applications
(in directory... demo)

*A detailed information resides in the application folder.*

=== Overview

Demo applications give a starting point to create your own application.

Whether you want to use {libmodbus} or a {hilscher} cifX board, module or chip, there are demo applications using one or the other or both.

There are applications with a Command Line Interface, a {GtkAda} Graphic User Interface, or {Gnoga} Web User Interface.

Of course, CLI applications are the simplest one to dive in.

The GtkAda Graphic User Interface is limited to general application interface and communication status.

This is because there are several ways to design the process interface :

* GtkAda,

* {AICWL} on top of GtkAda,

* or embedding Gnoga in a GtkAda window.

==== By Kernel

[plantuml, a4a-mindmap-demo-applications-kernel, png]     
----
@startuml

@startmindmap
caption figure 1
title Ada For Automation Demo Applications by kernel

+ A4A
-- K0
--- 000 a4a-k0-cli
--- 001 a4a-k0-gui
--- 010 a4a_piano
--- 030 app1simu-cli
--- 031 app1simu-gui
--- 032 app1simu-wui
--- 142 a4a_k0_S7

-- K0b
--- 000 a4a-k0b-cli
--- 001 a4a-k0b-gui
--- 010 a4a_k0b_piano

-- K1
--- 020 a4a-k1-cli
--- 021 a4a-k1-gui
--- 022 a4a-k1-wui
--- 040 app1-cli
--- 041 app1-gui
--- 042 app1-wui

-- K2
--- 080 app3-cli
--- 081 app3-gui
--- 082 app3-wui

++ K3
+++ 062 a4a-k3-wui
+++ 070 app2-cli
+++ 071 app2-gui
+++ 090 app4

++ K4
+++ 100 app5-cli
+++ 101 app5-gui

++ K5
+++ 110 app6-cli
+++ 111 app6-gui

++ K6
+++ 052 a4a_hilscherx_piano
+++ 122 app7-wui

++ K7
+++ 132 a4a-k7-wui

header
A4A Demo Applications Mindmap
endheader

center footer Applications one can start from

legend right
  What about yours ?
endlegend

@endmindmap

@enduml
----

==== By matchings

[plantuml, a4a-mindmap-demo-applications-matching-1, png]     
----
@startuml

@startmindmap
title Playing piano with Modbus TCP

+ Modbus TCP
++ K0
+++ 010 a4a_piano

-- K1
--- 020 a4a-k1-cli
--- 021 a4a-k1-gui
--- 022 a4a-k1-wui

@endmindmap

@enduml
----

[plantuml, a4a-mindmap-demo-applications-matching-2, png]     
----
@startuml

@startmindmap
title Playing piano with Modbus RTU

+ Modbus RTU
-- K2
--- 080 app3-cli
--- 081 app3-gui
--- 082 app3-wui

++ K0b
+++ 010 a4a_k0b_piano

@endmindmap

@enduml
----

[plantuml, a4a-mindmap-demo-applications-matching-3, png]     
----
@startuml

@startmindmap
title Example Application 1

+ Modbus TCP
++ K0
+++ 030 app1simu-cli
+++ 031 app1simu-gui
+++ 032 app1simu-wui

-- K1
--- 040 app1-cli
--- 041 app1-gui
--- 042 app1-wui

@endmindmap

@enduml
----

[plantuml, a4a-mindmap-demo-applications-matching-4, png]     
----
@startuml

@startmindmap
title Playing piano with a fieldbus using Hilscher cifX API

+ Fieldbus
++ K6
+++ 052 a4a_hilscherx_piano

-- K3
--- 062 a4a-k3-wui

@endmindmap

@enduml
----

[plantuml, a4a-mindmap-demo-applications-matching-5, png]     
----
@startuml

@startmindmap
title Monitoring E+H Radar level meter using PROFIBUS

+ PROFIBUS
++ E+H Radar level meter

-- K3
--- 070 app2-cli
--- 071 app2-gui
--- 090 app4

@endmindmap

@enduml
----

[plantuml, a4a-mindmap-demo-applications-matching-6, png]     
----
@startuml

@startmindmap
title Playing piano with Modbus TCP and one fieldbus using Hilscher cifX API

+ K4
-- 100 app5-cli
-- 101 app5-gui

++ Modbus TCP
+++ K0
++++ 010 a4a_piano

++ Fieldbus
+++ K6
++++ 052 a4a_hilscherx_piano

@endmindmap

@enduml
----

[plantuml, a4a-mindmap-demo-applications-matching-7, png]     
----
@startuml

@startmindmap
title Playing piano with Modbus TCP and two fieldbuses using Hilscher cifX API

+ K5
-- 110 app6-cli
-- 111 app6-gui

++ Modbus TCP
+++ K0
++++ 010 a4a_piano

++ Fieldbus 1
+++ K6
++++ 052 a4a_hilscherx_piano

++ Fieldbus 2
+++ K6
++++ 052 a4a_hilscherx_piano

@endmindmap

@enduml
----

[plantuml, a4a-mindmap-demo-applications-matching-8, png]     
----
@startuml

@startmindmap
title Control and Monitoring of an EtherCAT drive using Hilscher cifX API

+ EtherCAT
++ drive

-- K6
--- 122 app7-wui

@endmindmap

@enduml
----

[plantuml, a4a-mindmap-demo-applications-matching-9, png]     
----
@startuml

@startmindmap
title Modbus TCP to fieldbus Gateway using Hilscher cifX API

+ K7
-- 132 a4a-k7-wui

++ Fieldbus
+++ K3
++++ 062 a4a-k3-wui

++ Modbus TCP
+++ K0
++++ 010 a4a_piano

@endmindmap

@enduml
----

[plantuml, a4a-mindmap-demo-applications-matching-10, png]     
----
@startuml

@startmindmap
title Modbus TCP to S7 Communication Gateway using Snap7

+ 142 a4a_k0_S7
-- K0
--- Modbus TCP
---- K1
----- TODO App

-- S7 Com
--- S7 PLC

@endmindmap

@enduml
----

<<<

==== Summary

.Features
[width="100%",options="header",cols="<25%, <7%, <6%, <7%, <7%, <48%"]
|==================================================================================
|  Application             | UI  | Knl | Mbus   | WSP  |Description
|  000 a4a-k0b-cli         | CLI | K0b | 2      | -    |Modbus RTU Slave which mirrors inputs
|  000 a4a-k0-cli          | CLI | K0  | 1504   | -    |Modbus TCP Server which mirrors inputs
|  001 a4a-k0b-gui         | GUI | K0b | 2      | -    |Modbus RTU Slave which mirrors inputs
|  001 a4a-k0-gui          | GUI | K0  | 1504   | -    |Modbus TCP Server which mirrors inputs
|  010 a4a_k0b_piano       | WUI | K0b | 2      | 8081 |Modbus RTU Slave Piano
|  010 a4a_piano           | WUI | K0  | 1504   | 8081 |Modbus TCP Server Piano
|  020 a4a-k1-cli          | CLI | K1  | 1503   | -    |Modbus TCP Server / Client
|  021 a4a-k1-gui          | GUI | K1  | 1503   | -    |Modbus TCP Server / Client
|  022 a4a-k1-wui          | WUI | K1  | 1503   | 8080 |Modbus TCP Server / Client
|  030 app1simu-cli        | CLI | K0  | 1505   | -    |Modbus TCP Server simulation for App1
|  031 app1simu-gui        | GUI | K0  | 1505   | -    |Modbus TCP Server simulation for App1
|  032 app1simu-wui        | WUI | K0  | 1505   | 8083 |Modbus TCP Server simulation for App1
|  040 app1-cli            | CLI | K1  | 1502   | -    |Modbus TCP Server / Client App1
|  041 app1-gui            | GUI | K1  | 1502   | -    |Modbus TCP Server / Client App1
|  042 app1-wui            | WUI | K1  | 1502   | 8082 |Modbus TCP Server / Client App1
|  052 a4a_hilscherx_piano | WUI | K6  | -      | 8090 |HilscherX Fieldbus Piano
|  062 a4a_k3-wui          | WUI | K3  | 1503   | 8085 |Modbus TCP Server / HilscherX Fieldbus
|  070 app2-cli            | CLI | K3  | 1503   | -    |Modbus TCP Server / HilscherX Fieldbus E+H
|  071 app2-gui            | GUI | K3  | 1503   | -    |Modbus TCP Server / HilscherX Fieldbus E+H
|  080 app3-cli            | CLI | K2  | 1503   | -    |Modbus TCP Server / Modbus RTU Master
|  081 app3-gui            | GUI | K2  | 1503   | -    |Modbus TCP Server / Modbus RTU Master
|  082 app3-wui            | WUI | K2  | 1503   | 8085 |Modbus TCP Server / Modbus RTU Master
|  090 app4-cli            | CLI | K3  | 1503   | -    |Modbus TCP Server / HilscherX Fieldbus (App2)
|  100 app5-cli            | CLI | K4  | 1503   | -    |Modbus TCP Server / Client / HilscherX Fieldbus
|  101 app5-gui            | GUI | K4  | 1503   | -    |Modbus TCP Server / Client / HilscherX Fieldbus
|  110 app6-cli            | CLI | K5  | 1503   | -    |Modbus TCP Server / Client / 2 x HilscherX Fieldbus
|  111 app6-gui            | GUI | K5  | 1503   | -    |Modbus TCP Server / Client / 2 x HilscherX Fieldbus
|  122 app7-wui            | WUI | K6  | -      | 8090 |HilscherX Fieldbus / EtherCAT Drive
|  132 a4a-k7-wui          | WUI | K7  | -      | 8085 |Modbus TCP Client / HilscherX Fieldbus
|  142 a4a_k0_S7           | WUI | K0  | 1504   | 8181 |Modbus TCP Server / S7 Communication
|==================================================================================

[NOTE]
================
* UI : User Interface

* CLI : Command Line User Interface

* GUI : Graphical User Interface

* WUI : Web User Interface

* Knl : Kernel

* WSP : Web Server Port

* Mbus : Modbus TCP Server Port or Modbus RTU Slave address.

================

=== 000 a4a-k0b-cli
This application implements a Modbus RTU Slave which mirrors inputs. +
When one does not have a Slave device to play with, one can use this.

This is the Command Line incarnation. There is a GtkAda UI version.

Inherits from A4A.K0b and so features a kernel0b.

=== 000 a4a-k0-cli
This application implements a Modbus TCP Server which mirrors inputs. +
When one does not have a Server device to play with, one can use this.

This is the Command Line incarnation. There is a GtkAda UI version.

Inherits from A4A.K0 and so features a kernel0.

Modbus TCP Server port : 1504

=== 001 a4a-k0b-gui
This application implements a Modbus RTU Slave which mirrors inputs. +
It shares *000 a4a-k0b-cli* logic and provides a Graphic User Interface. +
When one does not have a Slave device to play with, one can use this.

=== 001 a4a-k0-gui
This application implements a Modbus TCP Server which mirrors inputs. +
It shares *000 a4a-k0-cli* logic and provides a Graphic User Interface. +
When one does not have a Slave device to play with, one can use this.

Modbus TCP Server port : 1504

=== 010 a4a_k0b_piano
This application implements a Modbus RTU Slave that mimics 16 push buttons and 16 LEDs with a web interface. +
When one does not have a Slave device to play with, one can use this.

Inherits from A4A.K0b and so features a kernel0b.

Web Server        port : 8081

=== 010 a4a_piano
This application implements a Modbus TCP Server that mimics 16 push buttons and 16 LEDs with a web interface. +
When one does not have a Server device to play with, one can use this.

Inherits from A4A.K0 and so features a kernel0.

Modbus TCP Server port : 1504

Web Server        port : 8081

=== 020 a4a-k1-cli
This application is meant to play with *010 a4a_piano*. +
It implements Modbus TCP IO Scanning and a Modbus TCP Server which does not much at the moment (could be used to reflect I/O for example).

This is the Command Line incarnation. There are GtkAda UI and Web UI versions.

Inherits from A4A.K1 and so features a kernel1.

Modbus TCP Server port : 1503 (default)

=== 021 a4a-k1-gui
This application is meant to play with *010 a4a_piano*. +
It shares *020 a4a-k1-cli* logic and provides a Graphic User Interface.

Modbus TCP Server port : 1503 (default)

=== 022 a4a-k1-wui
This application is meant to play with *010 a4a_piano*. +
It shares *020 a4a-k1-cli* logic and provides a Web User Interface.

Modbus TCP Server port : 1503 (default)

Web Server        port : 8080

=== 030 app1simu-cli
Here is the simulation application for the example application 1. +
This is the Command Line incarnation. There are GtkAda UI and Web UI versions.

Inherits from A4A.K0 and so features a kernel0.

Modbus TCP Server port : 1505

=== 031 app1simu-gui
This is the GtkAda UI incarnation of simulation application for the example application 1.

Modbus TCP Server port : 1505

=== 032 app1simu-wui
This is the Web UI incarnation of simulation application for the example application 1.

Modbus TCP Server port : 1505

Web Server        port : 8083

=== 040 app1-cli
This is where you will find the files of the example application 1. +
This application implements a Modbus TCP Server and Modbus TCP IO Scanning. +
This is the Command Line incarnation. There are GtkAda UI and Web UI versions.

Inherits from A4A.K1 and so features a kernel1.

Modbus TCP Server port : 1502

=== 041 app1-gui
This is the GtkAda UI incarnation of example application 1.

Modbus TCP Server port : 1502

=== 042 app1-wui
This is the Web UI incarnation of example application 1.

Modbus TCP Server port : 1502

Web Server        port : 8082

=== 052 a4a_hilscherx_piano
This project inherits from the A4A project adding the Kernel 6 featuring one Hilscher cifX channel.

It has same functionality as *010 a4a_piano* excepted the fieldbus connexion.

Web Server        port : 8090

=== 062 a4a-k3-wui
This application is meant to play with *052 a4a_hilscherx_piano*. +
The project inherits from the A4A project adding the Kernel 3 featuring a Modbus TCP Server and one Hilscher cifX channel.

It has same functionality as *022 a4a-k1-wui* excepted the fieldbus connexion replacing the Modbus TCP Client one.

There are two variants : cifX and netXTransport.

Modbus TCP Server port : 1503 (default)

Web Server        port : 8085

=== 070 app2-cli
In this one you will find the files for example application 2. +
It implements a Hilscher PROFIBUS DP Master cifX card and a Modbus TCP Server and demoes a PROFIBUS DPV2 communication with a Endress+Hauser radar level meter. +
This project inherits from the A4A project adding the Kernel 3 featuring a Modbus TCP Server and one Hilscher cifX channel.

This is the Command Line incarnation. There is a GtkAda UI versions.

Modbus TCP Server port : 1503 (default)

=== 071 app2-gui
This is the GtkAda UI incarnation of example application 2.

=== 080 app3-cli
Here you will find the files for example application 3. +
This application implements a Modbus TCP Server and Modbus RTU Master. +
This is the Command Line incarnation. There are GtkAda UI and Web UI versions.

Inherits from A4A.K2 and so features a kernel2.

Modbus TCP Server port : 1503 (default)

=== 081 app3-gui
This is the Web UI incarnation of example application 3.

=== 082 app3-wui
This is the GtkAda UI incarnation of example application 3.

=== 090 app4
It is same as example application 2 but using a Hilscher PROFIBUS DP Master netHOST instead of cifX.

=== 100 app5-cli
This project inherits from the A4A project adding the Kernel 4 featuring a Modbus TCP Server plus Modbus TCP IO Scanning and one Hilscher cifX channel.

=== 101 app5-gui

=== 110 app6-cli
This project inherits from the A4A project adding the Kernel 5 featuring a Modbus TCP Server plus Modbus TCP IO Scanning and two Hilscher cifX channels.

=== 111 app6-gui

=== 122 app7-wui
This project inherits from the A4A project adding the Kernel 6 featuring one Hilscher cifX channel.

This application allows the control and monitoring of an EtherCAT drive via a Web User Interface.

=== 132 a4a-k7-wui
This application implements a gateway with Modbus TCP IO Scanning connected to *010 a4a_piano* and one Hilscher cifX channel connected to *062 a4a-k3-wui*. +
It has a web interface.

Inherits from A4A.K7 and so features a kernel7.

Web Server        port : 8085

=== 142 a4a_k0_S7
This application implements a gateway with Modbus TCP Server and a S7 Communication channel, thanks to Snap7, connected to SIEMENS S7 PLC. +
It has a web interface.

Inherits from A4A.K0 and so features a kernel0.

Web Server        port : 8085


